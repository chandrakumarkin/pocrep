({
  doInit: function (cmp, event, helper) {
    // Take the param from attribute or take from URL.
    let newURL = new URL(window.location.href).searchParams;
    var flowIdParam = cmp.get("v.flowId")
      ? cmp.get("v.flowId")
      : newURL.get("flowId");
    var srIdParam = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
    var pageIdParam = cmp.get("v.pageId")
      ? cmp.get("v.pageId")
      : newURL.get("pageId");
    // set if comming from url
    cmp.set("v.pageId", pageIdParam);
    cmp.set("v.flowId", flowIdParam);
    cmp.set("v.srId", srIdParam);

    console.log("####@@@@@@ pageId ", pageIdParam);
    console.log("####@@@@@@ flowIdParam ", flowIdParam);
    console.log("####@@@@@@ srIdParam ", srIdParam);

    console.log("######### in detail do init ");
    helper.showIsthisindividualaPEP(cmp, event);
    //helper.loadSRDocs(cmp, event, helper);
  },
  onChangeCertifiedPassportCopy: function (cmp, event, helper) {
    console.log("@@@@@@@@@@@@@ onChangeCertifiedPassportCopy ");
    var amedWrap = cmp.get("v.amedWrap");
    var certifiedPassportCopy = amedWrap.amedObj.Certified_passport_copy__c;

    if (certifiedPassportCopy == "No") {
      //alert('Please visit DIFC for citation of original passport');
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        title: "Information",
        message: "Please visit DIFC for citation of original passport.",
      });
      toastEvent.fire();
      return;
    }
  },

  onAmedSave: function (cmp, event, helper) {
    let newURL = new URL(window.location.href).searchParams;

    var flowIdParam = cmp.get("v.flowId")
      ? cmp.get("v.flowId")
      : newURL.get("flowId");
    var srIdParam = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
    var pageIdParam = cmp.get("v.pageId")
      ? cmp.get("v.pageId")
      : newURL.get("pageId");

    console.log("@@@@@@@ onAmedSave");
    helper.showSpinner(cmp, event, helper);
    var allValid = true;

    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);

    //debugger;
    var fields = cmp.find("directorkey");
    for (var i = 0; i < fields.length; i++) {
      var inputFieldCompValue = fields[i].get("v.value");
      //console.log(inputFieldCompValue);
      if (!inputFieldCompValue && fields[i] && fields[i].get("v.required")) {
        console.log("no value");
        //console.log('bad');
        fields[i].reportValidity();
        allValid = false;
      }
    }

    if (allValid == false) {
      console.log("Error MSG");
      //cmp.set("v.errorMessage", "Please fill required field");
      //cmp.set("v.isError", true);
      helper.hideSpinner(cmp, event, helper);
      helper.showToast(
        cmp,
        event,
        helper,
        "Please fill required field",
        "error"
      );
    } else {
      //event.preventDefault();
      // For declaration.
      var iAgreeIndividual = cmp.get("v.amedWrap.amedObj.I_Agree_Director__c");
      if (
        $A.util.isEmpty(iAgreeIndividual) ||
        $A.util.isUndefined(iAgreeIndividual) ||
        iAgreeIndividual == false
      ) {
        //alert('Please accept the declaration');
        helper.hideSpinner(cmp, event, helper);
        helper.showToast(
          cmp,
          event,
          helper,
          "Please accept the declaration",
          "error"
        );
        return;
      }

      debugger;
      //handle err is remaining.
      var fileError = helper.fileUploadHelper(cmp, event, helper);
      if (fileError) {
        helper.hideSpinner(cmp, event, helper);
        return;
      }

      var onAmedSave = cmp.get("c.onAmedSaveDB");
      // amedWrap.amedObj.First_Name__c
      var amedWrap = cmp.get("v.amedWrap");
      var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");

      //console.log("@@@@@@@@ docMasterContentDocMap ", docMasterContentDocMap);
      var reqWrapPram = {
        amedWrap: amedWrap,
        docMasterContentDocMap: docMasterContentDocMap,
        srId: srIdParam,
      };
      console.log("@@@@@@@@ reqWrapPram ", reqWrapPram);
      // docMasterContentDocMap : docMasterContentDocMap

      console.log(
        "########## reset docMasterContentDocMap ",
        JSON.stringify(docMasterContentDocMap)
      );

      onAmedSave.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      onAmedSave.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          // amedObj
          if (respWrap.amedWrap && respWrap.amedWrap.amedObj.Id != "") {
            console.log(
              "######### respWrap.amedWrap.amedObj.id  ",
              respWrap.amedWrap.amedObj.Id
            );

            var amedId = respWrap.amedWrap.amedObj.Id;

            // resetting docMasterContentDocMap
            cmp.set("v.docMasterContentDocMap", []);
            var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
            console.log(
              "########## reset docMasterContentDocMap ",
              JSON.stringify(docMasterContentDocMap)
            );

            helper.hideSpinner(cmp, event, helper);
            var refeshEvnt = cmp.getEvent("refeshEvnt");
            refeshEvnt.setParams({
              amedId: respWrap.amedWrap.amedObj.id,
            });
            refeshEvnt.fire();
          } else {
            //alert(respWrap.errorMessage);
            helper.hideSpinner(cmp, event, helper);
            helper.showToast(
              cmp,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
          }
        } else {
          console.log("@@@@@ Error ... " + JSON.stringify(response.getError()));
          console.log("@@@@@ Error... " + response.getError()[0].message);

          // JSON.stringify(response.getError())
          //alert('Error ',JSON.stringify(response.getError()));
          helper.hideSpinner(cmp, event, helper);
          helper.showToast(
            cmp,
            event,
            helper,
            JSON.stringify(response.getError()),
            "error"
          );
        }
      });
      $A.enqueueAction(onAmedSave);
    }
  },
  handleLookupEvent: function (component, event, helper) {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log(JSON.parse(JSON.stringify(accObj)));
    component.set(
      "v.amedWrap.amedObj.Registration_No__c",
      accObj.Registration_License_No__c
    );
  },

  

  clearLookupEvent: function (component, event, helper) {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log(JSON.parse(JSON.stringify(accObj)));
    component.set("v.amedWrap.amedObj.Registered_No__c", "");
  },

  onCancel: function (cmp, event, helper) {
    var refeshEvnt = cmp.getEvent("refeshEvnt");
    refeshEvnt.setParams({
      amedId: "Cancel",
    });
    refeshEvnt.fire();
  },
  handleFilesChange: function (component, event, helper) {
    // Need to remove this method
    /*
            console.log('@@@@@@ in handleFilesChange ');
            var fileName = 'No File Selected..';
            if (event.getSource().get("v.files").length > 0) 
            {
                fileName = (event.getSource().get("v.files")[0][0]).name;
            }
            
                
            //console.log('@@@@@@ in handleFilesChange after fileName ',event.getSource().get("v.files")[0] );
            console.log(fileName);
            component.set("v.fileName", fileName);
        */
  },
  handleUploadFinished: function (cmp, event, helper) {
    /*
            // This  migth go away as per new implementation....same method is implemented in reusable component.
            helper.handleUploadFinished(cmp, event,helper);
            
            var uploadedFiles = event.getParam("files");
            var fileName = uploadedFiles[0].name;
            if(fileName)
            {
                cmp.set("v.fileName", fileName);
            }
            
            var contentDocumentId = uploadedFiles[0].documentId;
            console.log(contentDocumentId);
            var docMasterCode = event.getSource().get('v.name');  
            
            var mapDocValues = {};
            var docMap = cmp.get("v.docMasterContentDocMap");
            for(var key in docMap)
            {
                mapDocValues[key] = docMap[key];
            }
            
            mapDocValues[docMasterCode] = contentDocumentId;
            console.log(mapDocValues);
    
            console.log('@@@@@@ docMasterContentDocMap ');
            cmp.set("v.docMasterContentDocMap",mapDocValues);
            console.log(JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap"))));
        */
  },

  handleOCREvent: function (cmp, event, helper) {
    var ocrWrapper = event.getParam("ocrWrapper");
    var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
    var fields = cmp.find("directorkey");

    //parsing of code.
    for (var key in mapFieldApiObject) {
      for (var i = 0; i < fields.length; i++) {
        console.log(fields[i].get("v.fieldName"));
        if (fields[i].get("v.fieldName")) {
          console.log(fields[i].get("v.fieldName"));
          if (fields[i].get("v.fieldName").toLowerCase() == key.toLowerCase()) {
            fields[i].set("v.value", mapFieldApiObject[key]);
          }
        }
      }
    }
    helper.hideSpinner(cmp, event, helper);
  },

  closeModel: function (cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );

    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },
});