({
  doInit: function(component, event, helper) {
    helper.getNewsHelper(component, event);
  },

  closeComponent: function(component, event, helper) {
    component.set("v.showComponent", false);
  }
});