({
  doInit: function (component, event, helper) {
    helper.getPageContent(component, event, helper, true);
  },
  onChange: function (component, event, helper) {
    var relList = component.get("v.respWrap.dropdownList");
    var index = component.find("select").get("v.value");
    console.log(index);
    var selectedRel = relList.find((rel) => rel.Id === index);
    console.log(selectedRel);
    component.set("v.selectedRelation", selectedRel);
  },

  addAsAmendment: function (component, event, helper) {
    try {
      var selectedRel = component.get("v.selectedRelation");
      var amendlist = component.get("v.respWrap.relAmendment");
      var isDup = false;
      if (amendlist.length > 0) {
        var dupChecker =
          selectedRel.Individual_First_Name__c +
          selectedRel.Individual_Last_Name__c;

        console.log("dupChecker  " + dupChecker);
        for (var amedn of amendlist) {
          console.log(amedn);
          var amendDupCheck = amedn.First_Name__c + amedn.Last_Name__c;
          console.log("amedndup  " + amendDupCheck);
          if (amendDupCheck === dupChecker) {
            isDup = true;
            break;
          }
        }
      }

      if (isDup === true) {
        helper.showToast(
          component,
          event,
          helper,
          "this authorized signatory is already selected",
          "error"
        );
        return;
      }

      component.set("v.showSpinner", true);
      let newURL = new URL(window.location.href).searchParams;
      var srId = newURL.get("srId");
      var action = component.get("c.createAmendment");
      var requestWrap = {
        selectedRel: selectedRel,
        srId: srId
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          if (!respWrap.errorMessage) {
            helper.getPageContent(component, event, helper, false);
          } else {
            helper.showToast(component, event, "Record Added", "information");
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
        component.set("v.showSpinner", false);
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  delAmend: function (component, event, helper) {
    component.set("v.showSpinner", true);
    var action = component.get("c.delAmendment");
    var requestWrap = {
      amendIdToDel: event.target.id
    };
    console.log(requestWrap);

    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        if (!respWrap.errorMessage) {
          helper.showToast(
            component,
            event,
            helper,
            "Record Deleted",
            "information"
          );
          helper.getPageContent(component, event, helper, false);
          component.set("v.selectedRelation", null);
        } else {
          helper.showToast(
            component,
            event,
            helper,
            respWrap.errorMessage,
            "error"
          );
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.showSpinner", false);
    });
    $A.enqueueAction(action);
  },

  handleButtonAct: function (component, event, helper) {
    try {
      var amenlist = component.get("v.respWrap.relAmendment");
      console.log(amenlist);
      console.log(amenlist.length);

      if (!amenlist.length > 0) {
        helper.showToast(
          component,
          event,
          helper,
          "Please add at least one authorised Signatory",
          "error"
        );

        return;
      }
      component.set("v.showSpinner", true);
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");
      component.set(
        "v.respWrap.srObj.Community_User_Roles__c",
        component.get("v.rolesSelected")
      );

      console.log(component.get("v.respWrap.srObj"));

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
        srObj: component.get("v.respWrap.srObj")
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
        component.set("v.showSpinner", false);
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  manageRoleSelection: function (component, event, helper) {
    var rolesSelectedString = component.get("v.rolesSelected");
    var checked = event.getSource().get("v.checked");
    var value = event.getSource().get("v.value");

    if (rolesSelectedString) {
      var selectedRoleList = rolesSelectedString.split(";");
      console.log(selectedRoleList);
    }

    if (selectedRoleList && selectedRoleList.length > 0) {
      if (checked == true && !selectedRoleList.includes(value)) {
        selectedRoleList.push(value);
      } else if (checked == false && selectedRoleList.includes(value)) {
        var index = selectedRoleList.indexOf(value);
        if (index !== -1) selectedRoleList.splice(index, 1);
      }

      rolesSelectedString = selectedRoleList.join(";");
    } else {
      //string is null
      if (event.getSource().get("v.checked") == true) {
        rolesSelectedString = event.getSource().get("v.value");
      }
    }

    console.log(rolesSelectedString);
    component.set("v.rolesSelected", rolesSelectedString);
  }
});