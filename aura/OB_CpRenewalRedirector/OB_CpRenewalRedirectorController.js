({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");

    var action = component.get("c.redirectToPage");
    var requestWrap = {
      srId: srId,
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        //component.set('v.respWrap',respWrap);
        if (respWrap.errorMessage == null) {
          /* window.location.href =
              "ob-processflow" + respWrap.pageFlowURl; */
          window.location.href = respWrap.communityPage + respWrap.pageFlowURl;
        } else {
          console.log(respWrap.errorMessage);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },
});