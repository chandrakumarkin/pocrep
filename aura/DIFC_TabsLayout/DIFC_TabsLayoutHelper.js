({
  commLicRenderRule: function (component, event, helper, account) {
    var typeToShow = [
      "Dual license of DED licensed firms with an affiliate in DIFC",
      "ATM",
      "Vending Machines"
    ];
    if (
      account.Is_Commercial_Permission__c == true &&
      !typeToShow.includes(account.OB_Type_of_commercial_permission__c)
    ) {
      component.set("v.showSSTab", false);
    }
  }
});