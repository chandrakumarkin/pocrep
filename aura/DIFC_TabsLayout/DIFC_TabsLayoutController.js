({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;

    var refreshEvent = newURL.get("refreshEvent");
    console.log(refreshEvent);

    try {
      if (refreshEvent === "true") {
      }
    } catch (error) {
      console.log(error.message);
    }

    var action = component.get("c.difcTabsGetInfo");
    var requestWrap = {};
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        helper.commLicRenderRule(
          component,
          event,
          helper,
          respWrap.loggedInUser
        );
        component.set("v.respWrap", respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  callServer: function (component, event, helper) {
    try {
      console.log("==test url check=");
      //this.superAfterRender();
      var newURL = new URL(window.location.href).searchParams;
      var param = newURL.get("tab");
      console.log(param);
      component.getConcreteComponent().getElements();

      if (param === "services") {
        console.log("handler services");
        $A.enqueueAction(component.get("c.displayService"));
        /*component.set("v.enableServices", true);
      component.set("v.hometab", false);
      component.set("v.requestedtab", false);
      var elements = document.getElementsByClassName("selected");

      console.log(elements);
      for (var i = 0; i < elements.length; i++) {
        elements[i].classList.remove("selected");
      }

      var elements1 = document.getElementById("serviceTab");
      console.log(elements1);
      elements1.classList.add("selected");*/
      }
    } catch (err) {
      console.log("===error==");
      console.log(err.message);
    }
  },
  displayService: function (component, event, helper) {
    try {
      console.log("servicetab");
      component.set("v.enableServices", true);
      component.set("v.hometab", false);
      component.set("v.requestedtab", false);
      component.set("v.displayRosForms", false);
      component.set("v.coid19var", false);
      var elements = document.getElementsByClassName("selected");
      if (elements) {
        for (var i = 0; i < elements.length; i++) {
          elements[i].classList.remove("selected");
        }
      }

      if (event.currentTarget) {
        event.currentTarget.className = "slds-context-bar__item selected";
      } else {
        var elements = (document.getElementById("serviceTab").className =
          "slds-context-bar__item selected");
      }

      console.log(JSON.stringify(component.get("v.column1")));
      if (JSON.stringify(component.get("v.column1")) === "[]") {
        console.log("inside if");
        $A.createComponent("c:OB_Services", {}, function (
          childCom,
          status,
          errorMessage
        ) {
          if (status === "SUCCESS") {
            try {
              var body = component.get("v.column1");
              //console.log('$$$$$$$$$ ',JSON.stringify(body));
              body.push(childCom);
              component.set("v.column1", body);
              console.log("service comp created!!");
            } catch (err) {
              console.log(err.message);
            }
          } else if (status === "INCOMPLETE") {
            console.log("No response from server or client is offline.");
            // Show offline error
          } else if (status === "ERROR") {
            console.log("Error: " + errorMessage);
            // Show error message
          }
        });
      }
      /*console.log('fire event--');
        var appEvent = $A.get("e.c:aeEvent");
        appEvent.setParams({
            "serviceTab" : true });
        appEvent.fire();*/
    } catch (error) {
      console.log(error.message);
    }
  },
  displayHome: function (component, event, helper) {
    component.set("v.enableServices", false);
    component.set("v.hometab", true);
    component.set("v.requestedtab", false);
    component.set("v.displayRosForms", false);
    component.set("v.coid19var", false);
    var elements = document.getElementsByClassName("selected");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    event.currentTarget.className = "slds-context-bar__item selected";
  },
  displayROSForms: function (component, event, helper) {
    component.set("v.enableServices", false);
    component.set("v.hometab", false);
    component.set("v.requestedtab", false);
    component.set("v.displayRosForms", true);
    component.set("v.coid19var", false);
    var elements = document.getElementsByClassName("selected");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    event.currentTarget.className = "slds-context-bar__item selected";
  },
  displayCovid19: function (component, event, helper) {
    component.set("v.enableServices", false);
    component.set("v.hometab", false);
    component.set("v.requestedtab", false);
    component.set("v.displayRosForms", false);
    component.set("v.coid19var", true);
    var elements = document.getElementsByClassName("selected");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    event.currentTarget.className = "slds-context-bar__item selected";
  },
  displayRequest: function (component, event, helper) {
    console.log("handler");
    component.set("v.enableServices", false);
    component.set("v.hometab", false);
    component.set("v.requestedtab", true);
    component.set("v.displayRosForms", false);
    component.set("v.coid19var", false);
    var elements = document.getElementsByClassName("selected");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    var elements1 = document.getElementById("selectTab");
    elements1.classList.add("selected");

    /*var appEvent = $A.get("e.c:aeEvent");
        appEvent.setParams({
            "requestTab" : true });
        appEvent.fire();*/
    console.log(JSON.stringify(component.get("v.column8")));
    if (JSON.stringify(component.get("v.column8")) === "[]") {
      console.log("inside if");
      $A.createComponent(
        "c:OB_RequestedServicesList",
        {
          statusValue: "In Progress",
          headerSectionDisplay: true,
          searchSectionDisplay: true,
          inProgressHeaderDisplay: false
        },
        function (childCom, status, errorMessage) {
          if (status === "SUCCESS") {
            try {
              var body = component.get("v.column8");
              //console.log('$$$$$$$$$ ',JSON.stringify(body));
              body.push(childCom);
              component.set("v.column8", body);
              console.log("request comp created!!");
            } catch (err) {
              console.log(err.message);
            }
          } else if (status === "INCOMPLETE") {
            console.log("No response from server or client is offline.");
            // Show offline error
          } else if (status === "ERROR") {
            console.log("Error: " + errorMessage);
            // Show error message
          }
        }
      );
    }
  }
});