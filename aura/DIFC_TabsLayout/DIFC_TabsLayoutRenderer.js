({
  // Your renderer method overrides go here
  afterRender: function(component, helper) {
    //this.superAfterRender();
    console.log("XXXXX afterRender - title: ");
    try {
      console.log("==test url check=");
      //this.superAfterRender();
      var newURL = new URL(window.location.href).searchParams;
      var param = newURL.get("tab");
      console.log(param);
      component.getConcreteComponent().getElements();

      if (param === "requested") {
        console.log("requested services");
        component.set("v.enableServices", false);
        component.set("v.hometab", false);
        component.set("v.requestedtab", true);
        var elements = document.getElementsByClassName("selected");

        console.log("==te==" + elements);
        for (var i = 0; i < elements.length; i++) {
          elements[i].classList.remove("selected");
        }

        var elements1 = document.getElementById("selectTab");
        console.log(elements1);
        elements1.classList.add("selected");
        if (JSON.stringify(component.get("v.column8")) === "[]") {
          console.log("inside if");
          $A.createComponent(
            "c:OB_RequestedServicesList",
            {
              statusValue: "In Progress",
              headerSectionDisplay: true,
              searchSectionDisplay: true,
              inProgressHeaderDisplay: false
            },
            function(childCom, status, errorMessage) {
              if (status === "SUCCESS") {
                try {
                  var body = component.get("v.column8");
                  //console.log('$$$$$$$$$ ',JSON.stringify(body));
                  body.push(childCom);
                  component.set("v.column8", body);
                  console.log("request comp created!!");
                } catch (err) {
                  console.log(err.message);
                }
              } else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.");
                // Show offline error
              } else if (status === "ERROR") {
                console.log("Error: " + errorMessage);
                // Show error message
              }
            }
          );
        }
      }
      if (param === "services") {
        console.log("handler services");
        console.log("handler services");
        component.set("v.enableServices", true);
        component.set("v.hometab", false);
        component.set("v.requestedtab", false);
        var elements = document.getElementsByClassName("selected");

        console.log(elements);
        for (var i = 0; i < elements.length; i++) {
          elements[i].classList.remove("selected");
        }

        var elements1 = document.getElementById("serviceTab");
        console.log(elements1);
        elements1.classList.add("selected");

        if (JSON.stringify(component.get("v.column1")) === "[]") {
          console.log("inside if displayService");
          $A.createComponent("c:OB_Services", {}, function(
            childCom,
            status,
            errorMessage
          ) {
            if (status === "SUCCESS") {
              try {
                var body = component.get("v.column1");
                //console.log('$$$$$$$$$ ',JSON.stringify(body));
                body.push(childCom);
                component.set("v.column1", body);
                console.log("service comp created!!");
              } catch (err) {
                console.log(err.message);
              }
            } else if (status === "INCOMPLETE") {
              console.log("No response from server or client is offline.");
              // Show offline error
            } else if (status === "ERROR") {
              console.log("Error: " + errorMessage);
              // Show error message
            }
          });
        }
      }
    } catch (err) {
      console.log("===error==");
      console.log(err.message);
    }
  }
});