({
  onRemoveAmed: function(cmp, event, helper) {
    var onRemove = cmp.get("c.removeOprLocAmed");
    var reqWrapPram = {
      amedWrap: cmp.get("v.amedWrap")
    };

    onRemove.setParams({
      reqWrapPram: JSON.stringify(reqWrapPram)
    });

    onRemove.setCallback(this, function(response) {
      var state = response.getState();
      console.log("callback state: " + state);

      if (cmp.isValid() && state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log("@@@@@ respWrap " + respWrap);

        if (respWrap.errorMessage) 
        {
          //alert(respWrap.errorMessage);
          helper.showToast(cmp, event, helper,"Error.", "error");
        } 
        else 
        {
          //alert("Address Removed.");
          //helper.showToast(cmp, event, helper,"Address Removed.", "info"); 
            
          var refeshEvnt = cmp.getEvent("refeshEvnt");
          refeshEvnt.setParams({
            message: "refresh event"
          });
          refeshEvnt.fire();
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(onRemove);
  }
});