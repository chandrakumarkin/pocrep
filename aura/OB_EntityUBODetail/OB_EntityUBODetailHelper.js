({
	
    loadSRDocs : function(component,event, helper)
    { 
        /*
        var amendmentWrapper = component.get("v.amedWrap");
        console.log('amendmentWrapper----'+amendmentWrapper);
        var amedId = amendmentWrapper.amedObj.Id;
        console.log('amedId-----'); 
        var srId = component.get("v.srId");
        var action = component.get("c.viewSRDocs");
        var self = this;
        action.setParams({
            srId: srId,
            amendmentId: amedId
        });
        
        // Fixed by merul.
        var amendmentWrapper = component.get("v.amedWrap");
           console.log('amendmentWrapper----'+amendmentWrapper);
           var amedId = amendmentWrapper.amedObj.Id;
          //var srId = component.get("v.srId");
           var srId = component.get("v.srWrap").srObj.Id;
             console.log(' loadSRDocs srId-----',srId);
           var action = component.get("c.viewSRDocs");
           var self = this;
           action.setParams({
               srId: srId,
               amendmentId: amedId
           });
        
        
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('srDocs>>');
                console.log(result.Passport_Copy_Individual)
                console.log(result.Passport_Copy_Individual.FileName);
                component.set("v.srDocs",result);
                if(result.Passport_Copy_Individual){
                    component.set("v.fileName",result.Passport_Copy_Individual.FileName);
                    component.set("v.srDocId",result.Passport_Copy_Individual.Id);
                }
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message,'error');
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // enqueue the action 
        $A.enqueueAction(action);
        */
    },
    /**
     * Display a message
     */
    displayToast : function (title, message,type) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'title': title,
                'message': message,
                mode:'dismissible',
                type:type
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
    /**
     * Document Upload Section 
     */
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb
    associateFile:function(component, event, helper, parentId, docMasterCode, filesArr, srDocId) 
    {},
    uploadProcess: function(action,component, file, fileContents, parentId, docMasterCode, srDocId) {
    },
    uploadInChunk: function(action,component, file, fileContents, startPosition, endPosition, attachId, parentId, docMasterCode, srDocId) {
       
    },
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    }
})