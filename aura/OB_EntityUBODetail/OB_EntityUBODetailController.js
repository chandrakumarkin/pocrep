({
	doInit: function(component, event, helper) {
    var amendOBj = component.get("v.amedWrap.amedObj");
    console.log(JSON.stringify(amendOBj));
    console.log(JSON.stringify(component.get("v.Company")));
    var sr = component.get("v.srWrap");
    console.log(JSON.parse(JSON.stringify(sr)));
    var recordtypename = component.get("v.recordtypename");

    debugger;
    if (
      amendOBj.Id != null &&
      sr.srObj.legal_structures__c == "Company" &&
      recordtypename == "Individual" &&
      amendOBj.Role__c != "UBO"
    ) {
      var directArray = ["Direct"];

      component.set("v.dependent", directArray);
      component.set(
        "v.amedWrap.amedObj.Please_select_if_applicable__c",
        "Direct"
      );
    } else if (
      sr.srObj.legal_structures__c == "Company" &&
      recordtypename == "Individual"
    ) {
      var indirectArray = ["Indirect"];
      component.set("v.dependent", indirectArray);
      component.set(
        "v.amedWrap.amedObj.Please_select_if_applicable__c",
        "Indirect"
      );
    }
    component.set("v.selectedValue", "");
    var selectedValue = component.get(
      "v.amedWrap.amedObj.Type_of_Ownership_or_Control__c"
    );
    component.set("v.selectedValue", selectedValue);
    console.log("==selectedValue==" + selectedValue);

    //Invoke the SRDDocs
    if (recordtypename == "Individual") {
      if (
        !$A.util.isEmpty(selectedValue) &&
        !$A.util.isUndefined(selectedValue) &&
        (selectedValue ==
          "The individual holds 25% or more of the shares or ownership in the Company" ||
          selectedValue ==
            "The individual holds 25% or more of the voting rights in the Company")
      ) {
        component.set("v.isDependent", true);
      } else {
        component.set("v.isDependent", false);
      }

      if (
        !$A.util.isEmpty(selectedValue) &&
        !$A.util.isUndefined(selectedValue) &&
        selectedValue !=
          "Where no natural person has been identified as a UBO, each Director of the Company is deemed to be a UBO" &&
        selectedValue !=
          "The individual members of the Council who are deemed to be a UBO" &&
        selectedValue !=
          "The individual members of the governing body who are deemed to be a UBO"
      ) {
        component.set("v.isDisplayForm", true);
      } else {
        component.set("v.isDisplayForm", false);
      }
      //helper.loadSRDocs(component, event, helper);
    }
    
  },
  handleLookupEvent: function(component, event, helper) {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log("lookup--" + JSON.stringify(accObj));
    component.set(
      "v.amedWrap.amedObj.Registration_No__c",
      accObj.Registration_License_No__c
    );
  },
  clearLookupEvent: function(component, event, helper) {},
  ondependentChange: function(cmp, evt, helper) {
    var selectedValue = evt.getSource().get("v.value");
    cmp.set("v.amedWrap.amedObj.Please_select_if_applicable__c", selectedValue);

    var typeofOwner = cmp.get(
      "v.amedWrap.amedObj.Type_of_Ownership_or_Control__c"
    );
    console.log(selectedValue + "====typeofOwner===" + typeofOwner);
  },
  onChange: function(cmp, evt, helper) {
    cmp.set("v.amedWrap.amedObj.Please_select_if_applicable__c", "");
    var selectedValue = evt.getSource().get("v.value");
    console.log(selectedValue + "-- pie is good.");
    cmp.set(
      "v.amedWrap.amedObj.Type_of_Ownership_or_Control__c",
      selectedValue
    );
    cmp.set("v.selectedValue", selectedValue);

    if (
      !$A.util.isEmpty(selectedValue) &&
      !$A.util.isUndefined(selectedValue) &&
      (selectedValue ==
        "The individual holds 25% or more of the shares or ownership in the Company" ||
        selectedValue ==
          "The individual holds 25% or more of the voting rights in the Company")
    ) {
      cmp.set("v.isDependent", true);
    } else {
      cmp.set("v.isDependent", false);
    }

    if (
      !$A.util.isEmpty(selectedValue) &&
      !$A.util.isUndefined(selectedValue) &&
      selectedValue !=
        "Where no natural person has been identified as a UBO, each Director of the Company is deemed to be a UBO" &&
      selectedValue !=
        "The individual members of the Council who are deemed to be a UBO" &&
      selectedValue !=
        "The individual members of the governing body who are deemed to be a UBO"
    ) {
      console.log("====indside check==");
      cmp.set("v.isDisplayForm", true);
    } else {
      cmp.set("v.isDisplayForm", false);
    }
    var value = cmp.get("v.amedWrap.amedObj.Type_of_Ownership_or_Control__c");
    console.log("--selectedValue----" + value);
    
  },
  onAmedSave: function(cmp, event, helper) {
    console.log("######### onAmedSave ");
    try {
      //var isValid = true;
      // Store Regular Expression
      helper.show(cmp, event);
      var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var regExpPhoneformat = /^[+][(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;

      var recordtypename = cmp.get("v.recordtypename");
      cmp.set("v.isError", false);
      if (recordtypename == "Individual") {
        var typeOfControl = cmp.get("v.selectedValue");
        var legal = cmp.get("v.srWrap.srObj.legal_structures__c");
        console.log("===typeOfControl====" + typeOfControl);
        console.log("===legal====" + legal);

        if (
          typeOfControl !=
            "Where no natural person has been identified as a UBO, each Director of the Company is deemed to be a UBO" &&
          typeOfControl !=
            "The individual members of the Council who are deemed to be a UBO" &&
          typeOfControl !=
            "The individual members of the governing body who are deemed to be a UBO"
        ) {
          let isValid = true;
          let fields = cmp.find("requiredField"); // gets all required fields
          for (var i = 0; i < fields.length; i++) {
            var inputFieldCompValue = fields[i].get("v.value");
            //console.log(inputFieldCompValue);
            if (
              !inputFieldCompValue &&
              fields[i] &&
              fields[i].get("v.required")
            ) {
              console.log("no value");
              //console.log('bad');
              fields[i].reportValidity();
              isValid = false;
            }
          }
          
          // dependent picklist
          if (cmp.get("v.isDependent") == true && isValid == true) {
            var dependentField = cmp.find("dependentField").get("v.value");
            if (
              $A.util.isEmpty(dependentField) ||
              $A.util.isUndefined(dependentField)
            ) {
              isValid = false;
              console.log("not==dependent===");
            }
          }
          console.log("valid==error==" + isValid);
          if (isValid == false) {
            helper.hide(cmp, event);
            cmp.set("v.errorMessage", "Please fill required field");
            cmp.set("v.isError", true);
            helper.hide(cmp, event);
            helper.displayToast("Error", "Please fill required field", "error");
          }
        }
      } else {
        console.log('corporate value');
        let isValid = true;
        let fields = cmp.find("requiredField"); // gets all required fields
        for (var i = 0; i < fields.length; i++) {
          var inputFieldCompValue = fields[i].get("v.value");
          //console.log(inputFieldCompValue);
          if (
            !inputFieldCompValue &&
            fields[i] &&
            fields[i].get("v.required")
          ) {
            console.log("no --- value");
            //console.log('bad');
            fields[i].reportValidity();
            isValid = false;
          }
        }
        console.log("valid==error==" + isValid);
        if (isValid == false) {
          cmp.set("v.errorMessage", "Please fill required field");
          cmp.set("v.isError", true);
          helper.hide(cmp, event);
          helper.displayToast("Error", "Please fill required field", "error");
        }
      }
      var isError = cmp.get("v.isError");
      console.log("==out=isError===" + isError);
      if (isError != true) {
        console.log("-----no error---");
        var onAmedSave = cmp.get("c.onAmedSaveDB");
        var srId = cmp.get("v.srId");
        if(cmp.get("v.isDependent") != true){
        	cmp.set("v.amedWrap.amedObj.Please_select_if_applicable__c","");
        }
        var reqWrapPram = {
          srId: srId,
          typeOfInformation: cmp.get(
            "v.srWrap.srObj.Type_of_UBO_Information__c"
          ),
          recordtypename: recordtypename,
          amedWrap: cmp.get("v.amedWrap"),
          srWrap: cmp.get("v.srWrap")
        };

        //console.log('=====JSON.stringify(reqWrapPram)===='+recordtypename);
        onAmedSave.setParams({
          reqWrapPram: JSON.stringify(reqWrapPram)
        });

        onAmedSave.setCallback(this, function(response) {
          var state = response.getState();
          console.log("callback state: " + state);

          if (cmp.isValid() && state === "SUCCESS") {
            var respWrap = response.getReturnValue();
            //console.log('######### respWrap.amedWrap.amedObj.id--1  ',JSON.stringify(respWrap.amedWrap));

            //console.log('######### respWrap.amedWrap.amedObj.id  ',amedId);

            if (!$A.util.isEmpty(respWrap.errorMessage)) {
              console.log("=====error on sve====" + respWrap.errorMessage);
              cmp.set("v.spinner", false);

              cmp.set("v.errorMessage", respWrap.errorMessage);
              cmp.set("v.isError", true);
              helper.hide(cmp, event);
              helper.displayToast("Error", respWrap.errorMessage, "error");
            } else {
              var amedId = respWrap.amedWrap.amedObj.Id;
              if (respWrap.amedWrap.amedObj.Id != "") {
                console.log("==inside===");
                
                //this.closeForm(cmp, event, helper);
                cmp.set("v.amedWrap.amedObj", null);
                cmp.set("v.selectedValue", "");
                cmp.set("v.isDisplayForm", false);
                cmp.set("v.isDependent", false);
                cmp.set("v.fileName", "");

                var refeshEvnt = cmp.getEvent("refeshEvnt");
                var srWrap = respWrap.srWrap;
                console.log("@@@@@ srWrap ", srWrap);

                refeshEvnt.setParams({
                  srWrap: srWrap
                });
                helper.hide(cmp, event);
                refeshEvnt.fire();
              }
            }
          } else {
            console.log("@@@@@ Error " + response.getError()[0].message);
            cmp.set("v.spinner", false);
            cmp.set("v.errorMessage", response.getError()[0].message);
            cmp.set("v.isError", true);
            helper.hide(cmp, event);
            helper.displayToast(
              "Error",
              response.getError()[0].message,
              "error"
            );
          }
        });
        $A.enqueueAction(onAmedSave);
      }
    } catch (err) {
      console.log("===error==" + err.message);
    }
  },
  closeForm: function(cmp, event, helper) {
    //throw event.

    cmp.set("v.spinner", true);
    var isNew = cmp.get("v.isNew");
    if (isNew == true) {
      console.log("===new record====");
      cmp.set("v.amedWrap.amedObj", null);
    }
    var refeshEvnt = cmp.getEvent("refeshEvnt");
    refeshEvnt.setParams({
      amedId: "",
      isNewAmendment: isNew,
      amedWrap: cmp.get("v.amedWrap.amedObj")
    });
    cmp.set("v.spinner", false);
    refeshEvnt.fire();
  },
  closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );

    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },
  handleFilesChange: function(component, event, helper) {
    /*
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) 
        {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
        */

    console.log("@@@@@@ in handleFilesChange ");
    var fileName = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileName = event.getSource().get("v.files")[0][0].name;
    }

    //console.log('@@@@@@ in handleFilesChange after fileName ',event.getSource().get("v.files")[0] );
    console.log(fileName);
    component.set("v.fileName", fileName);
  },

  isRegistered: function(cmp, event, helper) {
    if (
      cmp.get("v.amedWrap.amedObj.Is_this_Entity_registered_with_DIFC__c") ==
      "Yes"
    ) {
      if (
        cmp.get("v.amedWrap.amedObj.Reason_for_exemption_under_DIFC_UBO__c") ==
        "Regulated entity"
      ) {
        cmp.set(
          "v.amedWrap.amedObj.Financial_Service_Regulator__c",
          "Dubai Financial Services Authority"
        );
      }
    }
  }
});