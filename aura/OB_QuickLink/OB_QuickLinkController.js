({
	doInit : function(component, event, helper) {
        var action = component.get("c.viewAccountInfo");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
				var responseResult = response.getReturnValue();
				component.set("v.CompanyInfo",responseResult);
				console.log(responseResult);
				var companyType = responseResult.type ? responseResult.type.toLowerCase() : '';
				if((companyType == 'financial - related' || (companyType=='non - financial' && responseResult.fatcaEligibility))
					&& responseResult.isCompanyService){
					component.set("v.showFACTCRS", "true");
				}
				if(companyType=='financial - related' || (companyType=='non - financial' && responseResult.fatcaEligibility)
					&& responseResult.isGIIN && responseResult.isCompanyService){
					component.set("v.showGIINSection", "true");
				}
				console.log('!!@@#'+companyType);
				if(companyType != 'financial - related'){
					component.set("v.isDFSAPortal", "true");
				}
                //Mudasir added the below code version 1.0 Start
                if(responseResult.isEventOrganizerLogin){
					component.set("v.isEventOrganizerLogin", "true");
                    component.set("v.isContractorLogin", "false");
				}
                else if(responseResult.isContractorLogin){
					component.set("v.isEventOrganizerLogin", "false");
                    component.set("v.isContractorLogin", "true");
				}
				//Mudasir added the below code version 1.0 End
            }else if (state === "INCOMPLETE") {
                console.log('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //alert("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);	
    },
    createCompliance:function(component,event,helper){
        
        var action = component.get("c.createComplianceCalendar");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
                console.log(responseResult);
                if(responseResult)
                    window.open(responseResult,'_blank');
            }else if (state === "INCOMPLETE") {
                console.log('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //alert("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
     navigatetoDFSAPortal:function(component,event,helper){
        
        var action = component.get("c.DFSAPortalDetails");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            var msg ='You have already submitted this request to DFSA portal. Do you wish to submit a new request ?';
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
                console.log('!!@@@'+responseResult);
                if(responseResult === true){
                    if (!confirm(msg)) {
                    		console.log('No');
                    		return false;
                    } else {
                    	var urlEvent = $A.get("e.force:navigateToURL");   
		                urlEvent.setParams({
		      				"url": "https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/dfsaportal"
		    			});
		                urlEvent.fire();
		            }
                }
                else{
                	
                	var urlEvent = $A.get("e.force:navigateToURL");   
		                urlEvent.setParams({
		      				"url": "https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/dfsaportal"
		    			});
		                urlEvent.fire();
                
                }
            }
        });
        $A.enqueueAction(action);
    },
    
})