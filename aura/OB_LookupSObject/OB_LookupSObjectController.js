({
    /**
     * load selected value in init 
     */
    
    init : function(cmp, event, helper)
    {
        console.log('Init initialized');
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId') ? newURL.get('srId') : '';
        cmp.set("v.srId",srId);
        debugger;
        helper.showSelectedLabel(cmp, event);  
    },
    
    /**
     * Search an SObject for a match 
     */
	search : function(cmp, event, helper) {
        console.log('in search');
		helper.doSearch(cmp);        
    },

    /**
     * Select an SObject from a list
     */
    select: function(cmp, event, helper) {
        console.log('in select' +event.currentTarget.dataset.rowindex);
        cmp.set("v.index",event.currentTarget.dataset.rowindex);
    	helper.handleSelection(cmp, event);
    },
    
    /**
     * Clear the currently selected SObject
     */
    clear: function(cmp, event, helper) {
        console.log('in clear');
    	helper.clearSelection(cmp);    
    }
})