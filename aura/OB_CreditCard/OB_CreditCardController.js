({
    doInit:function(component, event, helper) {
        console.log($A.get("$Label.c.Frames_Public_Key"));
        
        component.set("v.category",getUrlParameter('category'));
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId') == 'null' ? '': newURL.get('srId');
        component.set("v.srId",srID); 
        component.set("v.email",$A.get("$SObjectType.CurrentUser.Email"));
        
        if(srID && srID != 'null'){
            var action = component.get('c.loadPriceItem');
            action.setParams({
                "srId":srID,
                "category":component.get("v.category"),
                "isBalanceRequired" : false
            });
            action.setCallback(this, function(response){
                var opts = [];
                var state = response.getState(); // get the response state
                console.log('state'+state);
                if(state == 'SUCCESS') {
                    var responseResult = response.getReturnValue();
                    console.log(responseResult);
                    component.set("v.amountToPayInDollar",responseResult.amount);
                    component.set("v.amountToPay",responseResult.amountInAED);
                    var allValues = responseResult.mapCountry;
                    console.log(allValues);
                    if (allValues) {
                        opts.push({
                            class: "optionClass",
                            label: "--- None ---",
                            value: ""
                        });
                    }
                    
                    for(var key in allValues) {
                        //for (var i = 0; i < allValues.length; i++) {
                            opts.push({
                                class: "optionClass",
                                label: allValues[key],
                                value: key
                            });
                        }
                    
                    component.set("v.countryList", opts);
                }
                else if (state === 'ERROR') { // Handle any error by reporting it
                    var errors = response.getError();
                    console.log(response.getError());
                    
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            helper.displayToast('Error', errors[0].message, 'error');
                        }
                    } else {
                        helper.displayToast('Error', 'Unknown error.', 'error');
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else{
            console.log(newURL.get('amountToPay'));
            component.set("v.amountToPay",getUrlParameter('amountToPay'));
            helper.fetchCountry(component);
        }
        //helper.fetchPickListVal(component, 'Country_Of_Registration__c', 'accIndustryFirstName');
        function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};

    },
    scriptsLoaded : function(component, event, helper) {
        var paymentForm  = document.getElementById('payment-form');
        var payNowButton = document.getElementById('pay-now-button');
        var cardTokenn;
        console.log($A.get("$SObjectType.CurrentUser.Name"));
        var style = {
            '.embedded .card-form .input-group': {
                borderRadius: '5px'
            },
            /* focus */
            '.embedded .card-form .input-group.focus:not(.error)': {
                border: '1px solid green'
            },
            /* icons */
            '.embedded .card-form .input-group .icon': {
                color: 'slategray'
            },
            /* hint icon (CVV) */
            '.embedded .card-form .input-group .hint-icon': {
                color: 'slategray'
            },
            '.embedded .card-form .input-group .hint-icon:hover': {
                color: 'darkslategray'
            },
            /* error */
            '.embedded .card-form .input-group.error': {
                border: '1px solid red'
            },
            '.embedded .card-form .input-group.error .hint.error-message': { /* message container */
                background: 'lightgray', /* make sure to match the pointer color (below) */
                color: 'red'
            },
            '.embedded .card-form .input-group.error .hint.error-message .arrow': { /* message container pointer */
                borderBottomColor: 'lightgray' /* matches message container background */
            },
            '.embedded .card-form .input-group.error .hint-icon:hover': { /* hint icon */
                color: 'red'
            },
            
            '.embedded .card-form .input-group:not(.error) input': {
                color: 'dimgray'
            },
            '.embedded .card-form .input-group.focus input': {
                color: 'black'
            },
            '.embedded .card-form .input-group.error input': {
                color: 'red'
            },
            
            '.embedded .card-form .split-view .left': {
                paddingRight: '3px'
            },
            '.embedded .card-form .split-view .right': {
                paddingLeft: '3px'
            }
        }
        
        Frames.init({
            
            publicKey: $A.get("$Label.c.Frames_Public_Key"),
            containerSelector: '.frames-container',
            cardValidationChanged: function() {
                payNowButton.disabled = !Frames.isCardValid();
            },
            style: style,
            cardTokenised: function(event) {
                cardTokenn = event.data.cardToken; 
                helper.thirdpartyClientCaller(component,cardTokenn);
                Frames.addCardToken(paymentForm, cardTokenn);
            },
            
            cardSubmitted: function() {
                payNowButton.disabled = true;
            },
        }); 
        
    },
    makePayment:function(component, event, helper) { 
        
        console.log('submit');
        event.preventDefault();
        
        var allValid = component.find('requiredFieldCheck').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if(allValid){
            component.set("v.showSpinner", true);  
            //Create Receipt on Draft status
            var reqWrapPram  =
            {
                amount: component.get("v.amountToPay"),  
                paymentType: 'Card',
                receiptStatus: 'Pending',
                srId:component.get("v.srId")
            }
            var action = component.get("c.createReceiptForCard");
            action.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            action.setCallback(this, function(response) {
                console.log(response.getReturnValue());
                var state = response.getState();
                if (state === "SUCCESS") {
                    var receipt = response.getReturnValue();
                    component.set("v.receiptId",receipt.referenceNo);

                    //Submit the Frames Checkout details
                    Frames.submitCard();

                }else if (state === "INCOMPLETE") {
                    component.set("v.showSpinner", false); 
                    console.log('Response is Incompleted');
                    //component.set("v.showSpinner", false);
                }else if (state === "ERROR") {
                    component.set("v.showSpinner", false); 
                    //component.set("v.showSpinner", false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
            
        }
        else{
            console.log('bad values');
            var toast = $A.get('e.force:showToast');
            // For lightning1 show the toast
            if (toast) {
                //fire the toast event in Salesforce1
                toast.setParams({
                    'type': 'Error',
                    'title': 'Error',
                    'message': 'Please complete the mandatory field'
                });
                toast.fire();
            } else { // otherwise throw an alert        
                alert(title + ': ' + message);
            }
        }
    },
    closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
    },
    component2Event : function(cmp, event) { 
        //Get the event amount attribute
        console.log('Inside amount');
        var amountEntered = event.getParam("amount"); 
        //Set the handler attributes based on event data 
        cmp.set("v.amount", amountEntered);         
    },
     
    cancelName: function(component, event, helper) { 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/"
        });
        urlEvent.fire();
    }
})