({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srId = newURL.get("srId");
    component.set("v.srId", srId);
    var action = component.get("c.getEntityRecords");
    action.setParams({
      srId: srId,
      pageId: pageId,
      docMasterCode: component.get("v.documentMaster")
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseWrap = response.getReturnValue();
        console.log("responseWrap==>" + JSON.stringify(responseWrap));
        if (!responseWrap.hasError) {
          component.set("v.entityRecList", responseWrap.serviceObjList);
          component.set("v.commandButton", responseWrap.ButtonSection);
          component.set("v.fileName", responseWrap.uploadedFileName);
        } else {
          console.log("@@@@@ Error " + responseWrap.ErrorMessage);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  viewDetails: function (component, event, helper) {
    var currentId = event.target.id;
    component.set("v.currentSrId", currentId);
  },
  deleteEntity: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srId = newURL.get("srId");
    var currentId = event.target.id;
    var action = component.get("c.deleteEntityAndActivity");
    action.setParams({
      srId: srId,
      currentSrId: currentId
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseWrap = response.getReturnValue();
        if (!responseWrap.hasError) {
          component.set("v.entityRecList", responseWrap.serviceObjList);
        } else {
          helper.displayToast("Error", responseWrap.ErrorMessage);
        }
      } else {
        helper.displayToast("Error", response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },
  hideDetails: function (component, event, helper) {
    component.set("v.currentSrId", "");
  },
  addNew: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    var action = component.get("c.addNewEntity");
    action.setParams({
      srId: srId
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseWrap = response.getReturnValue();
        console.log("responseWrap==>" + JSON.stringify(responseWrap));
        if (!responseWrap.hasError) {
          component.set("v.entityRecList", responseWrap.serviceObjList);
          component.set("v.showAppInfo", true);
        } else {
          helper.displayToast("Error", responseWrap.ErrorMessage);
          console.log("Error", responseWrap.ErrorMessage);
        }
      } else {
        helper.displayToast("Error", response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  handleButtonAct: function (component, event, helper) {
    try {
      var uploadedfile = component.get("v.fileName");
      if (!uploadedfile) {
        helper.displayToast(
          "Error",
          "Please upload the stucture diagram",
          "error"
        );
        return;
      }
      let newURL = new URL(window.location.href).searchParams;
      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");
          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  handleUploadFinished: function (cmp, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var srID = newURL.get("srId");
    var pageID = newURL.get("pageId");

    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;
    var contentDocumentId = uploadedFiles[0].documentId;
    if (fileName) {
      cmp.set("v.fileName", fileName);
    }
    if (contentDocumentId) {
      cmp.set("v.contentDocumentId", contentDocumentId);
      // fileUploaded
      cmp.set("v.fileUploaded", true);

      var action = cmp.get("c.uploadStructureDiagram");
      action.setParams({
        srId: srID,
        contentDocumentId: contentDocumentId
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");
          console.log("ContentDocId==>" + response.getReturnValue());
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    }
  }
});