({
    
     doinits : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner  
       
           helper.doInit(component, event, helper); 
        
     },
    
     changeExpress : function (component, event, helper) {
       component.set("v.serviceRqst.Express_Service__c",event.getSource().get('v.checked'));
    },
        changenationality1 : function (component, event, helper) {
       
      component.set("v.serviceRqst.Nationality_list__c",component.find("nationality").get("v.value"));
      if(component.find("nationality").get("v.value") === 'United Arab Emirates' ||component.find("nationality").get("v.value") === 'Kuwait'
         || component.find("nationality").get("v.value") === 'Saudi Arabia' ||component.find("nationality").get("v.value") === 'Oman'
         || component.find("nationality").get("v.value") === 'Bahrain' || component.find("nationality").get("v.value") === 'Qatar'  ){
          
          component.set("v.isGCC",true);
           component.set("v.serviceRqst.Residence_Visa_No__c", '');
           component.set("v.serviceRqst.Residence_Visa_Expiry_Date__c", '');
      }
        else{
            component.set("v.isGCC",false);
      }
        
        
    },

    
    
    lookupchange: function(component, event, helper) {
        component.set("v.selectedLookId",   event.getSource().get("v.value"));
        component.set("v.islookup", "true");
    },
    
     typeOfamendChange: function(component, event, helper) {
        var selectedOptionValue = event.getParam("value");
   
        if(selectedOptionValue.toString().includes("Name")){
            component.set("v.isNameDisabled",false);
        }
         else{
             component.set("v.isNameDisabled",true);
         }
         
         if(selectedOptionValue.toString().includes("Nationality")){
            component.set("v.NationalityDisabled",false);
        }
         else{
             component.set("v.NationalityDisabled",true);
         }
         
          component.set("v.serviceRqst.Type_of_Amendment__c",event.getSource().get("v.value"));
    },
    
     closeLookUp: function(component, event, helper) {
        component.set("v.islookup", "false");
        component.set("v.islookupOther", "false");
       // location.reload(true);
    }, 
    
   
    
    closeLookUp: function(component, event, helper) {
        component.set("v.islookup", "false");
        component.set("v.islookupOther", "false");
    }, 
    
      AddLookUp: function(component, event, helper) {
          if(component.get("v.selectedLookUpRecord").Name == null){
              return;
          }
          component.set("v.serviceRqst.Occupation_GS__c", component.get("v.selectedLookUpRecord").Name);
          component.find("occup").set("v.value",component.get("v.selectedLookUpRecord").Name);
          component.set("v.islookup", "false");
    }, 
    
    saveotherRequests: function (cmp, event, helper) {
      
        var allValid = cmp
            .find('field')
            .reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();

                return validSoFar && inputCmp
                    .get('v.validity')
                    .valid;
            }, true);

        if (!allValid) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'All Required Fields Should Be Populated!');
            return;
        }
        if(cmp.get("v.otherServiceName")=='Renewal'){
        var allValid1 = cmp
            .find("Duration")
            .get("v.value");

            if (allValid1 === undefined || allValid1 === '') {
                cmp.set("v.isError", true);
                setTimeout(function () {
                    cmp.set("v.isError", false);
                }, 4500);
                cmp.set("v.successmsg", 'Duration field cannot be blank');
                return;
            }
        }
          cmp.set("v.Spinner", true); 
        var action = cmp.get("c.saveRequest");
            action.setParams({
                newServiceRequest : cmp.get("v.serviceRqst")
            });
           
            action.setCallback(this, function (a) {
                var state = a.getState();
                
             
                if (state == "SUCCESS") {
                    if(a.getReturnValue().startsWith('Error')){
                        cmp.set("v.isError",true);
                         setTimeout(function(){cmp.set("v.isError",false);}, 4500);
                         cmp.set("v.successmsg",JSON.stringify(a.getReturnValue())); 
                         cmp.set("v.Spinner", false);
                    }
                    else{
                        cmp.set("v.issaved", true); //&nooverride=1&RecordType=0123N000001SPY0QAO
                        window.open(a.getReturnValue()+'?isSR=GS',"_self");
                     }
                 } else {
                  
                    cmp.set("v.Spinner", false);
                 }
            });
            $A.enqueueAction(action);
     
    },
     getDuration: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Duration__c",
            component.find("Duration").get("v.value")
        );
    },
 changeRegis: function (component, event, helper) {
        component.set("v.isReg", event.getSource().get('v.checked'));
        component.set("v.serviceRqst.Apt_or_Villa_No__c", '');
        component.set(
            "v.serviceRqst.Use_Registered_Address__c",
            event.getSource().get('v.checked')
        );

        var action = component.get("c.getRegistratedAccounts");
        action.setParams({accountId: component.get("v.accountNew.Id")});

        action.setCallback(this, function (a) {
            var state = a.getState();
            if (state == "SUCCESS") {
               
                component.set("v.registiredAddress", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },    
    
    dateUpdate: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Passport_Date_of_Expiry__c") != '' && component.get("v.serviceRqst.Passport_Date_of_Expiry__c") <= todayFormattedDate) {
            component.set("v.dateValidationError", true);
        } else {
            component.set("v.dateValidationError", false);
        }
    },

    dateUpdate1: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Passport_Date_of_Issue__c") != '' && component.get("v.serviceRqst.Passport_Date_of_Issue__c") >= todayFormattedDate) {
            component.set("v.dateValidationError1", true);
        } else {
            component.set("v.dateValidationError1", false);
        }
    },

    dateUpdate2: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Date_of_Birth__c") != '' && component.get("v.serviceRqst.Date_of_Birth__c") >= todayFormattedDate) {
            component.set("v.dateValidationError2", true);
        } else {
            component.set("v.dateValidationError2", false);
        }
    },

    dateUpdate3: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Residence_Visa_Expiry_Date__c") != '' && component.get("v.serviceRqst.Residence_Visa_Expiry_Date__c") <= todayFormattedDate) {
            component.set("v.dateValidationError3", true);
        } else {
            component.set("v.dateValidationError3", false);
        }
    },
    mobilevalidation: function (component, event, helper) {
        if (component.get("v.serviceRqst.Courier_Mobile_Number__c") != '' && !component.get("v.serviceRqst.Courier_Mobile_Number__c").startsWith("+971")) {
            component.set("v.mobileValidation1", true);
        } else {
            component.set("v.mobileValidation1", false);
        }
    },
    mobilevalidation1: function (component, event, helper) {
        if (component.get("v.serviceRqst.Courier_Cell_Phone__c") != '' && !component.get("v.serviceRqst.Courier_Cell_Phone__c").startsWith("+971")) {
            component.set("v.mobileValidation2", true);
        } else {
            component.set("v.mobileValidation2", false);
        }
    }

    
	
})