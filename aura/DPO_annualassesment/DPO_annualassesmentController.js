({
  doInit: function (cmp, event, helper) {
    cmp.set("v.spinner", true);
    try {
	  
	  console.log("===pages assessment=="); 
      var getFormAction = cmp.get("c.getDataController");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      var flowId = newURL.get("flowId");

      var reqWrapPram = {
        srId: srIdParam,
        pageID: pageId,
        flowId: flowId
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(">>>>>>>>>>>>>>");

      getFormAction.setCallback(this, function (response) {
        var state = response.getState();

        if (cmp.isValid() && state === "SUCCESS") {
			var respWrap = response.getReturnValue();
			console.log(respWrap);
			
           /* console.log('============respWrap.howLongKeepOthers============'+respWrap.howLongKeepOthers);
            console.log('============respWrap.howOftenSuch============'+respWrap.howOftenSuch);
            console.log('============respWrap.natureOfrelationsShipOthers============'+respWrap.natureOfrelationsShipOthers);
            console.log('============respWrap.technicalOrganiZationalOthers============'+respWrap.technicalOrganiZationalOthers);
            console.log('============respWrap.currentStateEmergingOthers============'+respWrap.currentStateEmergingOthers);
            */ 
            cmp.set("v.howLongKeepOthers", respWrap.howLongKeepOthers);    
            cmp.set("v.howOftenSuch", respWrap.howOftenSuch);    
            cmp.set("v.natureOfrelationsShipOthers", respWrap.natureOfrelationsShipOthers);    
            cmp.set("v.technicalOrganiZationalOthers", respWrap.technicalOrganiZationalOthers);    
            cmp.set("v.currentStateEmergingOthers", respWrap.currentStateEmergingOthers);    
            cmp.set("v.typeOfOthersNone", respWrap.typeOfOthersNone);    
            
			cmp.set("v.assessmentObj", respWrap.annualAssesment);    
			console.log(
			"#########123456 ASsess ",
			JSON.stringify(cmp.get("v.assessmentObj"))
			);

			cmp.set("v.commandButton", respWrap.ButtonSection);
			cmp.set("v.spinner", false);
        } else {
          cmp.set("v.spinner", false);
          alert("@@@@@ Error " + response.getError()[0].message);
          //cmp.set("v.errorMessage", response.getError()[0].message);
          //cmp.set("v.isError", true);
         // helper.createToast(cmp, event, response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      alert("=error===" + err.message);
      cmp.set("v.spinner", false);
    }
  },
  
  handleRefreshEvnt: function (cmp, event, helper) {
   
  },
    
  onMultiselectChange: function (cmp, evt, helper) {

      var selectedOptionValue = evt.getParam("value");
      var fieldName  = evt.getSource().get("v.fieldName").toString();
      console.log('---vvvv-'+selectedOptionValue);
	  console.log('==========fieldName========'+fieldName);
     
      var selectedList = selectedOptionValue.toString().split(';');
      console.log('=======selectedList========='+selectedList.length);
      var isOtherSelected =false;
      var isNoneSelected =false;
      for (var i=0; i < selectedList.length; i++) {
          	console.log('---TTTT---' + selectedList[i]);
          if(selectedList[i] == 'Other')isOtherSelected = true;
          if(selectedList[i] == 'None of the above')isNoneSelected = true;
      }
      console.log('=======isOtherSelected========='+isOtherSelected);
      if(fieldName == 'How_long_will_you_keep_such_data__c'){
          console.log('=======How_long_will_you_keep_such_data__c=========');
         var isavailable = (isOtherSelected) ? true :false;
         cmp.set("v.howLongKeepOthers", isavailable);
      }
      
      if(fieldName == 'How_often_is_such_data_collected__c'){
          console.log('=======How_often_is_such_data_collected__c=========');
         var isavailable = (isOtherSelected) ? true :false;
         cmp.set("v.howOftenSuch", isavailable);
      }
       
      if(fieldName == 'Nature_of_your_relationship_with_the_Ind__c'){
          console.log('=======Nature_of_your_relationship_with_the_Ind__c=========');
         var isavailable = (isOtherSelected) ? true :false;
         cmp.set("v.natureOfrelationsShipOthers", isavailable);
      }
      if(fieldName == 'Technical_and_organizational_measures__c'){
          console.log('=======Technical_and_organizational_measures__c=========');
         var isavailable = (isOtherSelected) ? true :false;
         cmp.set("v.technicalOrganiZationalOthers", isavailable);
      } 
       if(fieldName == 'Current_state_of_emerging_or_advanced__c'){
          console.log('=======Current_state_of_emerging_or_advanced__c=========');
         var isavailable = (isOtherSelected) ? true :false;
         cmp.set("v.currentStateEmergingOthers", isavailable);
      } 
      
      if(fieldName == 'Types_of_processing_in_your_operations__c'){
          console.log('=======Types_of_processing_in_your_operations__c=========');
         var isavailable = (isNoneSelected) ? true :false;
         cmp.set("v.typeOfOthersNone", isavailable);
      }
  }, 

  
  onChange: function (cmp, evt, helper) {
  }, 

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.showForm", false);
  },

 
  openForm: function (cmp, evt, helper) {
    console.log("In openForm ", cmp.get("v.amedSelWrap"));
    cmp.set("v.showForm", true);
  },

 
  closeModel: function (cmp, event, helper) {
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },
  handleButtonAct: function (cmp, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId"); 
      var buttonId = event.target.id;
      console.log(buttonId);
      var flowId = newURL.get("flowId");
       	console.log("==handleButtonAct===");
        var allValid = true;
        cmp.set("v.errorMessage", "");
        cmp.set("v.isError", false);
        cmp.set("v.spinner", true);
       
        let fields = cmp.find("dataProtectionkey");

        for (var i = 0; i < fields.length; i++) {
            var inputFieldCompValue = fields[i].get("v.value");
            if(inputFieldCompValue){
                
                console.log('-----fieldname--------'+fields[i].get("v.fieldName"));
            	console.log('------required-------'+fields[i].get("v.required"));
                console.log('------inputFieldCompValue--'+inputFieldCompValue);
            }
            //console.log(inputFieldCompValue);
            if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                //console.log('bad');
                fields[i].reportValidity();
                allValid = false;
            }   
        }
        console.log(allValid);
         if (!allValid) {
            console.log("Error MSG");
            cmp.set("v.spinner", false);
            cmp.set("v.errorMessage", "Please fill required field");
            cmp.set("v.isError", true);
            helper.showToast(cmp,event,'Please fill required fields','error');
        } else {
             try {
              
               	var onAmedSave = cmp.get('c.onAmedSaveDB1');
				var reqWrapPram  =
				{
					annualAssesment : cmp.get('v.assessmentObj'),
          buttonId : buttonId,
          srId :srID,
          pageID :pageID,
          flowId :flowId
				};
		         
				onAmedSave.setParams(
				{
					"reqWrapPram": JSON.stringify(reqWrapPram)
				});
		        
				onAmedSave.setCallback(this, 
                    function(response) {
                        var state = response.getState();
                        console.log("callback state onAmedSave: " + state);
                        var respWrap = response.getReturnValue();
                        console.log('==inside=124==',respWrap);
                        console.log('==inside=12356==',cmp.isValid());
                        if (state === "SUCCESS") 
                        {
                          if (!respWrap.errorMessage) {
                            window.open(respWrap.pageActionName, "_self");
                          }else{
                            
                            cmp.set('v.spinner',false);
                            cmp.set("v.errorMessage",respWrap.errorMessage);
                            cmp.set("v.isError",true);
                            helper.showToast(cmp,event,respWrap.errorMessage,'error');
                          }  
                        }
                        else
                        {
                            console.log('@@@@@ Error '+response.getError()[0].message);
                            cmp.set('v.spinner',false);
                            cmp.set("v.errorMessage",response.getError()[0].message);
                            cmp.set("v.isError",true);
                            helper.showToast(cmp,event,response.getError()[0].message,'error');
                        }
                    });
                    $A.enqueueAction(onAmedSave);
            } catch (err) {
                cmp.set("v.spinner", false);
                cmp.set("v.isError", true);
                console.log(err.message);
            }
        }
      
    } catch (err) {
      console.log(err.message);
    }
  }
});