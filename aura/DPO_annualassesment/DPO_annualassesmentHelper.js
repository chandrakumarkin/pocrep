({
    showToast: function(component, event, msg, type) 
    {
        console.log('######## errorMessage inside ',msg);
                                                    
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissible",
          message: msg,
          type: type 
        });
        toastEvent.fire();
 	},
})