({
  doInit: function (cmp, event, helper) {
    cmp.set("v.spinner", true);
    try {
      console.log("===pages init manage Shareholders===");

      var getFormAction = cmp.get("c.getExistingAmendment");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      cmp.set("v.srId", srIdParam);
      var reqWrapPram = {
        srId: srIdParam,
        pageID: pageId,
        recordtypename: cmp.get("v.recordtypename"),
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      console.log(
        "@@@@@@@@@@33 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          cmp.set("v.srWrap", respWrap.srWrap);
          console.log(respWrap.ButtonSection);
          cmp.set("v.commandButton", respWrap.ButtonSection);
          cmp.set("v.AmendListWrapp", response.getReturnValue().amendWrapp);
          console.log(
            "#########123456 respWrap ",
            JSON.stringify(cmp.get("v.srWrap.shareholderAmedWrapLst"))
          );

          //Setting the Company Secretary label for the child components
          var srWrap = respWrap.srWrap;
          console.log("init label" + srWrap.srObj.Business_Sector__c);
          if (
            srWrap.srObj &&
            srWrap.srObj.Business_Sector__c ==
              "Non Profit Incorporated Organisation (NPIO)"
          )
            cmp.set("v.csLabelValue", "Secretary");

          cmp.set("v.spinner", false);
        } else {
          cmp.set("v.spinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          cmp.set("v.errorMessage", response.getError()[0].message);
          cmp.set("v.isError", true);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
      cmp.set("v.spinner", false);
    }
  },
  showConfirmForm: function (cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmend");
    console.log(selectedAmend);
    cmp.set("v.amedWrap", selectedAmend);
    cmp.set("v.amedIdInFocus", selectedAmend.displayName);
    console.log(cmp.get("v.amedIdInFocus"));
    console.log(cmp.get("v.selectedValue"));
    console.log(cmp.get("v.amedWrap"));
    console.log(cmp.get("v.srId"));
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    //cmp.set("v.showForm", false);
    cmp.set("v.selectedValue", "");
    cmp.set("v.amedWrap", null);
    cmp.set("v.amedIdInFocus", "none");
  },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      cmp.set("v.amedWrap", selectedLicenseGetFromEvent);
      cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
      console.log(cmp.get("v.selectedValue"));
      cmp.set("v.selectedAmend", selectedLicenseGetFromEvent);
      //cmp.set("v.amedIdInFocus", selectedLicenseGetFromEvent.amedObj.Id);
      cmp.set("v.amedIdInFocus", "none");
    } catch (error) {
      console.log(error.message);
    }
  },

  onChange: function (cmp, evt, helper) {
    //set selectedValue to the id of the selected amendobj from dropdown
    cmp.set("v.selectedValue", "");
    var otherList = cmp.get("v.srWrap.shareholderAmedWrapLst");
    console.log(otherList);
    var index = cmp.find("select").get("v.value");
    var selectedAmend = otherList[index];
    cmp.set("v.selectedAmend", selectedAmend);
    console.log(selectedAmend.amedObj);
    console.log(selectedAmend.amedObj);
    if (selectedAmend.amedObj) {
      cmp.set("v.selectedValue", selectedAmend.amedObj.Id);
    } else {
      cmp.set("v.selectedValue", "");
    }
    console.log(cmp.get("v.selectedValue"));
    cmp.set("v.amedIdInFocus", "none");
  },
  handleRefreshEvnt: function (cmp, event, helper) {
    var amedId = event.getParam("amedId");
    var srWrap = event.getParam("srWrap");
    var isNewAmendment = event.getParam("isNewAmendment");
    var amedWrap = event.getParam("amedWrap");
    console.log("$$$$$$$$$$$ handleRefreshEvnt== ", JSON.stringify(srWrap));
    console.log("2222222222 handleRefreshEvnt== ", amedWrap);
    cmp.set("v.amedId", amedId);
    cmp.set("v.selectedValue", "");
    if (amedId != null && amedId != "") {
      cmp.set("v.isNewPanelShow", false);
    } else {
      cmp.set("v.isNewPanelShow", true);
    }

    if (!$A.util.isEmpty(srWrap)) {
      console.log("====sr wrap assign===");
      cmp.set("v.srWrap", srWrap);
      cmp.set("v.selectedValue", "");
    }
    if (isNewAmendment == true) {
      console.log("====isNew===");
      cmp.set("v.amedWrap", amedWrap);
      cmp.set("v.selectedValue", "");
    }
    if (isNewAmendment == false) {
      console.log("====isNew===");
      cmp.set("v.amedWrap", amedWrap);
      cmp.set("v.selectedValue", "");
    }
    //for auto scolling
    var activeScreen = document.getElementById("activePage");
    console.log("id of active screen ------>" + activeScreen);
    if (activeScreen) {
      activeScreen.scrollIntoView({
        block: "start",
        behavior: "smooth",
      });
    }
  },
  showToast: function (title, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: title,
      message: message,
      mode: "dismissable",
      duration: 500,
    });
    toastEvent.fire();
  },
  closeModel: function (cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },
  handleButtonAct: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        console.log(state);
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log(response.getError());
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
});