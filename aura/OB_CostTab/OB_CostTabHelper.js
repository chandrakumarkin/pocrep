({
  getSRHelper: function(component, event, helper) {
    var action = component.get("c.getSrObjRec");
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue().srObjRecWrapList);
        component.set(
          "v.srItemsWrap",
          response.getReturnValue().srObjRecWrapList
        );
        try {
          //helper.clacTotalCost(component, event, helper, response.getReturnValue().srObjRecWrapList);
        } catch (err) {
          console.log(err.message);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  clacTotalCost: function(component, event, helper, srObjWrapp) {
    console.log("reached here");
    var totalCost = 0;
    try {
      for (var srPricelist of srObjWrapp) {
        for (var srPrice of srPricelist.srPriceItemList) {
          if (srPrice.HexaBPM__Status__c == "Added") {
            totalCost += srPrice.Total_Price_USD__c;
          }
        }
      }
    } catch (err) {
      console.log(err.message);
    }
    component.set("v.totalCost", totalCost);
    console.log(totalCost);
  }
});