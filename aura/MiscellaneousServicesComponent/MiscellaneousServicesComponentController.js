({
	onTypeChange : function(component, event, helper) {
	
        if(component.find("selectedVal").get("v.value") =='Family Hold Upon Cancellation'){
            component.set("v.isType",true);
        }
        else{
            component.set("v.isType",false);
        }
        component.set("v.selectedValue",component.find("selectedVal").get("v.value"));
        
     },
    onNumberChange : function(component, event, helper) {
        var numVal = component.find("field1").get("v.value");
        component.set("v.numvalue",numVal);
     },
    
    doInit: function (cmp, event, helper) {
            cmp.set("v.Spinner",true); 
            setTimeout(function(){ cmp.set("v.Spinner",false); }, 1000);      
            var action = cmp.get("c.getserviceRequest");
            action.setParams({
                recordId : cmp.get("v.recordIds")
            });
            action.setCallback(this, function (a) {
                var state = a.getState();
                if (state == "SUCCESS") {
        
                   var result = a.getReturnValue();
                    if(result.newSR.Id !== '' || result.newSR.Id !== null || result.newSR.Id !== undefined ){
                        cmp.find("selectedVal").set("v.value",result.newSR.Type_of_Request__c);
                        cmp.set("v.selectedValue",result.newSR.Type_of_Request__c)
                    
                    if(result.newSR.Quantity__c !== '' && result.newSR.Quantity__c !== null 
                       && result.newSR.Quantity__c !== undefined 
                       && result.newSR.Type_of_Request__c=='Family Hold Upon Cancellation' ){
                       cmp.set("v.isType",true);
                       cmp.find("field1").set("v.value",result.newSR.Quantity__c);
                       cmp.set("v.numvalue",result.newSR.Quantity__c)
                    }
                  }
                   cmp.set("v.serviceRqst",result.newSR);
                   cmp.set("v.accountNew",result.newAccount);
                 } else {
                 
                }
                  if(result.LinkedSR !== undefined ){
                      cmp.set("v.selectedLookUpRecord",result.LinkedSR);
                    }
            });
            $A.enqueueAction(action);               
    },
    saveRequests: function (cmp, event, helper) {
        cmp.set("v.Spinner",true);
        if(cmp.get("v.selectedValue") === undefined || cmp.get("v.selectedValue")=== ''){
            cmp.set("v.Spinner",false); 
            cmp.set("v.isError",true);
             setTimeout(function(){ cmp.set("v.isError",false); }, 3000);
          
           return;
        }
        else{
            
            cmp.get("v.serviceRqst").Type_of_Request__c = cmp.get("v.selectedValue");
            
        }
       
        if(cmp.get("v.selectedValue")==='Family Hold Upon Cancellation' &&
           cmp.get("v.numvalue")== null){
            cmp.set("v.Spinner",false);
           cmp.set("v.isError",true);  
            setTimeout(function(){ cmp.set("v.isError",false); }, 3000);
            return;
        }
        else{
           cmp.get("v.serviceRqst").Quantity__c = cmp.get("v.numvalue");
        }
    
        if(cmp.get("v.selectedLookUpRecord").Id=='' ||
           cmp.get("v.selectedLookUpRecord").Id == null){
            cmp.set("v.Spinner",false);
            cmp.set("v.isError",true);  
            setTimeout(function(){ cmp.set("v.isError",false); }, 3000);
            return;
        }
        cmp.set("v.Spinner",true);
        var action = cmp.get("c.saveRequest");
            action.setParams({
                newServiceRequest : cmp.get("v.serviceRqst"),
                relatedRequest    : cmp.get("v.selectedLookUpRecord.Id")
            });
           
            action.setCallback(this, function (a) {
                var state = a.getState();
                
                setTimeout(function(){ cmp.set("v.Spinner",false); }, 2000);
                if (state == "SUCCESS") {
                     window.open(a.getReturnValue()+'?isSR=GS&nooverride=1&RecordType=0123N0000003VhiQAE',"_self");
                } else {

                }
            });
            $A.enqueueAction(action);
     
    },
   
    
    hideSpinner : function(component,event,helper){
       component.set("v.Spinner", false);
    },
    cancel : function(component,event,helper){
        window.open('/digitalOnboarding/s/',"_self");
    }
})