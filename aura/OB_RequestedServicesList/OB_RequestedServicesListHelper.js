({
  SearchHelper: function (component, event, helper, reqWrapPram, isInit) {
    // show spinner message
    //alert('==entered');
    component.find("Id_spinner").set("v.class", "slds-show");
    var action = component.get("c.getRequestedServices");
    action.setParams({
      reqWrapPram: JSON.stringify(reqWrapPram)
    });

    action.setCallback(this, function (response) {
      // hide spinner when response coming from server

      var state = response.getState();
      if (state === "SUCCESS") {
        var storeResponse = response.getReturnValue();
        console.log("list response " + storeResponse);
        console.log(storeResponse);
        helper.setNavURl(
          component,
          event,
          helper,
          storeResponse.sObjectList,
          storeResponse
        );
        component.set("v.requestedServices", storeResponse.sObjectList);
        //component.set("v.requestTypeList", storeResponse.userRole);
        this.renderPage(component, event);

        // if storeResponse size is 0 ,display no record found message on screen.
        if (storeResponse.sObjectList.length == 0) {
          component.set("v.Message", true);
        } else {
          component.set("v.Message", false);
        }
        console.log(
          isInit + "===userAccessTypeSet====" + storeResponse.userAccessTypeSet
        );
        console.log("===requestType====" + storeResponse.requestType);
        if (isInit == true) {
          console.log("===init value==");
          component.set("v.userAccessTypeSet", storeResponse.userAccessTypeSet);
          component.set("v.requestType", storeResponse.requestType);
        }
        // set numberOfRecord attribute value with length of return value from server
        component.set(
          "v.TotalNumberOfRecord",
          storeResponse.sObjectList.length
        );
        component.find("Id_spinner").set("v.class", "slds-hide");
      } else if (state === "INCOMPLETE") {
        console.log("Response is Incompleted");
        component.find("Id_spinner").set("v.class", "slds-hide");
      } else if (state === "ERROR") {
        component.find("Id_spinner").set("v.class", "slds-hide");
        var errors = response.getError();

        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  renderPage: function (component, event) {
    var records = component.get("v.requestedServices");
    console.log(records);

    var recPerPage = component.get("v.recPerPage");
    component.set("v.maxPage", Math.ceil(records.length / recPerPage));

    var pageNumber = component.get("v.pageNumber");
    component.set("v.startIndex", (pageNumber - 1) * recPerPage);

    component.set("v.endIndex", pageNumber * recPerPage);
  },

  setNavURl: function (component, event, helper, wrap, respWrap) {
    try {
      for (var sObj of wrap) {
        if (sObj.HexaBPM__Record_Type_Name__c) {
          sObj.navUrl =
            respWrap.hexaSrNavLInk[sObj.HexaBPM__Record_Type_Name__c] +
            "&srId=" +
            sObj.Id;
          console.log(sObj);
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  }
});