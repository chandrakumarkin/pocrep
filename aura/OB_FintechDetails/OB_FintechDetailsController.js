({

    doInit: function (component, event, helper) {

        var action = component.get("c.getAccountDetails");

        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
         
            if(state == 'SUCCESS') {

                if(a.getReturnValue())
                {
                    var returnValue = a.getReturnValue();
                    if(returnValue == 'UPDATED'){
                        component.set("v.isDetailsUpdated",false);
                    }
                    else{
                        component.set("v.acc.Id",returnValue);
                    }
                  
                   
                    // component.set("v.displayComponent",returnValue.displayComponent);
                    
                }
            }
            else if(state == 'FAILURE')
            {
               
                console.log('Some problem ocurred');
            }
        });
        $A.enqueueAction(action);
        

    },

   startUpdating : function(component, event, helper) {

        component.set("v.isUpdate",true);
        component.set("v.currentbusinessmodel",true);
        component.set("v.onLoad",false);
        component.set("v.currentbusinessmodel",true);
        component.set("v.isnext",true);

       

    },
    clickNext : function(component, event, helper) {

        component.set("v.isDisable",false);

        if(component.get("v.currentbusinessmodel") == true){

            component.set("v.currentbusinessmodel",false);
            component.set("v.useTechnologies",true);
            return;
        }

        if(component.get("v.useTechnologies") == true){
            component.set("v.useTechnologies",false);
            component.set("v.fundingrounds",true);
            return;
        }

        if(component.get("v.useTechnologies") == true){
            component.set("v.useTechnologies",false);
            component.set("v.fundingrounds",true);

        }
    },

    handleBusinessModalChange : function(component, event, helper) {
        var selectedValues = event.getParam("value");
        if(selectedValues.length !== 0){
           component.set("v.isDisable",true);
        }
        else{
            component.set("v.isDisable",false);
        }
       
        if(selectedValues.includes('Other')){
            component.set("v.currentbusinessmodelOther",true);
           
        }
        component.set("v.acc.Type_of_Start_up__c",selectedValues);
     },

     handleBusinessothers : function(component, event, helper) {
        var selectedValues = event.getParam("value");
        if(selectedValues.length !== 0){
            component.set("v.isDisable",true);
         }
         else{
             component.set("v.isDisable",false);
         }
        
        component.set("v.acc.Other_Start_Up__c",selectedValues);
        
     },

     handleTechChange : function(component, event, helper) {
        var selectedValues = event.getParam("value");
        if(selectedValues.length !== 0){
           component.set("v.isDisable",true);
        }
        else{
            component.set("v.isDisable",false);
        }
        if(selectedValues.includes('Other')){
          
            component.set("v.useTechnologiesOther",true);
      
        }
        component.set("v.acc.Do_you_use_any_of_these_technologies__c",selectedValues);
     },

     handleTechothers : function(component, event, helper) {
        var selectedValues = event.getParam("value");
        if(selectedValues.length !== 0){
            component.set("v.isDisable",true);
         }
         else{
             component.set("v.isDisable",false);
         }
        
        component.set("v.acc.Other_Type_of_Technology__c",selectedValues);
     },


     handleFundingChange : function(component, event, helper) {
        var selectedValues = event.getParam("value");
        component.set("v.showSave",true);
       if(selectedValues.length !== 0){
           component.set("v.isDisable",true);
        }
        else{
            component.set("v.isDisable",false);
        }
        if(selectedValues == 'Yes'){
          
            component.set("v.fundingroundsOther",true);
           
        }
        component.set("v.acc.Have_you_conducted_any_funding_rounds__c",selectedValues);
        
     },

     handlefundingroundValue : function(component, event, helper) {
        var selectedValues = event.getParam("value");
        if(selectedValues.length !== 0){
            component.set("v.isDisable",true);
         }
         else{
             component.set("v.isDisable",false);
         }
        
        component.set("v.acc.Please_Select_Fund_Type__c",selectedValues);
        
     },

     skipnow : function(component, event, helper) {
        component.set("v.isUpdate",false);
        component.set("v.currentbusinessmodel",false);
        component.set("v.onLoad",false);
        component.set("v.currentbusinessmodel",false);
        component.set("v.isnext",false);
        component.set("v.fundingroundsOther",false);
        component.set("v.useTechnologiesOther",false);
        component.set("v.currentbusinessmodelOther",false);
        component.set("v.isDetailsUpdated",false);
        
     },



     saveDetails : function(component, event, helper) {

        component.set("v.isUpdate",false);
        component.set("v.currentbusinessmodel",false);
        component.set("v.onLoad",false);
        component.set("v.currentbusinessmodel",false);
        component.set("v.isnext",false);
        component.set("v.fundingroundsOther",false);
        component.set("v.useTechnologiesOther",false);
        component.set("v.currentbusinessmodelOther",false);
        component.set("v.isDetailsUpdated",false);
      

       //component.set("v.acc.Id",  component.get("v.recordId"));

      

        var action = component.get("c.saveAccountDetails");

        action.setParams(
            {
                "thisAccount": component.get("v.acc")
            });
        

        action.setCallback(this, function(a){

            var state = a.getState(); 
         
            if(state == 'SUCCESS') {

                if(a.getReturnValue())
                 {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Thank You.We have recieved the pending details"
                    });
                    toastEvent.fire();
                 }
            }
            else if(state == 'FAILURE')
            {
               
                console.log('Some problem ocurred');
            }
        });
        $A.enqueueAction(action);
        
     }
})