({
    doInit: function (component, event, helper) {
      console.log("in here init");
  
      const srWrap = component.get("v.srWrap.srObj");
      console.log(
        "^^^^^^^^^^^^^^srWrap^^^^^^^^^^^^",
        JSON.stringify(srWrap)
      );
      console.log(
        "^^^^^^^^^amendwrap^^^^^^^^^^^^^^^^^^",
        JSON.stringify(component.get("v.amedWrap"))
      );
     
      var entityfield = component.get(
        "v.amedWrap.amedObj.Is_this_Entity_registered_with_DIFC__c"
      );
      
      var today = $A.localizationService.formatDate(new Date(), "DD-MM-YYYY");
      var passportExpireDate = component.get(
        "v.amedWrap.amedObj.Passport_Expiry_Date__c"
      );
      
     
      if (entityfield != undefined || entityfield != null) {
        helper.displayEntityField(component);
      }
  
      var typeofEnt = component.get("v.srWrap.srObj.Type_of_Entity__c");
      if (typeofEnt != undefined || typeofEnt != null) {
        helper.displayPartnerType(component);
        helper.displayContriFields(component);
      }
  
      let newURL = new URL(window.location.href).searchParams;
      component.set("v.srId", newURL.get("srId"));
      var flowIdParam = component.get("v.flowId")
        ? component.get("v.flowId")
        : newURL.get("flowId");
      var pageIdParam = component.get("v.pageId")
        ? component.get("v.pageId")
        : newURL.get("pageId");
      component.set("v.pageId", pageIdParam);
      component.set("v.flowId", flowIdParam);
  
      //Firing the onchange event for Nationality
      var individualNationalityAction = component.get("c.selectNationality");
      $A.enqueueAction(individualNationalityAction);
      var corporateNationalityAction = component.get(
        "c.showHideCorporateRiskFields"
      );
      $A.enqueueAction(corporateNationalityAction);
  
      //Invoke the SRDDocs
      //helper.loadSRDocs(component, event, helper);
    },
  
    onAmedSave: function (cmp, event, helper) {
      helper.showSpinner(cmp, event, helper);
      console.log('srOldWrap::'+cmp.get("v.srOldWrap"));
      console.log('srOldWrap.rosRelObjDebtorList::'+JSON.stringify(cmp.get("v.srOldWrap.rosRelObjDebtorList")));
        console.log('srOldWrap.rosRelObjDebtorList::'+JSON.stringify(cmp.get("v.srOldWrap.rosRelObjDebtorList[0].rosObj.Original_Service_Request__c")));
      var allValid = true;
      let fields = cmp.find("shareholderkey");
        for (var i = 0; i < fields.length; i++) {
            var inputFldName = fields[i].get("v.fieldName");
            var inputFieldCompValue = fields[i].get("v.value");
            //console.log(inputFieldCompValue);
            if (
            !inputFieldCompValue &&
            inputFieldCompValue != "0" &&
            fields[i] &&
            fields[i].get("v.required")
            ) {
                console.log("no value", inputFldName);
                //console.log('bad');
                fields[i].reportValidity();
                allValid = false;
            }
        }
  
        if (allValid == false) {
            console.log("Error MSG");
            // cmp.set("v.errorMessage", "Please fill required field");
            // cmp.set("v.isError", true);
            helper.hideSpinner(cmp, event, helper);
            helper.createToast(cmp, event, "Please fill required field");
        } else{
            //console.log(JSON.stringify(cmp.find("fileId").get("v.files"))); fileToBeUploaded
            console.log("######### onAmedSave ");
            console.log(cmp.get("v.recordtypename"));
            console.log("isIndividual" + cmp.get("v.isIndividual"));
            console.log("isCorporate" + cmp.get("v.isCorporate"));
            var recordtyeindi = cmp.get("v.isIndividual");
            var recordtypecor = cmp.get("v.isCorporate");
          
            console.log("-----no error---");
  
             var onAmedSave = cmp.get("c.onAmedSaveDB");
  
             let newURL = new URL(window.location.href).searchParams;
            var srId = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
            console.log('srId:::'+srId);
            //handle err is remaining.
            var fileError = helper.fileUploadHelper(cmp, event, helper);
            if (fileError) {
                helper.hideSpinner(cmp, event, helper); 
                 return;
            }
            var OriginalSrId;
            if(cmp.get("v.srOldWrap.rosRelObjDebtorList[0].rosObj.Original_Service_Request__c") != null){
            OriginalSrId = JSON.stringify(cmp.get("v.srOldWrap.rosRelObjDebtorList[0].rosObj.Original_Service_Request__c"));}
           // var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
           // console.log("########## docMasterContentDocMap ", docMasterContentDocMap);
            var reqWrapPram = {
            srId: srId,
                OriginalSrId: OriginalSrId,
              amedWrap: cmp.get("v.amedWrap"),
               srWrap: cmp.get("v.srOldWrap"),
              recordtypename: cmp.get("v.recordtypename"),
             // docMasterContentDocMap: docMasterContentDocMap,
              entityTypeLabel: cmp.get("v.entityTypeLabel")
            };
            console.log("recordtype" + cmp.get("v.recordtypename"));
            console.log(JSON.parse(JSON.stringify(cmp.get("v.amedWrap"))));
            console.log(JSON.parse(JSON.stringify(reqWrapPram)));
            
            //return;
            onAmedSave.setParams({
              reqWrapPram: JSON.stringify(reqWrapPram)
            });
  
            onAmedSave.setCallback(this, function (response) {
            helper.hideSpinner(cmp, event, helper);
            var state = response.getState();
    
            console.log("callback state: " + state);
    
            if (cmp.isValid() && state === "SUCCESS") {
                var respWrap = response.getReturnValue();
                console.log(respWrap);
                if (!$A.util.isEmpty(respWrap.errorMessage)) {
                    console.log("=====error on sve====" + respWrap.errorMessage);
                    helper.hideSpinner(cmp, event, helper);
                    helper.createToast(cmp, event, respWrap.errorMessage);
                } else {
                  if (respWrap.amedWrap.debObj.Id != "") {
                      console.log(
                      "######### respWrap.amedWrap.debObj.id  ",
                        respWrap.amedWrap.debObj.Id
                      );
                    
                      /*Document Upload Section*/
                      console.log("amendmentId>>" + respWrap.amedWrap.debObj.Id);
                      //cmp.set("v.attachmentParentId", respWrap.amedWrap.amedObj.Id);
          
                      cmp.set("v.amedWrap.debObj", null);
                      //cmp.set("v.authfieldShow",true);
                      var refeshEvnt = cmp.getEvent("refeshEvnt");
                      var srWrap = respWrap.srWrap;   
                      console.log("@@@@@ srWrap ", srWrap);
      
                      // resetting docMasterContentDocMap
                      cmp.set("v.docMasterContentDocMap", []);
                      //var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
                     // console.log(
                      //"########## reset docMasterContentDocMap ",
                      //JSON.stringify(docMasterContentDocMap)
                      //);
      
                      refeshEvnt.setParams({
                      srWrap: srWrap,
                      //hideOldWrap: true
                      });
                      refeshEvnt.fire();
                      cmp.set("v.isSelectedIndividual", true);
                      cmp.set("v.isSelectedCorporate", true);
                  }
                }
            } else {
                console.log("@@@@@ Error " + response.getError()[0].message);
                helper.hideSpinner(cmp, event, helper);
                //cmp.set("v.errorMessage", response.getError()[0].message);
                //cmp.set("v.isError", true);
                helper.createToast(cmp, event, response.getError()[0].message);
            }
            });
            $A.enqueueAction(onAmedSave);
        }
    },

    onAmedSavecoll: function (cmp, event, helper) {
      helper.showSpinner(cmp, event, helper);
     
      var allValid = true;
      let fields = cmp.find("shareholderkey");
        for (var i = 0; i < fields.length; i++) {
            var inputFldName = fields[i].get("v.fieldName");
            var inputFieldCompValue = fields[i].get("v.value");
            console.log(inputFieldCompValue);
            if (
            !inputFieldCompValue &&
            inputFieldCompValue != "0" &&
            fields[i] &&
            fields[i].get("v.required")
            ) {
                console.log("no value", inputFldName);
                //console.log('bad');
                fields[i].reportValidity();
                allValid = false;
            }
        }
  
        if (allValid == false) {
            console.log("Error MSG");
            // cmp.set("v.errorMessage", "Please fill required field");
            // cmp.set("v.isError", true);
            helper.hideSpinner(cmp, event, helper);
            helper.createToast(cmp, event, "Please fill required field");
        } else{
            //console.log(JSON.stringify(cmp.find("fileId").get("v.files"))); fileToBeUploaded
            console.log("######### onAmedSave ");
            console.log(cmp.get("v.recordtypename"));
            console.log("isIndividual" + cmp.get("v.isIndividual"));
            console.log("isCorporate" + cmp.get("v.isCorporate"));
            var recordtyeindi = cmp.get("v.isIndividual");
            var recordtypecor = cmp.get("v.isCorporate");
            console.log('srOldWrap::'+cmp.get("v.srOldWrap"));
          
            console.log("-----no error---");
  
             var onAmedSave = cmp.get("c.onAmedSaveCollateral");
  
             let newURL = new URL(window.location.href).searchParams;
            var srId = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
  
            //handle err is remaining.
            var fileError = helper.fileUploadHelper(cmp, event, helper);
            if (fileError) {
                helper.hideSpinner(cmp, event, helper); 
                 return;
            }
    
           // var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
           // console.log("########## docMasterContentDocMap ", docMasterContentDocMap);
            var reqWrapPram = {
            srId: srId,
              srWrap: cmp.get("v.srOldWrap"),
              amedWrap: cmp.get("v.amedWrap"),
              recordtypename: cmp.get("v.recordtypename"),
             // docMasterContentDocMap: docMasterContentDocMap,
              entityTypeLabel: cmp.get("v.entityTypeLabel")
            };
            console.log("recordtype" + cmp.get("v.recordtypename"));
            console.log(JSON.parse(JSON.stringify(cmp.get("v.amedWrap"))));
            console.log(JSON.parse(JSON.stringify(reqWrapPram)));
            //return;
            onAmedSave.setParams({
              reqWrapPram: JSON.stringify(reqWrapPram),
            });
  
            onAmedSave.setCallback(this, function (response) {
            helper.hideSpinner(cmp, event, helper);
            var state = response.getState();
    
            console.log("callback state: " + state);
    
            if (cmp.isValid() && state === "SUCCESS") {
                var respWrap = response.getReturnValue();
                console.log('respWrap::'+respWrap);
                if (!$A.util.isEmpty(respWrap.errorMessage)) {
                    console.log("=====error on sve====" + respWrap.errorMessage);
                    helper.hideSpinner(cmp, event, helper);
                    helper.createToast(cmp, event, respWrap.errorMessage);
                } else {
                  if (respWrap.amedWrap.collObj.Id != "") {
                      console.log(
                      "######### respWrap.amedWrap.collObj.id  ",
                        respWrap.amedWrap.collObj.Id
                      );
                    
                      /*Document Upload Section*/
                      console.log("amendmentId>>" + respWrap.amedWrap.collObj.Id);
                      //cmp.set("v.attachmentParentId", respWrap.amedWrap.amedObj.Id);
          
                      cmp.set("v.amedWrap.collObj", null);
                      //cmp.set("v.authfieldShow",true);
                      var refeshEvnt = cmp.getEvent("refeshEvnt");
                      var srWrap = respWrap.srWrap;   
                      console.log("@@@@@ srWrap ", srWrap);
      
                      // resetting docMasterContentDocMap
                      cmp.set("v.docMasterContentDocMap", []);
                      //var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
                     // console.log(
                      //"########## reset docMasterContentDocMap ",
                      //JSON.stringify(docMasterContentDocMap)
                      //);
      
                      refeshEvnt.setParams({
                      srWrap: srWrap,
                      });
                      refeshEvnt.fire();
                      cmp.set("v.isSelectedIndividual", true);
                      cmp.set("v.isSelectedCorporate", true);
                  }
                }
            } else {
                console.log("@@@@@ Error " + response.getError()[0].message);
                helper.hideSpinner(cmp, event, helper);
                //cmp.set("v.errorMessage", response.getError()[0].message);
                //cmp.set("v.isError", true);
                helper.createToast(cmp, event, response.getError()[0].message);
            }
            });
            $A.enqueueAction(onAmedSave);
        }
    },
    closeForm: function (cmp, event, helper) {
    
  
      cmp.set("v.spinner", true);
      var isNew = cmp.get("v.isNew");
      cmp.set("v.isCorporate", false);
      cmp.set("v.isIndividual", false);
      cmp.set("v.isSelectedIndividual", true);
      cmp.set("v.isSelectedCorporate", true);
      if (isNew == true) {
        console.log("===new record====");
        cmp.set("v.amedWrap.debObj", null);
      }
      var refeshEvnt = cmp.getEvent("refeshEvnt");
      refeshEvnt.setParams({
        amedId: "",
        isNewAmendment: isNew,
        amedWrap: cmp.get("v.amedWrap.amedObj"),
      });
      cmp.set("v.spinner", false);
      refeshEvnt.fire();
    },
    closeModel: function (cmp, event, helper) {
      //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
      console.log("--toast close--");
      cmp.set("v.errorMessage", "");
      cmp.set("v.isError", false);
    },
  
    handleRecordType: function (cmp, event, helper) {
      var index = event.target.dataset.amedtype;
      console.log("###############@@@@@@@@@@" + index);
      cmp.set("v.recordtypename", index);
      console.log(cmp.get("v.recordtypename"));
      if (index == "Individual") {
        cmp.set("v.isIndividual", true);
        cmp.set("v.isCorporate", false);
      } else {
        cmp.set("v.isCorporate", true);
        cmp.set("v.isIndividual", false);
      }
    },
  
    showAuthFields: function (cmp, event, helper) {
    
    },
    showAuthFieldsCor: function (cmp, event, helper) {
    
    },
    selectNationality: function (cmp, event, helper) {
      var nationality = cmp.get("v.amedWrap.amedObj.Nationality_list__c");
      var resident = cmp.get(
        "v.amedWrap.amedObj.Are_you_a_resident_in_the_U_A_E__c"
      );
  
      if (nationality)
        helper.showFieldOnRiskScore(cmp, event, "individual", nationality);
    },
    showHideCorporateRiskFields: function (cmp, event, helper) {
      var nationality = cmp.get("v.amedWrap.amedObj.Country_of_Registration__c");
      console.log("nationality" + nationality);
      if (nationality)
        helper.showFieldOnRiskScore(cmp, event, "corporate", nationality);
    },
  
   
    showOtherContriType: function (cmp, event, helper) {
      var type = cmp.get("v.amedWrap.amedObj.Type_of_Contribution__c");
  
      if (type == "Other") {
        cmp.set("v.showOtherContri", true);
      } else {
        cmp.set("v.showOtherContri", false);
      }
    },
    showOtherincomewealth: function (cmp, event, helper) {
      var type = cmp.get("v.amedWrap.amedObj.Source_of_income_wealth__c");
      console.log('$$$$$'+type);
      if (type == "Others") {
        cmp.set("v.isSelectedOther", true);
      } else {
        cmp.set("v.isSelectedOther", false);
      }
    },
    showOtherContriTypeCorporate: function (cmp, event, helper) {
      var type = cmp.get("v.amedWrap.amedObj.Type_of_Contribution__c");
  
      if (type == "Other") {
        cmp.set("v.showOtherContriCor", true);
      } else {
        cmp.set("v.showOtherContriCor", false);
      }
    },
  
    showEntityfield: function (cmp, event, helper) {
      helper.displayEntityField(cmp);
    },
  
    handleLookupEvent: function (component, event, helper) {
      console.log("handleLookupEvent");
      var accObj = event.getParam("sObject");
      console.log("lookup--" + JSON.stringify(accObj));
      component.set(
        "v.amedWrap.amedObj.Registration_No__c",
        accObj.Registration_License_No__c
      );
    },
  
    clearRegistrationNumber: function (component, event, helper) {
      console.log("clearRegistrationNumber");
      var accObj = event.getParam("instanceId");
      console.log("lookup--" + JSON.stringify(accObj));
      component.set("v.amedWrap.amedObj.Registration_No__c", "");
    },
    handleFilesChange: function (component, event, helper) {
      console.log("@@@@@@ in handleFilesChange ");
      var fileName = "No File Selected..";
      if (event.getSource().get("v.files").length > 0) {
        fileName = event.getSource().get("v.files")[0][0].name;
      }
  
      //console.log('@@@@@@ in handleFilesChange after fileName ',event.getSource().get("v.files")[0] );
      console.log(fileName);
      component.set("v.fileName", fileName);
    },
    handleFilesChangeAttorney: function (component, event, helper) {
      var fileNameAttorney = "No File Selected..";
      if (event.getSource().get("v.files").length > 0) {
        fileNameAttorney = event.getSource().get("v.files")[0][0].name;
      }
      component.set("v.fileNameAttorney", fileNameAttorney);
    },
  
    handleFilesChangeCertificationIncorporation: function (
      component,
      event,
      helper
    ) {
      var fileNameCertificationIncorporation = "No File Selected..";
      if (event.getSource().get("v.files").length > 0) {
        fileNameCertificationIncorporation = event
          .getSource()
          .get("v.files")[0][0].name;
      }
      component.set(
        "v.fileNameCertificationIncorporation",
        fileNameCertificationIncorporation
      );
    },
    handleFilesChangeBankStatement: function (component, event, helper) {
      var fileNameBankStatement = "No File Selected..";
      if (event.getSource().get("v.files").length > 0) {
        fileNameBankStatement = event.getSource().get("v.files")[0][0].name;
      }
      component.set("v.fileNameBankStatement", fileNameBankStatement);
    },
    handleFilesChangeTaxDoc: function (component, event, helper) {
      var fileNameTaxDoc = "No File Selected..";
      if (event.getSource().get("v.files").length > 0) {
        fileNameTaxDoc = event.getSource().get("v.files")[0][0].name;
      }
      component.set("v.fileNameTaxDoc", fileNameTaxDoc);
    },
    handleFilesChangeBiography: function (component, event, helper) {
      var fileNameBiography = "No File Selected..";
      if (event.getSource().get("v.files").length > 0) {
        fileNameBiography = event.getSource().get("v.files")[0][0].name;
      }
      component.set("v.fileNameBiography", fileNameBiography);
    },
    ShowCertificationMesage: function (component, event, helper) {
      var certifc = component.get(
        "v.amedWrap.amedObj.Certified_passport_copy__c"
      );
      if (certifc == "No") {
        //component.set("v.errorMessage","Please visit DIFC for citation of original passport");
        //component.set("v.isError", true);
        helper.showToast(
          component,
          event,
          helper,
          "Please visit DIFC for citation of original passport",
          "information"
        );
      } else {
        // component.set("v.errorMessage", "");
        //component.set("v.isError", false);
      }
    },
    handleOCREvent: function (cmp, event, helper) {
      console.log("@@@@@@@@@ handleOCREvent ");
      var ocrWrapper = event.getParam("ocrWrapper");
      var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
      // Change the key here
      var fields = cmp.find("shareholderkey");
      var nationality;
      //parsing of code.
      for (var key in mapFieldApiObject) {
        for (var i = 0; i < fields.length; i++) {
          //console.log(fields[i].get("v.fieldName"));
          var fieldName = fields[i].get("v.fieldName");
          if (fieldName) {
            console.log(fieldName);
            if (fieldName.toLowerCase() == key.toLowerCase()) {
              fields[i].set("v.value", mapFieldApiObject[key]);
            }
          }
  
          // for risk score
          if (fieldName && fieldName.toLowerCase() == "nationality_list__c") {
            nationality = fields[i].get("v.value");
          }
        }
      }
  
      console.log("####### nationality ", nationality);
      console.log("####### nationality ");
  
      if (nationality)
        helper.showFieldOnRiskScore(cmp, event, "individual", nationality);
    },
  });