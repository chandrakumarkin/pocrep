({
  onDynamicButtonAction: function (cmp, event, reqWrapPram) {
    //Opening Section
    var objRequestMapTemp = cmp.get("v.objRequestMap");
    var dycBtnAction = cmp.get("c.dynamicButtonAction");
    console.log("############ reqWrapPram ", reqWrapPram);

    dycBtnAction.setParams({
      reqWrapPram: JSON.stringify(reqWrapPram)
    });

    dycBtnAction.setCallback(this, function (response) {
      var state = response.getState();
      console.log("callback state: " + state);

      if (cmp.isValid() && state === "SUCCESS") {
        //cmp.set('v.spinner',false);
        console.log("resp ", JSON.stringify(response.getReturnValue()));
        var respWrap = response.getReturnValue();

        console.log("######### respWrap.errorMessage ", respWrap.errorMessage);
        if (!$A.util.isEmpty(respWrap.errorMessage)) {
          console.log("######### inside ", respWrap.errorMessage);
          cmp.set("v.errorMessage", respWrap.errorMessage);
          cmp.set("v.messageType", "error");

          cmp.set("v.isError", true);
          cmp.set("v.spinner", false);
          //component.find("toastCmp").showToastModel(respWrap.errorMessage, messageType);
          //this.showToast(cmp,event,respWrap.errorMessage);
        } else {
          cmp.set("v.isError", false);
        }
        var isError = cmp.get("v.isError");
        console.log("######### isError----- ", isError);
        if (!isError) {
          var pathname = window.location.pathname;
          console.log("==pathname===" + pathname);

          var pageURL = "";
          if (respWrap.isPublicSite == true) {
            // for site

            var newPathName = "/" + respWrap.sitePageName;
            if (pathname == newPathName) {
              console.log(
                pathname + "==inside same path name===" + newPathName
              );
              let newURL = new URL(window.location.href).searchParams;
              //console.log('==srID url=='+newURL.get('srId'));

              /* var appEvent = $A.get("e.c:OB_RenderPageFlowAppEvent");
              var srId = respWrap.srId;
              var flowId = respWrap.flowId;
              var pageId = respWrap.pageId;

              appEvent.setParams({
                pageId: pageId,
                srId: srId,
                flowId: flowId,
                isButton: true
              });
              console.log("-next button -event fire-");
              appEvent.fire(); */
              window.open(response.getReturnValue().pageActionName, "_self");
            } else {
              if (
                !$A.util.isUndefined(respWrap.sitePageName) &&
                !$A.util.isEmpty(respWrap.sitePageName) &&
                respWrap.sitePageName.indexOf("?") > -1
              ) {
                pageURL =
                  respWrap.strBaseUrl +
                  "/" +
                  respWrap.sitePageName +
                  "&srId=" +
                  respWrap.srId +
                  "&flowId=" +
                  respWrap.flowId +
                  "&pageId=" +
                  respWrap.pageId;
              } else {
                pageURL =
                  respWrap.strBaseUrl +
                  "/" +
                  respWrap.sitePageName +
                  "?srId=" +
                  respWrap.srId +
                  "&flowId=" +
                  respWrap.flowId +
                  "&pageId=" +
                  respWrap.pageId;
                console.log("==site url===");
              }
              window.open(pageURL, "_self");
            }
            window.open(pageURL, "_self");
          } else {
            // for community
            var newPathName =
              "/" + respWrap.communityName + "/" + respWrap.CommunityPageName;
            console.log(pathname + "=1=inside same path name===" + newPathName);
            if (pathname == newPathName) {
              /* console.log(
                pathname + "==inside same path name===" + newPathName
              );
              let newURL = new URL(window.location.href).searchParams;
              console.log("==srID url==" + newURL.get("srId"));

              var appEvent = $A.get("e.c:OB_RenderPageFlowAppEvent");
              var srId = respWrap.srId;
              var flowId = respWrap.flowId;
              var pageId = respWrap.pageId;

              appEvent.setParams({
                pageId: pageId,
                srId: srId,
                flowId: flowId,
                isButton: true
              });
              console.log("-next button -event fire-");
              appEvent.fire(); */
              window.open(response.getReturnValue().pageActionName, "_self");
            } else {
              if (
                !$A.util.isUndefined(respWrap.CommunityPageName) &&
                !$A.util.isEmpty(respWrap.CommunityPageName) &&
                respWrap.CommunityPageName.indexOf("?") > -1
              ) {
                pageURL =
                  respWrap.strBaseUrl +
                  "/" +
                  respWrap.communityName +
                  "/" +
                  respWrap.CommunityPageName +
                  "&srId=" +
                  respWrap.srId +
                  "&flowId=" +
                  respWrap.flowId +
                  "&pageId=" +
                  respWrap.pageId;
              } else {
                pageURL =
                  respWrap.strBaseUrl +
                  "/" +
                  respWrap.communityName +
                  "/" +
                  respWrap.CommunityPageName +
                  "?srId=" +
                  respWrap.srId +
                  "&flowId=" +
                  respWrap.flowId +
                  "&pageId=" +
                  respWrap.pageId;
              }
              window.open(pageURL, "_self");
            }
          }
        }
      } else {
        cmp.set("v.spinner", false);
        console.log("@@@@@ Error " + response.getError()[0].message);
        hepler.showToast(
          cmp,
          event,
          helper,
          response.getError()[0].message,
          "error"
        );
      }
    });
    $A.enqueueAction(dycBtnAction);
  },

  // function automatic called by aura:waiting event
  showSpinner: function (component, event, helper) {
    // make Spinner attribute true for displaying loading spinner
    component.set("v.spinner", true);
  },

  // function automatic called by aura:doneWaiting event
  hideSpinner: function (component, event, helper) {
    // make Spinner attribute to false for hiding loading spinner
    component.set("v.spinner", false);
  },

  fieldCount: function (cmp, event, sectionWrapperList) {
    var fieldCound = 0;
    var fieldContainingValue = 0;
    for (var section of sectionWrapperList) {
      if (
        section.sectionObj.HexaBPM__Section_Type__c != "CommandButtonSection"
      ) {
        for (var field of section.SIWraps) {
          if (field.bRenderSecDetail) {
            fieldCound += 1;
            if (field.FieldValue != "") {
              fieldContainingValue += 1;
            }
          }
        }
      }
    }
    cmp.set("v.numbOfFields", fieldCound);
    cmp.set("v.numbOfFieldsFilled", fieldContainingValue);
  },

  handleUploadFinished: function (cmp, event, helper) {
    /*
        // Get the list of uploaded files
        var uploadedFile = event.getParam("files")[0];
       
        //reparenting code.
        console.log('$$$$$$$$$ secDetId ');
        console.log('$$$$$$$$$ secDetId ',event.getSource().get("v.class"));
        var secDetId = event.getSource().get("v.class");
        
        //console.log('%%%%%%% DOM ',event.getSource().getElement);
        
        //alert("Files uploaded : " + JSON.stringify(uploadedFile));
        if(!secDetId)
		{
            alert("Missing Section Detail Configuration.");
            return;
        }
        if(!uploadedFile)
		{
            alert("Missing File Details.");
            return;
        }
        
        var documentIdParam = uploadedFile.documentId;
        var fileName = uploadedFile.name;
        
        cmp.set("v.passportFileName",fileName);
        
        try
        {
        	var getFormAction = cmp.get('c.reparentToSRDocs');
            let newURL = new URL(window.location.href).searchParams;
            
            var flowIdParam = ( cmp.get('v.flowId') ? cmp.get('v.flowId') : newURL.get('flowId'));     
            var pageIdParam = ( cmp.get('v.pageId') ?  cmp.get('v.pageId') : newURL.get('pageId'));  
            var srIdParam =  (  cmp.get('v.srId') ?  cmp.get('v.srId') : newURL.get('srId')); 
            
            var reqWrapPram  =
                {
                    
                    flowId: flowIdParam,  
                    pageId: pageIdParam,
                    srId: srIdParam,
                    documentId : documentIdParam, 
                    secObjID : secDetId
                }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              console.log('>Wrap.....>');
                                              console.log(respWrap);
                                              
                                              var secItmWrap = respWrap.secItmWrap; 
                                              
                                              //Displaying file upload name.
                                              
                                              console.log('####### secItmWrap.sectionDetailOb.Id ',secItmWrap.sectionDetailObj.Id);
                                              console.log('####### secItmWrap.sectionDetailOb.Parse_File__c ',secItmWrap.sectionDetailObj.Parse_File__c);
                                              console.log('####### documentIdParam ',documentIdParam);
                           
                                              //Calling OCR.
                                              //check is parse check remaining.
                                              helper.callOCR(cmp, event,helper,documentIdParam,secItmWrap);
                                              
                                              
                                              var uploadedFiles = event.getParam("files");
                                              var fileName = uploadedFiles[0].name;
                                              var docMasterCode = event.getSource().get('v.name');  
                                                
                                                var mapDocValues = {};
                                                var docMap = cmp.get("v.docMasterContentDocNameMap");
                                                for(var key in docMap){
                                                    mapDocValues[key] = docMap[key];
                                                }
                                                
                                                mapDocValues[docMasterCode] = fileName;
                                                console.log(mapDocValues);
                                        
                                                console.log('@@@@@@ docMasterContentDocMap ');
                                                cmp.set("v.docMasterContentDocNameMap",mapDocValues);
                                             
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              alert('Error  ',response.getError()[0].message);
                                          }
                                      }
                                     );
            $A.enqueueAction(getFormAction);
            
            
        }catch(err){
            console.log('=error==='+err.message);
        }
        */
  },
  callOCR: function (cmp, event, helper, documentIdParam, secItmWrap) {
    /*			
        			
        			//Calling OCR comes here.....parseFile == true.... remaining
                    if(secItmWrap.sectionDetailObj.Parse_File__c && documentIdParam)
                    {
                        
                        console.log('==call OCR===');
                        helper.showSpinner(cmp, event, helper);
                        var OCRAction = cmp.get("c.processOCR");
                        
                        OCRAction.setParams({
                                                fileId: documentIdParam
                                            });
                        
                         OCRAction.setCallback(this, function(response)
                                    {
                                      
                                           console.log('@@@@@@@@@@@@ respOCR inner',JSON.stringify(response.getReturnValue()) );
                                           var state = response.getState();
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                             	var ocrWrapper = response.getReturnValue();
                                                ocrWrapper = ocrWrapper.mapFieldApiObject;
                                                console.log('ocrWrapper===>'+JSON.stringify(ocrWrapper));
                                                
                                                var secWrapLst = cmp.get('v.secWrapLst');
                                                console.log('@@@@@@@2 Calling parser');
                                                secWrapLst.forEach(secWrap);
                                                function secWrap(sec, index) 
                                              	{
                                                    
                                                    if(!$A.util.isEmpty(sec.SIWraps))
                                                    {
                                                        
                                                        sec.SIWraps.forEach(secItemWrap);
                                                        function secItemWrap(secItem, itemIndex) 
                                                        {
                                                            console.log('@@@@@@@2 inside for loop ',secItem.FieldApi);
                                                            if(!$A.util.isEmpty(secItem.FieldApi) 
                                                               				&& !$A.util.isEmpty(ocrWrapper[secItem.FieldApi.toLowerCase()]))
                                                            {
                                                                secItem.FieldValue = ocrWrapper[secItem.FieldApi.toLowerCase()];
                                                                console.log('secItem.FieldValue==>'+secItem.FieldValue);
                                                            }
                                                        }
                                                    }
                                                }
                                               cmp.set('v.secWrapLst',secWrapLst);
                                               helper.hideSpinner(cmp, event, helper);
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              helper.hideSpinner(cmp, event, helper);
                                              alert('Error  ',response.getError()[0].message);
                                              
                                          }
                                    }  
                            );        
                         $A.enqueueAction(OCRAction);
                         
                    }
        			else
                    {
                        console.log('=no ocr creation=='+parseFile);
                        helper.hideSpinner(cmp, event, helper);
                    	
                	}
              */
  },
  showToast: function (component, event, helper, msg, type) {
    // Use \n for line breake in string
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  },
  fileUploadValidation: function (cmp, event, helper) {
    var fileUploadValid = true;
    var fileHolder = cmp.find("obfilekey");
    //console.log('########### fileHolder ',fileHolder.length);
    if (fileHolder && Array.isArray(fileHolder)) {
      for (var i = 0; i < fileHolder.length; i++) {
        if (fileHolder[i].get("v.requiredDocument")) {
          fileUploadValid =
            fileUploadValid && fileHolder[i].get("v.fileUploaded");
          console.log("@@@@@@@@@@@ fileUploadValid ", fileUploadValid);
          //requiredText
          if (!fileUploadValid && !fileHolder[i].get("v.fileUploaded")) {
            fileHolder[i].set("v.requiredText", "Please upload document.");
          }
        }
      }
    } else if (fileHolder) {
      if (fileHolder.get("v.requiredDocument")) {
        fileUploadValid = fileHolder.get("v.fileUploaded");
        console.log("@@@@@@@@@@@ fileUploadValid ", fileUploadValid);
        if (!fileUploadValid) {
          fileHolder.set("v.requiredText", "Please upload document.");
        }
      }
    }
    console.log(" Final file upload ", fileUploadValid);
    if (!fileUploadValid) {
      helper.showToast(
        cmp,
        event,
        helper,
        "Please upload required files",
        "error"
      );
      cmp.set("v.spinner", false);
      return "file error";
    }
  },

  checkAllSectionFieldValidation: function (cmp, event, helper, srObject) {
    return (
      srObject &&
      !$A.util.isEmpty(srObject.Address_Line_1__c) &&
      //!$A.util.isEmpty(srObject.Address_Line_2__c) &&
      !$A.util.isEmpty(srObject.City_Town__c) &&
      !$A.util.isEmpty(srObject.Country__c) &&
      !$A.util.isEmpty(srObject.Created_By_Agent__c) &&
      !$A.util.isEmpty(srObject.Date_of_Birth__c) &&
      !$A.util.isEmpty(srObject.Date_of_Expiry__c) &&
      !$A.util.isEmpty(srObject.Date_of_Issue__c) &&
      !$A.util.isEmpty(srObject.Gender__c) &&
      !$A.util.isEmpty(srObject.HexaBPM__Email__c) &&
      !$A.util.isEmpty(srObject.HexaBPM__Send_SMS_to_Mobile__c) &&
      !$A.util.isEmpty(srObject.Nationality__c) &&
      !$A.util.isEmpty(srObject.Passport_No__c) &&
      !$A.util.isEmpty(srObject.Place_of_Birth__c) &&
      !$A.util.isEmpty(srObject.Place_of_Issuance__c) &&
      !$A.util.isEmpty(srObject.Po_Box_Postal_Code__c) &&
      !$A.util.isEmpty(srObject.State_Province_Region__c) &&
      !$A.util.isEmpty(srObject.first_name__c) &&
      !$A.util.isEmpty(srObject.last_name__c) &&
      !$A.util.isEmpty(srObject.Entity_Name__c) &&
      !$A.util.isEmpty(srObject.Business_Sector__c) &&
      !$A.util.isEmpty(srObject.Entity_Type__c) &&
      !$A.util.isEmpty(srObject.relationship_with_entity__c) &&
      !$A.util.isEmpty(srObject.I_Agree__c)
    );
  }
});