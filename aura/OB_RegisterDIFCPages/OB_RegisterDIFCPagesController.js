({
  init: function (cmp, event, helper) {
    try {
      console.log("===pages init===");
      //var sobjectName = cmp.get('v.sObjectName');
      helper.showSpinner(cmp, event, helper);
      var getFormAction = cmp.get("c.getForm");

      let newURL = new URL(window.location.href).searchParams;

      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var pageIdParam = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");

      cmp.set("v.srId", srIdParam);
      var reqWrapPram = {
        flowId: flowIdParam,
        pageId: pageIdParam,
        srId: srIdParam
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(
        "@@@@@@@@@@33 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function (response) {
        var state = response.getState();

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          cmp.set("v.initCustomer", respWrap.initCustomer);
          console.log(
            "@@@@@@@@@@@ in onload initCustomer ",
            respWrap.initCustomer
          );

          console.log("objRequest-->" + JSON.stringify(respWrap));

          cmp.set("v.pageName", respWrap.pageName); //1.5

          console.log(
            srIdParam != null && $A.util.isEmpty(respWrap.objRequest)
          );

          if (srIdParam != null && $A.util.isEmpty(respWrap.objRequest)) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              url: "/"
            });
            urlEvent.fire();
          }

          if (respWrap.isSRLocked) {
            var pageURL = respWrap.communityReviewPageURL;
            window.open(pageURL, "_self");
          }

          //console.log('######### respWrap.sectWrapLst ',respWrap.sectWrapLst);
          // srRecId
          if (respWrap.serviceRecordTypeId) {
            cmp.set("v.srRecId", respWrap.serviceRecordTypeId);
          }

          cmp.set("v.secWrapLst", respWrap.sectWrapLst);
          helper.fieldCount(cmp, event, respWrap.sectWrapLst);
          cmp.set("v.secItemWrapMap", respWrap.sectWrapItmMap);
          cmp.set("v.objRequest", respWrap.objRequest);
          helper.hideSpinner(cmp, event, helper);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          helper.hideSpinner(cmp, event, helper);
        }
      });
      $A.enqueueAction(getFormAction);

      if (srIdParam && srIdParam.length > 0) {
        let getPassportSRDocCount = cmp.get("c.getPassportSRDocCount");
        getPassportSRDocCount.setParams({
          srObjId: srIdParam
        });
        getPassportSRDocCount.setCallback(this, function (response) {
          let state = response.getState();
          let respData = response.getReturnValue();
          console.log(`🚀 Init ->C->PPDocCount APEX respData-->`, respData);
          if (state === "SUCCESS") {
            if (respData !== "undefined" || respData !== "null") {
              cmp.set("v.passportDocCount", respData);
            }
          }
        });

        $A.enqueueAction(getPassportSRDocCount);
      }
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },
  handleDocMap: function (cmp, event, helper) {
    console.log("handleDocMap");
    var docMapParam = event.getParam("docMap");
    console.log(JSON.parse(JSON.stringify(docMapParam)));
    var JSONDocMap = JSON.parse(JSON.stringify(docMapParam));
    var mapDocValues = {};
    var docMap = cmp.get("v.docMasterContentDocNameMap");
    console.log(docMap);
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }
    for (var key in JSONDocMap) {
      mapDocValues[key] = JSONDocMap[key];
    }
    console.log(mapDocValues);
    cmp.set("v.docMasterContentDocNameMap", mapDocValues);
    console.log(mapDocValues);
  },
  checkRenderCritera: function (cmp, event, helper) {
    //console.log("=checkRenderCritera==" + event.target);
    var compElem = event.getSource();
    //console.log("====" + compElem);
    //console.log(comp.getAttribute("data-id"));
    //cmp.set('v.spinner',true);

    //Opening Section
    var secWrapLst = cmp.get("v.secWrapLst");
    //for SR insertion.
    var objRequestMap = cmp.get("v.objRequestMap");

    for (var i = 0; i < secWrapLst.length; i++) {
      var SIWraps = secWrapLst[i].SIWraps;
      for (var j = 0; j < SIWraps.length; j++) {
        // If the field is not shown on screenthen blank it out.
        /*
                if( SIWraps[j].FieldApi 
                     && !SIWraps[j].bRenderSecDetail)
                {
                    objRequestMap[ SIWraps[j].FieldApi ] = '';
                    console.log('$$$$$ blank out ',SIWraps[j].FieldApi);
                }
                */

        //Preparing SR object fresh value.
        if (
          SIWraps[j].FieldApi &&
          SIWraps[j].sectionDetailObj.HexaBPM__Component_Type__c !=
            "Output Field" &&
          SIWraps[j].bRenderSecDetail
        ) {
          objRequestMap[SIWraps[j].FieldApi] = SIWraps[j].FieldValue;
        }
      }
    }
    //objRequestMap['OB_DocumentMasterCodeMap__c'] = component.get("v.docMasterContentDocNameMap");

    var classVar = event.getSource().get("v.id");
    var secItemWrap = cmp.get("v.secItemWrapMap")[classVar];
      
    //console.log('ZZZZ secItemWrapMap ',JSON.stringify(secItemWrap));
    
    //#DIFC-12086
    if (secItemWrap && 
        secItemWrap.sectionDetailObj &&
        !secItemWrap.sectionDetailObj.HexaBPM__Disable__c  &&
        secItemWrap.sectionDetailObj.HexaBPM__Field_API_Name__c == 'Co_working_Facility__c' &&
        secItemWrap.sectionDetailObj.Name &&
        secItemWrap.sectionDetailObj.Name.toLowerCase().includes('co-working') &&
        event.getSource().get("v.value") == 'Yes'
       ) {
    	alert('Please ensure you apply for your space on the Co Working portal. You will receive an email with the user credentials to access the system upon receiving initial approval or alternatively, you can browse the co-work portal (https://cowork.fintechhive.difc.ae/), and use the forgot password option to receive an email.');
    }   
   

    if (secItemWrap && secItemWrap.sectionDetailObj.HexaBPM__hasOnChange__c) {
      var fldName = event.getSource().get("v.fieldName");
      var fldVal = event.getSource().get("v.value");
      var fldChk = event.getSource().get("v.checked") ? "true" : "false";
      var name = event.getSource().get("v.name");

      var fnlName = fldName ? fldName : name;
      var fnlValue = fldVal ? fldVal : fldChk;

      console.log("-----fnlName" + fnlName);
      console.log("-----fnlValue" + fnlValue);

      var act = cmp.get("c.evaluateOnChange");
      let newURL = new URL(window.location.href).searchParams;

      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var pageIdParam = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");

      var reqWrapPram = {
        flowId: flowIdParam,
        pageId: pageIdParam,
        srId: srIdParam,
        fieldApi: fnlName,
        fieldVal: fnlValue,
        objRequestMap: objRequestMap
      };
      act.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      act.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          //console.log("logs---" + JSON.stringify(respWrap.sectWrapLst));
          helper.fieldCount(cmp, event, respWrap.sectWrapLst);
          cmp.set("v.secWrapLst", respWrap.sectWrapLst);
          cmp.set("v.secItemWrapMap", respWrap.sectWrapItmMap);
          cmp.set("v.objRequest", respWrap.objRequest);
          cmp.set("v.spinner", false);
        } else {
          cmp.set("v.spinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(act);
    } else {
      cmp.set("v.spinner", false);
    }
  },
  onDynamicButtonActionClick: function (cmp, event, helper) {
    console.log("=onDynamicButtonActionClick===");
    var curTarget = event.currentTarget;
    cmp.set("v.buttonClickID", curTarget.dataset.sectiondetailobjid);
  },

  onDynamicButtonAction: function (cmp, event, helper) {
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
    //var srRecordEditForm = cmp.find('srRecordEditForm');
    //srRecordEditForm.submit();

    console.log("=onDynamicButtonAction=== test");
    cmp.set("v.spinner", true);
    //Opening Section
    var secWrapLst = cmp.get("v.secWrapLst");

    //for SR insertion.
    var objRequestMap = cmp.get("v.objRequestMap");
    console.log("ZZZ objRequestMap test: ", JSON.stringify(objRequestMap));

    var curTarget = event.currentTarget;
    var callSave = true;
    // Store Regular Expression
    var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regExpPhoneformat = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;

    let newURL = new URL(window.location.href).searchParams;
    var srId = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
    var filecheck = document.getElementsByClassName("required-doc");
    var isFileNotValid = false;

    // For required fld chck.
    var allValid = true;
    let fields = cmp.find("obconfigkey");
      console.log('fields: '+fields);
    if (fields) {
      for (var i = 0; i < fields.length; i++) {
        var inputFieldCompValue = fields[i].get("v.value");
        console.log("inputFieldCompValue------" + inputFieldCompValue);
        //console.log(inputFieldCompValue);
        debugger;
        if (
          !inputFieldCompValue &&
          fields[i] &&
          fields[i].get("v.required") &&
          inputFieldCompValue.length == 0
        ) {
          console.log("no value");
            debugger;
          console.log("final------" + fields[i].get("v.fieldName"));
          //console.log('bad');
          fields[i].reportValidity();
          allValid = false;
        }
      }
    }
    if (!allValid) {
      //cmp.set("v.errorMessage", "Please fill required field");
      //cmp.set("v.isError", true);
      helper.showToast(
        cmp,
        event,
        helper,
        "Please fill required field",
        "error"
      );
      cmp.set("v.spinner", false);
      return;
    }

    //obfilekey
    debugger;
    var fileError = helper.fileUploadValidation(cmp, event, helper);
    if (fileError) {
      return;
    }

    for (var i = 0; i < secWrapLst.length; i++) {
      var SIWraps = secWrapLst[i].SIWraps;
      for (var j = 0; j < SIWraps.length; j++) {
        //siwr.
        if (
          SIWraps[j].sectionDetailObj.HexaBPM__Mark_it_as_Required__c &&
          !$A.util.isUndefined(SIWraps[j].FieldType) &&
          !$A.util.isEmpty(SIWraps[j].FieldType) &&
          SIWraps[j].bRenderSecDetail &&
          !$A.util.isEmpty(SIWraps[j].FieldValue) &&
          !$A.util.isUndefined(SIWraps[j].FieldValue) &&
          SIWraps[j].declarationText != "" &&
          SIWraps[j].FieldValue == false
        ) {
          console.log(
            SIWraps[j].sectionDetailObj.HexaBPM__Mark_it_as_Required__c +
              "=test===Iagree===" +
              SIWraps[j].sectionDetailObj.Id
          );

          helper.showToast(
            cmp,
            event,
            helper,
            "Please accept the decalration",
            "error"
          );
          cmp.set("v.spinner", false);
          callSave = callSave && false;
        }

        /* 
                if(!$A.util.isUndefined(SIWraps[j].FieldType) && 
                   SIWraps[j].FieldType == 'PHONE' && 
                    SIWraps[j].bRenderSecDetail &&
                   ( !$A.util.isEmpty(SIWraps[j].FieldValue)
                    && !$A.util.isUndefined(SIWraps[j].FieldValue) ) 
                   && SIWraps[j].FieldValue.match(regExpPhoneformat) == null)
                {
                    console.log('---PHONE not valid');
                    // is not validated
                    SIWraps[j].showFldError = true;
                    SIWraps[j].fldError = 'Not valid number';
                    callSave = callSave && false;
                }
               
                if(!$A.util.isUndefined(SIWraps[j].FieldType) && 
                         SIWraps[j].FieldType == 'EMAIL' && 
                         SIWraps[j].bRenderSecDetail &&
                         ( !$A.util.isEmpty(SIWraps[j].FieldValue)
                          && !$A.util.isUndefined(SIWraps[j].FieldValue) ) 
                         && SIWraps[j].FieldValue.match(regExpEmailformat) == null)
                {
                    console.log('---email address not valid');
                    // is not validated
                    SIWraps[j].showFldError = true; 
                    SIWraps[j].fldError = 'Invalid Email Address';
                    callSave = callSave && false;
                }
              
               if(SIWraps[j].sectionDetailObj.HexaBPM__Mark_it_as_Required__c &&  
                         SIWraps[j].bRenderSecDetail 
                         && SIWraps[j].sectionDetailObj.HexaBPM__Component_Type__c != 'File Upload'
                         && ( $A.util.isEmpty(SIWraps[j].FieldValue) || $A.util.isUndefined(SIWraps[j].FieldValue) ) 
                        )
                {
                    console.log(SIWraps[j].FieldType+'---required field---'+SIWraps[j].FieldApi);
                    // is not validated
                    SIWraps[j].showFldError = true;
                    SIWraps[j].fldError = 'Please fill this field.';
                    callSave = callSave && false;
                }
                else
                {
                    // is validated
                    SIWraps[j].showFldError = false;
                    SIWraps[j].fldError = '';
                    callSave = callSave && true;
                    
                }
                 */

        ///////////////////////////////////////////////////
        // remove below three line
        //SIWraps[j].showFldError = false;
        //SIWraps[j].fldError = '';
        //callSave = true && true;
        // remove above three line
        // ///////////////////////////////////////
        //Preparing SR object fresh value.
        if (
          SIWraps[j].FieldApi &&
          SIWraps[j].sectionDetailObj.HexaBPM__Component_Type__c !=
            "Output Field" &&
          SIWraps[j].bRenderSecDetail
        ) {
          //Field conversion.
          if (
            SIWraps[j].FieldType == "CURRENCY" &&
            !$A.util.isEmpty(SIWraps[j].FieldValue) &&
            !$A.util.isUndefined(SIWraps[j].FieldValue)
          ) {
            console.log("CURRENCY----");
            var cunn = parseFloat(SIWraps[j].FieldValue);
            console.log("===cunn==" + cunn);
            objRequestMap[SIWraps[j].FieldApi] = cunn;
          } else {
            objRequestMap[SIWraps[j].FieldApi] = SIWraps[j].FieldValue;
          }
        }
      }
    }

    console.log("@@@@@@@2 inside", callSave);

    //1.5
    let srObject = cmp.get("v.objRequest");
    //console.log(`🚀 ~ ~ srObject->`, JSON.stringify(srObject));

    let pageName = cmp.get("v.pageName");
    console.log(`🚀 ~ ~ pageName->`, pageName);

    if (
      srObject &&
      srObject.HexaBPM__Record_Type_Name__c == "New_User_Registration" &&
      pageName &&
      pageName == "Define Your Relationship to the Entity"
    ) {
      let ifAllSectionFieldsFilled = helper.checkAllSectionFieldValidation(
        cmp,
        event,
        helper,
        srObject
      );
      console.log(`🚀 ZZZ ifAllSectionFieldsFilled`, ifAllSectionFieldsFilled);

      if (!ifAllSectionFieldsFilled) {
        helper.showToast(
          cmp,
          event,
          helper,
          "Please fill all field in each section",
          "error"
        );
        cmp.set("v.spinner", false);
        return;
      } else {
        //checking passport upload
        let passportDocCount = cmp.get("v.passportDocCount");
        console.log(`🚀 ZZZ passportDocCount->`, passportDocCount);
        if (passportDocCount < 1) {
          helper.showToast(
            cmp,
            event,
            helper,
            "Please upload passport document",
            "error"
          );
          cmp.set("v.spinner", false);
          return;
        }
      }
    }

    debugger;
    if (callSave) {
      if (srId) {
        objRequestMap["Id"] = srId;
      }
      cmp.set("v.objRequestMap", objRequestMap);

      let newURL = new URL(window.location.href).searchParams;
      /*if(newURL.get('srId'))
                {
                    cmp.set('v.srId',newURL.get('srId'));
                }*/

      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var pageIdParam = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      var cmdActionIdparam = curTarget.dataset.sectiondetailobjid;
      //var cmdActionIdparam = cmp.get('v.buttonClickID');

      var initCustomer = cmp.get("v.initCustomer");

      console.log(cmp.get("v.docMasterContentDocNameMap"));
      console.log("===srIdParam===" + srIdParam);
      var reqWrapPram = {
        flowId: flowIdParam,
        pageId: pageIdParam,
        srId: srIdParam,
        cmdActionId: cmdActionIdparam,
        objRequestMap: objRequestMap,
        initCustomer: initCustomer,
        docMasterContentDocMap: cmp.get("v.docMasterContentDocNameMap")
      };
      //,cmdActionId:

      console.log("############ reqWrapPram out ", reqWrapPram);
      // 'a3r1x0000001cxp'
      // calling server
      helper.onDynamicButtonAction(cmp, event, reqWrapPram);
    } else {
      cmp.set("v.spinner", false);
    }
    cmp.set("v.secWrapLst", secWrapLst);
  },
  closeModel: function (cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },

  handleOCREvent: function (cmp, event, helper) {
    // this is valid
    var ocrWrapper = event.getParam("ocrWrapper");
    ocrWrapper = ocrWrapper.mapFieldApiObject;
    console.log("ocrWrapper===>" + JSON.stringify(ocrWrapper));

    var secWrapLst = cmp.get("v.secWrapLst");
    secWrapLst.forEach(secWrap);
    function secWrap(sec, index) {
      if (!$A.util.isEmpty(sec.SIWraps)) {
        sec.SIWraps.forEach(secItemWrap);
        function secItemWrap(secItem, itemIndex) {
          if (
            !$A.util.isEmpty(secItem.FieldApi) &&
            !$A.util.isEmpty(ocrWrapper[secItem.FieldApi.toLowerCase()])
          ) {
            secItem.FieldValue = ocrWrapper[secItem.FieldApi.toLowerCase()];
            console.log("secItem.FieldValue==>" + secItem.FieldValue);
          }
        }
      }
    }
    cmp.set("v.secWrapLst", secWrapLst);
  },
  handleUploadFinished: function (cmp, event, helper) {
    helper.handleUploadFinished(cmp, event, helper);
  },
    handleLookupEvent: function(cmp, event, helper){
        console.log("handleLookupEvent");
        var sObjectId = event.getParam("sObjectId");
        var fieldApiName = event.getParam("fieldApiName");
        console.log('in difc pages fieldApiName: '+fieldApiName);
        //Sabeeha - DIFC2-16970 - 
        //Added this to check whether the Resgistered Agent is a lease holder or not
        if(fieldApiName == 'Registered_Agent__c'){
            cmp.set("v.registerAgentAccId", sObjectId);
            var action = cmp.get("c.isAccountLeaseHolder");
            action.setParams({
                accountId: sObjectId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                
                if (cmp.isValid() && state === "SUCCESS") {
                    var res = response.getReturnValue();
                    console.log('is account lease holder: '+res);
                    cmp.set("v.isRegAgentLeaseHolder", res);
                    
                } else {
                    console.log("@@@@@ Error " + response.getError()[0].message);
                }
            });
            $A.enqueueAction(action);
        }
    },
    handleClearLookupEvent: function(cmp, event, helper){
        console.log('handleClearLookupEvent');
        if(event.getParam("fieldApiName") == 'Registered_Agent__c'){
            cmp.set("v.registerAgentAccId", "");
        }
        
    }
});