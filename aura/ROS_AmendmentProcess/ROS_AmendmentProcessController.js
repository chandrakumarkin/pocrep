({
    doInit: function (cmp, event, helper) {
      cmp.set("v.spinner", true);
      try {
        console.log("===pages init manage Shareholders===");
  
        var getFormAction = cmp.get("c.getExistingAmendment");
  
        let newURL = new URL(window.location.href).searchParams;
        var pageId = newURL.get("pageId");
        var srIdParam = cmp.get("v.srId")
          ? cmp.get("v.srId")
          : newURL.get("srId");
        var flowId = newURL.get("flowId");
  
        var reqWrapPram = {
          srId: srIdParam,
          pageID: pageId,
          flowId: flowId
        };
        console.log(">>>>>JSON.stringify(reqWrapPram)>>>>>>>>>"+JSON.stringify(reqWrapPram));
        getFormAction.setParams({
          reqWrapPram: JSON.stringify(reqWrapPram)
        });
  
        console.log(">>>>>>>>>>>>>>");
  
        getFormAction.setCallback(this, function (response) {
          var state = response.getState();
          console.log("callback state: " + state);
  
          if (cmp.isValid() && state === "SUCCESS") {
            var respWrap = response.getReturnValue();
            //console.log(JSON.parse(JSON.stringify(respWrap)));
            console.log("respWrap is here");
            console.log(respWrap);
  
            // setting entityTypeLabel
            if (respWrap.entityTypeLabel) {
              cmp.set(
                "v.entityTypeLabel",
                respWrap.entityTypeLabel.toLowerCase()
              );
            }
  
            cmp.set("v.srWrap", respWrap.srWrap);
            cmp.set("v.approvedForm", respWrap.srWrap.approvedFormOneROS);
            cmp.set("v.commandButton", respWrap.ButtonSection);
            cmp.set("v.isValidSR", respWrap.srWrap.isValidSR);
            console.log(
              "$$$$$$$$%approvedForm%%%%%%%%%",
              JSON.stringify(respWrap.srWrap.approvedFormOneROS)
            );

            console.log(
              "$$$$$$$$%%%%%%%%%%res",
              JSON.stringify(respWrap.srWrap.srObj)
            );
  
            console.log(
              "#########123456 respWrap ",
              JSON.stringify(cmp.get("v.srWrap.shareholderAmedWrapLst"))
            );
  
            cmp.set("v.spinner", false);
          } else {
            cmp.set("v.spinner", false);
            console.log("@@@@@ Error " + response.getError()[0].message);
            //cmp.set("v.errorMessage", response.getError()[0].message);
            //cmp.set("v.isError", true);
            helper.createToast(cmp, event, response.getError()[0].message);
          }
        });
        $A.enqueueAction(getFormAction);
      } catch (err) {
        console.log("=error===" + err.message);
        cmp.set("v.spinner", false);
      }
    },
    handleRefreshEvnt: function (cmp, event, helper) {
      var amedId = event.getParam("amedId");
      var srWrap = event.getParam("srWrap");
      var isNewAmendment = event.getParam("isNewAmendment");
      var amedWrap = event.getParam("amedWrap");
      console.log("$$$$$$$$$$$ handleRefreshEvnt== ", JSON.stringify(srWrap));
      console.log("2222222222 handleRefreshEvnt== ", amedWrap);
      cmp.set("v.amedId", amedId);
      cmp.set("v.selectedValue", "");
  
      //for auto scolling
      var activeScreen = document.getElementById("activePage");
      console.log("id of active screen ------>" + activeScreen);
      debugger;
  
      if (activeScreen) {
        activeScreen.scrollIntoView({
          block: "start",
          behavior: "smooth"
        });
      }
  
      if (amedId != null && amedId != "") {
        cmp.set("v.isNewPanelShow", false);
      } else {
        cmp.set("v.isNewPanelShow", true);
      }
  
      if (!$A.util.isEmpty(srWrap)) {
        console.log("====sr wrap assign===");
        cmp.set("v.srWrap", srWrap);
        cmp.set("v.selectedValue", "");
      }
      if (isNewAmendment == true) {
        console.log("====isNew===");
        cmp.set("v.amedWrap", amedWrap);
        cmp.set("v.selectedValue", "");
      }
      if (isNewAmendment == false) {
        console.log("====isNew===");
        cmp.set("v.amedWrap", amedWrap);
        cmp.set("v.selectedValue", "");
      }
    },
    onChange: function (cmp, evt, helper) {
      cmp.set("v.showForm", false);
      var amedSelWrap;
      //cmp.set("v.spinner", true);
      //console.log("-convert UBO--onChange-" + cmp.find("select").get("v.value"));
  
      var amedIdParam = cmp.find("select").get("v.value");
      var shareholderAmedWrapLst = cmp.get("v.srWrap").shareholderAmedWrapLst;
      cmp.set("v.selectedValue", amedIdParam);
      console.log("########### shareholderAmedWrapLst ", shareholderAmedWrapLst);
      var amedSelWrap = shareholderAmedWrapLst[amedIdParam];
  
      /*
        for (var i = 0; i < shareholderAmedWrapLst.length; i++) 
        {
          var amedWrap = shareholderAmedWrapLst[i];
          if (amedWrap.amedObj.Id == amedIdParam) 
          {
            amedSelWrap = amedWrap;
            break;
          }
        }
       */
  
      console.log("########### amedSelWrap1 ", amedSelWrap);
      if (amedSelWrap) {
        console.log("########### amedSelWrap2 ", amedSelWrap);
        //cmp.set("v.showForm", true);
        cmp.set("v.amedSelWrap", amedSelWrap);
      }
    },
  
    handleComponentEventClear: function (cmp, evt, helper) {
      cmp.set("v.showForm", false);
      cmp.set("v.selectedValue", "");
      cmp.set("v.amedSelWrap", null);
    },
  
    handleComponentEvent: function (cmp, event, helper) {
      try {
        // get the selected License record from the COMPONETNT event
        var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
        console.log("selected in grandparent");
        console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));
  
        //component.set("v.selectedRecord", selectedLicenseGetFromEvent);
  
        cmp.set("v.amedSelWrap", selectedLicenseGetFromEvent);
        cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
        //cmp.set("v.amedIdInFocus", "none");
      } catch (error) {
        console.log(error.message);
      }
    },
  
    openForm: function (cmp, evt, helper) {
      console.log("In openForm ", cmp.get("v.amedSelWrap"));
      cmp.set("v.showForm", true);
    },
  
    
    closeModel: function (cmp, event, helper) {
      //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
      console.log("--toast close--");
      cmp.set("v.errorMessage", "");
      cmp.set("v.isError", false);
    },

    searchSRRecords: function (cmp, event, helper) {
      
      var srNo = cmp.get("v.srWrap.approvedFormOneROS.SR_Statement_File_No__c");
      //var amendmentAffects  = cmp.get("v.srWrap.approvedFormOneROS.Amendment_Party__c");
      //var typeOfAmendment = cmp.get("v.srWrap.approvedFormOneROS.Type_Of_Amendment__c");
      var isError = ($A.util.isEmpty(srNo) 
        //|| $A.util.isEmpty(amendmentAffects) 
        //|| $A.util.isEmpty(typeOfAmendment)
        ) ? true : false;    
      if(!isError){
        console.log('--No error-'+isError);
        
        var getFormAction = cmp.get("c.getSearchRecords");

        let newURL = new URL(window.location.href).searchParams;
        var pageId = newURL.get("pageId");
        var srIdParam = cmp.get("v.srId")
          ? cmp.get("v.srId")
          : newURL.get("srId");
        var flowId = newURL.get("flowId");
  
        var reqWrapPram = {
          srId: srIdParam,
          pageID: pageId,
          flowId: flowId
        };
      
        //console.log(">>>>>JSON.stringify(reqWrapPram)>>>>>>>>>"+JSON.stringify(reqWrapPram));
        getFormAction.setParams({
          reqWrapPram: JSON.stringify(reqWrapPram),
          srId: srNo,
          appForm: cmp.get("v.srWrap.approvedFormOneROS")
        });
  
        console.log(">>>>>>>>>>>>>>");
  
        getFormAction.setCallback(this, function (response) {
          var state = response.getState();
          cmp.set("v.isValidSR", false);
          if (cmp.isValid() && state === "SUCCESS") {
            
            var respWrap = response.getReturnValue();

            if (respWrap.errorMessage == null) {
              console.log("respWrap is here");
              console.log('===??????==^^^^^^^^^==========='+JSON.stringify(respWrap.srWrap.debtObjLst));
              console.log('===??????==^^^^^^^^^isValidSR==========='+JSON.stringify(respWrap.srWrap.isValidSR));
              cmp.set("v.spinner", false);
             // cmp.set("v.srWrap.debtObjLst", respWrap.srWrap.debtObjLst);
              cmp.set("v.isValidSR", respWrap.srWrap.isValidSR);
              console.log('==Final=======');
              console.log('===??????==^^^srWrap^^^^^^==========='+JSON.stringify(respWrap.srWrap));
              cmp.set("v.srWrap", respWrap.srWrap);
              cmp.set("v.approvedForm", respWrap.srWrap.approvedFormOneROS);
              cmp.set("v.commandButton", respWrap.ButtonSection);
  

            } else {
              helper.createToast(cmp, event, respWrap.errorMessage);
            }
           
          } else {
            cmp.set("v.spinner", false);
            console.log("@@@@@ Error " + response.getError()[0].message);
            helper.createToast(cmp, event, response.getError()[0].message);
          }
        });
        $A.enqueueAction(getFormAction);

      }else{
        console.log('--first check--'+isError);
        helper.createToast(cmp, event, "Please fill SR No# field");
      }
      
    
     // console.log('=======isallValid========='+isallValid);
       // if(isallValid){
         /* var action = component.get("c.searchSRRecords");
          action.setParams({
            debObj :component.get("v.srWrap.approvedFormOneROS")
          });
      
          action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              console.log("button succsess");
              console.log(response.getReturnValue());
            } else {
              console.log("@@@@@ Error " + response.getError()[0].message);
              helper.createToast(component, event, response.getError()[0].message);
            }
          });
          $A.enqueueAction(action);*/
        //}
    },

    handleButtonAct: function (component, event, helper) {
      try {
         
          var allValid = true;
          let fields = component.find("approvedFormFields");
            for (var i = 0; i < fields.length; i++) {
              var inputFldName = fields[i].get("v.fieldName");
              var inputFieldCompValue = fields[i].get("v.value");
              //console.log(inputFieldCompValue);
              if (
              !inputFieldCompValue &&
              inputFieldCompValue != "0" &&
              fields[i] &&
              fields[i].get("v.required")
              ) {
                  console.log("no value", inputFldName);
                  //console.log('bad');
                  fields[i].reportValidity();
                  allValid = false;
              }
            }    

          if (allValid == false) {
            console.log("Error MSG");
            // cmp.set("v.errorMessage", "Please fill required field");
            // cmp.set("v.isError", true);
            helper.hideSpinner(component, event, helper);
            helper.createToast(component, event, "Please fill required field");
        }else{
          
          let newURL = new URL(window.location.href).searchParams;
          var srID = newURL.get("srId");
          var pageID = newURL.get("pageId");
          var buttonId = event.target.id;
          console.log(buttonId);
          var action = component.get("c.getButtonAction");

          var reqWrapPram = {
            SRID: srID,
            pageId: pageID, 
            ButtonId: buttonId,
            debObj :component.get("v.srWrap.approvedFormOneROS")
          };

          action.setParams({
            SRID: srID,
            pageId: pageID, 
            ButtonId: buttonId,
            reqWrapPram: JSON.stringify(reqWrapPram),
            debObj :component.get("v.srWrap.approvedFormOneROS")
          });
          console.log("********************");
          action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              console.log("button succsess");
              console.log(response.getReturnValue());
              window.open(response.getReturnValue().pageActionName, "_self");
            } else {
              console.log("@@@@@ Error " + response.getError()[0].message);
              helper.createToast(component, event, response.getError()[0].message);
            }
          });
          $A.enqueueAction(action);
        }
      } catch (err) {
        console.log(err.message);
      }
    }
  });