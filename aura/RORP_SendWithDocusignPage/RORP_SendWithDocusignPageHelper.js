({
    getDocumentData: function(component, event, helper) {
      var action = component.get("c.fetchDocusignData");
      var requestWrap = {
        srId: component.get("v.srId")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          component.set("v.respWrap", respWrap);
          helper.generateDocOptions(component, event, helper, respWrap);
          helper.generateAmendOptions(component, event, helper, respWrap);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    },
  
    GetSendEnvelopeLink: function(component, event, helper) {
      component.set("v.showSpinner", true);
      var actionItemId = component.get("v.respWrap.relActionIteam.Id");
      if (!actionItemId) {
        component.set(
          "v.customErrorMessage",
          "No action iteam for entity signing found"
        );
        return;
      }
      var action = component.get("c.generateSendEnvelopeLink");
      var requestWrap = {
        docIds: component.get("v.selectedDocument"),
        amendList: component.get("v.selectedRecipientList"),
        srId: component.get("v.srId"),
        stepId: actionItemId
      };
        console.log('requestWrapParam');
        console.log(requestWrap);
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
      
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          component.set(
            "v.envelopeId",
            respWrap.createdEnvilope.docuSignId.value
          );
          helper.getPreview(component, event, helper);
          component.set("v.showSpinner", false);
        } else {
          component.set("v.showSpinner", false);
          component.set("v.isError", true);
          component.set("v.errorMessage", response.getError()[0].message);
  
          console.log("@@@@@ Error 1" + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    },
  
    updateSentDate: function(component, event, helper) {
      component.set("v.showSpinner", true);
  
      var action = component.get("c.updateEnvelopeSentDate");
      var requestWrap = {
        envelopeID: component.get("v.envelopeIdToUpdate"),
        stepId: component.get("v.stepId")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          helper.createEnvelopeStatusHelper(component, event, helper);
          component.set("v.showSpinner", false);
        } else {
          component.set("v.showSpinner", false);
          console.log("@@@@@ Error 2" + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    },
  
    createEnvelopeStatusHelper: function(component, event, helper) {
      component.set("v.showSpinner", true);
  
      var action = component.get("c.createEnvelopeStatus");
      var requestWrap = {
        srId: component.get("v.srId")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          component.set("v.showSpinner", false);
          component.set("v.customErrorMessage", "Email sent successfully.");
        } else {
          component.set("v.showSpinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    },
    /*  handelSendEnvelope: function(component, event, helper) {
      component.set("v.showSpinner", true);
      var action = component.get("c.sendEnvelope");
      
      action.setParams({ contactId: "try" });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          component.set("v.respWrap", respWrap);
          component.set(
            "v.envelopeId",
            respWrap.createdEnvilope.docuSignId.value
          );
          helper.getPreview(component, event, helper);
        } else {
          component.set("v.showSpinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    }, */
  
    generateDocOptions: function(component, event, helper, respWrap) {
      try {
        var docList = respWrap.relDocList;
        var docSelect = [];
        for (const document of docList) {
          docSelect.push({
            label: document.Name,
            /* value:
              document.ContentDocumentLinks[0].ContentDocument
                .LatestPublishedVersionId */
            value: document.Id
          });
        }
        component.set("v.DocumentOptions", docSelect);
        console.log(docSelect);
      } catch (error) {
        console.log(error.message);
      }
    },
  
    generateAmendOptions: function(component, event, helper, respWrap) {
      try {
        var amendList = respWrap.relAmendList;
        var amendMultiselect = [];
        for (const amend of amendList) {
          amendMultiselect.push({
            label: amend.Given_Name__c,
            value: amend.Id
          });
        }
        component.set("v.RecipientOptions", amendMultiselect);
      } catch (error) {
        console.log(error.message);
      }
    },
  
    showToast: function(component, event, helper, msg, type) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "dismissible",
        message: msg,
        type: type
      });
      toastEvent.fire();
    },
  
    getPreview: function(component, event, helper) {
      component.set("v.showSpinner", true);
      var action = component.get("c.sendEnvelopeTagging");
  
      action.setParams({
        myEnvelopeID: component.get("v.envelopeId"),
        stepId: component.get("v.respWrap.relActionIteam.Id"),
        srId: component.get("v.srId")
      });
      console.log(component.get("v.envelopeId"));
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          component.set("v.iframeUrl", respWrap.docuSignTaggingURLString);
          component.set("v.showSpinner", false);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
          component.set("v.showSpinner", false);
        }
      });
      $A.enqueueAction(action);
    }
  });