({
  loadDataInit: function(component, event, helper) {
    var coulmnMDObject = component.get("v.columnsToDisplay");
    var fieldsToQuery = [];

    for (var cloumnData of coulmnMDObject) {
      if (cloumnData.fieldName.includes("__r_")) {
        fieldsToQuery.push(cloumnData.fieldName.replace("__r_", "__r."));
      } else if (cloumnData.fieldName.includes("RecordType_")) {
        fieldsToQuery.push(
          cloumnData.fieldName.replace("RecordType_", "RecordType.")
        );
      } else if (cloumnData.fieldName.includes("CreatedBy_")) {
        fieldsToQuery.push(
          cloumnData.fieldName.replace("CreatedBy_", "CreatedBy.")
        );
      }
       else if (cloumnData.fieldName.includes("Contact_")) {
       
        fieldsToQuery.push(
          cloumnData.fieldName.replace("Contact_", "Contact.")
        );
      }
          else {
        fieldsToQuery.push(cloumnData.fieldName);
      }
    }
    var fieldsQuery = fieldsToQuery.join(",");

    var action = component.get("c.fetchRelatedRecords");
    var requestWrap = {
      parentRelationFieldname: component.get("v.parentRelationFieldname"),
      fieldsToQuery: fieldsQuery,
      parentRecordId: component.get("v.parentRecordId"),
      objectToQueryFrom: component.get("v.ChildObjectAPIName"),
      initialRows: component.get("v.initialRows"),
      additionalFilters: component.get("v.additionalFilters")
    };

    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        var respWrap = response.getReturnValue();
        component.set("v.totalRows", respWrap.relatedRecList.length);
        try {
          respWrap.relatedRecList.forEach(row => {
            for (const col in row) {
              const curCol = row[col];
              if (typeof curCol === "object") {
                const newVal = curCol.Id ? "/" + curCol.Id : null;
                helper.flattenStructure(row, col + "_", curCol);
                if (newVal === null) {
                  delete row[col];
                } else {
                  row[col] = newVal;
                }
              }
            }
          });
        } catch (err) {
          console.log(err.message);
        }

        component.set("v.sObjectList", respWrap.relatedRecList);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  flattenStructure: function(topObject, prefix, toBeFlattened) {
    for (const prop in toBeFlattened) {
      const curVal = toBeFlattened[prop];
      if (typeof curVal === "object") {
        flattenStructure(topObject, prefix + prop + "_", curVal);
      } else {
        topObject[prefix + prop] = curVal;
      }
    }
  }
});