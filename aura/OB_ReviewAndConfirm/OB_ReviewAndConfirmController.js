({
  init: function (component, event, helper) {
    component.set("v.openingDiv", "<div>");
    component.set("v.closingDiv", "</div>");

    let newURL = new URL(window.location.href).searchParams;
    var pageId = newURL.get("pageId");
    console.log("init");
    //helper.getButtonDetails(component, event, helper, pageId);
    helper.getSrFields(component, event, helper);
  },

  onChange: function (component, event, helper) {
    console.log(event.getSource().get("v.value"));
  },

  submit: function (component, event, helper) {
    console.log('submit');
    var allValid = component
      .find("fieldId")
      .reduce(function (validSoFar, inputCmp) {
        return validSoFar && !inputCmp.get("v.validity").valueMissing;
      }, true);
    if (allValid) {
      helper.updateSR(component, event, helper);
    } else {
      component.set("v.errorMessage", "Please Accept the declaration");
      component.set("v.showError", true);
    }
  },
  getSelected: function (component, event, helper) {
    /*
      console.log('getSelected');
      component.set("v.hasModalOpen" , true);
      component.set("v.selectedDocumentId" , event.currentTarget.getAttribute("data-Id")); 
      */

    var contentId = event.currentTarget.getAttribute("data-Id");
    console.log(contentId);
    console.log($A.get("e.lightning:openFiles"));
    var openFileEvent = $A.get("e.lightning:openFiles");
    if (openFileEvent) {
      $A.get("e.lightning:openFiles").fire({
        recordIds: [contentId]
      });
    } else {
      var contentRecordPageLink =
        "/lightning/r/ContentDocument/" + contentId + "/view";
      window.open(contentRecordPageLink, "_blank");

      /* window.location.href =
        "/lightning/r/ContentDocument/" + contentId + "/view"; */
    }
  },
  closeModal: function (component, event, helper) {
    // for Close Model, set the "hasModalOpen" attribute to "FALSE"
    component.set("v.hasModalOpen", false);
    component.set("v.selectedDocumentId", null);
  },
  handleButtonAct: function (component, event, helper) {
    console.log('handleButtonAct');	
    try {
      let formValidity = helper.formValidator(
        component,
        event,
        helper,
        "fieldId"
      );
      console.log(formValidity);

      /*
          var allValid = component.find('fieldId');
          var v = allValid.get("v.checked")
          let newURL = new URL(window.location.href).searchParams;
          var srids = newURL.get('srId');
          console.log(v);*/
      if (formValidity) {
        // var isValid = helper.runAllValidations(component, event, helper, srids);
        // if(isValid){
        try {
          var srObj = component.get("v.srObj");
          let newURL = new URL(window.location.href).searchParams;
          var srID = newURL.get("srId");
          var pageflowId = newURL.get("flowId");
          var pageId = newURL.get("pageId");
          var buttonId = event.target.id;
          //console.log('srObj-->'+JSON.stringify(srObj));  
          var action = component.get("c.getButtonAction");
          action.setParams({
            srObj: srObj,
            SRID: srID,
            pageId: pageId,
            flowId: pageflowId
          });
          action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("state " + state);

            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var responseObj = response.getReturnValue();
                console.log(responseObj);
                if (responseObj.haserror) {
                    /*var errorMess = responseObj.errormessage;
                    console.log('errorMess: '+errorMess);
                    var amedaction = component.get("c.fetchAmendment");                    
                    amedaction.setParams({
                        srId: srID
                    });                    
                    amedaction.setCallback(this, function (response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            if(response.getReturnValue() == true){
                                
                            }
                            else{
                                errorMess = errorMess + "Please Add Beneficial Owner"
                            }                            
                            console.log('res: '+response.getReturnValue());
                            helper.showToast(
                                component,
                                event,
                                helper,
                                errorMess,
                                "error"
                            );
                        } else {
                            console.log("@@@@@ Error " + response.getError()[0].message);
                        }
                    });
                    $A.enqueueAction(amedaction);   
                    */helper.showToast(
                                component,
                                event,
                                helper,
                                responseObj.errormessage,
                                "error"
                            );
              } else {
                  /*var amedaction1 = component.get("c.fetchAmendment");
                  amedaction1.setParams({
                        srId: srID
                    }); 
                  amedaction1.setCallback(this, function (response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            if(response.getReturnValue() == true){
                                window.open(responseObj.pageActionName, "_self");
                            }
                            else{
                                errorMess = errorMess + "Please Add Beneficial Owner"
                            }                            
                            console.log('res: '+response.getReturnValue());
                            helper.showToast(
                                component,
                                event,
                                helper,
                                errorMess,
                                "error"
                            );
                        } else {
                            console.log("@@@@@ Error " + response.getError()[0].message);
                        }
                    });
                    $A.enqueueAction(amedaction1); */
                window.open(responseObj.pageActionName, "_self");
              }
            } else {
              console.log("@@@@@ Error " + response.getError()[0].message);
            }
          });
          $A.enqueueAction(action);
        } catch (err) {
          console.log(err.message);
        }
      } else {
        // component.set("v.errorMessage" , 'Please Accept the declaration');
        //  component.set("v.showError" , true);
        helper.showToast(
          component,
          event,
          helper,
          "Please Accept the declaration",
          "error"
        );
      }
    } catch (error) {
      console.log(error.message);
    }
  },
  handleAutoClose: function (component, event, helper) {
    try {
      console.log("in js");

      let newURL = new URL(window.location.href).searchParams;
      var srID = newURL.get("srId");
      var action = component.get("c.autoCloseCustomerStep");

      action.setParams({
        srId: srID
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("apex call succsess");
          const respWrap = response.getReturnValue();

          console.log(response.getReturnValue());
          if (!respWrap.IsSuccess) {
            helper.showToast(
              component,
              event,
              helper,
              respWrap.Message,
              "error"
            );
          } else {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              url: "/"
            });
            urlEvent.fire();
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  openPrint: function (component, event, helper) {
    window.print();
  }
});