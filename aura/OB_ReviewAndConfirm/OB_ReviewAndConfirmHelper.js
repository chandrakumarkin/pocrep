({
  getSrFields: function(component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;
      /* var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId"); */

      var pageId = component.get("v.pageId")
        ? component.get("v.pageId")
        : newURL.get("pageId");

      var pageflowId = component.get("v.flowId")
        ? component.get("v.flowId")
        : newURL.get("flowId");
      var srId = component.get("v.srId")
        ? component.get("v.srId")
        : newURL.get("srId");
      console.log(srId);

      component.set("v.flowId", pageflowId);
      component.set("v.pageId", pageId);
      component.set("v.srId", srId);

      var action = component.get("c.fetchSRObjFields");
      var requestWrap = {
        flowID: pageflowId,
        srId: srId,
        pageId: pageId
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap)
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          debugger;
          console.log(response.getReturnValue());
          const repWrap = response.getReturnValue();
          component.set("v.respWrapp", repWrap);
          component.set("v.secWrapp", repWrap.secDetailWrap);
          component.set(
            "v.difcActivityLable",
            repWrap.licenseActs.activityLable
          );
          component.set("v.isBackEndUser", repWrap.isBackendUser);
          component.set("v.compoanyNameWrapp", repWrap.companyNameWrap);
          //component.set("v.pageWrapp",response.getReturnValue().pageWrapList);
		  console.log("----------repWrap.amendWrap"+repWrap.amendWrap);
          component.set("v.amndWrapp", repWrap.amendWrap);
            console.log("shareholderwrap"+repWrap.shareholderAmendmentList);
          // console.log(response.getReturnValue().amendWrap);
          component.set("v.docWrapp", repWrap.docWrap);
          component.set("v.srObj", repWrap.srObj);
          //component.set("v.qualifedAppWrapp", repWrap.qualPurpsFieldWrap);
          component.set("v.commandButton", repWrap.ButtonSection);
          component.set("v.showDecAndButton", repWrap.ShowDecAndButton);
          component.set("v.showButton", repWrap.showButton);

          component.set("v.licenseActWrapp", repWrap.licenseActs);
          document
            .getElementById("difc-review-form")
            .classList.remove("difc-display-none");
          //component.set("v.pgWrapp",response.getReturnValue().pageNameList);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
      console.log(error);
    }
  },

  getButtonDetails: function(component, event, helper, pgId) {
    var action = component.get("c.retrieveCompanyWrapper");
    action.setParams({
      pageId: pgId
    });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseResult = response.getReturnValue();
        component.set("v.commandButton", responseResult.ButtonSection);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  runAllValidations: function(component, event, helper, srIds) {
    var action = component.get("c.updateSRObj");
    var requestWrap = {
      srId: srIds
    };
  },

  updateSR: function(component, event, helper) {
    var action = component.get("c.updateSRObj");

    var requestWrap = {
      srId: component.get("v.srId")
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    });
    console.log(JSON.stringify(requestWrap));
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          url: "/"
        });
        urlEvent.fire();
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  showToast: function(component, event, helper, msg, type) {
    // Use \n for line breake in string
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "sticky",
      message: msg,
      type: type
    });
    toastEvent.fire();
  },

  formValidator: function(component, event, helper, fieldsToValidate) {
    let fields = component.find(fieldsToValidate);

    if (fields == undefined) {
      return true;
    } else if (!Array.isArray(fields)) {
      if (fields.get("v.checked") != undefined) {
        return fields.get("v.checked");
      } else {
        return false;
      }
    } else if (Array.isArray(fields)) {
      for (const field of fields) {
        if (field.get("v.checked") != undefined) {
          if (!field.get("v.checked")) {
            return false;
          }
        } else {
          return false;
        }
      }
      return true;
    }
  },

  navToDetailPage: function(component, event, helper, fieldsToValidate) {
    var state = res.getState();
    if (state === "SUCCESS") {
      var oppId = res.getReturnValue();
      var navEvt = $A.get("e.force:navigateToSObject");
      navEvt.setParams({
        recordId: oppId
      });
      navEvt.fire();
    }
  }
});