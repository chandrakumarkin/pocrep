({
  fetchAccounts: function (component, event, helper) {
    var action = component.get("c.getAccountDetail");
    action.setCallback(this, function (response) {
      // Getting the response state
      var state = response.getState();
      // Check if response state is success
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        var userRole = JSON.stringify(respWrap.userObj.Community_User_Role__c);
      
          if(userRole.includes('Employee')){
                component.set("v.EmployeeName", 'Employee');
          }
          
        component.set("v.recordId", respWrap.accountId);
        component.set("v.respWrap", respWrap);
          
        
          
        
          
        helper.fetchMetaDataForFieldsDisplay(
          component,
          event,
          helper,
          respWrap
        );
        helper.convertToJSON(
          component,
          event,
          helper,
          respWrap.visaQuotaInfoJSON
        );
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error " + response.getError()[0].stackTrace);
      }
    });

    $A.enqueueAction(action);
  },

  convertToJSON: function (component, event, helper, listToBeConverted) {
    var visaQuotaObjList = [];
    var mapOfChildNodes = [];

    var listofLableToCovertToTable = [
      "Number of Employees - Current",
      "Number of Employees - In Process",
      "Details of Accounts and Visa Quota Utilized from Hosting Company",
      "Details of Hosting Company",
    ];

    var listofLableToAddNewItem = [
      "Number of Employees - Current",
      "Number of Employees - In Process",
    ];

    try {
      console.log(listToBeConverted);
      for (var i = 0; i < listToBeConverted.length; i++) {
        if (
          listToBeConverted[i].search(
            "Details of Accounts and Visa Quota Utilized from Hosting Company"
          ) > -1 ||
          listToBeConverted[i].search("Details of Hosting Company") > -1
        ) {
          break;
        }
        listToBeConverted[i] = listToBeConverted[i].replace(
          /Pipeline/gi,
          "In Process"
        );
        listToBeConverted[i] = listToBeConverted[i].replace(
          /Seconded/gi,
          "Non Sponsored"
        );

        var innerItemList = listToBeConverted[i].replace(/\:/, "&").split("&");

        if (innerItemList[0] && !innerItemList[1].includes("|")) {
          visaQuotaObjList.push({
            label: innerItemList[0],
            value: innerItemList[1],
          });
        } else if (innerItemList[0] && innerItemList[1].includes("|")) {
          var innerSplit = innerItemList[1].split("|");
          visaQuotaObjList.push({
            label: innerItemList[0],
            value: innerSplit[0],
          });
          var childInnerSplit = innerSplit[1].split(":");
          visaQuotaObjList.push({
            label: childInnerSplit[0],
            value: childInnerSplit[1],
          });
        }
        if (listofLableToAddNewItem.includes(innerItemList[0].trim())) {
          visaQuotaObjList.push({
            label: "    " + innerItemList[0].split("-")[1].trim() + "- Total",
            value: innerItemList[1],
          });
        }
      }

      for (let i = 0; i < visaQuotaObjList.length; i++) {
        if (
          listofLableToCovertToTable.includes(visaQuotaObjList[i].label.trim())
        ) {
          var mappObj = {
            parentNode: visaQuotaObjList[i].label,
            parentNodePosition: i,
            relatedChildNode: [],
          };
          for (let j = i + 1; j < visaQuotaObjList.length; j++) {
            if (visaQuotaObjList[j].label.startsWith("    ")) {
              mappObj.relatedChildNode.push(j);
            } else {
              break;
            }
          }
          mapOfChildNodes.push(mappObj);
        }
      }

      for (var mapItem of mapOfChildNodes) {
        var ListOfChildNodesObj = [];
        for (var childNodes of mapItem.relatedChildNode) {
          ListOfChildNodesObj.push({
            label: visaQuotaObjList[childNodes].label,
            value: visaQuotaObjList[childNodes].value,
          });
        }
        visaQuotaObjList[
          mapItem.parentNodePosition
        ].value = ListOfChildNodesObj;
        visaQuotaObjList[mapItem.parentNodePosition].type = "table";
      }

      //deleting child nodes
      visaQuotaObjList = visaQuotaObjList.filter(
        (visaQuotaObj) => !visaQuotaObj.label.startsWith("    ")
      );

      component.set("v.visaQuota", visaQuotaObjList);

      console.log(visaQuotaObjList);
    } catch (error) {
      console.log(error.message);
    }

    return;
  },

  fetchMetaDataForFieldsDisplay: function (component, event, helper, respWrap) {
    /*  var fieldSet = [
      {
        fieldList: [
          "Name",
          "Legal_Entity_Type__c",
          "Former_Name__c",
          "Arabic_Name__c",
          "Trade_Name__c",
          "Former_Name_Arabic__c",
          "Former_Trading_Name__c",
          "Trading_Name_Arabic__c",
          "Company_Type__c",
          "Former_Trading_Name_Arabic__c",
          "Active_License__c",
          "Tax_Registration_Number_TRN__c",
          "Next_Renewal_Date__c",
          "Legal_Type_of_Entity__c",
          "Index_Card__c",
          "OB_Sector_Classification__c",
          "Index_Card_Status__c",
          "Index_Card_Expiry_Date__c",
          "Bank_Name__c",
          "OB_Franchise__c",
          "Has_open_AOR__c",
          "Registered_Address__c"
        ]
      },
      {
        sectionheader: "Registration Details",
        fieldList: [
          "Registration_License_No__c",
          "ROC_Status__c",
          "ROC_reg_incorp_Date__c",
          "Liquidated_Date__c",
          "Place_of_Registration__c",
          "Pending_Dissolution_Date__c"
        ]
      },
      {
        sectionheader: "Address Information",
        fieldList: [
          "Sys_Office__c",
          "Phone",
          "Office__c",
          "Website",
          "Building_Name__c",
          "Postal_Code__c",
          "Street__c",
          "Country__c",
          "PO_Box__c",
          "Hosting_Company__c",
          "City__c",
          "Emirate__c"
        ]
      },
      {
        sectionheader: "Government Services Details",
        fieldList: [
          "Calculated_Visa_Quota__c",
          "Short_Term_Visit_Visa_Service__c",
          "PSA_Deposit__c",
          "Long_Term_Visit_Visa_Service__c",
          "PSA_Guarantee__c",
          "PSA_Available__c"
        ]
      },
      {
        sectionheader: "Aditional Legal Structure Details",
        fieldList: [
          "Sub_Legal_Type_of_Entity__c",
          "Qualifying_Type__c",
          "Qualifying_Purpose_Type__c"
        ]
      },
      {
        sectionheader: "DFSA License / DNFBP Details (if applicable)",
        fieldList: [
          "DNFBP__c",
          "DFSA_Approval__c",
          "DFSA_Lic_InPrAppDate__c",
          "DFSA_Withdrawn_Date__c"
        ]
      },
      {
        sectionheader: "Details of Share Capital",
        fieldList: [
          "Currency__c",
          "Issued_Share_Capital_AccShare__c",
          "No_of_Issued_Shares__c"
        ]
      },
      {
        sectionheader: "Car Parking Details",
        fieldList: ["Total_Reserve_Parkingc__c"]
      },
      {
        sectionheader: "Additional Information",
        fieldList: ["Name_Identical__c"]
      },
      {
        sectionheader: "System Information",
        fieldList: [
          "Has_Permit_1__c",
          "Last_PR_Push__c",
          "Has_Permit_2__c",
          "Executive_Company__c",
          "Has_Permit_3__c",
          "LastModifiedById",
          "RecordTypeId",
          "License_Activity__c",
          "Principal_User__c",
          "Unit_Number__c"
        ]
      }
    ];
    component.set("v.fieldSet", fieldSet); */

    var relatedRecordListSet = [
      /* {
        relatedTableHeader: "Receipts",
        parentRelationFieldname: "Customer__c",
        ChildObjectAPIName: "Receipt__c",
        columnsToDisplay: [
          { label: "Receipt Number", fieldName: "Name", type: "text" },
          { label: "Amount", fieldName: "Amount__c", type: "currency" },
          { label: "Receipt Type", fieldName: "Receipt_Type__c", type: "text" },
          { label: "Status", fieldName: "Status__c", type: "text" },
          {
            label: "Transaction Date",
            fieldName: "Transaction_Date__c",
            type: "date"
          }
        ]
      }, */
      {
        relatedTableHeader: "Account Share Details",
        parentRelationFieldname: "Account__c",
        ChildObjectAPIName: "Account_Share_Detail__c",
        columnsToDisplay: [
          { label: "class", fieldName: "Name", type: "text" },
          {
            label: "Nominal Value",
            fieldName: "Nominal_Value__c",
            type: "number",
          },
          {
            label: "No. of shares per class",
            fieldName: "No_of_shares_per_class__c",
            type: "number",
          },
          {
            label: "Status",
            fieldName: "Status__c",
            type: "text",
          },
        ],
      },
      {
        relatedTableHeader: "Shareholder Details",
        parentRelationFieldname: "Account__c",
        ChildObjectAPIName: "Shareholder_Detail__c",
        columnsToDisplay: [
          { label: "Shareholder No", fieldName: "Name", type: "text" },
          {
            label: "Shareholder Account",
            fieldName: "Shareholder_Account__r_Name",
            type: "text",
          },
          {
            label: "Shareholder Contact",
            fieldName: "Shareholder_Contact__r_Name",
            type: "text",
          },
          {
            label: "No of Shares",
            fieldName: "No_of_Shares__c",
            type: "number",
          },
          {
            label: "Total Nominal Value",
            fieldName: "Total_Nominal_Val__c",
            type: "number",
          },
          { label: "class", fieldName: "Account_Share__r_Name", type: "text" },
          { label: "Status", fieldName: "Status__c", type: "text" },
        ],
      },
      {
        relatedTableHeader: "Place of records and registers",
        parentRelationFieldname: "Account__c",
        ChildObjectAPIName: "Records_kept__c",
        columnsToDisplay: [
          { label: "Records Kept No", fieldName: "Name", type: "text" },
          {
            label: "Address To Print",
            fieldName: "Address_to_Print__c",
            type: "text",
          },
          /* {
            label: "City/Town",
            fieldName: "City__c",
            type: "text"
          },
          {
            label: "Building Name",
            fieldName: "Building_Name__c",
            type: "text"
          },
          {
            label: "Country",
            fieldName: "Country__c",
            type: "text"
          }, */
          {
            label: "Status",
            fieldName: "Status__c",
            type: "text",
          },
          {
            label: "Service Request",
            fieldName: "Service_Request__r_Name",
            type: "text",
          },
        ],
      },

      {
        relatedTableHeader: "Cases",
        parentRelationFieldname: "AccountID",
        ChildObjectAPIName: "case",
        columnsToDisplay: [
          { label: "Case Number", fieldName: "CaseNumber", type: "text" },
          {
            label: "contact",
            fieldName: "contactId",
            type: "text",
          },
          {
            label: "Priority",
            fieldName: "Priority",
            type: "text",
          },
          {
            label: "	Date/Time Opened",
            fieldName: "CreatedDate",
            type: "date",
          },
        ],
      },
      /*  {
        relatedTableHeader: "Company Names",
        parentRelationFieldname: "OB_Related_To__c",
        ChildObjectAPIName: "Company_Name__c",
        columnsToDisplay: [
          { label: "Company Name", fieldName: "Name", type: "text" }
        ]
      }, */
       {
        relatedTableHeader: "Activities",
        parentRelationFieldname: "Account__c",
        ChildObjectAPIName: "License_Activity__c",
        columnsToDisplay: [
          { label: "Name", fieldName: "Activity__r_Name", type: "text" },
          {
            label: "Activity Name Arabic",
            fieldName: "Activity__r_Activity_Name_Arabic__c",
            type: "text",
          },
            {
            label: "End Date",
            fieldName: "End_Date__c",
            type: "text",
          },
        ],
      },
      /*  {
        relatedTableHeader: "Company Relationships",
        parentRelationFieldname: "Subject_Account__c",
        ChildObjectAPIName: "Relationship__c",
        additionalFilters: "AND Relationship_Group__c = 'ROC'",
        columnsToDisplay: [
          {
            label: "Body Corporate Name",
            fieldName: "Object_Account__r_Name",
            type: "text"
          },
          {
            label: "Individual Name",
            fieldName: "Object_Contact__r_Name",
            type: "text"
          },
          {
            label: "Relationship Type",
            fieldName: "Relationship_Type_formula__c",
            type: "text"
          },
          { label: "Active", fieldName: "Active__c", type: "boolean" },
          {
            label: "Start Date",
            fieldName: "Start_Date__c",
            type: "date"
          },
          {
            label: "End Date",
            fieldName: "End_Date__c",
            type: "date"
          }
        ]
      },
      {
        relatedTableHeader: "Employee Relationships",
        parentRelationFieldname: "Subject_Account__c",
        ChildObjectAPIName: "Relationship__c",
        additionalFilters:
          "AND (End_Date__c > TODAY OR(Active__c = TRUE AND Relationship_Type__c = 'Has DIFC Sponsored Employee')) AND Relationship_Group__c = 'GS'",
        columnsToDisplay: [
          {
            label: "Individual Name",
            fieldName: "Object_Contact__r_Name",
            type: "text"
          },
          {
            label: "Relationship Type",
            fieldName: "Relationship_Type_formula__c",
            type: "text"
          },
          {
            label: "Active",
            fieldName: "Active__c",
            type: "boolean"
          },
          {
            label: "Visa Expiry Date",
            fieldName: "Object_Contact__r_Visa_Expiry_Date__c",
            type: "date"
          },
          {
            label: "Is Absconder?",
            fieldName: "Object_Contact__r_Is_Absconder__c",
            type: "boolean"
          },
          {
            label: "Is Violator?",
            fieldName: "Object_Contact__r_Is_Violator__c",
            type: "boolean"
          }
        ]
      } */
    ];

    console.log("heeer");
    var isCp = respWrap.userObj.Contact.Account.Is_Commercial_Permission__c;
    console.log(isCp);
    if (isCp == "Yes") {
      relatedRecordListSet = relatedRecordListSet.filter(
        (metadata) => metadata.relatedTableHeader == "Activities"
      );
    }

    component.set("v.relatedRecordListSet", relatedRecordListSet);

    var dataProtectionField = [
      "Name",
      "Active__c",
      "Date_From__c",
      "Account__c",
      "Date_To__c",
      "License__c",
      "Transferring_Personal_Data_Outside_DIFC__c",
      "Permit_Type__c",
      "Purpose_of_Processing_Data__c",
      "Other_Purpose_of_Processing_Data__c",
      "Data_Subjects_Personal_Data__c",
      "Other_Data_Subjects_Personal_Data__c",
      "Class_of_Personal_Data__c",
      "In_Accordance_with_Article_11__c",
      "Recognized_Jurisdictions__c",
      "In_accordance_with_Article_10_1__c",
      "Description_of_Safeguards_SD__c",
      "In_Accordance_with_Article_12_1a__c",
      "Nature_of_Sensitive_Personal_Data__c",
      "How_will_Senstive_Personal_Data_Process__c",
      "Personal_Data_Processing_Procedure__c",
      "Other_Data_Subjects_Senstive_Data__c",
      "Data_Subjects_Senstive_Data__c",
      "Other_Purpose_of_Senstive_Data__c",
      "Purpose_of_Processing_Senstive_Data__c",
      "Provisions_of_Article_12__c",
      "In_Accordance_with_Article_12_1bj__c",
    ];

    component.set("v.dataProtectionFieldSet", dataProtectionField);
  },
});