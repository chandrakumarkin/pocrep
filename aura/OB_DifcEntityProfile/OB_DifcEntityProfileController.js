({
  callServer: function (component, event, helper) {
    helper.fetchAccounts(component, event, helper);
    //helper.fetchMetaDataForFieldsDisplay(component, event, helper);
  },

  backtohome: function (component, event, helper) {
    console.log("---component--");
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      url: "/",
    });

    urlEvent.fire();
    helper.backtohomeHelper(component, event);
  },
  print: function (component, event, helper) {
    window.print();
  },
});