({
	sendEmail : function(component, event, helper) {
           var toggleText = component.find("text");
           $A.util.toggleClass(toggleText, "toggle"); 
           var toggleText1 = component.find("text1");
           $A.util.toggleClass(toggleText1, "toggle");
          // alert('start');
           var relationshipType = component.find('relationshipType').get('v.value');	
         //  alert('relationshipType=='+relationshipType);     
           var actionNext=component.get("c.createEmailLink");
           var recordId = component.get("v.recordId");
           var OpportunityRec = component.get("v.Opportunity");
          // alert('Opportunity Id'+OpportunityRec.Id);
           var accountId = OpportunityRec.AccountId;
         //  alert('accountId== '+accountId);
           var StageNamerec = OpportunityRec.StageName;
         //  alert('stageName'+StageNamerec);
           var stageSteprec = OpportunityRec.Stage_Steps__c;
          // alert('stageSteprec'+stageSteprec);  
           var RMEmailrec = OpportunityRec.OwnerId;
         //  alert('RMEmail=='+RMEmailrec);
           actionNext.setParams({"relatedToId" : recordId,"accountId" :accountId, "stageName" : StageNamerec,"stageStep" : stageSteprec,"pointOfOrigin" : relationshipType,"emailTemplateCode" : recordId.OwnerId,"RMEmail" : RMEmailrec});
           actionNext.setCallback(this, function(response) {
            // alert('Hello=='+response.getReturnValue());
             var state = response.getState();            
           //  alert('state' +state);
             if (component.isValid() && state === "SUCCESS") {
               //  alert(actionNext.toString().indexOf('Error'));
				if(actionNext.toString().indexOf('Error') != -1){
					alert(actionNext);
				}
				else {
                 var tempVars = response.getReturnValue();
                    if(tempVars.indexOf('EmailAuthor')>0){  
                 var urlEvent = $A.get("e.force:navigateToURL");
                 urlEvent.setParams({
                 "url":tempVars
                });
               urlEvent.fire(); 
                    }
               else{    
                   component.set("v.message", tempVars);
                   }
				
             }			
            }
                else {
                    alert('Something went wrong while processing your request. Please try again later.');
                }
        });
         $A.enqueueAction(actionNext);
   },
    
    removeCSS: function(cmp, event) {
      $A.get("e.force:closeQuickAction").fire();  
    },   
    
    doInit : function(component, event,helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getopportunity");
       action.setParams({"relatedToId" : recordId});
        action.setCallback(this, function(a) {
            console.log('Account Id'+JSON.stringify(a.getReturnValue()));
            component.set("v.Opportunity", a.getReturnValue());
          //  alert('response'+JSON.stringify(a.getReturnValue()));
        });
        $A.enqueueAction(action);
    }   
})