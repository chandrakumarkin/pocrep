({
    saveRequests: function (cmp, event, helper) {

        var allValid = cmp
            .find('field')
            .reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();

                return validSoFar && inputCmp
                    .get('v.validity')
                    .valid;
            }, true);

        if (!allValid) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'All Required Fields Should Be Populated!');
            return;
        }
        if(cmp.get("v.isNewRenewal")){
        var allValid1 = cmp
            .find("Duration")
            .get("v.value");

        if (allValid1 === undefined || allValid1 === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Duration field cannot be blank');
            return;
        }
        }

        var allValid1 = cmp
            .find("Title")
            .get("v.value");

        if (allValid1 === undefined || allValid1 === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Title field cannot be blank');
            return;
        }
        if(cmp.get("v.courierNeeded")){
        var allValid12 = cmp
            .find("field113")
            .get("v.checked");

        if (!allValid12) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select undertaking statement');
            return;
        }
        }

        if (cmp.find("nationality").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select Nationality');
            return;
        }

        if (cmp.find("Probation") !== undefined && cmp.find("Probation").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select Probation Period');
            return;
        }

        if (cmp.find("Reason") !== undefined && cmp.find("Reason").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select Will this individual be enrolled into the DIFC Emplotment Workplace Savings');
            return;
        }

        if (cmp.find("Wage")!== undefined && cmp.find("Wage").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Type of Wage Payment');
            return;
        }

        if (cmp.find("enrolment") !== undefined && cmp.find("enrolment").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select Reason for non enrolment');
            return;
        }


        if (cmp.find("occup").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select Occupation');
            return;
        }

        if (cmp.find("emirates") !== undefined && cmp.find("emirates").get("v.value") === '') {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Please Select Emirates');
            return;
        }

        if (cmp.get("v.dateValidationError")) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Passport Expiry must be in future');
            return;
        }

        if (cmp.get("v.dateValidationError1")) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Passport Issue  must not be in future');
            return;
        }

        if (cmp.get("v.dateValidationError2")) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Date od Birth must not be in future');
            return;
        }

        if (cmp.get("v.dateValidationError3")) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Visa Expiry must be in future');
            return;
        }

        if (cmp.get("v.mobileValidation1")) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Phone Number must start with +971');
            return;
        }

        if (cmp.get("v.mobileValidation2")) {
            cmp.set("v.isError", true);
            setTimeout(function () {
                cmp.set("v.isError", false);
            }, 4500);
            cmp.set("v.successmsg", 'Phone Number must start with +971');
            return;
        }

        cmp.set("v.Spinner", true);
        var action = cmp.get("c.saveRequest");
        action.setParams({newServiceRequest: cmp.get("v.serviceRqst")});

        action.setCallback(this, function (a) {
            var state = a.getState();

            if (state == "SUCCESS") {
                if (a.getReturnValue().startsWith('Error')) {
                    cmp.set("v.isError", true);
                    setTimeout(function () {
                        cmp.set("v.isError", false);
                    }, 2000);
                    cmp.set("v.successmsg", JSON.stringify(a.getReturnValue()));
                    cmp.set("v.Spinner", false);
                } else {
                    //&nooverride=1&RecordType=0123N000001SPY0QAO
                    cmp.set("v.issaved", true);
                    window.open(
                        a.getReturnValue() + '?isSR=GS',
                        "_self"
                    );   
                }
            } else {
                cmp.set("v.Spinner", false);

            }
        });
        $A.enqueueAction(action);

    },

    // this function automatic call by aura:waiting event
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner
        component.set("v.Spinner", true);
    },

    lookupchange: function (component, event, helper) {
        component.set("v.selectedLookId", event.getSource().get("v.value"));
        component.set("v.islookup", "true");
    },

    AddLookUp: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Occupation_GS__c",
            component.get("v.selectedLookUpRecord").Name
        );
        component
            .find("occup")
            .set("v.value", component.get("v.selectedLookUpRecord").Name);
        component.set("v.islookup", "false");
    },

    // this function automatic call by aura:doneWaiting event
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner
        component.set("v.Spinner", false);
    },

    openSR: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner
       // window.open('/customers/' + component.get("v.accountRecord.Id"), "_self");
        window.open('/customers/'+component.get("v.accountRecord.Id"),"_self");
    },

    doinits: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner
        if (component.get("v.recordIds") !== '') {
            component.set("v.isServiceTypeNew", true);
            helper.doInit(component, event, helper);
        }

    },

    changeServiceType: function (component, event, helper) {
      
        component.set("v.serviceTypeVal",component.find("ServiceType").get("v.value"));

         if(component.find("ServiceType").get("v.value") =='New'){
            component.set("v.serviceRqst.Service_Category__c",'New Equity Holder Card');
            component.set("v.isServiceTypeNew",true);
            component.set("v.otherService",false);
            component.set("v.isServiceSelected",true);
            component.set("v.isNewRenewal",true);
            component.set("v.courierNeeded",true);
            component.set("v.otherServiceName",'New');
            helper.doInit(component, event, helper);
           
        }
        else{
           
            component.set("v.isServiceSelected",true);
            component.set("v.otherService",true);
            component.set("v.isServiceTypeNew",false);
            
            //if(component.find("ServiceType").get("v.value")=='Renewal'){
              component.set("v.isNewRenewal",true);  
           // }
           
            component.set("v.otherServiceSelected",component.find("ServiceType").get("v.value"));
            component.set("v.islookupOther", "true");
            component.set("v.serviceRqst.Service_Category__c",component.find("ServiceType").get("v.value"));
        }
         
    },
      closeLookUpApp: function(component, event, helper) {
        component.set("v.islookup", "false");
        component.set("v.islookupOther", "false");
        location.reload(true);
    }, 
    changevisa: function (component, event, helper) {

        component.set("v.isQuestionAnswered", true);
        

        var count = component.get("v.CurrentStep");
        component.set("v.CurrentStep", "2");

        component.set("v.serviceRqst.Current_Visa_Status__c", component.find("visaType").get("v.value"));

        component
            .find("question12")
            .set("v.value", '');

        component.set("v.isProgressbarerror", false);
        component.set("v.isYesSelected", false);
        component.set("v.issponsor", false);
    },

    removepopup: function (component, event, helper) {
        component.set("v.isQuestion", true);
    },
    
     saveotherRequests: function (cmp, event, helper) {
        cmp.set("v.Spinner", true);   
        var action = cmp.get("c.saveRequest");
            action.setParams({
                newServiceRequest : cmp.get("v.serviceRqst")
            });
           
            action.setCallback(this, function (a) {
                var state = a.getState();
                
             
                if (state == "SUCCESS") {
                    if(a.getReturnValue().startsWith('Error')){
                        cmp.set("v.isError",true);
                         setTimeout(function(){cmp.set("v.isError",false);}, 4500);
                         cmp.set("v.successmsg",JSON.stringify(a.getReturnValue())); 
                         cmp.set("v.Spinner", false);
                    }
                    else{
                         cmp.set("v.issaved", true); 
                         window.open(a.getReturnValue()+'?isSR=GS&nooverride=1&RecordType=0123N000001SPY0QAO',"_self");
                     }
                 } else {
                  
                    cmp.set("v.Spinner", false);
                 }
            });
            $A.enqueueAction(action);
     
    },

    changeques2: function (component, event, helper) {
        component.set("v.isQuestionAnswered", true);
        component.set("v.serviceRqst.Kindly_Note__c", component.find("question12").get("v.value"));
        
        component.set("v.CurrentStep", "3");
        if (component.find("question12").get("v.value") == 'No' && component.find("visaType").get("v.value") == 'Employment visa') {
            component.set("v.isProgressbarerror", true);
            component.set("v.isYesSelected", false);
        }
        if (component.find("question12").get("v.value") == 'Yes' && component.find("visaType").get("v.value") == 'Employment visa') {
            component.set("v.isProgressbarerror", false);
            component.set("v.isYesSelected", true);
            component.set("v.issponsor", true);
            component.set("v.CurrentStep", "4");
        }

        if (component.find("question12").get("v.value") == 'Yes' && component.find("visaType").get("v.value") == 'Long Term Visa') {
            component.set("v.isProgressbarerror", false);
            component.set("v.isYesSelected", true);
            component.set("v.CurrentStep", "4");
        }

        if (component.find("question12").get("v.value") == 'No' && component.find("visaType").get("v.value") == 'Long Term Visa') {
            component.set("v.isProgressbarerror", false);
            component.set("v.isYesSelected", true);
            component.set("v.issponsor", false);
            component.set("v.isNoLongTerm", true);
            component.set("v.CurrentStep", "4");
        }
    },

    getcourierSelected: function (component, event, helper) {
        if (component.find("couriersel").get("v.value") == 'Yes') {
            component.set("v.isCourierSelected", true);
        } else {
            component.set("v.isCourierSelected", false);
        }
        component.set(
            "v.serviceRqst.Would_you_like_to_opt_our_free_couri__c",
            component.find("couriersel").get("v.value")
        );
    },

    changeRegis: function (component, event, helper) {
        component.set("v.isReg", event.getSource().get('v.checked'));
        component.set("v.serviceRqst.Apt_or_Villa_No__c", '');
        component.set(
            "v.serviceRqst.Use_Registered_Address__c",
            event.getSource().get('v.checked')
        );

        var action = component.get("c.getRegistratedAccounts");
        action.setParams({accountId: component.get("v.accountNew.Id")});

        action.setCallback(this, function (a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                
                component.set("v.registiredAddress", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

  /* changeServiceType: function (component, event, helper) {
        if (component.find("ServiceType").get("v.value") == 'New') {
            component.set("v.isServiceTypeNew", true);
            component.set("v.otherService", false);
            helper.doInit(component, event, helper);
        } else {
            component.set("v.otherService", true);
            component.set("v.isServiceTypeNew", false);
        }
        // component.set("v.serviceRqst.Express_Service__c",event.getSource().get('v.c
        // hecked'));
    },*/

    changeExpress: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Express_Service__c",
            event.getSource().get('v.checked')
        );
    },

    changeOccupation: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Occupation_GS__c",
            component.find("occupation").get("v.value")
        );
    },

    getDuration: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Duration__c",
            component.find("Duration").get("v.value")
        );
    },

    getProbation : function (component, event, helper) {
        component.set(
            "v.serviceRqst.Duration_in_months__c",
            component.find("Probation").get("v.value")
        );
    },

    getReason: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Reason_for_Request__c",
            component.find("Reason").get("v.value")
        );
        if (component.find("Reason").get("v.value") == 'No') {
            component.set(
                "v.isnotEnrolled",true
            );
        } else {
            component.set(
                "v.isnotEnrolled",false
            );
        }
        
    },

    getenrolment : function (component, event, helper) {
        component.set(
            "v.serviceRqst.UID_Number__c",
            component.find("enrolment").get("v.value")
        );

        if (component.find("enrolment").get("v.value") == 'Exempted') {
            component.set(
                "v.isExempted",true
            );
        } else {
            component.set(
                "v.isExempted",false
            );
        }
        
    },

    getWagePayment : function (component, event, helper) {
        component.set(
            "v.serviceRqst.Monthly_Salary__c",
            component.find("Wage").get("v.value")
        );
    },

    getTitle: function (component, event, helper) {

        component.set("v.serviceRqst.Title__c", component.find("Title").get("v.value"));
    },

    getEmirates: function (component, event, helper) {

        component.set(
            "v.serviceRqst.Emirate__c",
            component.find("emirates").get("v.value")
        );
    },

    getUndertakingChange: function (component, event, helper) {
        component.set(
            "v.serviceRqst.Statement_of_Undertaking__c",
            component.find("field113").get("v.checked")
        );
    },

    changenationality1: function (component, event, helper) {

        component.set(
            "v.serviceRqst.Nationality_list__c",
            component.find("nationality").get("v.value")
        );
        if (component.find("nationality").get("v.value") === 'United Arab Emirates' || component.find("nationality").get("v.value") === 'Kuwait' || component.find("nationality").get("v.value") === 'Saudi Arabia' || component.find("nationality").get("v.value") === 'Oman' || component.find("nationality").get("v.value") === 'Bahrain' || component.find("nationality").get("v.value") === 'Qatar') {

            component.set("v.isGCC", true);
            component.set("v.serviceRqst.Residence_Visa_No__c", '');
            component.set("v.serviceRqst.Residence_Visa_Expiry_Date__c", '');
        } else {
            component.set("v.isGCC", false);
        }
    },
    refresh: function (component, event, helper) {
        location.reload(true);
    },
    open: function (component, event, helper) {
      //  window.open('/digitalOnboarding/s/', "_self");
        window.open('/clientportal/s/',"_self");
    },
    closeLookUp: function (component, event, helper) {
        component.set("v.islookup", "false");
        component.set("v.isExempted", "false");
    },
    
    selectedApplicant : function(component, event, helper) {
         
         component.set("v.islookupOther", "false");
         component.set("v.selectedContactId", component.get("v.selectedLookUpRecord").Id);
        if(component.get("v.selectedLookUpRecord").Id !== '' || component.get("v.selectedLookUpRecord").Id !== undefined ){
            component.set("v.isContactSel", true);
        }else{
            component.set("v.isContactSel", false);
        }
    },

    dateUpdate: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Passport_Date_of_Expiry__c") != '' && component.get("v.serviceRqst.Passport_Date_of_Expiry__c") <= todayFormattedDate) {
            component.set("v.dateValidationError", true);
        } else {
            component.set("v.dateValidationError", false);
        }
    },

    dateUpdate1: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Passport_Date_of_Issue__c") != '' && component.get("v.serviceRqst.Passport_Date_of_Issue__c") >= todayFormattedDate) {
            component.set("v.dateValidationError1", true);
        } else {
            component.set("v.dateValidationError1", false);
        }
    },

    dateUpdate2: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Date_of_Birth__c") != '' && component.get("v.serviceRqst.Date_of_Birth__c") >= todayFormattedDate) {
            component.set("v.dateValidationError2", true);
        } else {
            component.set("v.dateValidationError2", false);
        }
    },

    dateUpdate3: function (component, event, helper) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.serviceRqst.Residence_Visa_Expiry_Date__c") != '' && component.get("v.serviceRqst.Residence_Visa_Expiry_Date__c") <= todayFormattedDate) {
            component.set("v.dateValidationError3", true);
        } else {
            component.set("v.dateValidationError3", false);
        }
    },
    mobilevalidation: function (component, event, helper) {
        if (component.get("v.serviceRqst.Courier_Mobile_Number__c") != '' && !component.get("v.serviceRqst.Courier_Mobile_Number__c").startsWith("+971")) {
            component.set("v.mobileValidation1", true);
        } else {
            component.set("v.mobileValidation1", false);
        }
    },
    mobilevalidation1: function (component, event, helper) {
        if (component.get("v.serviceRqst.Courier_Cell_Phone__c") != '' && !component.get("v.serviceRqst.Courier_Cell_Phone__c").startsWith("+971")) {
            component.set("v.mobileValidation2", true);
        } else {
            component.set("v.mobileValidation2", false);
        }
    }

})