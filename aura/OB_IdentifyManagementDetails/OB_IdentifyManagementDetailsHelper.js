({
  //for file validation in helper.
  fileUploadHelper: function(cmp, event, helper) {
    //New method.
    console.log("########### fileHolder Start");

    // For file upload and validation.
    var fileUploadValid = true;
    var fileHolder = cmp.find("filekey");

    if (fileHolder && Array.isArray(fileHolder)) {
      for (var i = 0; i < fileHolder.length; i++) {
        if (fileHolder[i].get("v.requiredDocument")) {
          helper.fileUploadProcess(cmp, event, helper, fileHolder[i]);
          fileUploadValid = fileHolder[i].get("v.fileUploaded");
          //requiredText
          if (!fileUploadValid) {
            fileHolder[i].set("v.requiredText", "Please upload document.");
          }
        }
      }
    } else if (fileHolder) {
      console.log(
        "########### fileHolder content docID ",
        fileHolder.get("v.contentDocumentId")
      );
      //here
      helper.fileUploadProcess(cmp, event, helper, fileHolder);

      if (fileHolder.get("v.requiredDocument")) {
        fileUploadValid = fileHolder.get("v.fileUploaded");
        console.log("@@@@@@@@@@@ fileUploadValid ", fileUploadValid);
        if (!fileUploadValid) {
          fileHolder.set("v.requiredText", "Please upload document.");
        }
      }
    }

    console.log(" Final file upload ", fileUploadValid);
    if (!fileUploadValid) {
      helper.showToast(
        cmp,
        event,
        helper,
        "Please upload required files",
        "error"
      );
      cmp.set("v.spinner", false);
      return "file error";
    }
    console.log("########### fileHolder end");
  },

  fileUploadProcess: function(cmp, event, helper, fileHolder) {
    //New Method
    var contentDocumentId = fileHolder.get("v.contentDocumentId");
    var documentMaster = fileHolder.get("v.documentMaster");

    if (contentDocumentId && documentMaster) {
      var mapDocValues = {};
      var docMap = cmp.get("v.docMasterContentDocMap");
      for (var key in docMap) {
        mapDocValues[key] = docMap[key];
      }

      mapDocValues[documentMaster] = contentDocumentId;
      console.log(mapDocValues);
      console.log("@@@@@@ docMasterContentDocMap ");
      cmp.set("v.docMasterContentDocMap", mapDocValues);
      console.log(
        JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap")))
      );
    }
  },

  showToast: function(component, event, helper, msg, type) {
    // Use \n for line breake in string
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  }
});