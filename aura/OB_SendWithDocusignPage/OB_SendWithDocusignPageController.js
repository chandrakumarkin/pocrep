({
  doInit: function(component, event, helper) {
    var event = component.get("v.event");
    if (event) {
      if (event == "Send" || event == "Sent") {
        helper.updateSentDate(component, event, helper);
      } else {
        component.set(
          "v.customErrorMessage",
          "Document failed to send , please try again"
        );
      }
    } else {
      helper.getDocumentData(component, event, helper);
    }
  },

  closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },

  handleSendButton: function(component, event, helper) {
    //helper.handelSendEnvelope(component, event, helper);
    helper.GetSendEnvelopeLink(component, event, helper);
  },

  docSelectChange: function(component, event, helper) {
    var selectedValues = event.getParam("value");
    component.set("v.selectedDocument", selectedValues);
    console.log(selectedValues);
  },

  handleRecipientChange: function(component, event, helper) {
    var selectedValues = event.getParam("value");
    var amendList = component.get("v.respWrap.relAmendList");
    const selectedAmend = amendList.filter(amend =>
      selectedValues.includes(amend.Id)
    );

    //component.set("v.selectedRecipientList", selectedValues);
    component.set("v.selectedRecipientList", selectedAmend);
    console.log(selectedAmend);
  },

  refreshComp: function(component, event, helper) {
    $A.get("e.force:refreshView").fire();
  }
});