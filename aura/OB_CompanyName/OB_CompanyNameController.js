({
	doInit : function(component, event, helper) {
		let newURL = new URL(window.location.href).searchParams;
        var pageflowId = newURL.get('flowId');
        var pageId = newURL.get('pageId');
		var srId = newURL.get('srId'); 
		var companyNameId = newURL.get("NameId");       
		console.log(companyNameId);
		var action = component.get('c.retrieveCompanyWrapper');
		action.setParams({
			"srId": srId,
			"pageId":pageId,
			"companyNameId":companyNameId
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
				console.log(response.getReturnValue());
				component.set("v.companyList",responseResult.lstCompany);
				component.set("v.TransactinalStageSR",responseResult.sr);
				component.set("v.ResponseResult",responseResult);
				component.set('v.commandButton', responseResult.ButtonSection);
				component.set("v.CompanyNameInstance",responseResult.company);
				component.set("v.isCompanyReserved",responseResult.isCompanyReserved);
				component.set("v.price",responseResult.price);
				component.set("v.displayResAndSubmtBtn",responseResult.displayResAndSubmtBtn);

				helper.fallChangeNameHlpr(component, event, helper); //Fire onchange based on SR Yes/No Value
				
				//Open the form if list size = 0
				if(responseResult.lstCompany.length == 0){
					console.log('showNewPanel true');
					component.set("v.showNewPanel","true");
					//helper.createObjectData(component, event);
				}
				else{
					component.set("v.showAnotherName","true");
				}

				//The Name id will appear in url once the name is searched and returned.
				if(companyNameId && companyNameId != 'null')
					component.set("v.showNewPanel","true");
				
				/*
				#843: This field (Does the proposed entity fall within a group of entities?) should not appear for if any of the below is true (not all combinations come together)
				- DNFBP = True
				- Entity Type = Non Financial AND Business Sector = Prescribed  OR Single Family Office OR Investment Fund OR General Partner for a Limited Partnership Fund
				- Entity Type = Retail
				#817: hide this field(Does the proposed entity fall within a group of entities?) when business sector = Investment Fund ""
				*/
				var srBusinessSector = responseResult.sr.Business_Sector__c ? responseResult.sr.Business_Sector__c.toLowerCase() : '';
				var srEntityType = responseResult.sr.Type_of_Entity__c ? responseResult.sr.Type_of_Entity__c.toLowerCase() : '';
				var hasEntityType = ['Non Financial'];
				var hasBusinessSector = ['prescribed companies','single family office','investment fund','general partner for a limited partnership fund'];
				if(responseResult.sr.DNFBP__c || 
					(srEntityType == 'non financial' &&
						srBusinessSector && hasBusinessSector.indexOf(srBusinessSector) > -1
					) ||
					srEntityType == 'retail' ||
					srBusinessSector == 'investment fund'
				){
						console.log('false>>>');
						component.set("v.showProposedEntityField","false");
				}
					
            }
            else if (state === 'ERROR') { // Handle any error by reporting it
                var errors = response.getError();
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message,'error');
                    }
                } else {
                    helper.displayToast('Error', 'Unknown error.','error');
                }
            }
        });
        $A.enqueueAction(action);
        
	},
	viewDetails :function(component,event,helper){
		component.set("v.showNewPanel","false");
		let newURL = new URL(window.location.href).searchParams;
        var pageflowId = newURL.get('flowId');
        var pageId = newURL.get('pageId');
		var srId = newURL.get('srId');        
		var companyId = event.currentTarget.dataset.cid;
		var action = component.get('c.returnCompanyDetail');
		action.setParams({
			"srId": srId,
			"companyId" : companyId
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
				console.log(response.getReturnValue());
				component.set("v.CompanyNameInstance",responseResult);
				component.set("v.showNewPanel","true");
            }
            else if (state === 'ERROR') { // Handle any error by reporting it
                var errors = response.getError();
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message,'error');
                    }
                } else {
                    helper.displayToast('Error', 'Unknown error.','error');
                }
            }
        });
        $A.enqueueAction(action);
	},
	remove :function(component,event,helper)
    {
		component.set("v.showNewPanel","false");
		var companyId = event.currentTarget.dataset.cid;
		var index = event.currentTarget.dataset.rowindex;
		var action = component.get('c.deleteCompanyName');
		console.log(index);
		console.log(companyId);
		action.setParams({
			"companyId": companyId
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
				console.log(response.getReturnValue());
				var allRowsList = component.get("v.companyList");
				allRowsList.splice(index, 1);
				console.log(allRowsList.length);
				component.set("v.companyList", allRowsList);
				if(allRowsList && allRowsList.length == 0){
					component.set("v.showNewPanel","true");
				}
				helper.setDefaultData(component,event);
            }
            else if (state === 'ERROR') { // Handle any error by reporting it
                var errors = response.getError();
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message,'error');
                    }
                } else {
                    helper.displayToast('Error', 'Unknown error.','error');
                }
            }
        });
        $A.enqueueAction(action);
	},
	FallChangeName: function(component, event, helper) {
        helper.fallChangeNameHlpr(component, event, helper);
	},
	handleComponentEvent:function(component,event,helper){
		var showNewPanel = event.getParam("showNewPanel");
		var ResponseResult = event.getParam("ResponseResult");
		console.log(JSON.parse(JSON.stringify(ResponseResult)));
		console.log(showNewPanel);
		component.set("v.showNewPanel",showNewPanel);
		if(event.getParam("companyList"))
			component.set("v.companyList",event.getParam("companyList"));
		
		component.set("v.ResponseResult",ResponseResult);
		
		console.log(component.get("v.companyList").length);
		console.log(component.get("v.isCompanyReserved"));
		console.log(component.get("v.showAnotherName"));
	},
	createRecord:function(component,event,helper){
		helper.createObjectData(component, event);
	},
	navigateToCheckoutAndPay: function(component, event, helper) {
		console.log("inside navigateToCheckoutAndPay");
		var serviceRequest = component.get("v.TransactinalStageSR");
		var allValid = true;

		var isFallRecognisedEntity = component.get("v.TransactinalStageSR.Fall_within_existing_entities__c");
		console.log(isFallRecognisedEntity);
		console.log('inside');
		console.log(component.find('requiredNameCheck'));
		var arrObj = component.find('requiredNameCheck');
		if(arrObj && Array.isArray(arrObj)){
			for(var key in arrObj){
				var inputFieldCompValue = arrObj[key].get("v.value");
				console.log('inputFieldCompValue'+inputFieldCompValue);
				if(!inputFieldCompValue && arrObj[key] && arrObj[key].get("v.required")){
					console.log('bad');
					arrObj[key].reportValidity();
					allValid = false;
				}
			}
		}
		else{
			if(arrObj && arrObj.get("v.required") && !arrObj.get("v.value")){
				arrObj.reportValidity();
				allValid = false;
			}
		}
		
		var companyListLength = component.get("v.companyList").length;
		console.log(companyListLength);
		console.log("inside navigateToCheckoutAndPay 2");
		if(companyListLength && companyListLength > 0){
			var isAgreeTrue = component.get("v.TransactinalStageSR.I_Agree_EntityNameCheck__c");
			if (allValid) {
				if(!isAgreeTrue){
					helper.displayToast('Error', 'Please click on the I agree checkbox to proceed.','error');
					return;
				}
				//Resetting the value to blank for yes/no options
				var selectedValue = component.get("v.TransactinalStageSR.Fall_within_existing_entities__c");//event.getSource().get("v.value");
				console.log("selected FallChangeName value of identical is "+selectedValue);
				if(selectedValue=='No'){
					helper.clearSRValues(component, event, helper);
				}
				var action = component.get('c.updateSR');
				action.setParams({
					"serviceRequest": serviceRequest,
					"amount":component.get("v.price")
				});
				console.log('set param');
				action.setCallback(this, function(response) {
					//store state of response
					var state = response.getState();
					console.log(state);
					if (state === "SUCCESS") {
						var responseResult = response.getReturnValue();
						if(!responseResult.errorMessage){
							console.log(response.getReturnValue());
							console.log("inside if allValid navigateToCheckoutAndPay");
							var amount = component.get("v.price");
							console.log("amount is "+amount);
							/**let newURL = new URL(window.location.href).searchParams;
							var srId = newURL.get('srId');
							var urlEvent = $A.get("e.force:navigateToURL");
							urlEvent.setParams({
								"url": responseResult.paymentpageURL
							});
							urlEvent.fire();*/
                           if(component.get("v.price") == 0){
								helper.nextPage(component,event);
							  }
							  else{
							console.log("amount is "+amount);
							let newURL = new URL(window.location.href).searchParams;
							var srId = newURL.get('srId');
							var urlEvent = $A.get("e.force:navigateToURL");
							urlEvent.setParams({
								"url": responseResult.paymentpageURL
							});
							urlEvent.fire();
						}
						}
					
						else{
							helper.displayToast('Error', responseResult.errorMessage,'error');
						}
						///difc-checkouttopupbalance?category=nameReserve&amountToPay="+ amount+"&srId="+srId
						//helper.displayToast('success', 'The Company Names are submitted successfully for approval');
					}
					else if (state === 'ERROR') { // Handle any error by reporting it
						var errors = response.getError();
						console.log(response.getError());
						
						if (errors) {
							if (errors[0] && errors[0].message) {
								helper.displayToast('Error', errors[0].message,'error');
							}
						} else {
							helper.displayToast('Error', 'Unknown error.','error');
						}
					}
				});
				$A.enqueueAction(action);
				

				
			} else {

				console.log("inside else checkMandatoryFieldsandClose");
				helper.displayToast("Error","Please fill the mandatory fields",'error');
				//alert('Please fill the mandatory fields');
			}
		}
		else{
			helper.displayToast('Error', 'Please add atleast one name to reserve and submit','error');
		}
	},
	handleButtonAct : function(component, event, helper) {
        try{
			var serviceRequest = component.get("v.TransactinalStageSR");
			var allValid = true;
			console.log(JSON.parse(JSON.stringify(serviceRequest)));
			var isFallRecognisedEntity = component.get("v.TransactinalStageSR.Fall_within_existing_entities__c");
			console.log(isFallRecognisedEntity);
			var arrObj = component.find('requiredNameCheck');
			if(arrObj && Array.isArray(arrObj)){
				for(var key in arrObj){
					var inputFieldCompValue = arrObj[key].get("v.value");
					console.log('inputFieldCompValue'+inputFieldCompValue);
					if(!inputFieldCompValue && arrObj[key] && arrObj[key].get("v.required")){
						console.log('bad');
						arrObj[key].reportValidity();
						allValid = false;
					}
				}
			}
			else{
				if(arrObj && arrObj.get("v.required") && !arrObj.get("v.value")){
					arrObj.reportValidity();
					allValid = false;
				}
			}
			//if(isFallRecognisedEntity == "Yes"){
				/*
				console.log('inside');
				var arrObj = component.find('requiredNameCheck');
				for(var key in arrObj){
					var inputCmp = arrObj[key];
					if(inputCmp.get('v.validity') && !inputCmp.get('v.validity').valid){
						inputCmp.showHelpMessageIfInvalid();
						allValid = false;
					}
					else{
						var inputFieldCompValue = inputCmp.get("v.value");
						if(!inputFieldCompValue && arrObj[key] && arrObj[key].get("v.required")){
							inputCmp.reportValidity();
							allValid = false;
						}

					}
				}
				*/
			//}
			console.log(allValid);
			var companyListLength = component.get("v.companyList").length;
			console.log(companyListLength);
			console.log("inside navigateToCheckoutAndPay 2");
			if(companyListLength && companyListLength > 0){
				var isAgreeTrue = component.get("v.TransactinalStageSR.I_Agree_EntityNameCheck__c");
				if (allValid) {
					if(!isAgreeTrue){
						helper.displayToast('Error', 'Please click on the I agree checkbox to proceed.','error');
						return;
					}
					//Resetting the value to blank for yes/no options
					var selectedValue = component.get("v.TransactinalStageSR.Fall_within_existing_entities__c");//event.getSource().get("v.value");
					console.log("selected FallChangeName value of identical is "+selectedValue);
					if(selectedValue=='No'){
						helper.clearSRValues(component, event, helper);
					}
					helper.nextPage(component,event);
				} else {

					console.log("inside else checkMandatoryFieldsandClose");
					helper.displayToast("Error","Please fill the mandatory fields",'error');
					//alert('Please fill the mandatory fields');
				}
			}
			else{
				helper.displayToast('Error', 'Please add atleast one name to reserve and submit','error');
			}
			
          
        }catch(err) {
           console.log(err.message) 
        } 
        

    }
})