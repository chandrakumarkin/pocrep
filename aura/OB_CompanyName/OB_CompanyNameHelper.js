({
	/**
     * Display a message
     */
    displayToast : function (title, message, type) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'type': type,
                'title': title,
                'message': message,
                mode:'sticky'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
	commitNameRecordHelper: function(component, event,helper) {
        let newURL = new URL(window.location.href).searchParams;
        //console.log("inside commitNameApex DIFC_NameCheckblock");
        //console.log(JSON.parse(JSON.stringify(component.get("v.TransactinalStageSR"))));
        var action = component.get("c.commitNameApex");   
        action.setParams({
            srId : newURL.get('srId'),
            nameCheckTransStage: component.get("v.TransactinalStageSR"),
            companyName : component.get("v.CompanyName")
        })
        action.setCallback(this, function(response) {
            var retVal = response.getReturnValue();
            //console.log(retVal);
            var state=response.getState();
            //console.log("response from commitNameApex DIFC_NameCheckblock"+retVal);
            //console.log("state is "+state);
            
            if(state=="SUCCESS" && retVal.includes('Success') ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The Names are successfully submitted for approval"
                });
                toastEvent.fire();  
            }
            
            else if(retVal.includes('ENTITY_IS_LOCKED')){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "The Names are already submitted for approval",
                    "type":"error"
                });
                toastEvent.fire();
            }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please enter the names again",
                        "type":"error"
                    });
                    toastEvent.fire();
                }
            //console.log("inside success submit for name commitNameRecordHelper");
        });
        
        $A.enqueueAction(action); 
        //helper.expandAndCollapseAccordian(component, event, helper);
	},
	clearSRValues : function(component,event,helper){
        //component.set('v.TransactinalStageSR.Check_Identical_to_existing__c','');
        //component.set('Fall_within_existing_entities__c','');
        component.set('v.TransactinalStageSR.Parent_Entity_Name__c','');
        component.set('v.TransactinalStageSR.Former_Name__c','');
        component.set('v.TransactinalStageSR.Date_of_Registration__c','');
        component.set('v.TransactinalStageSR.Place_of_Registration__c','');
        component.set('v.TransactinalStageSR.Country_of_Registration__c','');
        component.set('v.TransactinalStageSR.Registered_No__c','');
        component.set('v.TransactinalStageSR.Family_Group__c','');
        component.set('v.TransactinalStageSR.Registered_address__c','');
        component.set('v.TransactinalStageSR.Address_Line_1__c','');
        component.set('v.TransactinalStageSR.Address_Line_2__c','');
        component.set('v.TransactinalStageSR.Country__c','');
        component.set('v.TransactinalStageSR.City_Town__c','');
        component.set('v.TransactinalStageSR.State_Province_Region__c','');
        component.set('v.TransactinalStageSR.Po_Box_Postal_Code__c','');
	},
	createObjectData: function(component,event){
        console.log('createObjectData >>>>>>>>>>>>>>>>');
        component.set("v.showNewPanel","false");
        let newURL = new URL(window.location.href).searchParams;
        
		 // set the updated list to attribute (contactList) again 
		var companyList = component.get("v.companyList");
		console.log(companyList.length);
		if(companyList.length >= 3){
			this.displayToast('Alert', 'The maximum entity name that you can choose is three.','error');
		}
		else{   
            
            component.set("v.showNewPanel","true");
            this.setDefaultData(component,event);
            
            
            /*
            var action = component.get("c.createCompanyRecord");
        
            action.setCallback(this, function(response) {
            
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(response.getReturnValue());
                    component.set("v.CompanyNameInstance", response.getReturnValue());
                }else{
                    console.log('@@@@@ Error '+response.getError()[0].message);
                }
            });
            $A.enqueueAction(action);
            */

        }
    },
    setDefaultData:function(component,event){
        var newCompany = {'sobjectType': 'Company_Name__c',
                'Entity_Name__c': '',
                'Ends_With__c': '',
                'Trading_Name__c': '',
                'Arabic_Entity_Name__c':'',
                'Arabic_Ends_With__c':'',
                'Arabic_Trading_Name__c':'',
                'Check_Identical_to_existing__c':''
        };
        component.set("v.CompanyNameInstance", newCompany);
    },
    nextPage:function(component,event){
        var serviceRequest = component.get("v.TransactinalStageSR");
        console.log('callpage');
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId');
        var pageID = newURL.get('pageId');
        var buttonId =  event.target.id;
        console.log(buttonId);
        
        var action = component.get("c.getButtonAction");
        action.setParams({
            "SRID": srID,
            "pageId": pageID,
            "ButtonId": buttonId,
            "serviceRequest" : serviceRequest
        });
    
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("button succsess")
                
                console.log(response.getReturnValue());
                debugger;
                if(response.getReturnValue().pageActionName)
                    window.open(response.getReturnValue().pageActionName,"_self");
            }else{
                console.log('@@@@@ Error '+response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    },
	fallChangeNameHlpr:function(component,event,helper){
		var selectedValue = component.get("v.TransactinalStageSR.Fall_within_existing_entities__c");//event.getSource().get("v.value");
        console.log("selected FallChangeName value of identical is "+selectedValue);
        if(selectedValue=='Yes'){
            component.set("v.showFallFieldsName","true");
        }
        if(selectedValue=='No'){
            component.set("v.showFallFieldsName","false");
            //helper.clearSRValues(component, event, helper);
           
        }
	}
})