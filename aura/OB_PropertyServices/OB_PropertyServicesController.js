({
    doInit : function(component, event, helper) {
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                let userObj = response.getReturnValue();
                component.set('v.profileName',userObj.Profile.Name);
                component.set('v.initialAccId',userObj.Contact.AccountId);
            } 
        });
        $A.enqueueAction(action);
    },
    validateAccount : function(component, event, helper) {
        var action = component.get("c.getCurrentUser");
        var ctarget = event.currentTarget;
        var redirectURL = ctarget.dataset.value;
        
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                if(a.getReturnValue()){
                    let userObj = a.getReturnValue();
                    var currentAccId = userObj.Contact.AccountId;
                    var currentAccName = userObj.Contact.Account.Name;
                    var initialAccId = component.get('v.initialAccId');
                    if(initialAccId == currentAccId){
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": redirectURL,
                        });
                        urlEvent.fire();
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error Message',
                            message:'The account that you are managing has been changed to '+currentAccName+'. Please refresh the page to continue.',
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            }else if(state == 'FAILURE'){
                console.log('Some problem ocurred');
            }
        });
        $A.enqueueAction(action);
    }
})