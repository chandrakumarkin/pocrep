({
  viewNextStep: function (component, event, limitRow) {
    var action = component.get("c.getNextStep");
    /*
        action.setParams({
            "limitRow": limitRow
        });
        */
    action.setCallback(this, function (response) {
      // hide spinner when response coming from server
      //component.find("Id_spinner").set("v.class" , 'slds-hide');
      var state = response.getState();
      if (state === "SUCCESS") {
        var storeResponse = response.getReturnValue();
        console.log(storeResponse);
        component.set("v.nextStep", storeResponse.lstPendingApproval);
      } else if (state === "INCOMPLETE") {
        console.log("Response is Incompleted");
      } else if (state === "ERROR") {
        var errors = response.getError();
        console.log(JSON.parse(JSON.stringify(errors)));
        if (errors) {
          if (errors[0] && errors[0].message) {
            //alert("Error message: " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  showToast: function (component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  }
});