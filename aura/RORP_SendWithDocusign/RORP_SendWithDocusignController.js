({
    sendDocument: function(component, event, helper) {
     /* var urlEvent = $A.get("e.force:navigateToURL");
      console.log('***'+component.get("v.respWrap.relActionIteam.Id"));
  
      urlEvent.setParams({
        url:
          "/apex/RORP_DocuSignVFPage?srId=" +
          component.get("v.recordId") +
          "&stepId" +
          component.get("v.respWrap.relActionIteam.Id")
      });
      urlEvent.fire();
      */
      var myURL = "/apex/RORP_DocuSignVFPage?srId="+component.get("v.srId") +"&stepId=" +component.get("v.respWrap.relActionIteam.Id");
      window.location.href=myURL;
    },
    doInit: function(component, event, helper) {
      console.log('**SRid'+component.get("v.srId"))
      var action = component.get("c.checkRender");
      var requestWrap = {
        srId: component.get("v.srId")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log('***'+JSON.stringify(respWrap));
          component.set("v.respWrap", respWrap);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    }
  });