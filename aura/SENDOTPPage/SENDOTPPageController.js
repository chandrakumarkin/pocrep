({
	myAction1 : function(component, event, helper) {
		
        var isOTPentered = component.get("v.sendOTP");       
        console.log('!!@@@'+isOTPentered);
        if(isOTPentered !=''){
        	var action = component.get("c.getotpValidation");
            action.setParams({ OTP : component.get("v.sendOTP") });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log('!!@@##'+state);
                if(state == 'SUCCESS'){
                    console.log('!!@@return##'+response.getReturnValue());
                    var isStatus = response.getReturnValue();
                    if(isStatus == 'Success'){
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                        "url": "https://portal.difc.ae/clientportal/s/thankyoupage"
                        });
                        urlEvent.fire(); 
                    }
                    else {
                        component.set('v.enterOTP', true);
                    }
                }
            });   
            $A.enqueueAction(action);
        }
        else{
            component.set('v.enterOTP', true);
        }
        
	},
    reSendOTP : function(component, event, helper) {
        
        console.log('@@@##OTP');
		var action = component.get("c.sendOTPAgain");
        $A.enqueueAction(action);
	},
    gotoURL : function (component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "https://portal.difc.ae/clientportal/s"
    });
    	urlEvent.fire();
	}
})