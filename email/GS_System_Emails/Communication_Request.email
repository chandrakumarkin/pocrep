<table border="0" cellpadding="5" width="550" cellspacing="5" height="400" >
<tr valign="top" height="400" >
<td tEditID="c1r1" style=" background-color:#FFFFFF; color:#000000; bEditID:r3st1; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r1" locked="0" >
<![CDATA[<p><strong><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif">Dear
Valued Clients, </span></strong><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif"><o:p></o:p></span></p><p><strong><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif"><br></span></strong></p>

<p style="text-align:justify"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif">In
line with the recent announcement made by the Federal Authority for Identity
and Citizenship concerning the extension of residence visa validity until <strong><span style="font-family:&quot;Arial&quot;,sans-serif">31 December 2020</span></strong>, we are
pleased to update you on the changes and updates as follows:&nbsp;<o:p></o:p></span></p>

<ul type="disc">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">All
     employees and dependents with visas expiring from <strong><span style="font-family:&quot;Arial&quot;,sans-serif">1 March 2020</span></strong> will
     have their visa validity extended until <strong><span style="font-family:
     &quot;Arial&quot;,sans-serif">31 December 2020</span></strong>. Employee records in
     DIFC’s Client Portal will be updated accordingly.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Clients
     who wish to submit visa renewal can still apply on the DIFC Portal. The
     visa will be valid from the renewal date for 3 years and medical fitness
     test will not be required.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Individuals
     who need to exit the UAE and have a visa that has been expired (based on
     the original expiry date not the revised one) are advised to renew the
     visa to avoid any unexpected issues in the county of destination that they
     are traveling to.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">All
     individuals with entity permits (visit visa, tourist visa or resident
     entry permit) that are expired from 1 March 2020 will have their entry
     permit validity extended until 31 December 2020.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Employees
     with a cancelled visa that were planning to exit the UAE but couldn’t due
     to the current lockdown will be able to have their overstay fines waived
     at the airport at the time of exit.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Employees
     with a cancelled visa or visit visa that are in the process of obtaining a
     visa with a new employer must complete the change of status within one
     month of visa cancelation to avoid fines. Fines will still be applicable
     for this category.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Late
     fines will still be applicable for delay in renewal of company
     establishment card. Companies are kindly requested to ensure the
     establishment card is renewed on time to avoid fine of AED 100 per month.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Visa
     of Individuals who completed 6 months or above stay outside the UAE after
     1 March 2020 will remain valid until 31 December 2020.<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:
     9.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Individuals
     applying for a new visa while they are inside the UAE given that no over
     stay fines are issued to them will be allowed to complete the new visa
     process without the medical fitness test if the visa is issued for one
     year only. While for visas issued for 3 years, it will still require a
     medical fitness test and DIFC Services will advise clients on the
     available centres in Dubai.<o:p></o:p></span></li>
</ul>

<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif">Please be noted
that the above updates are according to an announcement made by the Federal
Authority for Identity and Citizenship. Should there be any changes, DIFC
Services will update you immediately.<o:p></o:p></span></p>

<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif">For any
enquiries, please e-mail us on <u><a href="https://gateway.difc.aemailto:gs.helpdesk@difc.ae">gs.helpdesk@difc.ae</a>
</u>or call us at 04 362 2222.<br><br><o:p></o:p></span></p>

<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif">Thank you.<o:p></o:p></span></p>

<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif">DIFC Services<o:p></o:p></span></p>]]></td>
</tr>
</table>