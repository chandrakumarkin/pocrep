<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Notification_to_DFSA</fullName>
        <ccEmails>tip@dfsa.ae</ccEmails>
        <description>Send Notification to DFSA</description>
        <protected>false</protected>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/DFSA_Online_submission_Template</template>
    </alerts>
    <rules>
        <fullName>DFSA Online submission Notification</fullName>
        <actions>
            <name>Send_Notification_to_DFSA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>