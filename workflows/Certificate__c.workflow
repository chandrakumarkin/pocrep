<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_alert_on_design_drawing_expiry</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Email alert on design drawing expiry</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Authorized_Representative__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contractor_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Design_Drawing_Expiry</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_on_work_permit_expiry</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Email alert on work permit</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Authorized_Representative__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contractor_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Work_Permit_Expiry</template>
    </alerts>
    <rules>
        <fullName>Design Drawing Expiry</fullName>
        <actions>
            <name>Email_alert_on_design_drawing_expiry</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule is used for sending out notification design drawing expiry</description>
        <formula>AND(Notification_Date__c = TODAY(),ISPICKVAL(Type__c, &quot;Design Approval&quot;), Service_Request__r.External_Status_Name__c  &lt;&gt; &quot;Project Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fit - Out Work Permit Expiry</fullName>
        <actions>
            <name>Email_alert_on_work_permit_expiry</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule is used for sending out notification on work permit and design drawing expiry</description>
        <formula>AND(Notification_Date__c = TODAY(),ISPICKVAL(Type__c, &quot;Fit-Out Permit&quot;), Service_Request__r.External_Status_Name__c  &lt;&gt; &quot;Project Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>