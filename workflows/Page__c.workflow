<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Whatd_ID_field_update</fullName>
        <field>What_Id__c</field>
        <formula>Page_Flow__r.Master_Object__c</formula>
        <name>Whatd ID field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>What ID field defualt value rule</fullName>
        <actions>
            <name>Whatd_ID_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(What_Id__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>