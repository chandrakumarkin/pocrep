<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Mass_Read_Only_Layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mass_Update_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Mass Read Only Layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Read_Only_Layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Read Only Layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submit_Date</fullName>
        <field>Submitted_Date__c</field>
        <formula>now()</formula>
        <name>Submit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unique_CRS</fullName>
        <field>Unique__c</field>
        <formula>FI_ID__c + text(Status__c) +text(Tax_Residence_Country_new__c)+Account_Number__c+Ids__c+&apos;-&apos;+CRS_Year__c+Child_No__c</formula>
        <name>Unique CRS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unique_CRS_Cancelled</fullName>
        <field>Unique__c</field>
        <formula>FI_ID__c + text(Status__c) +text(Tax_Residence_Country_new__c)+Account_Number__c+text(Now())</formula>
        <name>Unique CRS Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Cancelled Status Change layout</fullName>
        <actions>
            <name>Read_Only_Layout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Submit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unique_CRS_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CRS_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submitted Status Change layout</fullName>
        <actions>
            <name>Read_Only_Layout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Submit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unique_CRS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CRS_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>CRS_Form__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Mass Upload Draft</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submitted Status Change layout-Mass update</fullName>
        <actions>
            <name>Mass_Read_Only_Layout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Submit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unique_CRS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CRS_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>CRS_Form__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mass Upload Draft</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>