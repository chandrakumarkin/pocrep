<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lease_is_Registered</fullName>
        <ccEmails>Ali.Aycha@difc.ae</ccEmails>
        <ccEmails>Frederic.Azaredo@difc.ae</ccEmails>
        <description>Lease is Registered</description>
        <protected>false</protected>
        <recipients>
            <recipient>saleh.abdulhalim@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>t-sharyl.mathias@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thusith.dhananjaya@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/Notification_for_Lease_Activation_For_Fitout</template>
    </alerts>
    <rules>
        <fullName>Commercial Lease Partner Created</fullName>
        <actions>
            <name>Lease_is_Registered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lease_Partner__c.Lease_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease_Partner__c.Third_Party__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease_Partner__c.Unit_Type__c</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease_Partner__c.Lease_Types__c</field>
            <operation>notContain</operation>
            <value>Business Centre Lease,PLMA Lease</value>
        </criteriaItems>
        <description>Once an office lease partner is created email should be delivered</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Non Commercial Lease Partner Created</fullName>
        <actions>
            <name>Lease_is_Registered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lease_Partner__c.Lease_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease_Partner__c.Third_Party__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease_Partner__c.Unit_Type__c</field>
            <operation>notEqual</operation>
            <value>Commercial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease_Partner__c.Lease_Types__c</field>
            <operation>notContain</operation>
            <value>Business Centre Lease,PLMA Lease</value>
        </criteriaItems>
        <description>Once a non commercial lease partner is created email should be delivered</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>