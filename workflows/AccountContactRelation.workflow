<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Portal_User_Roles_Changed</fullName>
        <description>Portal User Roles Changed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Digital_Onboarding_Project/Portal_User_Roles_Changed</template>
    </alerts>
    <alerts>
        <fullName>Send_Portal_link_to_enable_Super_user_Form</fullName>
        <ccEmails>difcuseraccess@difc.ae</ccEmails>
        <description>Send Portal link to enable Super user Form</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Send_Portal_link_to_Super_User_Data</template>
    </alerts>
    <rules>
        <fullName>Send Portal link to enable Super user access</fullName>
        <actions>
            <name>Send_Portal_link_to_enable_Super_user_Form</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AccountContactRelation.Enable_as_a_Super_user__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>AccountContactRelation.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>