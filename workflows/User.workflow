<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Active_User_Notification</fullName>
        <ccEmails>Deepak.Sachdeva@difc.ae,Veena.Dorairajan@difc.ae,c-Sabirsalihu.A@difc.ae</ccEmails>
        <description>Active User Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-veera.narayana@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Active_User_Notification_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>De_Active_User_Notification</fullName>
        <ccEmails>Deepak.Sachdeva@difc.ae,Veena.Dorairajan@difc.ae,c-Sabirsalihu.A@difc.ae</ccEmails>
        <description>De-Active User Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-veera.narayana@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/DeActive_User_Notification_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Freeze_User_Notification_Email_Alert</fullName>
        <ccEmails>Deepak.Sachdeva@difc.ae,Veena.Dorairajan@difc.ae,c-Sabirsalihu.A@difc.ae</ccEmails>
        <description>Freeze User Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-veera.narayana@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Freeze_User_Notification_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notification_Liquidator_Appointed_on_Creation</fullName>
        <description>Notification :Liquidator Appointed on Creation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notification_Liquidator_Appointed_on_Creation</template>
    </alerts>
    <alerts>
        <fullName>OB_Send_agent_username</fullName>
        <description>OB Send agent username</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Agent_Email_Notification_v2</template>
    </alerts>
    <alerts>
        <fullName>Un_Freeze_User_Notification_Email_Alert</fullName>
        <ccEmails>Deepak.Sachdeva@difc.ae,Veena.Dorairajan@difc.ae,c-Sabirsalihu.A@difc.ae</ccEmails>
        <description>Un Freeze User Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-veera.narayana@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Un_Freeze_User_Notification_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>User_Deactivation_Alert</fullName>
        <ccEmails>Sayed.Ahmed@difc.ae</ccEmails>
        <ccEmails>Veena.Dorairajan@difc.ae</ccEmails>
        <ccEmails>c-Sabirsalihu.A@difc.ae</ccEmails>
        <description>User Deactivation Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>DIFC_IT_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/User_Deactivation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Uncheck_to_make_inactive</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Uncheck to make inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contractor_Hidden_Field</fullName>
        <description>Updates the contractor ID text field for cross-object purposes</description>
        <field>Contractor_Id__c</field>
        <formula>Contact.Contractor__c</formula>
        <name>Update Contractor Hidden Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Freeze User Notification</fullName>
        <actions>
            <name>Freeze_User_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(PRIORVALUE( User_Freeze__c ) == FALSE, User_Freeze__c , LEN(ContactId) = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification %3ALiquidator Appointed on Creation</fullName>
        <actions>
            <name>Notification_Liquidator_Appointed_on_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Is_Liquidator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ContactId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create an email that goes to the liquidator upon appointing a liquidator # 1217</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Send agent username</fullName>
        <actions>
            <name>OB_Send_agent_username</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.Send_Agent_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Un Freeze User Notification</fullName>
        <actions>
            <name>Un_Freeze_User_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(PRIORVALUE( User_Freeze__c ) == TRUE, NOT(User_Freeze__c), LEN(ContactId) = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Contractor Field</fullName>
        <active>true</active>
        <formula>NOT(ISBLANK( Contractor__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Contractor_Hidden_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>User Activate Notification</fullName>
        <actions>
            <name>Active_User_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(IsActive) ,PRIORVALUE(IsActive) == FALSE, IsActive, LEN(ContactId) = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User De-Activate Notification</fullName>
        <actions>
            <name>De_Active_User_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(IsActive),PRIORVALUE(IsActive) == True, IsActive, LEN(ContactId) = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User Deactivation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.User_Deactivation_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>User_Deactivation_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Uncheck_to_make_inactive</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.User_Deactivation_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>