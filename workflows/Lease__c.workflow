<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BC_Lease_Renewal_Notification</fullName>
        <ccEmails>Businesscentre@difc.ae</ccEmails>
        <description>BC - Lease Renewal Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/BC_Lease_Renewal_Notification</template>
    </alerts>
    <alerts>
        <fullName>SMS_BC_Lease_Renewal_Notification</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SMS BC - Lease Renewal Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/SMS_BC_Lease_Renewal_Notification</template>
    </alerts>
    <rules>
        <fullName>BC - Lease Renewal Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lease__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease__c.Lease_Types__c</field>
            <operation>equals</operation>
            <value>Business Centre Lease,Center of Excellence</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lease__c.Customer_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notify Client 
1 month prior to the lease expiry 
15 days prior to the lease expiry
10 days prior to the lease expiry
on the same day of lease expiry</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SMS_BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lease__c.End_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SMS_BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lease__c.End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SMS_BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lease__c.End_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SMS_BC_Lease_Renewal_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lease__c.End_Date__c</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>