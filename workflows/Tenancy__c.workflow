<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Square_Feet_For_Sub_lease_from</fullName>
        <field>Square_Feet__c</field>
        <formula>VALUE(Lease__r.Square_Feet__c)</formula>
        <name>Populate Square Feet For Sub-lease from</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate Square Feet For Sub-lease from Lease</fullName>
        <actions>
            <name>Populate_Square_Feet_For_Sub_lease_from</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>TEXT(Lease__r.Lease_Types__c) == &apos;Leased - Sub Leasing&apos; &amp;&amp; OR(ISBLANK(Square_Feet__c), Square_Feet__c == 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>