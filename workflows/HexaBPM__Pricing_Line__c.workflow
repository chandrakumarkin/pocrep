<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OB_New_PriceLine_Approval</fullName>
        <description>OB New PriceLine Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_New_PriceLine_Approval</template>
    </alerts>
    <alerts>
        <fullName>OB_New_PriceLine_Rejection</fullName>
        <description>OB New PriceLine Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_New_PriceLine_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>OB_New_PriceLine_Active</fullName>
        <field>HexaBPM__Active__c</field>
        <literalValue>1</literalValue>
        <name>OB New PriceLine Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_New_PriceLine_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>OB New PriceLine Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_New_PriceLine_Rejection</fullName>
        <description>Status of the priceline is updated to Rejected</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>OB New PriceLine Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_New_Priceline_Status_Submitted</fullName>
        <description>Update the status to submitted for approval</description>
        <field>Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>OB New Priceline Status Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>