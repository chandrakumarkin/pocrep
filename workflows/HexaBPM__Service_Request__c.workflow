<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_User_Pending_Registration</fullName>
        <ccEmails>rola.abuizzeddin@pwc.com</ccEmails>
        <description>OB New User - Pending Registration</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/New_User_Pending_Registration</template>
    </alerts>
    <alerts>
        <fullName>OB_Approval</fullName>
        <description>OB Approval</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Approval</template>
    </alerts>
    <alerts>
        <fullName>OB_Email_Notification_to_Client_CP_SR_Not_Submitted</fullName>
        <description>OB Notify Customer_Commercial Permission not Submitted</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Application_Not_Submitted_Commercial_Permission_Pocket_Shops</template>
    </alerts>
    <alerts>
        <fullName>OB_In_Principle_Approval</fullName>
        <description>OB - In Principle Approval</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Send_In_Principle_Document_to_Customer</template>
    </alerts>
    <alerts>
        <fullName>OB_Onboarding_steps</fullName>
        <description>OB Onboarding steps</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_onboarding_steps</template>
    </alerts>
    <alerts>
        <fullName>OB_Rejection</fullName>
        <description>OB Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Rejection</template>
    </alerts>
    <alerts>
        <fullName>OB_Rejection_Commercial_Permission_Renewal</fullName>
        <description>OB Rejection Commercial Permission Renewal</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Rejection_Commercial_Permission_Renewal</template>
    </alerts>
    <alerts>
        <fullName>OB_Rejection_Super_User</fullName>
        <description>OB Rejection Super User</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Rejection_Super_User</template>
    </alerts>
    <alerts>
        <fullName>OB_Send_Customer_Email_at_Commercial_Permission_Creation</fullName>
        <description>OB Send Customer Email at Commercial Permission Creation</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Commercial_Permission_Creation_V_1</template>
    </alerts>
    <alerts>
        <fullName>OB_Submission</fullName>
        <description>OB Submission</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Submission</template>
    </alerts>
    <alerts>
        <fullName>OB_Submission_Commercial_Permission</fullName>
        <description>OB Submission - Commercial Permission</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Submission_Commercial_Permission</template>
    </alerts>
    <alerts>
        <fullName>RS_Officer_Internal_Clearance_License_Suspension</fullName>
        <description>RS Officer Internal Clearance - License Suspension</description>
        <protected>false</protected>
        <recipients>
            <field>HexaBPM__Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Client_License_Suspension_Pending_Clearance_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>OB_Update_In_Principle_Date</fullName>
        <field>In_Principle_Date__c</field>
        <formula>NOW()</formula>
        <name>OB Update In Principle Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Update_portal_username_on_application</fullName>
        <field>Portal_User_Name__c</field>
        <formula>HexaBPM__Contact__r.Portal_Username__c</formula>
        <name>OB Update portal username on application</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_type_of_entity_to_foundation</fullName>
        <description>type_of_entity__c</description>
        <field>Type_of_Entity__c</field>
        <literalValue>Foundation</literalValue>
        <name>Set type of entity to foundation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OB Approval</fullName>
        <actions>
            <name>OB_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>AOR_Financial</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>AOR_NF_R</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>New_User_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Converted_User_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Multi_Structure</value>
        </criteriaItems>
        <description>On approval of SR, customer receives an email. Not applicable for AORs and In Principle SRs.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Auto fill type of entity for foundation</fullName>
        <actions>
            <name>Set_type_of_entity_to_foundation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Business_Sector__c</field>
            <operation>equals</operation>
            <value>Foundation</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <description>Auto fill the type of entity for an entity with a foundation business sector</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Commercial Permission Creation Notification</fullName>
        <actions>
            <name>OB_Send_Customer_Email_at_Commercial_Permission_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sending notification to customer when CP is created</description>
        <formula>AND(RecordType.DeveloperName = &quot;Commercial_Permission&quot;,  Parent_SR_Status__c != &quot;Rejected&quot;,  NOT(ISBLANK(HexaBPM__Contact__c )),  HexaBPM__External_Status_Name__c = &quot;Draft&quot;, NOT(ISBLANK( Portal_User_Name__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Commercial Permission Renewal Rejection</fullName>
        <actions>
            <name>OB_Rejection_Commercial_Permission_Renewal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Parent_SR_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission_Renewal</value>
        </criteriaItems>
        <description>On rejection of SR, customer receives and email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Commercial Permission Setting portal username</fullName>
        <actions>
            <name>OB_Update_portal_username_on_application</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>setting portal username</description>
        <formula>AND(RecordType.DeveloperName = &quot;Commercial_Permission&quot;,  Parent_SR_Status__c != &quot;Rejected&quot;,  NOT(ISBLANK(HexaBPM__Contact__c )),  HexaBPM__External_Status_Name__c = &quot;Draft&quot;, ISBLANK( Portal_User_Name__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB In Principle Approval</fullName>
        <actions>
            <name>OB_In_Principle_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (1 AND 2 AND 4)</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.DNFBP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Type_of_Property__c</field>
            <operation>equals</operation>
            <value>Lease a Kiosk</value>
        </criteriaItems>
        <description>On approval of In Principle Sr, where DNFBP is true or Property type is Lease a kiosk, customer receives an email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Notify Customer_Commercial Permission not Submitted</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>This workflow rule is used to notify the clients when CP SR is not submitted in 15, 30 and 60 days from the application creation date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>HexaBPM__Service_Request__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>OB_Email_Notification_to_Client_CP_SR_Not_Submitted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>HexaBPM__Service_Request__c.CreatedDate</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>OB_Email_Notification_to_Client_CP_SR_Not_Submitted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>HexaBPM__Service_Request__c.CreatedDate</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>OB_Email_Notification_to_Client_CP_SR_Not_Submitted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>HexaBPM__Service_Request__c.CreatedDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OB Onboarding steps email through send portal link</fullName>
        <actions>
            <name>OB_Onboarding_steps</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Converted_User_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Created_By_Agent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email to customer informing them of the onboarding steps</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Onboarding steps emails self registration</fullName>
        <actions>
            <name>OB_Onboarding_steps</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Created</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Entity_Type__c</field>
            <operation>equals</operation>
            <value>Non - financial,Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>New_User_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Business_Sector__c</field>
            <operation>notEqual</operation>
            <value>Investment Fund</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Created_By_Agent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email to self registration customer informing them of the onboarding steps</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Rejection</fullName>
        <actions>
            <name>OB_Rejection</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Superuser_Authorization</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Commercial_Permission_Renewal</value>
        </criteriaItems>
        <description>On rejection of SR, customer receives and email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Rejection - Super User</fullName>
        <actions>
            <name>OB_Rejection_Super_User</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Superuser_Authorization</value>
        </criteriaItems>
        <description>On rejection of SR, customer receives and email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Submission</fullName>
        <actions>
            <name>OB_Submission</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>New_User_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Multi_Structure</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>notEqual</operation>
            <value>Superuser_Authorization</value>
        </criteriaItems>
        <description>On submission of SR, customer receives an email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Submission - Commercial Permission</fullName>
        <actions>
            <name>OB_Submission_Commercial_Permission</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>On submission of Commercial Permission SR, customer receives an email.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Update In Principle Date</fullName>
        <actions>
            <name>OB_Update_In_Principle_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>In Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.HexaBPM__External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Update in principle approval date when the SR of in principle approval is approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RS Officer Internal Clearance - License Suspension</fullName>
        <actions>
            <name>RS_Officer_Internal_Clearance_License_Suspension</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request for Suspension Of License</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Service_Request__c.Clearance_Check_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>