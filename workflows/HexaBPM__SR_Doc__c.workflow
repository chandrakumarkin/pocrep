<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OB_Reset_Generate_Flag</fullName>
        <field>HexaBPM__Generate_Document__c</field>
        <literalValue>0</literalValue>
        <name>OB Reset Generate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Update_Document_Number</fullName>
        <field>Document_No_Id__c</field>
        <formula>Document_No__c</formula>
        <name>OB Update Document Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>OB_Auto_Generate_Application_Docs</fullName>
        <apiVersion>47.0</apiVersion>
        <endpointUrl>https://apps.drawloop.com/package/111</endpointUrl>
        <fields>Drawloop_Next__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration@difc.com</integrationUser>
        <name>OB Auto-Generate Application Docs</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>OB Auto-Generate Application Document</fullName>
        <actions>
            <name>OB_Reset_Generate_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OB_Auto_Generate_Application_Docs</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>HexaBPM__Generate_Document__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Copy Document No</fullName>
        <actions>
            <name>OB_Update_Document_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Document_No__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>