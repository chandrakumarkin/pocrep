<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>GS_Pending_Doc_Upload_Notification</fullName>
        <ccEmails>gs.bopendings@difc.ae</ccEmails>
        <description>GS Pending Doc Upload Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/GS_Notification_for_Doc_Uploaded</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_Health_Insurance_Certificate_upload</fullName>
        <description>Notification for Health Insurance Certificate upload</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Health_Insurance_Certificate_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Notify_GS_Team_After_Doc_Uploaded</fullName>
        <ccEmails>gs.bopendings@difc.ae</ccEmails>
        <description>Notify GS Team After Doc Uploaded</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/GS_Notification_for_Doc_Uploaded</template>
    </alerts>
    <alerts>
        <fullName>SR_submission_for_new_portal_user_creation</fullName>
        <description>SR submission for new portal user creation Post Document Generated</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notification_DG_Portal_User</template>
    </alerts>
    <alerts>
        <fullName>Sending_out_notification_to_contractor_for_new_sr_doc</fullName>
        <description>Sending out notification to contractor for new sr doc</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/New_SR_Documents_Notification_fit_out</template>
    </alerts>
    <fieldUpdates>
        <fullName>Pending_Doc</fullName>
        <field>Is_Pending_Document__c</field>
        <literalValue>1</literalValue>
        <name>Pending Doc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Generate_Flag</fullName>
        <field>Generate_Document__c</field>
        <literalValue>0</literalValue>
        <name>Reset Generate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transfer_instruments_Completed_Proper</fullName>
        <field>Marital_Status__c</field>
        <literalValue>Unspecific</literalValue>
        <name>Transfer instruments - Completed Proper</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Service_Request__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Is_Pending</fullName>
        <field>Is_Pending_Document__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Is Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTriggerDate</fullName>
        <field>Trigger_Date__c</field>
        <formula>TODAY()</formula>
        <name>UpdateTriggerDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Document_Number</fullName>
        <field>Document_No_Id__c</field>
        <formula>Document_No__c</formula>
        <name>Update Document Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Instruction</fullName>
        <field>Document_Description_External__c</field>
        <formula>&quot;Kindly click on &apos;Download/Upload Documents&apos; to view and print the issued online &quot; + Name</formula>
        <name>Update Instruction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Trigger_Date</fullName>
        <field>Trigger_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Trigger Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updating_email_on_SR_Doc</fullName>
        <field>Applicant_Email__c</field>
        <formula>Service_Request__r.Email__c</formula>
        <name>Updating email on SR Doc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Auto_Generate_Docs_SR</fullName>
        <apiVersion>31.0</apiVersion>
        <endpointUrl>https://apps.drawloop.com/package/111</endpointUrl>
        <fields>Drawloop_Next__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration@difc.com</integrationUser>
        <name>Auto-Generate Docs_SR</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Application Submission notification - Portal new user - Post Document Generation</fullName>
        <actions>
            <name>SR_submission_for_new_portal_user_creation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>if(AND(Service_Request__r.Internal_Status_Name__c =&apos;Submitted&apos;,ISBLANK(Service_Request__r.Parent_SR__c), TEXT( Service_Request__r.User_Form_Action__c) =&apos;Create New User&apos;,Service_Request__r.RecordType.DeveloperName =&apos;User_Access_Form&apos;,NOT(ISBLANK(Doc_ID__c)), Document_Name__c=&apos;User Access Form&apos;, TEXT(Status__c) =&apos;Generated&apos;),true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto-Generate Docs</fullName>
        <actions>
            <name>Reset_Generate_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Auto_Generate_Docs_SR</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>A work flow to trigger outbound Message to Drawloop</description>
        <formula>Generate_Document__c  = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Document Instruction</fullName>
        <actions>
            <name>Update_Instruction</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(Name=&apos;Entry Permit&apos;,Name=&apos;Cancellation Confirmation&apos;,Name=&apos;Visit Visa&apos;,Name=&apos;Medical Certificate&apos;),NOT(ISBLANK(Doc_ID__c)), Service_Request__r.SR_Group__c =&apos;GS&apos;,ISBLANK(SR_Template_Doc__c),OR(Step__r.Step_Template__r.Code__c =&apos;Entry Permit Application is submitted Online&apos;,Step__r.Step_Template__r.Code__c =&apos;Sponsor form for entry permit is ready for collection&apos;,Step__r.Step_Template__r.Code__c =&apos;Online Entry Permit is Typed&apos;,Step__r.Step_Template__r.Code__c =&apos;Cancellation form typed&apos;,Step__r.Step_Template__r.Code__c =&apos;Visit Visa Application is submitted Online&apos;,Step__r.Step_Template__r.Code__c =&apos;E-Form Visit Visa is Typed&apos;,Step__r.Step_Template__r.Code__c =&apos;Medical Fitness Test scheduled&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy of new visa Upload Notification to BO</fullName>
        <actions>
            <name>Notify_GS_Team_After_Doc_Uploaded</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>SR_Doc__c.Name</field>
            <operation>equals</operation>
            <value>Copy of New visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Status__c</field>
            <operation>equals</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Name</field>
            <operation>equals</operation>
            <value>Health Insurance Certificate</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy_Document_No</fullName>
        <actions>
            <name>Update_Document_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK( Document_No__c ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Health Insurance Certificate Reminder</fullName>
        <actions>
            <name>Update_Trigger_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SR_Doc__c.Name</field>
            <operation>equals</operation>
            <value>Health Insurance Certificate</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Health Insurance Certificate Reminder 1</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>SR_Doc__c.Name</field>
            <operation>equals</operation>
            <value>Health Insurance Certificate</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Trigger_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.External_Status_Name__c</field>
            <operation>notEqual</operation>
            <value>Process Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_for_Health_Insurance_Certificate_upload</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>UpdateTriggerDate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>SR_Doc__c.Trigger_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify GS Team After Doc Upload</fullName>
        <actions>
            <name>Notify_GS_Team_After_Doc_Uploaded</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>SR_Doc__c.Is_Pending_Document__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Status__c</field>
            <operation>equals</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <description>his rule triggers and email alert to the GS Team if the portal user uploaded document which is pending TKT#1359</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify GS Team After Doc Upload after posting to SAP</fullName>
        <actions>
            <name>Notify_GS_Team_After_Doc_Uploaded</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Is_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SR_Doc__c.Is_Pending_Document__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Status__c</field>
            <operation>equals</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.External_Status_Name__c</field>
            <operation>equals</operation>
            <value>Application is pending</value>
        </criteriaItems>
        <description>his rule triggers and email alert to the GS Team if the portal user uploaded document which is pending TKT#1359</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email for Transfer instruments - Completed Property</fullName>
        <actions>
            <name>Transfer_instruments_Completed_Proper</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Request__c.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Freehold_Transfer_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Doc__c.Status__c</field>
            <operation>equals</operation>
            <value>Generated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>RORP</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Pending if Doc Re-Upload</fullName>
        <actions>
            <name>Pending_Doc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule is to set  a document as pending if it is set for Re-Upload</description>
        <formula>ISPICKVAL(Status__c, &apos;Re-upload&apos;) &amp;&amp;  Service_Request__r.SR_Group__c =&apos;GS&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updating SR creator email address</fullName>
        <actions>
            <name>Updating_email_on_SR_Doc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For updating the service request creator email address in the email field in the SR Doc object.</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>