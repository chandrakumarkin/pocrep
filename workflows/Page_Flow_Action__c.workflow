<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Is_Custom_Component_Auto_update</fullName>
        <field>Is_Custom_Component__c</field>
        <literalValue>1</literalValue>
        <name>Is Custom Component Auto update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Is Custom Component Auto update Rule</fullName>
        <actions>
            <name>Is_Custom_Component_Auto_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Page__r.Is_Custom_Component__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>