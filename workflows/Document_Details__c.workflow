<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Document_Detail_SAP_Update_to_True</fullName>
        <field>SAP_Update__c</field>
        <literalValue>1</literalValue>
        <name>Document Detail SAP Update to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Document_Detail_SAP_Update_to_false</fullName>
        <field>SAP_Update__c</field>
        <literalValue>0</literalValue>
        <name>Document Detail SAP Update to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Document Detail Not Updated by SAP</fullName>
        <actions>
            <name>Document_Detail_SAP_Update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Document Detail Not Updated by SAP</description>
        <formula>NOT(ISCHANGED( Updated_from_SAP__c )) &amp;&amp; SAP_Update__c=TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Document Detail Updated by SAP</fullName>
        <actions>
            <name>Document_Detail_SAP_Update_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Document Detail Updated by SAP</description>
        <formula>ISCHANGED( Updated_from_SAP__c ) &amp;&amp; SAP_Update__c=FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>