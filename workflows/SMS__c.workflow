<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Absconder_Confirmation_is_ready_for_collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Absconder Confirmation is ready for collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Absconder_Confirmation_is_ready_for_collection</template>
    </alerts>
    <alerts>
        <fullName>Access_Card_is_Ready_for_Collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Access Card is Ready for Collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Document_Ready_for_Collection</template>
    </alerts>
    <alerts>
        <fullName>Company_Index_Card_nearing_Expiry</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Company Index Card nearing Expiry</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Company_Index_Card_nearing_Expiry</template>
    </alerts>
    <alerts>
        <fullName>Document_Delivery_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Document Delivery Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Documents_Delivery_Notification</template>
    </alerts>
    <alerts>
        <fullName>Document_is_Ready_for_Collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Document is Ready for Collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Document_Ready_for_Collection</template>
    </alerts>
    <alerts>
        <fullName>Employee_passport_is_ready_for_collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Employee passport is ready for collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Employee_passport_is_ready_for_collection</template>
    </alerts>
    <alerts>
        <fullName>Expired_Company_Index_Card_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Expired Company Index Card Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Expired_Company_Index_Card_Notification</template>
    </alerts>
    <alerts>
        <fullName>Expired_Visa_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Expired Visa Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Expired_Visa_Notification</template>
    </alerts>
    <alerts>
        <fullName>Expired_Visa_Reminder</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Expired Visa Reminder</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Expired_Visa_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Family_passport_is_ready_for_collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Family passport is ready for collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Family_passport_is_ready_for_collection</template>
    </alerts>
    <alerts>
        <fullName>Immigration_Form_is_Ready_for_Sponsor_Signature</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Immigration Form is Ready for Sponsor Signature</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Immigration_Form_is_Ready_for_Sponsor_Signature</template>
    </alerts>
    <alerts>
        <fullName>Immigration_Form_is_Ready_for_Sponsor_Signature_Express</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Immigration Form is Ready for Sponsor Signature (Express)</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Immigration_Form_is_Ready_for_Sponsor_Signature_Express</template>
    </alerts>
    <alerts>
        <fullName>Medical_Fitness_Test_Appointment_Notification_Express</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Medical Fitness Test Appointment Notification (Express)</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Medical_Fitness_Test_Appointment_Notification_Express</template>
    </alerts>
    <alerts>
        <fullName>Medical_Fitness_Test_Appointment_Notification_Normal</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Medical Fitness Test Appointment Notification (Normal)</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Medical_Fitness_Test_Appointment_Notification_Normal</template>
    </alerts>
    <alerts>
        <fullName>Medically_unfit_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Medically unfit Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Medically_unfit_Notification</template>
    </alerts>
    <alerts>
        <fullName>Online_Visa_is_Ready</fullName>
        <ccEmails>sms_service@2fhclg3nzex9mkumrioozb5nnwbpizdwzeezhirfbjoee0g015.11-bzmwweaf.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Online Visa is Ready</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Online_Visa_is_Ready</template>
    </alerts>
    <alerts>
        <fullName>Original_Entry_Permit_Ready_for_Collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Original Entry Permit Ready for Collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Original_Entry_Permit_Ready_for_Collection</template>
    </alerts>
    <alerts>
        <fullName>Original_Visit_Visa_is_Ready_for_Collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Original Visit Visa is Ready for Collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Original_Visit_Visa_is_Ready_for_Collection</template>
    </alerts>
    <alerts>
        <fullName>Original_Visit_Visa_is_Ready_for_Collection_Express</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Original Visit Visa is Ready for Collection (Express)</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Original_Visit_Visa_is_Ready_for_Collection_Express</template>
    </alerts>
    <alerts>
        <fullName>Original_entry_permit_has_been_deposited_at_Dubai_international_airport</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Original entry permit has been deposited at Dubai international airport</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Original_entry_permit_has_been_deposited_at_Dubai_international_airport</template>
    </alerts>
    <alerts>
        <fullName>Pending_Application_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Pending Application Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Pending_Requirements_for_application</template>
    </alerts>
    <alerts>
        <fullName>Rejected_visa_application</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Rejected visa application</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Rejected_visa_application</template>
    </alerts>
    <alerts>
        <fullName>Reminder_to_renew_expired_visa</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Reminder to renew expired visa</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Reminder_to_renew_expired_visa</template>
    </alerts>
    <alerts>
        <fullName>The_entry_permit_for_individual_name_has_expired</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>The entry permit for individual name has expired</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/The_entry_permit_for_individual_name_has_expired</template>
    </alerts>
    <alerts>
        <fullName>Violator_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Violator Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Violator_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visa_Transfer_Form_is_Ready_for_Collection</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Visa Transfer Form is Ready for Collection</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Visa_Transfer_Form_is_Ready_for_Collection</template>
    </alerts>
    <alerts>
        <fullName>Visa_nearing_Expiry_Notification</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Visa nearing Expiry Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Visa_nearing_Expiry_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visit_Visa_or_Entry_Permit_is_Cancelled</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Visit Visa or Entry Permit is Cancelled</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Visit_Visa_or_Entry_Permit_is_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>Withdrawal_of_absconder_status_is_completed</fullName>
        <ccEmails>sms_service@jqt2fr00ea9ln75c2q76rcsx4y9vdhmxjh8uzuxw2latesb38.11-8fpkeam.cs18.apex.sandbox.salesforce.com</ccEmails>
        <description>Withdrawal of absconder status is completed</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Withdrawal_of_absconder_status_is_completed</template>
    </alerts>
    <rules>
        <fullName>Absconder Confirmation is ready for collection</fullName>
        <actions>
            <name>Absconder_Confirmation_is_ready_for_collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1039</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Company Index Card nearing Expiry</fullName>
        <actions>
            <name>Company_Index_Card_nearing_Expiry</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1032</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Document Ready for Collection</fullName>
        <actions>
            <name>Document_is_Ready_for_Collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1003,1006,1013,1014,1024,1040,1041,1042,1043,1046,1047,1060</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Documents Delivery Notification</fullName>
        <actions>
            <name>Document_Delivery_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1052,1053,1054,1057,1058,1059</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Employee passport is ready for collection</fullName>
        <actions>
            <name>Employee_passport_is_ready_for_collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1019</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expired Company Index Card Notification</fullName>
        <actions>
            <name>Expired_Company_Index_Card_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1033</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expired Visa Notification</fullName>
        <actions>
            <name>Expired_Visa_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1034</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expired Visa Reminder</fullName>
        <actions>
            <name>Expired_Visa_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1025</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Family passport is ready for collection</fullName>
        <actions>
            <name>Family_passport_is_ready_for_collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1020</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Immigration Form is Ready for Sponsor Signature</fullName>
        <actions>
            <name>Immigration_Form_is_Ready_for_Sponsor_Signature</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This is changed to work only for Express service according to Tkt # 2874</description>
        <formula>AND(CONTAINS(Email_Template_ID__c,&apos;1016&apos;), Service_Request__r.Express_Service__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Immigration Form is Ready for Sponsor Signature  Normal Service</fullName>
        <active>false</active>
        <description>This rule is created to work for Normal Service as per Tkt # 2874</description>
        <formula>AND(CONTAINS(Email_Template_ID__c,&apos;1016&apos;), NOT(Service_Request__r.Express_Service__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Immigration_Form_is_Ready_for_Sponsor_Signature</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Immigration Form is Ready for Sponsor Signature %28Express%29</fullName>
        <actions>
            <name>Immigration_Form_is_Ready_for_Sponsor_Signature_Express</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1055</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Medical Fitness Test Appointment Notification %28Normal%29</fullName>
        <actions>
            <name>Medical_Fitness_Test_Appointment_Notification_Normal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1008</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Medical%C2%A0Fitness Test Appointment Notification%C2%A0%28Express%29</fullName>
        <actions>
            <name>Medical_Fitness_Test_Appointment_Notification_Express</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1007</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Medically unfit Notification</fullName>
        <actions>
            <name>Medically_unfit_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1044</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Online Visa is Ready</fullName>
        <actions>
            <name>Online_Visa_is_Ready</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1012</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Original Entry Permit Ready for Collection</fullName>
        <actions>
            <name>Original_Entry_Permit_Ready_for_Collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1011</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Original Visit Visa is Ready for Collection</fullName>
        <actions>
            <name>Original_Visit_Visa_is_Ready_for_Collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1018</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Original Visit Visa is Ready for Collection %28Express%29</fullName>
        <actions>
            <name>Original_Visit_Visa_is_Ready_for_Collection_Express</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1056</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Original entry permit has been deposited at Dubai international airport</fullName>
        <actions>
            <name>Original_entry_permit_has_been_deposited_at_Dubai_international_airport</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1045</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Application Notification</fullName>
        <actions>
            <name>Pending_Application_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1021,1022,1049,1050,1051</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected visa application</fullName>
        <actions>
            <name>Rejected_visa_application</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1023</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reminder to renew expired visa</fullName>
        <actions>
            <name>Reminder_to_renew_expired_visa</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1029</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>The entry permit for individual name has expired</fullName>
        <actions>
            <name>The_entry_permit_for_individual_name_has_expired</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1035</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Violator Notification</fullName>
        <actions>
            <name>Violator_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1036</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Visa Transfer Form%C2%A0 is Ready for Collection</fullName>
        <actions>
            <name>Visa_Transfer_Form_is_Ready_for_Collection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This is changed to work only for Express service according to Tkt # 2874</description>
        <formula>AND(CONTAINS(Email_Template_ID__c,&apos;1017&apos;), Service_Request__r.Express_Service__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Visa Transfer Form%C2%A0 is Ready for Collection Normal</fullName>
        <active>false</active>
        <description>This rule is created to work for Normal Service as per Tkt # 2874</description>
        <formula>AND(CONTAINS(Email_Template_ID__c,&apos;1017&apos;),NOT( Service_Request__r.Express_Service__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Visa_Transfer_Form_is_Ready_for_Collection</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Visa nearing Expiry Notification</fullName>
        <actions>
            <name>Visa_nearing_Expiry_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1038</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Visit Visa or Entry Permit is Cancelled</fullName>
        <actions>
            <name>Visit_Visa_or_Entry_Permit_is_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>contains</operation>
            <value>1009</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Withdrawal of absconder status is completed</fullName>
        <actions>
            <name>Withdrawal_of_absconder_status_is_completed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SMS__c.Email_Template_ID__c</field>
            <operation>equals</operation>
            <value>1048</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>