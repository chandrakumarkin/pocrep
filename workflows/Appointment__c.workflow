<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Appointment_StartDate_Txt</fullName>
        <field>AppointmentStartDateTxt__c</field>
        <formula>Text(Appointment_StartDate_Time__c)</formula>
        <name>Appointment StartDate Txt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Step_Number</fullName>
        <field>StepNameTxt__c</field>
        <formula>Step__c</formula>
        <name>Update Step Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Appointment Avoid Duplicate</fullName>
        <actions>
            <name>Appointment_StartDate_Txt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Step_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Appointment__c.Appointment_StartDate_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>