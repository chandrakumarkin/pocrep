<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Discounted_package_Approved</fullName>
        <ccEmails>Jodea.Iidefonso@fintechhive.ae</ccEmails>
        <ccEmails>kiran.cornelio@difc.ae</ccEmails>
        <description>Discounted package Approved-Fintech Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>nasser.belhabala@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sharon.plong@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Package_Activated_for</template>
    </alerts>
    <alerts>
        <fullName>Expiry_Notification_for_discounted_package</fullName>
        <ccEmails>Khadija.Ali@difc.ae</ccEmails>
        <description>Expiry Notification for discounted package</description>
        <protected>false</protected>
        <recipients>
            <field>RM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Discounted_Package_Expiry_Notification</template>
    </alerts>
    <alerts>
        <fullName>Package_Approved</fullName>
        <description>Package Request Approved /Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Company_package_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Company_Package_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Company Package Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Package_Approval_Date</fullName>
        <field>Approved_Date__c</field>
        <formula>today()</formula>
        <name>Package Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Package_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Package Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Package_End_Date_Update</fullName>
        <field>End_Date__c</field>
        <formula>today()+366</formula>
        <name>Package End Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Package_End_Date_to_Package_End</fullName>
        <field>End_Date__c</field>
        <formula>today()+Package__r.Duration_in_Days__c</formula>
        <name>Package End Date to Package End</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Package_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Package Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Package_Start_Date</fullName>
        <field>Start_Date__c</field>
        <formula>today()+1</formula>
        <name>Package Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Under_Approval_Request</fullName>
        <field>Status__c</field>
        <literalValue>Under Approval</literalValue>
        <name>Under Approval Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_End_Date</fullName>
        <field>End_Date__c</field>
        <formula>Start_Date__c + Package__r.Duration_in_Days__c</formula>
        <name>Update End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Company Package End Date</fullName>
        <actions>
            <name>Package_End_Date_to_Package_End</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Company_package__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Company_package__c.Package_Code__c</field>
            <operation>notContain</operation>
            <value>VC</value>
        </criteriaItems>
        <description>Set Package end date based on Selected package</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expired Company Package</fullName>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Company_package__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Company_package__c.Status__c</field>
            <operation>equals</operation>
            <value>Default</value>
        </criteriaItems>
        <description>Changed Company Package status to expired one Package End Data #10555</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Company_Package_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Company_package__c.End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Expired Discounted Package Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Company_package__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Company_package__c.Package_Code__c</field>
            <operation>equals</operation>
            <value>VCFundManagerInitial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Company_package__c.Account_SubClassifcation__c</field>
            <operation>equals</operation>
            <value>Venture Capital New Regime</value>
        </criteriaItems>
        <description>This workflow is used to send notification when the package is about to expire (1 month before)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Expiry_Notification_for_discounted_package</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Company_package__c.End_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>VC Company Package End Date</fullName>
        <actions>
            <name>Update_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Company_package__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Company_package__c.Package_Code__c</field>
            <operation>contains</operation>
            <value>VC</value>
        </criteriaItems>
        <description>Set Package end date based on Selected package</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>