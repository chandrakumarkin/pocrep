<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Establishment_Card_Expiry_Fine_Notification</fullName>
        <description>Establishment Card Expiry Fine Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>c-veeranarayana.p@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GS_System_Emails/Establishment_Card_Expiry_Fine_Notification</template>
    </alerts>
    <alerts>
        <fullName>Establishment_Card_Expiry_Notification</fullName>
        <description>Establishment Card Expiry Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Establishment_Card_Expiry_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Establishment_Card_Expiry_Notification</fullName>
        <field>Send_Expiry_Mail__c</field>
        <literalValue>0</literalValue>
        <name>Establishment Card Expiry Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Evaluate</fullName>
        <field>Evaluate__c</field>
        <literalValue>0</literalValue>
        <name>Evaluate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Identification_SAP_Update_to_True</fullName>
        <description>Identification SAP Update to True</description>
        <field>SAP_Update__c</field>
        <literalValue>1</literalValue>
        <name>Identification SAP Update to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Identification_SAP_Update_to_false</fullName>
        <description>Identification SAP Update to false</description>
        <field>SAP_Update__c</field>
        <literalValue>0</literalValue>
        <name>Identification SAP Update to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Index_Card_Status_Expired</fullName>
        <field>Index_Card_Status__c</field>
        <formula>&quot;Expired&quot;</formula>
        <name>Index Card Status Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Index_Card_Status_Update</fullName>
        <field>Index_Card_Status__c</field>
        <formula>IF(Valid_To__c &gt; TODAY(),&apos;Active&apos;,&apos;Expired&apos;)</formula>
        <name>Index Card Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_Email_Fine_is_unchecked</fullName>
        <field>Send_Email_Fine__c</field>
        <literalValue>0</literalValue>
        <name>Send Email Fine is unchecked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Establishment Card Expiry Fine Notification</fullName>
        <actions>
            <name>Establishment_Card_Expiry_Fine_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Identification__c.Send_Email_Fine__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_Fine_is_unchecked</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Identification__c.Fine_Notification_last_Send_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Establishment Card Expiry Notification</fullName>
        <actions>
            <name>Establishment_Card_Expiry_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Establishment_Card_Expiry_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Identification__c.Send_Expiry_Mail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Identification Not Updated by SAP</fullName>
        <actions>
            <name>Identification_SAP_Update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>identification Not Updated by SAP</description>
        <formula>NOT(ISCHANGED( Updated_from_SAP__c )) &amp;&amp; SAP_Update__c=TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Identification Updated by SAP</fullName>
        <actions>
            <name>Identification_SAP_Update_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identification Updated by SAP</description>
        <formula>ISCHANGED( Updated_from_SAP__c ) &amp;&amp; SAP_Update__c=FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Index Card Status Update</fullName>
        <actions>
            <name>Index_Card_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Identification__c.Evaluate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Index_Card_Status__c</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Evaluate</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Index_Card_Status_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Identification__c.Valid_To__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>