<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Case_comments</fullName>
        <field>TR_case_comments__c</field>
        <formula>IF(LEN(Case_Comments__c)&lt;200,Case_Comments__c,&apos;case comments updated&apos;)</formula>
        <name>Update Case comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Case Comments Update</fullName>
        <actions>
            <name>Update_Case_comments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Thompson_Reuters_Check__c.Case_Comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Thompson_Reuters_Check__c.TRCase_No__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Comments Update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>