<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_for_Oppty_Stage_Change</fullName>
        <description>Notification for Oppty Stage Change</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Sector_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Stage_Change_Notification</template>
    </alerts>
    <rules>
        <fullName>Notification to RM and SL on opportunity stage change</fullName>
        <actions>
            <name>Notification_for_Oppty_Stage_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity_Stage_History__c.Stage__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>