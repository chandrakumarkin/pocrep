<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Fine_Reduction_Alert</fullName>
        <description>Fine Reduction Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Principal_User_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_5__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Fine_Reduction</template>
    </alerts>
    <alerts>
        <fullName>Price_Change_Notification_when_SR_Status_is_Draft_or_Submitted</fullName>
        <ccEmails>Deepak.Sachdeva@difc.ae,it.security@difc.ae,Chandrakumar.K@difc.ae</ccEmails>
        <description>Price Change email template when when Price change for GS Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>moosa.chola@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Price_Change_when_SR_Status_is_Draft_or_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Send_Price_change_Notification_To_HOD_change_for_ROC_Product</fullName>
        <ccEmails>Deepak.Sachdeva@difc.ae,it.security@difc.ae,Chandrakumar.K@difc.ae</ccEmails>
        <description>Send Price change Notification To HOD change for ROC Product</description>
        <protected>false</protected>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Price_Change_when_Notification_to_HOD_by_ROC_change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Calculate_Index_Card_Expiry_Months</fullName>
        <field>Price__c</field>
        <formula>CEILING((TODAY() - ServiceRequest__r.Customer__r.Index_Card_Expiry_Date__c) / 30) * Price__c</formula>
        <name>Calculate Index Card Expiry Months</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Calculate_Price_in_USD</fullName>
        <field>Price_in_USD__c</field>
        <formula>IF(ServiceRequest__r.Record_Type_Name__c = &apos;Over_Stay_Fine&apos; &amp;&amp; Product__r.ProductCode=&apos;DNRD Fine&apos;,(ServiceRequest__r.Linked_SR__r.Fine_Amount_AED__c - 250)/3.675,
IF(Pricing_Line__r.Material_Code__c=&apos;GO-00202&apos;,(Price__c+ 40)/3.675,
IF(Pricing_Line__r.Material_Code__c=&apos;FI_5000_FIT04&apos;,Price__c* ServiceRequest__r.Hoarding_Area__c /3.675,
IF(Min_Max_Value__c =false,Price__c/3.675,
IF(Price__c &lt; Pricing_Line__r.Min_Value__c,Pricing_Line__r.Min_Value__c/3.675,IF(Price__c &gt; Pricing_Line__r.Max_Value__c,Pricing_Line__r.Max_Value__c/3.675,Price__c/3.6725))))))</formula>
        <name>Calculate Price in USD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DNRD_amount</fullName>
        <description>IF( (ServiceRequest__r.Linked_SR__r.Fine_Amount_AED__c - 250) &gt; 0, ServiceRequest__r.Linked_SR__r.Fine_Amount_AED__c - 250, 0)</description>
        <field>Price__c</field>
        <formula>IF( (ServiceRequest__r.Linked_SR__r.Fine_Amount_AED__c - 250) &gt; 0, ServiceRequest__r.Linked_SR__r.Fine_Amount_AED__c - 250, 0)</formula>
        <name>DNRD amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Entry_Permit_Guarantee_add_20_AED</fullName>
        <description>Price changed from 20 to 40 per Moosa request</description>
        <field>Price__c</field>
        <formula>Price__c+ 40</formula>
        <name>Entry Permit Guarantee - add 20 AED</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fit_Out_charge_no_of_days</fullName>
        <field>Price__c</field>
        <formula>Price__c * ServiceRequest__r.Quantity__c</formula>
        <name>Fit-Out charge no of days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hoarding_Fee</fullName>
        <field>Price__c</field>
        <formula>Price__c *  ServiceRequest__r.Hoarding_Area__c</formula>
        <name>Hoarding Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Price_Update_min_or_max_value</fullName>
        <field>Price__c</field>
        <formula>IF(Price__c &lt; Pricing_Line__r.Min_Value__c,Pricing_Line__r.Min_Value__c,IF(Price__c &gt; Pricing_Line__r.Max_Value__c,Pricing_Line__r.Max_Value__c,Price__c))</formula>
        <name>Price Update min or max value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quantity_Price</fullName>
        <field>Price__c</field>
        <formula>Price__c  *  ServiceRequest__r.Quantity__c</formula>
        <name>Quantity Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SRPI_FU_001_Copy_VAT_Amount</fullName>
        <description>This field will be used to copy the VAT Amount to the Calculated VAT field</description>
        <field>Calculated_VAT__c</field>
        <formula>VAT_Amount__c</formula>
        <name>SRPI-FU-001 - Copy VAT Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SRPI_FU_002_Copy_Total_Service_Amount</fullName>
        <description>This field update will copy the total service amount value to the calculated total service amount</description>
        <field>Calculated_Total_Service_Amount__c</field>
        <formula>Total_Service_Amount__c</formula>
        <name>SRPI-FU-002 - Copy Total Service Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SRPI_FU_003_Copy_VAT_Amount</fullName>
        <description>This will copy the old vat amount to the calculated vat field</description>
        <field>Calculated_VAT__c</field>
        <formula>PRIORVALUE(  VAT_Amount__c  )</formula>
        <name>SRPI-FU-003 - Copy VAT Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SRPI_FU_004_Copy_Total_Service_Amount</fullName>
        <description>This field update will copy the prior value of the total service amount</description>
        <field>Calculated_Total_Service_Amount__c</field>
        <formula>PRIORVALUE(  Total_Service_Amount__c  )</formula>
        <name>SRPI-FU-004 - Copy Total Service Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SR_Price_0</fullName>
        <field>Price__c</field>
        <formula>0</formula>
        <name>SR Price 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Difc_margin_to_Difc_Margin_Amount</fullName>
        <field>DIFC_Margin__c</field>
        <formula>Difc_Margin_Amount__c</formula>
        <name>Update Difc margin to Difc Margin Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Old_Price_when_SR_Approved_Sub</fullName>
        <description>Update Old Price when SR is Approved or submitted</description>
        <field>Old_Price__c</field>
        <formula>PRIORVALUE( Price__c )</formula>
        <name>Update Old Price when SR Approved_Sub</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Price_for_Refund_SR</fullName>
        <field>Price__c</field>
        <formula>ServiceRequest__r.Refund_Amount__c</formula>
        <name>Update Price for Refund SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SR_Price_Item_Price_to_0</fullName>
        <field>Price__c</field>
        <formula>0</formula>
        <name>Update SR Price Item Price to &apos;0&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_old_price_item</fullName>
        <field>Previous_Value__c</field>
        <formula>PRIORVALUE( Price__c)</formula>
        <name>Update old price item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Visa_Exception_is_True</fullName>
        <field>Is_Visa_Exception__c</field>
        <literalValue>1</literalValue>
        <name>Visa Exception is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Calculate Index Card Expiry Months</fullName>
        <actions>
            <name>Calculate_Index_Card_Expiry_Months</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate index card expiry months</description>
        <formula>OR(AND( ServiceRequest__r.RecordType.DeveloperName = &quot;Index_Card_Other_Services&quot;, OR( ISPICKVAL(ServiceRequest__r.Service_Category__c, &quot;Renewal&quot;), ISPICKVAL(ServiceRequest__r.Service_Category__c, &quot;Renewal &amp; Amendment&quot;), ISPICKVAL(ServiceRequest__r.Service_Category__c, &quot;Renewal &amp; Lost / Replacement&quot;) ,ISPICKVAL(ServiceRequest__r.Service_Category__c, &quot;Renewal, Amendment &amp; Lost / Replacement&quot;) ,ISPICKVAL(ServiceRequest__r.Service_Category__c, &quot;Cancellation&quot;) ), Product__r.Name = &quot;Index Card Fine&quot;,Text(ServiceRequest__r.Is_Applicant_Salary_Above_AED_20_000__c) !=&apos;Yes&apos;,ISBlank(ServiceRequest__r.Linked_SR__c)),AND(ServiceRequest__r.RecordType.DeveloperName = &quot;License_Renewal&quot;,Product__r.Name = &quot;Index Card Fine&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Calculate the Price in USD</fullName>
        <actions>
            <name>Calculate_Price_in_USD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>SR_Price_Item__c.Price__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.Fine_Amount_AED__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To populate the Price( in USD) field with the converted value from Price which is in AED</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Entry Permit Guarantee - add 20 AED</fullName>
        <actions>
            <name>Entry_Permit_Guarantee_add_20_AED</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Pricing_Line__r.Material_Code__c=&apos;GO-00202&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fine Reduction</fullName>
        <actions>
            <name>Fine_Reduction_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Allow user to reduce the fine amount 5620</description>
        <formula>ISCHANGED(Price__c)&amp;&amp;NOT(ISNULL(Old_Price__c)) &amp;&amp; SendNotification__c &amp;&amp;  ServiceRequest__r.SR_Group__c ==&apos;ROC&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Hoarding Fee</fullName>
        <actions>
            <name>Hoarding_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Pricing_Line__r.Material_Code__c=&apos;FI_5000_FIT04&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Difc Margin Amount</fullName>
        <actions>
            <name>Update_Difc_margin_to_Difc_Margin_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SR_Price_Item__c.Difc_Margin_Amount__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <description>To populate the difc margin from difc margin amount in SR Price Item</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate min or max value</fullName>
        <actions>
            <name>Price_Update_min_or_max_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Min_Max_Value__c =true &amp;&amp;  (NOT(ISBLANK(Pricing_Line__r.Min_Value__c)) &amp;&amp; Price__c &lt; Pricing_Line__r.Min_Value__c)|| (NOT(ISBLANK(Pricing_Line__r.Max_Value__c)) &amp;&amp; Price__c &gt; Pricing_Line__r.Max_Value__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pre Go Live Price to 0</fullName>
        <actions>
            <name>SR_Price_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Request__c.Pre_GoLive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Renewal of FitOut Permit Fee</fullName>
        <actions>
            <name>Fit_Out_charge_no_of_days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Pricing_Line__r.Material_Code__c=&apos;FI_5000_FIT05&apos; &amp;&amp;  ISPICKVAL(Status__c,&apos;Added&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SRPI-WFR-001 - Copy Field on Creation</fullName>
        <actions>
            <name>SRPI_FU_001_Copy_VAT_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SRPI_FU_002_Copy_Total_Service_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will copy the field from one to another on creation of the record</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SRPI-WFR-002 - Copy Field on Pricing Line Change</fullName>
        <actions>
            <name>SRPI_FU_003_Copy_VAT_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SRPI_FU_004_Copy_Total_Service_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will copy the field from one to another on change of pricing line</description>
        <formula>ISCHANGED(  Pricing_Line__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Price change Notification To HOD change for GS Product</fullName>
        <actions>
            <name>Price_Change_Notification_when_SR_Status_is_Draft_or_Submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Old_Price_when_SR_Approved_Sub</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send Price Change Notification when Price change for GS Team</description>
        <formula>AND(ISCHANGED( Price__c ) ,  ISBLANK($User.ContactId), ServiceRequest__r.SR_Group__c = &apos;GS&apos;, OR($Profile.Name == &apos;System Administrator&apos;, $Profile.Name == &apos;Super System Administrator&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Price change Notification To HOD change for ROC Product</fullName>
        <actions>
            <name>Send_Price_change_Notification_To_HOD_change_for_ROC_Product</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Old_Price_when_SR_Approved_Sub</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send Price change Notification To HOD if any change is requested for pricing change by ROC team</description>
        <formula>AND(ISCHANGED( Price__c ) ,  ISBLANK($User.ContactId),  ServiceRequest__r.SR_Group__c = &apos;ROC&apos;,  OR($Profile.Name == &apos;System Administrator&apos;,  $Profile.Name == &apos;Super System Administrator&apos;  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Track Old Price Item Value</fullName>
        <actions>
            <name>Update_old_price_item</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Price__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update DNRD Fine in Price Item</fullName>
        <actions>
            <name>DNRD_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update DNRD Fine in Price Item</description>
        <formula>AND(ServiceRequest__r.Record_Type_Name__c = &apos;Over_Stay_Fine&apos;, ServiceRequest__r.External_SR_Status__r.Code__c=&apos;DRAFT&apos;, Product__r.ProductCode=&apos;DNRD Fine&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Is Visa Exception to True</fullName>
        <actions>
            <name>Visa_Exception_is_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SR_Price_Item__c.SRPriceLine_Text__c</field>
            <operation>equals</operation>
            <value>Visa Exception - 3 Years</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Refund Amount in Price Item</fullName>
        <actions>
            <name>Update_Price_for_Refund_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Refund Amount in Price Item</description>
        <formula>ServiceRequest__r.Record_Type_Name__c = &apos;Refund_SR&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update SR Price Item Price to %270%27</fullName>
        <actions>
            <name>Update_SR_Price_Item_Price_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISBLANK( ServiceRequest__r.CreatedBy.ContactId), OR( ServiceRequest__r.Record_Type_Name__c = &apos;Notification_of_Personal_Data_Operations&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>