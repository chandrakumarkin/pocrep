<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Listing_Email_Remainder_for_Listing_Expiration</fullName>
        <description>Listing : Email Remainder for Listing Expiration</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Listing_Email_Reminder_for_Listing_Expire</template>
    </alerts>
    <fieldUpdates>
        <fullName>Featured_till</fullName>
        <field>Featured__c</field>
        <literalValue>0</literalValue>
        <name>Featured till</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Listing_Status_to_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update the Listing Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Featured till</fullName>
        <active>false</active>
        <description>This workflow updated the featured to false if the date has been reached till the time it was asked to be featured.</description>
        <formula>AND(Featured_till__c  &gt;=  TODAY(), Featured__c =True)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Featured_till</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Listing__c.Featured_till__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Listing %3A Email Reminder before 10 Days</fullName>
        <active>false</active>
        <description>Listing : Email Reminder before 10 Days</description>
        <formula>AND(TEXT(Status__c)=&apos;Active&apos;,End_Date__c -  TODAY() &gt;= 10)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Listing_Email_Remainder_for_Listing_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Listing__c.End_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Listing %3A Email Reminder before 5 Days</fullName>
        <active>false</active>
        <description>Listing : Email Reminder before 5 Days</description>
        <formula>AND(TEXT(Status__c)=&apos;Active&apos;,End_Date__c -  TODAY() &gt;= 5)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Listing_Email_Remainder_for_Listing_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Listing__c.End_Date__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Listing %3A Email Reminder on End Date</fullName>
        <active>false</active>
        <description>Listing : Email Reminder on End Date</description>
        <formula>AND(TEXT(Status__c)=&apos;Active&apos;,End_Date__c &gt;= TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Listing_Email_Remainder_for_Listing_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Listing__c.End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Property Listing Expire</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Listing__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Listing__c.End_Date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Listing__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This will be timebased workflow which will fire on End Date of listing and updates the status to Expired</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_the_Listing_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Listing__c.End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>