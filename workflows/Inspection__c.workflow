<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_To_Inspector_For_More_Information</fullName>
        <description>Notification To Inspector For More Information</description>
        <protected>false</protected>
        <recipients>
            <recipient>ammar.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_with_Extra_info/HTML_Notification_to_Inspector_regarding_more_information</template>
    </alerts>
    <alerts>
        <fullName>Notification_in_case_the_entity_is_in_good_standing</fullName>
        <description>Notification in case the entity is in good standing</description>
        <protected>false</protected>
        <recipients>
            <recipient>elhoussein.a@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_with_Extra_info/HTML_Notification_in_case_the_entity_is_in_good_standing</template>
    </alerts>
    <alerts>
        <fullName>Notification_incase_the_ROC_view_the_company_in_good_standing_and_wants_to_chang</fullName>
        <description>Notification incase the ROC view the company in good standing and wants to change it – to be sent</description>
        <protected>false</protected>
        <recipients>
            <recipient>elhoussein.a@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_with_Extra_info/HTML_Notification_incase_the_ROC_view_the_company_in_good_standing_want_change</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Team_and_ROC_help_desk_once_the_approver_approves_that_the_entit</fullName>
        <description>Notification in case the entity is in good standing</description>
        <protected>false</protected>
        <recipients>
            <recipient>elhoussein.a@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_with_Extra_info/HTML_Notification_in_case_the_entity_is_in_good_standing</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Teams_and_ROC_help_desk_once_the_approver_approves_that_the_enti</fullName>
        <description>Notification to Team and ROC help desk, once the approver approves that the entity is not in good standing</description>
        <protected>false</protected>
        <recipients>
            <recipient>elhoussein.a@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_with_Extra_info/HTML_Notification_to_Team_and_ROC_help_desk_once_the_approver_approves</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Do_Not_Trigger_Email</fullName>
        <field>Do_Not_Trigger_Email__c</field>
        <literalValue>1</literalValue>
        <name>Update Do Not Trigger Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_To_Resubmit_For_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Information Updated</literalValue>
        <name>Update Status To Resubmit For Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>More Information Updated For Approval</fullName>
        <actions>
            <name>Notification_To_Inspector_For_More_Information</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Do_Not_Trigger_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_To_Resubmit_For_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Status__c, &apos;Need More Information&apos;),ISPICKVAL(NonRegulated_Q24__c, &apos;Yes&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification in case the entity is in good standing</fullName>
        <actions>
            <name>Notification_in_case_the_entity_is_in_good_standing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.NonRegulated_Q24__c</field>
            <operation>contains</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inspection__c.Status__c</field>
            <operation>notContain</operation>
            <value>Draft,Awaiting for Client Response,Need More Information,Submitted for Approval,Information Updated,Approved,Submitted,Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inspection__c.Do_Not_Trigger_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Send Notification in case the entity is in good standing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification incase the ROC view the company in good standing and wants to change it %E2%80%93 to be sent</fullName>
        <actions>
            <name>Notification_incase_the_ROC_view_the_company_in_good_standing_and_wants_to_chang</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.Status__c</field>
            <operation>contains</operation>
            <value>Need More Information</value>
        </criteriaItems>
        <description>Send Notification incase the ROC view the company in good standing and wants to change it – to be sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Team and ROC help desk once the approver approves</fullName>
        <actions>
            <name>Notification_to_Teams_and_ROC_help_desk_once_the_approver_approves_that_the_enti</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.Do_Not_Trigger_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inspection__c.Status__c</field>
            <operation>contains</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to team when entity is in good standing</fullName>
        <actions>
            <name>Notification_to_Team_and_ROC_help_desk_once_the_approver_approves_that_the_entit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.Status__c</field>
            <operation>contains</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inspection__c.NonRegulated_Q24__c</field>
            <operation>contains</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inspection__c.Do_Not_Trigger_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification to team when entity is in good standing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>