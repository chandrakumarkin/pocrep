<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_on_approve</fullName>
        <description>Email on approve</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Master_Approval</template>
    </alerts>
    <alerts>
        <fullName>Email_on_rejection_of_the_approval</fullName>
        <description>Email on rejection of the approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Master_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_by_HOD</fullName>
        <field>Status__c</field>
        <literalValue>Approved by HOD</literalValue>
        <name>Update Status to &quot;Approved by HOD&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_CancellaApproved_by_HOD</fullName>
        <field>Status__c</field>
        <literalValue>Cancellation Approved</literalValue>
        <name>Update Status to &quot;Cancellation Approved&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected by HOD</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_UnderReview</fullName>
        <field>Status__c</field>
        <literalValue>Under Review</literalValue>
        <name>Update Status to Under Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>