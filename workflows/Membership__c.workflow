<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Car_Parking_Membership_Expired</fullName>
        <description>Car Parking Membership Expired</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Car_Parking_Expired_Notification</template>
    </alerts>
    <alerts>
        <fullName>Expiry_Notification</fullName>
        <description>This email alert is used to send email about expiry of membership</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Expiry_Notification</template>
    </alerts>
    <alerts>
        <fullName>Membership_Activation</fullName>
        <description>Membership Activation</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Activation_Notification</template>
    </alerts>
    <rules>
        <fullName>Car Parking Activation Notification</fullName>
        <actions>
            <name>Membership_Activation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to notify the recipients about activation of membership</description>
        <formula>AND( ISCHANGED(MembershipStatus__c ), ISPICKVAL( PRIORVALUE(MembershipStatus__c ),&apos;Expired&apos;) ,ISPICKVAL( MembershipStatus__c , &apos;Active&apos;), RecordType.Name = &apos;Membership&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Car Parking Expired Notification</fullName>
        <actions>
            <name>Car_Parking_Membership_Expired</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to notify the recipients about membership expiry</description>
        <formula>AND( ISCHANGED(MembershipStatus__c ), ISPICKVAL( PRIORVALUE(MembershipStatus__c ),&apos;Active&apos;) ,ISPICKVAL( MembershipStatus__c , &apos;Expired&apos;), RecordType.Name = &apos;Membership&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Car Parking Expiry Notification</fullName>
        <actions>
            <name>Expiry_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to notify the recipients about membership expiry date 7 days ago</description>
        <formula>AND(ISPICKVAL( MembershipStatus__c , &apos;Active&apos;),  NOT(ISBLANK(ToDate__c)),(ToDate__c - TODAY() = 7))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>