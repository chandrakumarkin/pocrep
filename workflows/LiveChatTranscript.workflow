<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Customer_the_Transcript_body</fullName>
        <description>Email Customer the Transcript body</description>
        <protected>false</protected>
        <recipients>
            <field>Visitor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Snap_in_Live_Chat/Live_Chat_Email</template>
    </alerts>
    <rules>
        <fullName>Email Customer the Transcript body once chat is ended</fullName>
        <actions>
            <name>Email_Customer_the_Transcript_body</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>LiveChatTranscript.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>LiveChatTranscript.Visitor_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>