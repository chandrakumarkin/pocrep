<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Building_Name</fullName>
        <field>Building_Name__c</field>
        <formula>Building__r.Name</formula>
        <name>Building Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update building name</fullName>
        <actions>
            <name>Building_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Building__c !=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>