<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_User_Email</fullName>
        <description>New User Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Pre_User_Access_Form_New</template>
    </alerts>
    <outboundMessages>
        <fullName>Auto_Generate_UserSR_Doc</fullName>
        <apiVersion>32.0</apiVersion>
        <endpointUrl>https://apps.drawloop.com/package/111</endpointUrl>
        <fields>Drawloop_Next__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>integration@difc.com</integrationUser>
        <name>Auto-Generate UserSR Doc</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Auto-Generate User Doc</fullName>
        <actions>
            <name>Auto_Generate_UserSR_Doc</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>A workflow to generate user doc</description>
        <formula>Generate_Doc__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pre-User Access Form</fullName>
        <actions>
            <name>New_User_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_SR__c.DDP_Status__c</field>
            <operation>equals</operation>
            <value>Generated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>