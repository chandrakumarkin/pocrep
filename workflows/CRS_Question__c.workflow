<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CRS_Question_Email</fullName>
        <ccEmails>PortalSupport@difc.ae</ccEmails>
        <description>CRS Question Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/CRS_Compliance_Form</template>
    </alerts>
    <rules>
        <fullName>CRS Question Submit</fullName>
        <actions>
            <name>CRS_Question_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CRS_Question__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>