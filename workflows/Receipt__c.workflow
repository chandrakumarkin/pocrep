<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_notification_to_all_portal_users_when_an_amount_is_topped_up</fullName>
        <description>Send notification to all portal users when an amount is topped up</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Portal_User_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Portal_User_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Portal_User_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Portal_User_5__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notification_For_Portal_Recharge</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Moved_to_SAP_checkbox</fullName>
        <field>Moved_to_SAP__c</field>
        <literalValue>1</literalValue>
        <name>Check Moved to SAP checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Receipt_Record_Type_to_Card</fullName>
        <description>Update Receipt Record Type to Card</description>
        <field>RecordTypeId</field>
        <lookupValue>Card</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Receipt Record Type to Card</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Notification For Portal Recharge</fullName>
        <actions>
            <name>Send_notification_to_all_portal_users_when_an_amount_is_topped_up</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notification to all portal users when an amount is topped up</description>
        <formula>Pushed_to_SAP__c = true &amp;&amp; Sys_Send_Email__c = true &amp;&amp;  TEXT(Payment_Status__c) = &apos;Success&apos; &amp;&amp;  Customer__r.BP_No__c  &lt;&gt; &apos;0000400042&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Moved to SAP flag</fullName>
        <actions>
            <name>Check_Moved_to_SAP_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Receipt__c.Moved_to_SAP_by__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Receipt__c.Payment_Status__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type for Card Type</fullName>
        <actions>
            <name>Update_Receipt_Record_Type_to_Card</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Receipt__c.Receipt_Type__c</field>
            <operation>equals</operation>
            <value>Card</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>