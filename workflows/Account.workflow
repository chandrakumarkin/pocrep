<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Square_feet_change_Notification</fullName>
        <ccEmails>c-Saikalyan.s@difc.ae</ccEmails>
        <description>Account Square feet change Notification</description>
        <protected>false</protected>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Account_Square_feet_change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_for_New_PO_Box</fullName>
        <ccEmails>c-swati.sehrawat@difc.ae</ccEmails>
        <description>Application for New PO Box</description>
        <protected>false</protected>
        <recipients>
            <field>E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_for_New_PO_Box</template>
    </alerts>
    <alerts>
        <fullName>Company_Registered_Notification</fullName>
        <description>Company Registered Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>khadija.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All_with_Extra_info/Company_registration_Notification</template>
    </alerts>
    <alerts>
        <fullName>Dissolved_Voluntary_Strike_off_Notification</fullName>
        <description>Dissolved - Voluntary Strike off Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ROC_Officer</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Voluntary_strike_off_is_completed</template>
    </alerts>
    <alerts>
        <fullName>Email_to_SF_Admin</fullName>
        <description>Email to SF Admin</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/SF_admin_Notification</template>
    </alerts>
    <alerts>
        <fullName>Fund_Company_Not_setup_notification</fullName>
        <ccEmails>DIFCRegistries@difc.ae</ccEmails>
        <ccEmails>Khalid.AlZarouni@difc.ae</ccEmails>
        <description>Fund Company Not setup notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>RoC_Team_Notification</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>kevin.atterbury@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khadija.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>linda.brooker@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Fund_Company_Not_setup_notification</template>
    </alerts>
    <alerts>
        <fullName>Fund_Company_Not_setup_notification_11_months</fullName>
        <ccEmails>DIFCRegistries@difc.ae</ccEmails>
        <ccEmails>Khalid.AlZarouni@difc.ae</ccEmails>
        <description>Fund Company Not setup notification 11 months</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>RoC_Team_Notification</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>kevin.atterbury@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khadija.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>linda.brooker@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Fund_Company_Not_setup_notification_expired</template>
    </alerts>
    <alerts>
        <fullName>New_Company_Signup_for_Co_working</fullName>
        <ccEmails>Jodea.Iidefonso@fintechhive.ae,kiran.cornelio@difc.ae,Sharon.PLong@difc.ae, Nasser.Belhabala@difc.ae</ccEmails>
        <description>New Company Signup for Co-working</description>
        <protected>false</protected>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fintech/New_Company_Signup_for_Co_working</template>
    </alerts>
    <alerts>
        <fullName>New_user_access_request_form</fullName>
        <ccEmails>Khushboo.Tahilramani@difc.ae,Khadija.Ali@difc.ae,roc.helpdesk@difc.ae</ccEmails>
        <description>New user access request form</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Initial_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/New_User_Access_Form</template>
    </alerts>
    <alerts>
        <fullName>New_user_access_request_form_Retail</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <ccEmails>SpecialtyLeasing@difc.ae</ccEmails>
        <description>New user access request form Retail</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Initial_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/New_User_Access_Form</template>
    </alerts>
    <alerts>
        <fullName>Notification</fullName>
        <description>Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Exempted_from_UBO_Regulations_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_Account_Creation</fullName>
        <description>Notification for Account Creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>RoC_Officer</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>difcadmin@nsigulf.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Account_Creation_RoC_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_Account_Transfer</fullName>
        <description>Notification for Account Transfer</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Sector_Lead_lookup__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Account_Transfer_RoC_Notification</template>
    </alerts>
    <alerts>
        <fullName>PSA_Negative_Notification</fullName>
        <description>PSA Negative Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>c-veeranarayana.p@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PSA_Negative_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Portal_link_to_Customer</fullName>
        <ccEmails>difcuseraccess@difc.ae</ccEmails>
        <description>Send Portal link to Customer</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Address_to_send_Portal_Link__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Send_Portal_link_Customer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Exempted_from_UBO_Regulations</fullName>
        <field>Exempted_from_UBO_Regulations__c</field>
        <literalValue>1</literalValue>
        <name>Account Exempted from UBO Regulations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Master_Data_Update_Push_to_SAP</fullName>
        <field>Push_SAP_Update__c</field>
        <formula>NOW()</formula>
        <name>Account Master Data Update Push to SAP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Authorization_Group_Field</fullName>
        <description>Authorization Group update to BDD Team</description>
        <field>Authorization_Group__c</field>
        <literalValue>BDD Team</literalValue>
        <name>Account Update Authorization Group Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Attrition_Date_for_Inactive_Struck_Of</fullName>
        <field>Attrition_Override__c</field>
        <formula>Today()</formula>
        <name>Update Attrition Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DFSA_License_Status</fullName>
        <field>DFSA_License_Status__c</field>
        <literalValue>Active</literalValue>
        <name>DFSA License Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DFSA_License_Status_Withdrawn</fullName>
        <field>DFSA_License_Status__c</field>
        <literalValue>Withdrawn</literalValue>
        <name>DFSA License Status Withdrawn</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DFSA_and_Share</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DFSA_and_Share</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DFSA and Share</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DFSA_not_Share</fullName>
        <field>RecordTypeId</field>
        <lookupValue>DFSA_not_Share</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DFSA not Share</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dissolution_Date_Update</fullName>
        <field>Liquidated_Date__c</field>
        <formula>today()</formula>
        <name>Dissolution Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Event_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Event Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FRC_layout_V1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>FRC_Companies</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FRC_layout_V1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Foundation_NPIO_Layout_v1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Foundation_NPIO_Companies</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Foundation_NPIO_Layout_v1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>General_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>General</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>General Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LLP_GP_LP_layout_V1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LLP_GP_LP_companies</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LLP_GP_LP_layout_V1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LTD_IC_PCC_Layout_v1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LTD_IC_PCC_Companies</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>LTD_IC_PCC_Layout_v1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_PR_Push_to_Now</fullName>
        <field>Last_PR_Push__c</field>
        <formula>NOW()</formula>
        <name>Last PR Push to Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>License_Activity_Changed</fullName>
        <field>License_Activity_Updated_Time__c</field>
        <formula>NOW()</formula>
        <name>License Activity Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_Min_capital_requirements</fullName>
        <description>No Min capital requirements ?</description>
        <field>Min_capital_requirements__c</field>
        <literalValue>1</literalValue>
        <name>No Min capital requirements</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Non_Registered_Accounts</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Non_Registered_Accounts</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Non Registered Accounts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_DFSA_Not_Share</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Not_DFSA_not_Share</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Not DFSA Not Share</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_DFSA_and_Share</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Not_DFSA_and_Share</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Not DFSA and Share</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Commercial_permission_record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Commercial_Permission</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OB Commercial permission record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Dissolution_Date</fullName>
        <field>Pending_Dissolution_Date__c</field>
        <formula>Today()</formula>
        <name>Pending Dissolution Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Private_Public_National_Supra_National_V</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Private_Public_National_Supra</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Private_Public_National_Supra National V</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Push_to_PR_Update</fullName>
        <field>Last_PR_Push__c</field>
        <formula>Now()</formula>
        <name>Push to PR Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RLP_RLLP_RP_layout_V1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RLP_RLLP_RP_Companies</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RLP__RLLP_RP_layout_V1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ROC_Number</fullName>
        <field>Registration_License_No__c</field>
        <formula>Active_License__r.Name</formula>
        <name>ROC Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ROC_Populate_Initial_Allotment_Check</fullName>
        <field>Initial_Allotment_of_Shares_Done__c</field>
        <literalValue>1</literalValue>
        <name>ROC- Populate Initial Allotment Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ROC_Status_to_Not_Renewed</fullName>
        <field>ROC_Status__c</field>
        <literalValue>Not Renewed</literalValue>
        <name>ROC Status to Not Renewed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ROC_status_to_Voluntary_Strike_off</fullName>
        <description>Changed ROC status to Voluntary Strike off after 90 days from pending</description>
        <field>ROC_Status__c</field>
        <literalValue>Dissolved - Voluntary Strike off</literalValue>
        <name>ROC status to Voluntary Strike off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Registered_Address</fullName>
        <field>Registered_Address__c</field>
        <formula>IF(Office__c!=&apos;&apos;,Office__c &amp; &apos;, &apos;,&apos;&apos;) &amp; IF(Building_Name__c!=&apos;&apos;,Building_Name__c &amp; &apos;, &apos;,&apos;&apos;)&amp;
IF(Street__c!=&apos;&apos;, Street__c &amp; &apos;, &apos;,&apos;&apos;) &amp;  City__c &amp; &apos;, &apos; &amp; PO_Box__c &amp; &apos;, &apos; &amp;  TEXT( Country__c)</formula>
        <name>Registered Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Discount</fullName>
        <description>Remove discount flag from account after 600 days</description>
        <field>Discount_Applied__c</field>
        <literalValue>No</literalValue>
        <name>Remove Discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Update_to_True</fullName>
        <field>SAP_Update__c</field>
        <literalValue>1</literalValue>
        <name>SAP Update to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Update_to_false</fullName>
        <field>SAP_Update__c</field>
        <literalValue>0</literalValue>
        <name>SAP Update to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Secupass_Push_Time</fullName>
        <field>Secupass_Push__c</field>
        <formula>NOW()</formula>
        <name>Secupass Push Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Authorization_Group</fullName>
        <field>Authorization_Group__c</field>
        <literalValue>BDD Team</literalValue>
        <name>Set Authorization Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Authorization_Group_for_Events</fullName>
        <field>Authorization_Group__c</field>
        <literalValue>Event Management</literalValue>
        <name>Set Authorization Group for Events</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_change_pending_dissolution_Date</fullName>
        <field>Status_change_to_pending_dissolution__c</field>
        <formula>TODAY()</formula>
        <name>Status change  pending dissolution Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TempField_update</fullName>
        <field>TempField__c</field>
        <formula>IF(NOT(ISBLANK(PRIORVALUE(TempField__c))),PRIORVALUE(TempField__c)&amp;&apos;; &apos;,&apos;&apos;)&amp; TempField__c</formula>
        <name>TempField update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTEODiscount</fullName>
        <description>This field is used to update TEO discount when TEO Entity flag is selected</description>
        <field>TEO_Discount__c</field>
        <literalValue>1</literalValue>
        <name>UpdateTEODiscount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_to_Re_push_to_SAP</fullName>
        <field>Re_push_Update_to_SAP__c</field>
        <literalValue>1</literalValue>
        <name>Update Account to Re-push to SAP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IS_PR_Push_to_False</fullName>
        <field>Is_PR_Push__c</field>
        <literalValue>0</literalValue>
        <name>Update IS PR Push to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ROC_Status_to_Strike_Off</fullName>
        <field>ROC_Status__c</field>
        <literalValue>Inactive - Struck Off</literalValue>
        <name>Update ROC Status to Strike Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Security_Status_to_Expire</fullName>
        <description>after 3 months of security review date set this field as expired</description>
        <field>Security_Review_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update Security Status to Expire</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Share_Capital</fullName>
        <field>Share_Capital_Updated__c</field>
        <formula>NOW()</formula>
        <name>Update Share Capital</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Account Master Data Update Push to SAP</fullName>
        <actions>
            <name>Account_Master_Data_Update_Push_to_SAP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Last_PR_Push_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Custom workflow to control on when to push an update to SAP on following fields (mentioned in criteria) changes</description>
        <formula>AND( TEXT(ROC_Status__c) != &apos;&apos;,  OR( ISCHANGED(Company_Type__c), ISCHANGED(Sys_Name_Org1__c), ISCHANGED(Sys_Name_Org2__c), ISCHANGED(Sys_Name_Org3__c), ISCHANGED(Sys_Name_Org4__c), ISCHANGED(ROC_reg_incorp_Date__c), ISCHANGED(Liquidated_Date__c), ISCHANGED(Company_Type__c), ISCHANGED(BD_Sector__c	), ISCHANGED(Priority_to_DIFC__c), ISCHANGED(Geographic_Region__c), ISCHANGED(Legal_Type_of_Entity__c), ISCHANGED(DFSA_Lic_InPrAppDate__c), ISCHANGED(DFSA_Lic_Appr_Date__c), ISCHANGED(OwnerId), ISCHANGED(ROC_Status__c), ISCHANGED(Nature_of_business__c), ISCHANGED(Office__c), ISCHANGED(Building_Name__c), ISCHANGED(Street__c), ISCHANGED(City__c), ISCHANGED(Country__c), ISCHANGED(Postal_Code__c), ISCHANGED(PO_Box__c), ISCHANGED(Phone), ISCHANGED(Fax), ISCHANGED(DFSA_Lic_Appr_Date__c), ISCHANGED(E_mail__c), ISCHANGED(Website) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Not Updated by SAP</fullName>
        <actions>
            <name>SAP_Update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISCHANGED( Updated_from_SAP__c )) &amp;&amp;  SAP_Update__c=TRUE &amp;&amp;  $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Square feet change Notification</fullName>
        <actions>
            <name>Account_Square_feet_change_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Total_Square_Feet__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Updated by SAP</fullName>
        <actions>
            <name>SAP_Update_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Updated_from_SAP__c ) &amp;&amp; SAP_Update__c=FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign Authorization Group</fullName>
        <actions>
            <name>Set_Authorization_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Account_RecType__c)) &amp;&amp; Account_RecType__c != &apos;Event Leads&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type-DFSA %26 Share</fullName>
        <actions>
            <name>DFSA_and_Share</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>LLC,LTD,LTD PCC,LTD IC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type-DFSA not Share</fullName>
        <actions>
            <name>DFSA_not_Share</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>LTD National,LTD Supra National,LLP,RLLP,FRC,GP,RP,RLP,LP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type-Event Customers</fullName>
        <actions>
            <name>Event_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Authorization_Group_for_Events</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Account_RecType__c =&apos;Event&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type-Non Registered Accounts</fullName>
        <actions>
            <name>Non_Registered_Accounts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(TEXT(ROC_Status__c)) &amp;&amp; For_Updates__c =false &amp;&amp; RecordType.Name&lt;&gt;&apos;System Account&apos; &amp;&amp; RecordType.Name&lt;&gt;&apos;RORP Account&apos; &amp;&amp; RecordType.Name&lt;&gt;&apos;Contractor Account&apos; &amp;&amp; RecordType.Name&lt;&gt;&apos;Event Account&apos; &amp;&amp; ISBLANK( RecordTypeId )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type-Not DFSA %26 Share</fullName>
        <actions>
            <name>Not_DFSA_and_Share</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>LTD SPC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type-Not DFSA Not Share</fullName>
        <actions>
            <name>Not_DFSA_Not_Share</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>Foundation,NPIO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change in Authroized Share</fullName>
        <actions>
            <name>Update_Share_Capital</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Authorized_Share_Capital__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change the ROC Status to Strike Off</fullName>
        <active>true</active>
        <description>Once ROC has raised the Stike Off to any Customer, customer have to raise an objection with in 90 days other wise the ROC Status will become &quot;Strike Off&quot;</description>
        <formula>AND( NOT(ISBLANK(Strike_Off_Initiation_Date__c)),NOT(Sys_Check__c),$User.Username&lt;&gt; $Label.User_WF_Trigger_Disabled)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Attrition_Date_for_Inactive_Struck_Of</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_ROC_Status_to_Strike_Off</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Strike_Off_Initiation_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Company registered Notification</fullName>
        <actions>
            <name>Company_Registered_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Registration_License_No__c  != Null  &amp;&amp; Trade_Name__c != Null  &amp;&amp;  NOT(ISBLANK(TEXT( Legal_Type_of_Entity__c))) &amp;&amp; Registered_Address__c != Null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFSA License Status Active</fullName>
        <actions>
            <name>DFSA_License_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.DFSA_Approval__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.DFSA_Withdrawn_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DFSA License Status Withdrawn</fullName>
        <actions>
            <name>DFSA_License_Status_Withdrawn</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.DFSA_Withdrawn_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Discounted Account</fullName>
        <actions>
            <name>No_Min_capital_requirements</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Discount_Applied__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>check No Min capital requirements ?if lead is discounted  :ticket #5367</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Discounted Account Remove</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Discount_Applied__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ROC_reg_incorp_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FinTech_Classification__c</field>
            <operation>notEqual</operation>
            <value>FinTech</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FinTech_Classification__c</field>
            <operation>notEqual</operation>
            <value>Venture Capital New Regime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FinTech_Classification__c</field>
            <operation>notEqual</operation>
            <value>Fund Manager New Regime</value>
        </criteriaItems>
        <description>Remove discount flag after 600 days :ticket #5367</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remove_Discount</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.ROC_reg_incorp_Date__c</offsetFromField>
            <timeLength>600</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Exempted from UBO Regulations</fullName>
        <actions>
            <name>Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Exempted_from_UBO_Regulations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 Or 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Type__c</field>
            <operation>equals</operation>
            <value>Financial - related</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.License_Activity__c</field>
            <operation>contains</operation>
            <value>042</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Exempted_from_UBO_Regulations__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>#6533:Tick the box Exempted from UBO for newly registered entities that are financial or activity as investment fund</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FRC_layout_V1</fullName>
        <actions>
            <name>FRC_layout_V1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>FRC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assigned page layout to FRC_layout_V1  for FRC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Foundation_NPIO_Layout_v1</fullName>
        <actions>
            <name>Foundation_NPIO_Layout_v1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>Foundation,NPIO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assigned page layout to Foundation_NPIO_Layout_v1 for Foundation and NPIO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fund Company Not setup notification</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.DFSA_Lic_Appr_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sector_Classification__c</field>
            <operation>equals</operation>
            <value>Authorized Firm</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ROC_reg_incorp_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Fund_Setup_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Discount_Applied__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FinTech_Classification__c</field>
            <operation>equals</operation>
            <value>Start Up Fund Manager new Regime</value>
        </criteriaItems>
        <description>Ticket #5367 send notification to Customer after 11 month and fund not setup</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Fund_Company_Not_setup_notification_11_months</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.DFSA_Lic_Appr_Date__c</offsetFromField>
            <timeLength>334</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Fund_Company_Not_setup_notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.DFSA_Lic_Appr_Date__c</offsetFromField>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Ignore Company</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Sector_Classification__c</field>
            <operation>equals</operation>
            <value>DIFC Living</value>
        </criteriaItems>
        <description>Ignore company from Reports and PR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LLP_GP_LP_layout_V1</fullName>
        <actions>
            <name>LLP_GP_LP_layout_V1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>GP,LLP,LP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assigned page layout to LLP_GP_LP_layout_V1  for GP,LLP and LP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LTD_IC_PCC_Layout_v1</fullName>
        <actions>
            <name>LTD_IC_PCC_Layout_v1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>LTD IC,LTD PCC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assigned page layout to RLP__RLLP_RP_layout_V1  for LTD PCC and LTD IC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>License Activity Changed</fullName>
        <actions>
            <name>License_Activity_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( License_Activity__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Company Signup for Co-working</fullName>
        <actions>
            <name>New_Company_Signup_for_Co_working</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Working_from_Co_Working_Space__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Under Formation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Security_Check_Done__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email to Co working when Fintech company got security approval</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New User Access Email</fullName>
        <actions>
            <name>New_user_access_request_form</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>User_Access_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( Send_Portal_Email__c==true,$User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled ,TEXT(Company_Type__c) !=&apos;Retail&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New User Access Email For Retail</fullName>
        <actions>
            <name>New_user_access_request_form_Retail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Send_Portal_Email__c==true,$User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled ,TEXT(Company_Type__c) =&apos;Retail&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to RoC on Account Creation</fullName>
        <actions>
            <name>Notification_for_Account_Creation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Notification_for_Account_Transfer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When Account is created in Salesforce (BD pushes the Account from SAP); RoC team needs to be notified to complete missing details and initiate portal user creation process</description>
        <formula>AND( ISBLANK(BP_No__c ) ==false,  ISPICKVAL(ROC_Status__c, &apos;Under Formation&apos;),$User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Check Security Review Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Security_Review_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Security_Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Security Review status on the account should be set to Expired after 3 months of receiving the approval date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Security_Status_to_Expire</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Security_Review_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OB Commercial permission record type</fullName>
        <actions>
            <name>OB_Commercial_permission_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Is_Commercial_Permission__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>set the account record type to commercial permission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB On Creation Update Authorization Group</fullName>
        <actions>
            <name>Account_Update_Authorization_Group_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.OB_Sector_Classification__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Authorization Group field to BDD Team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PSA Negative Notification</fullName>
        <actions>
            <name>PSA_Negative_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(TEXT(ROC_Status__c) &lt;&gt; &apos;&apos; ||  TEXT(ROC_Status__c) &lt;&gt; null) &amp;&amp;
 PSA_Actual__c &lt; 0 &amp;&amp; ISCHANGED (PSA_Actual__c ) &amp;&amp;
 TEXT((ROC_Status__c))&lt;&gt;&apos;Under Formation&apos; &amp;&amp;
 TEXT((Sector_Classification__c)) &lt;&gt;&apos;Government&apos; &amp;&amp; 
 Index_Card_Status__c &lt;&gt;&apos;Cancelled&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pending Voluntary struck offROC Status Changed</fullName>
        <actions>
            <name>Pending_Dissolution_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Push_to_PR_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Pending Voluntary Strike off</value>
        </criteriaItems>
        <description>Request for Voluntary Strike off  8921</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ROC_status_to_Voluntary_Strike_off</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate ROC No</fullName>
        <actions>
            <name>ROC_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Active_License__c) &amp;&amp;  NOT(ISBLANK(Active_License__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Registered Address</fullName>
        <actions>
            <name>Registered_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>((ISCHANGED( Office__c )|| ISCHANGED( Building_Name__c )|| ISCHANGED( Street__c )|| ISCHANGED( PO_Box__c )) || For_Updates__c =true) &amp;&amp;  RecordType.DeveloperName  &lt;&gt; &apos;Contractor_Account&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Private_Public_National_Supra National V1</fullName>
        <actions>
            <name>Private_Public_National_Supra_National_V</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>LTD National,LTD Supra National,LTD,Public Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assigned page layout to Private_Public_National_Supra National V1 for LTD National,LTD Supra National,Private Company,Public Company</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Push to PR</fullName>
        <actions>
            <name>Push_to_PR_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Push_to_PR__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Push to PR when ROC SR is Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RLP%5F%5FRLLP_RP_layout_V1</fullName>
        <actions>
            <name>RLP_RLLP_RP_layout_V1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>RLLP,RLP,RP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.For_Updates__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assigned page layout to RLP__RLLP_RP_layout_V1  for RLLP, RLP and RP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ROC Status to Not Renewed</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Next_Renewal_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Legal_Type_of_Entity__c</field>
            <operation>notEqual</operation>
            <value>LTD SPC</value>
        </criteriaItems>
        <description>Change Account ROC status to Not Renewed after 30 days form Next renewal date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_SF_Admin</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>ROC_Status_to_Not_Renewed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Next_Renewal_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ROC- Populate Initial Allotment Check</fullName>
        <actions>
            <name>ROC_Populate_Initial_Allotment_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>For LTD SPC once company got registered from under formation to active update initial allotment of shares to checked</description>
        <formula>TEXT(Legal_Type_of_Entity__c) = &apos;LTD SPC&apos; &amp;&amp; TEXT(ROC_Status__c) &lt;&gt; &apos;&apos; &amp;&amp; ISPICKVAL(PRIORVALUE(ROC_Status__c),&quot;Under Formation&quot;) &amp;&amp; TEXT(ROC_Status__c) = &apos;Active&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Portal link to Customer</fullName>
        <actions>
            <name>Send_Portal_link_to_Customer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Portal_Access_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Send_Portal_Link_to_Customer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Email_Address_to_send_Portal_Link__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Active,Not Renewed,Inactive - DFSA Lic. Wdn</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set time for Secupass Push</fullName>
        <actions>
            <name>Secupass_Push_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISPICKVAL(ROC_Status__c,&apos;&apos;)) &amp;&amp; NOT(ISPICKVAL(ROC_Status__c,&apos;Under Formation&apos;)) &amp;&amp; NOT(ISPICKVAL(ROC_Status__c,&apos;Account Created&apos;)) &amp;&amp; (ISCHANGED( ROC_Status__c) ||ISCHANGED( Name ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status change to pending dissolution</fullName>
        <actions>
            <name>Status_change_pending_dissolution_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR( isPickVal(PRIORVALUE(ROC_Status__c),&quot;Active&quot;), isPickVal(PRIORVALUE(ROC_Status__c),&quot;Not Renewed&quot;), isPickVal(PRIORVALUE(ROC_Status__c),&quot;Inactive - DFSA Lic. Wdn&quot;) ),  OR( isPickVal(ROC_Status__c,&quot;Pending Dissolution&quot;), isPickVal(ROC_Status__c,&quot;Pending De-registration&quot;) ), ISBLANK(Status_change_to_pending_dissolution__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account RecType to General</fullName>
        <actions>
            <name>General_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName =&apos;BD_Fund&apos; &amp;&amp;  ISPICKVAL(ROC_Status__c ,&apos;Under Formation&apos;) &amp;&amp;  Transferred_to_ROC__c = true &amp;&amp; NOT(ISBLANK(Transferred_to_ROC_Date__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Account to Re-push to SAP</fullName>
        <actions>
            <name>Update_Account_to_Re_push_to_SAP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>custom workflow created for ROC revenue report in SAP if any of below field changes then it will become to true 

Company Type , Legal Type, License Type</description>
        <formula>AND( NOT(ISBLANK(Registration_License_No__c )), TEXT(ROC_Status__c) != &apos;Under Formation&apos;, NOT(ISBLANK( BP_No__c )),  OR( AND(ISCHANGED(ROC_Status__c), PRIORVALUE(ROC_Status__c) = &apos;Under Formation&apos;, TEXT(ROC_Status__c) = &apos;Active&apos;), ISCHANGED(Company_Type__c),  ISCHANGED(Legal_Type_of_Entity__c),    AND(ISCHANGED(License_Activity__c),     OR(  AND(NOT(CONTAINS(PRIORVALUE(License_Activity__c),&apos;066&apos;)) , CONTAINS(License_Activity__c,&apos;066&apos;)),  AND(NOT(CONTAINS(PRIORVALUE(License_Activity__c),&apos;042&apos;)) , CONTAINS(License_Activity__c,&apos;042&apos;)),  AND(NOT(CONTAINS(PRIORVALUE(License_Activity__c),&apos;344&apos;)) , CONTAINS(License_Activity__c,&apos;344&apos;))   ) ), AND(ISCHANGED(Captive_Insurance__c), Captive_Insurance__c = true)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Client Type on Temp Field</fullName>
        <actions>
            <name>TempField_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( TempField__c ) &amp;&amp; NOT(ISBLANK(TempField__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update is PR Push and Date</fullName>
        <actions>
            <name>Last_PR_Push_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_IS_PR_Push_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Is_PR_Push__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateTEODiscount</fullName>
        <actions>
            <name>UpdateTEODiscount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Executive_Company__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow is used to update TEO Discount field when TEO entity is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Voluntary Strike off- ROC Status Changed</fullName>
        <actions>
            <name>Dissolved_Voluntary_Strike_off_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Attrition_Date_for_Inactive_Struck_Of</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Dissolution_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Push_to_PR_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Dissolved - Voluntary Strike off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Initial_Contact_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Pending_Dissolution_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Warning : email to Account where ROC status change to Dissolved - Voluntary Strike off #8921</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>application for New PO Box</fullName>
        <actions>
            <name>Application_for_New_PO_Box</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>application for New PO Box</description>
        <formula>ISPICKVAL((ROC_Status__c),&apos;Active&apos; )  &amp;&amp; PO_Box_Issued__c &amp;&amp; ISPICKVAL(PRIORVALUE((ROC_Status__c)),&apos;Under Formation&apos; ) &amp;&amp; $User.Username&lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Portal_Access_Email</fullName>
        <assignedTo>integration@difc.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Portal Access Email</subject>
    </tasks>
    <tasks>
        <fullName>User_Access_Email_Sent</fullName>
        <assignedTo>integration@difc.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Email to access portal sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>User Access Email Sent</subject>
    </tasks>
</Workflow>