<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Code</fullName>
        <field>Code__c</field>
        <formula>Pricing_Line__r.DEV_Id__c &amp;&apos;-&apos;&amp; TEXT(S_No__c)</formula>
        <name>Update Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>