<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cancel_Training_Email</fullName>
        <description>Cancel Training Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Cancellation</template>
    </alerts>
    <alerts>
        <fullName>Email_Feedback_for_Training_Request</fullName>
        <description>Email Feedback for Training Request</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Request_Feedback</template>
    </alerts>
    <alerts>
        <fullName>Email_contact_on_Training_Request_submission</fullName>
        <description>Email contact on Training Request submission</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Submission</template>
    </alerts>
    <alerts>
        <fullName>Reschedule_Training_Email</fullName>
        <description>Reschedule Training Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Reschedule</template>
    </alerts>
    <alerts>
        <fullName>Send_Updated_Training_Information_Email</fullName>
        <description>Send Updated Training Information Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Update</template>
    </alerts>
    <alerts>
        <fullName>Training_Request_Invitation_Email</fullName>
        <description>Training Request Invitation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Templates/Training_Invitation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Send_Update_to_False</fullName>
        <field>Send_Update__c</field>
        <literalValue>0</literalValue>
        <name>Send Update to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Send_Update__c</field>
        <literalValue>0</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Update_Field</fullName>
        <field>Send_Update__c</field>
        <literalValue>0</literalValue>
        <name>Update Status Update Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Email the contact once the training request is cancelled</fullName>
        <actions>
            <name>Cancel_Training_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Request__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Email the contact once the training request is cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email the contact once the training request is rescheduled</fullName>
        <actions>
            <name>Reschedule_Training_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Request__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Rescheduled</value>
        </criteriaItems>
        <description>Email the contact once the training request is rescheduled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email the contact once the training request is submitted</fullName>
        <actions>
            <name>Email_contact_on_Training_Request_submission</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Request__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email the contact once the training request is submitted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email the customer for the Feedback - Training Request</fullName>
        <actions>
            <name>Email_Feedback_for_Training_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Request__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Attended</value>
        </criteriaItems>
        <description>Email the customer for the Feedback - Training Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send updated training information</fullName>
        <actions>
            <name>Send_Updated_Training_Information_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Status_Update_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Request__c.Send_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Request__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Rejected,Invalid,Cancelled,Attended</value>
        </criteriaItems>
        <description>Send updated training information when Status_Update Flag = true</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Training Request - Send Email on Status Invitation Sent</fullName>
        <actions>
            <name>Training_Request_Invitation_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Invitation Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Request__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send email to the Training Request Email when status is changed to &quot;Invitation Sent&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>