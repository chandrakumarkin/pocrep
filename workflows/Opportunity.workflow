<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Mail_to_CBD_Team_for_Stage_Inactivity</fullName>
        <ccEmails>shabbir.ahmed@difc.ae</ccEmails>
        <description>Mail to CBD Team for Stage Inactivity</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Mail_to_Opportunity_Owner_for_Stage_Inactivity</template>
    </alerts>
    <alerts>
        <fullName>Mail_to_Opportunity_Owner_for_Stage_Inactivity</fullName>
        <ccEmails>shabbir.ahmed@difc.ae</ccEmails>
        <description>Mail to Opportunity Owner for Stage Inactivity</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Mail_to_Opportunity_Owner_for_Stage_Inactivity</template>
    </alerts>
    <alerts>
        <fullName>Mail_to_Opportunity_Sector_Lead_for_Stage_Inactivity</fullName>
        <ccEmails>shabbir.ahmed@difc.ae</ccEmails>
        <description>Mail to Opportunity Sector Lead for Stage Inactivity</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Mail_to_Opportunity_Owner_for_Stage_Inactivity</template>
    </alerts>
    <alerts>
        <fullName>Retail_Email_Reminder_for_Company_Name_Approval</fullName>
        <description>Retail: Email Reminder for Company Name Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>ammar.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_Company_Name_Approval_Pending</template>
    </alerts>
    <alerts>
        <fullName>Retail_Email_Reminder_for_LAF</fullName>
        <description>Retail: Email Reminder for LAF</description>
        <protected>false</protected>
        <recipients>
            <recipient>DIFC_IT_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_LAF_Approval_Pending</template>
    </alerts>
    <alerts>
        <fullName>Retail_Email_Reminder_for_Legal_Team</fullName>
        <description>Retail: Email Reminder for Legal Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_Legal_Approval_Pending</template>
    </alerts>
    <alerts>
        <fullName>Retail_Email_Reminder_for_Registration_Review_Committee</fullName>
        <description>Retail: Email Reminder for Registration Review Committee</description>
        <protected>false</protected>
        <recipients>
            <recipient>khalid.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_RRC_Approval_Pending</template>
    </alerts>
    <alerts>
        <fullName>Retail_Email_Reminder_for_Security_Team</fullName>
        <description>Retail: Email Reminder for Security Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_Security_Approval_Pending</template>
    </alerts>
    <alerts>
        <fullName>Retail_Gate_Avenue_Awaiting_Documents</fullName>
        <description>Retail: Gate Avenue Awaiting Documents</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_Gate_Avenue_Awaiting_Document</template>
    </alerts>
    <alerts>
        <fullName>Retail_In_Principal_Approval_Granted</fullName>
        <ccEmails>retail@difc.ae;SpecialtyLeasing@difc.ae;roc.helpdesk@difc.ae;rorp@difc.ae;Khushboo.Tahilramani@difc.ae</ccEmails>
        <description>Retail: In Principal Approval Granted</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_In_Principal_Approval</template>
    </alerts>
    <alerts>
        <fullName>Retail_Space_Not_Available</fullName>
        <description>Retail: Space Not Available</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_Space_Not_Available</template>
    </alerts>
    <alerts>
        <fullName>Retail_on_Boarding_In_Principal_Approval</fullName>
        <ccEmails>retail@difc.ae;Khushboo.Tahilramani@difc.ae;SpecialtyLeasing@difc.ae</ccEmails>
        <description>Retail on Boarding: In Principal Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_Step_3_Initiation_Retail_on_Boarding</template>
    </alerts>
    <alerts>
        <fullName>Retail_on_Boarding_RRC_Approval_Retail_Review</fullName>
        <description>Retail on Boarding: RRC Approval (Retail Review)</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Retail_RRC_Approval_Retail_Review</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_General_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>General</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account General Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_In_Principle_approval_Update</fullName>
        <field>DFSA_Lic_InPrAppDate__c</field>
        <formula>Today()</formula>
        <name>Account In Principle approval Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disable_ROC_Transfer</fullName>
        <description>Disables the functionality to transfer an account to ROC</description>
        <field>Crossed_Opportunity_Stage__c</field>
        <literalValue>1</literalValue>
        <name>Disable ROC Transfer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Record_Type_to_Fin_Securit</fullName>
        <description>Field Update : Update Record Type to Financial Security when stage becomes &apos;Letter of Intent (LOI) Received&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>BD_Financial_Fund_with_Security</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Field Update Record Type to Fin. Securit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Update_Record_Type_to_NFS</fullName>
        <description>Field Update : Update Record Type to NFS when stage becomes &apos;company name checked&apos; tkt # 4473</description>
        <field>RecordTypeId</field>
        <lookupValue>NFS</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Field Update Update Record Type to NFS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinTech_Non_Financial_Record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Fintech_Non_Financial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FinTech Non Financial Record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Commercial_permission_record_type</fullName>
        <description>Update opportunity record type to commercial permission when the entity is applying for a commercial permission</description>
        <field>RecordTypeId</field>
        <lookupValue>Commercial_Permission</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OB Commercial permission record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Update_stage_LOI_Received_on_Lead_Con</fullName>
        <description>Update stage LOI Received on Lead Conversion</description>
        <field>StageName</field>
        <literalValue>Letter of Intent (LOI) Received</literalValue>
        <name>OB Update stage LOI Received on Lead Con</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_as_Today</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Set Close Date as Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Events_Record_Type_for_Oppty</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Events</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Events Record Type for Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Financial_Record_Type_for_Oppty</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BD_Financial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Financial Record Type for Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Non_Financial_Record_Type_for_Oppty</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BD_Non_Financial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Non Financial Record Type for Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Retail_Record_Type_for_Oppty</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BD_Retail</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Retail Record Type for Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_for_Events</fullName>
        <field>StageName</field>
        <literalValue>Inquiry for Proposal</literalValue>
        <name>Set Stage for Events</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_for_Financial</fullName>
        <field>StageName</field>
        <literalValue>Letter of Intent (LOI) Received</literalValue>
        <name>Set Stage for Financial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_for_Non_Financial</fullName>
        <field>StageName</field>
        <literalValue>Entity Name Check</literalValue>
        <name>Set Stage for Non Financial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_for_Retail</fullName>
        <field>StageName</field>
        <literalValue>Documents Received</literalValue>
        <name>Set Stage for Retail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AML_Rating</fullName>
        <description>ticket #5955</description>
        <field>AML_Rating__c</field>
        <literalValue>Low</literalValue>
        <name>Update AML Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Bank_Details_on_Account</fullName>
        <field>Opportunity_Bank_Account__c</field>
        <formula>Bank_Account__c</formula>
        <name>Update Opp Bank Details on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Estimated_Sq_ft_on_Account</fullName>
        <field>Opportunity_Estimated_Square_Ft__c</field>
        <formula>Office_Space__c</formula>
        <name>Update Opp Estimated Sq.ft on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_Step_on_Account</fullName>
        <field>Stage_Step__c</field>
        <formula>TEXT(Stage_Steps__c)</formula>
        <name>Update Opportunity Stage Step on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_on_Account</fullName>
        <field>Current_Opportunity_Stage__c</field>
        <formula>TEXT(StageName)</formula>
        <name>Update Opportunity Stage on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Security_Check_on_Account</fullName>
        <field>Security_Check_Done__c</field>
        <literalValue>1</literalValue>
        <name>Update Security Check on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Workflow_FinTech</fullName>
        <field>Workflow_Check__c</field>
        <literalValue>FinTech</literalValue>
        <name>Workflow FinTech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AML Rating</fullName>
        <actions>
            <name>Update_AML_Rating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.AML_Rating__c</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <description>Ticket #5955</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Events Lead Conversion Set Record Type %26 Stage</fullName>
        <actions>
            <name>Set_Close_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Events_Record_Type_for_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stage_for_Events</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_Rec_Type__c</field>
            <operation>equals</operation>
            <value>Event</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FinTech Non Financial Record type</fullName>
        <actions>
            <name>FinTech_Non_Financial_Record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>notEqual</operation>
            <value>FinTech Hive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FinTech_Classification__c</field>
            <operation>equals</operation>
            <value>FinTech</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Type__c</field>
            <operation>equals</operation>
            <value>Non - financial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Workflow_Check__c</field>
            <operation>equals</operation>
            <value>FinTech</value>
        </criteriaItems>
        <description>Change Record type only when Non Financial FinTech Hive lead looking for discount #ticket 5367</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Financial Lead Conversion Set Record Type %26 Stage</fullName>
        <actions>
            <name>Set_Close_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Financial_Record_Type_for_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_Rec_Type__c</field>
            <operation>equals</operation>
            <value>Financial</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Non-Financial Lead Conversion Set Record Type %26 Stage</fullName>
        <actions>
            <name>Set_Close_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Non_Financial_Record_Type_for_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stage_for_Non_Financial</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Workflow_FinTech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_Rec_Type__c</field>
            <operation>equals</operation>
            <value>Non Financial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Workflow_Check__c</field>
            <operation>notEqual</operation>
            <value>FinTech</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Commercial permission record type</fullName>
        <actions>
            <name>OB_Commercial_permission_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.OB_Is_Commercial_Permission__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>set the opportunity record type to commercial permission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Update stage LOI Received on Lead Conversion</fullName>
        <actions>
            <name>OB_Update_stage_LOI_Received_on_Lead_Con</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Lead_Status__c</field>
            <operation>equals</operation>
            <value>LOI Received</value>
        </criteriaItems>
        <description>Update stage LOI Received on Lead Conversion</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Obtain In Principal Approval</fullName>
        <actions>
            <name>Retail_In_Principal_Approval_Granted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Re_Sent_Obtain_In_Principal_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Retail Onboarding (Security Checklist)</value>
        </criteriaItems>
        <description>Use to resend Obtain In Principal Approval if case of any issue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity In Principle Approved Update</fullName>
        <actions>
            <name>Account_In_Principle_approval_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>In principle issued by DFSA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.DFSA_Lic_InPrAppDate__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Account In Principle date value when user change opp stage to In Principle Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ROCStatusChange</fullName>
        <actions>
            <name>Account_General_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Disable_ROC_Transfer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changes the ROC status based on opportunity stage</description>
        <formula>PRIORVALUE(Crossed_Opportunity_Stage__c) == false &amp;&amp; Crossed_Opportunity_Stage__c == false &amp;&amp;  Transferred_to_ROC__c == true &amp;&amp;  ROC_Transferable__c == true &amp;&amp;  PRIORVALUE(ROC_Transferable__c) == true &amp;&amp;  NOT(ISNEW()) &amp;&amp; ISPICKVAL( Account.ROC_Status__c , &apos;Account Created&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Retail Lead Conversion Set Record Type %26 Stage</fullName>
        <actions>
            <name>Set_Close_Date_as_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Retail_Record_Type_for_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stage_for_Retail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Oppty_Rec_Type__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Retail%3A Send to Company Name Approver</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Company_Name_Approval__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Company_Name_Approval</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Company_Name_Approval</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Retail%3A Send to LAF</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.LAF_Approval__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_LAF</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_LAF</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Retail%3A Send to Legal Team</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Legal_Approval__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Legal_Team</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Legal_Team</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Retail%3A Send to RRC Team</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RRC_Approval__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Registration_Review_Committee</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Registration_Review_Committee</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Retail%3A Send to Security Team</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Security_Check__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Retail_Email_Reminder_for_Security_Team</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Security Approval Update on Account</fullName>
        <actions>
            <name>Update_Security_Check_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Company_Type__c</field>
            <operation>equals</operation>
            <value>Financial - related,Non - financial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Security_Check__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Details on Account</fullName>
        <actions>
            <name>Update_Opp_Bank_Details_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opp_Estimated_Sq_ft_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opportunity_Stage_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Opp Stage, Estimated Sq Feet</description>
        <formula>OR( AND(ISCHANGED(StageName) , PRIORVALUE(StageName) &lt;&gt; TEXT(StageName)), AND(ISCHANGED(Office_Space__c) , PRIORVALUE(Office_Space__c) &lt;&gt; Office_Space__c), AND(ISCHANGED(Bank_Account__c) , PRIORVALUE(Bank_Account__c) &lt;&gt; Bank_Account__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type to Financial Security</fullName>
        <actions>
            <name>Field_Update_Record_Type_to_Fin_Securit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Record Type to Financial Security when stage becomes Letter of Intent (LOI) Received&apos; tkt # 4473</description>
        <formula>AND(RecordType.DeveloperName != &apos;BD_Financial_Fund_with_Security&apos;,  ISPICKVAL(StageName, &apos;Letter of Intent (LOI) Received&apos;),  Company_Type__c == &apos;Financial - related&apos;,  RecordType.DeveloperName == &apos;BD_Financial&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type to NFS</fullName>
        <actions>
            <name>Field_Update_Update_Record_Type_to_NFS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Record Type to NFS when stage becomes Company Name Checked&apos; tkt # 4473</description>
        <formula>AND(RecordType.DeveloperName != &apos;NFS&apos;,  ISPICKVAL(StageName, &apos;Entity Name Check&apos;),  Company_Type__c == &apos;Non - financial&apos;,  RecordType.DeveloperName == &apos;BD_Non_Financial&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>