<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OB_Update_visibility_check_on_SR_step</fullName>
        <description>This is to update the visibility of DIFC approval steps and display them on the community</description>
        <field>Visible_In_Community__c</field>
        <literalValue>1</literalValue>
        <name>OB Update visibility check on SR step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OB BD Quick review visible for approval on community</fullName>
        <actions>
            <name>OB_Update_visibility_check_on_SR_step</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flagging BD Quick review step on in principle as visible for the community</description>
        <formula>OR ( HexaBPM__Step_Template__r.HexaBPM__Code__c = &quot;BD_QUICK_REVIEW&quot;,   HexaBPM__Step_Template__r.HexaBPM__Code__c = &quot;ENTITY_NAME_REVIEW&quot;,   HexaBPM__Step_Template__r.HexaBPM__Code__c = &quot;SECURITY_REVIEW&quot;  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>