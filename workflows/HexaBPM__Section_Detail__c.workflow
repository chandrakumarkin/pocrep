<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HexaBPM__Update_Blank_Space_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Blank_Space</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Blank Space Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HexaBPM__Update_Command_Button_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Command_Button</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Command Button Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HexaBPM__Update_Input_Field_Record_Ty</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Input_Field</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Input Field Record Ty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HexaBPM__Update_Output_Field_Record_Ty</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Output_Field</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Output Field Record Ty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Fileupload_Recordtype</fullName>
        <field>RecordTypeId</field>
        <lookupValue>File_Upload</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Fileupload Recordtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Recordtype_to_Lookup</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Lookup_Field</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Recordtype to Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HexaBPM__Update Record Type Blank Space</fullName>
        <actions>
            <name>HexaBPM__Update_Blank_Space_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section_Detail__c.HexaBPM__Component_Type__c</field>
            <operation>equals</operation>
            <value>Blank Space</value>
        </criteriaItems>
        <description>Update to Blank Space Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HexaBPM__Update Record Type Command Button</fullName>
        <actions>
            <name>HexaBPM__Update_Command_Button_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section_Detail__c.HexaBPM__Component_Type__c</field>
            <operation>equals</operation>
            <value>Command Button</value>
        </criteriaItems>
        <description>Update to Command Button Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HexaBPM__Update Record Type Command Button Section</fullName>
        <actions>
            <name>HexaBPM__Update_Input_Field_Record_Ty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section_Detail__c.HexaBPM__Component_Type__c</field>
            <operation>equals</operation>
            <value>Input Field</value>
        </criteriaItems>
        <description>Update Record Type to Input Field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HexaBPM__Update Record Type Output Field</fullName>
        <actions>
            <name>HexaBPM__Update_Output_Field_Record_Ty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section_Detail__c.HexaBPM__Component_Type__c</field>
            <operation>equals</operation>
            <value>Output Field</value>
        </criteriaItems>
        <description>Update to Output Field Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update File Upload Recordtype</fullName>
        <actions>
            <name>Update_Fileupload_Recordtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section_Detail__c.HexaBPM__Component_Type__c</field>
            <operation>equals</operation>
            <value>File Upload</value>
        </criteriaItems>
        <description>Update the record type of section detail to file upload if the component type equals to file upload</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lookup Recordtype</fullName>
        <actions>
            <name>Update_Recordtype_to_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section_Detail__c.HexaBPM__Component_Type__c</field>
            <operation>equals</operation>
            <value>Lookup</value>
        </criteriaItems>
        <description>Update the record type of section detail to lookup if the component type equals to lookup</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>