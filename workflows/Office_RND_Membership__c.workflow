<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_With_Attached_Lease_agreement</fullName>
        <ccEmails>coworking@difc.ae</ccEmails>
        <ccEmails>Nasser.Belhabala@difc.ae</ccEmails>
        <description>Email With Attached Lease agreement</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Fintech_Lease_Notification</template>
    </alerts>
    <rules>
        <fullName>Email to Team on RND</fullName>
        <actions>
            <name>Email_With_Attached_Lease_agreement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email to Team on RND- Resend</fullName>
        <actions>
            <name>Email_With_Attached_Lease_agreement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Office_RND_Membership__c.Re_Send__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>