<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Car_Parking_Deactivation_Email_Alert</fullName>
        <description>Car Parking Deactivation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>difc@zoneparking.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Car_Parking_Deactivation_Email_Template</template>
    </alerts>
    <rules>
        <fullName>Car Parking Deactivate Notification</fullName>
        <actions>
            <name>Car_Parking_Deactivation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is only for car Parking</description>
        <formula>Text(Status__c) == &apos;Deactivated&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>