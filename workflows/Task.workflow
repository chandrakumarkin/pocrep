<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Meeting_Notes</fullName>
        <field>Description</field>
        <formula>PRIORVALUE(Description) +&apos; &apos; + Description</formula>
        <name>Meeting Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_and_Time_From_Field</fullName>
        <field>Date_Time_From__c</field>
        <formula>CreatedDate</formula>
        <name>Update Date and Time From Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Meeting notes from SAP</fullName>
        <actions>
            <name>Meeting_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(Meeting_ID__c)) &amp;&amp;  Append_Notes_SAP__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date and Time From Field</fullName>
        <actions>
            <name>Update_Date_and_Time_From_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Email&apos; &amp;&amp;  CONTAINS(UPPER(Subject), &apos;EMAIL&apos;) &amp;&amp;  ISNULL(Date_Time_From__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>