<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IF_camp</fullName>
        <description>IF camp</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>leo.cruz@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yahia.mageed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Leads_New_assignment_notification_IF_coma</template>
    </alerts>
    <alerts>
        <fullName>Lead_Creation_Email</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Lead Creation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Lead_Initial_Email_V2</template>
    </alerts>
    <alerts>
        <fullName>Lead_Creation_Website_Self_Registration_Financial_Funds</fullName>
        <description>OB Lead Creation - Website Self Registration Financial Funds</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Lead_Creation_Website_Financial_Fund</template>
    </alerts>
    <alerts>
        <fullName>Lead_Creation_Website_and_Call_Center</fullName>
        <description>OB Lead Creation - Website &amp; Call Center</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/Lead_Creation_Website_Call_Center</template>
    </alerts>
    <alerts>
        <fullName>Lead_Non_VIP_Retailer_Operator_Notification</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Lead: Non-VIP Retailer/Operator Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Lead_Non_VIP_Retailer_Operator_Template</template>
    </alerts>
    <alerts>
        <fullName>Lead_Non_VIP_Retailer_Operator_Notification_Retail_Gate_Avenue</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Lead: VIP /Non-VIP Retailer/Operator Notification (Retail - Gate Avenue)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Lead_VIP_Retailer_Operator_Template_DIFC_Gate_Avenue</template>
    </alerts>
    <alerts>
        <fullName>Lead_Retail_Receipt</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Lead: Retail Receipt</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Lead_Initial_Email</template>
    </alerts>
    <alerts>
        <fullName>Lead_VIP_NON_VIP_Retail_Specialty_Leasing_3rd_Party</fullName>
        <description>Lead: VIP/NON VIP - Retail Specialty Leasing - 3rd Party</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Email_Templates/Lead_VIP_NON_VIP_Retail_Specialty_Leasing_3rd_Party</template>
    </alerts>
    <alerts>
        <fullName>Lead_VIP_Retailer_Operator_Notification</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Lead: VIP Retailer/Operator Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Notifications/Lead_VIP_Retailer_Operator_Template</template>
    </alerts>
    <alerts>
        <fullName>Lead_VIP_Retailer_Operator_Notification_3rd_Party</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Lead: VIP Retailer/Operator Notification (3rd Party)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Retail_Email_Templates/Lead_VIP_NON_VIP_Retailer_Operator_Template_Retail_3rd_Party</template>
    </alerts>
    <alerts>
        <fullName>Notify_Team_on_N_nbr_of_Days_after_lead_creation</fullName>
        <description>Notify Team on N nbr of Days after lead creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Lead_Escalation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_user_for_lead_updates</fullName>
        <ccEmails>Shabbir.Ahmed@difc.ae</ccEmails>
        <description>Notify user for lead updates</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/X1st_Notification_for_RM</template>
    </alerts>
    <alerts>
        <fullName>Notify_user_for_lead_updates_2nd_Notification</fullName>
        <description>Notify user for lead updates (2nd Notification)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Sector_Lead_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/X2nd_Notification_for_RM</template>
    </alerts>
    <alerts>
        <fullName>Notify_user_for_lead_updates_for_CBD</fullName>
        <description>Notify user for lead updates for CBD</description>
        <protected>false</protected>
        <recipients>
            <recipient>CBD_Users</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notification_for_Sector_lead</template>
    </alerts>
    <alerts>
        <fullName>Notify_user_for_lead_updates_for_sector_lead</fullName>
        <description>Notify user for lead updates for sector lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>DIFC_IT_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notification_for_Sector_lead</template>
    </alerts>
    <alerts>
        <fullName>OB_Lead_Creation_Website_Self_Registration_Financial_Funds</fullName>
        <description>OB Lead Creation - Website Self Registration Financial Funds</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Lead_Creation_Website_Financial_Fund</template>
    </alerts>
    <alerts>
        <fullName>Web_to_Lead_email_response</fullName>
        <ccEmails>shabbir.ahmed@difc.ae</ccEmails>
        <description>Web-to-Lead email response</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/LeadsWebtoLeademailresponse</template>
    </alerts>
    <fieldUpdates>
        <fullName>Append_Alternate_Phone_Country_Prefix</fullName>
        <description>Appends the country code to the alternative phone number</description>
        <field>Alternative_Phone_No__c</field>
        <formula>Alt_Phone_with_Country_Code__c</formula>
        <name>Append Alternate Phone Country Prefix</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Append_Mobile_Country_Code_Prefix</fullName>
        <description>Appends the country code prefix whenever the mobile number and the country is changed</description>
        <field>MobilePhone</field>
        <formula>Mobile_with_Country_Code__c</formula>
        <name>Append Mobile Country Code Prefix</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Record_Type_Events</fullName>
        <description>For assigning the event event record type when the lead is created through web to lead</description>
        <field>RecordTypeId</field>
        <lookupValue>Event</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Record Type - Events</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Record_Type_Financial</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Financial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Record Type - Financial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Record_Type_Non_Financial</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Non_Financial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Record Type - Non Financial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Record_Type_Retail</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Retail</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Record Type - Retail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Record_Type_Speciality_Leasing</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Special_Leasing</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Record Type - Speciality Leasing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Centre_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BusinessCentre</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Business Centre Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Not_Applicable</fullName>
        <description>Changes the Lead status to not applicable</description>
        <field>Status</field>
        <literalValue>Not Applicable</literalValue>
        <name>Change Status to Not Applicable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Sent</fullName>
        <field>Send_Email__c</field>
        <literalValue>Sent</literalValue>
        <name>Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leasing_OfferProcess_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Leasing_OfferProcess</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Leasing OfferProcess Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Long_term_Office_Space</fullName>
        <description>Leasing Enquiry Long-term Office Space (Commercial office space)</description>
        <field>RecordTypeId</field>
        <lookupValue>Leasing_OfferProcess</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Long-term Office Space</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Notification_Date</fullName>
        <description>Set the notification date to today</description>
        <field>Date_of_notification__c</field>
        <formula>NOW()</formula>
        <name>Reset Notification Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Created_Date</fullName>
        <field>Created_Date__c</field>
        <formula>CreatedDate</formula>
        <name>Set Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_Changed_Time</fullName>
        <field>Lead_Status_Changed__c</field>
        <formula>Now()</formula>
        <name>Set Lead Status Changed Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type</fullName>
        <field>Lead_RecType__c</field>
        <formula>CASE(TEXT(Area_of_Business__c), &apos;Banking&apos;, &apos;Financial&apos;,&apos;Insurance&apos;,&apos;Financial&apos;,&apos;Wealth Management&apos;, &apos;Financial&apos;,&apos;Capital Markets&apos;,&apos;Financial&apos;,&apos;Professional Services&apos;,&apos;Non Financial&apos;,&apos;Management Offices&apos;,&apos;Non Financial&apos;, &apos;Global Corporates&apos;,&apos;Non Financial&apos;,&apos;Other&apos;,&apos;Non Financial&apos;, &apos;Retail&apos;, &apos;Retail&apos;, IF(
ISPICKVAL(Enquiry_Type__c,&apos;Hire event space&apos;),&apos;Event&apos;,RecordType.Name))</formula>
        <name>Set Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_for_Oppty</fullName>
        <field>Lead_RecType_for_Oppty__c</field>
        <formula>CASE(TEXT(Area_of_Business__c), &apos;Banking&apos;, &apos;Financial&apos;,&apos;Insurance&apos;,&apos;Financial&apos;,&apos;Wealth Management&apos;, &apos;Financial&apos;,&apos;Capital Markets&apos;,&apos;Financial&apos;,&apos;Professional Services&apos;,&apos;Non Financial&apos;,&apos;Management Offices&apos;,&apos;Non Financial&apos;, &apos;Global Corporates&apos;,&apos;Non Financial&apos;,&apos;Other&apos;,&apos;Non Financial&apos;, &apos;Retail&apos;, &apos;Retail&apos;, IF( 
ISPICKVAL(Enquiry_Type__c,&apos;Hire event space&apos;),&apos;Event&apos;,RecordType.Name))</formula>
        <name>Set Record Type for Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Time_line_Changed</fullName>
        <field>Last_Changed_Time_line_Date__c</field>
        <formula>today()</formula>
        <name>Time line Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_address</fullName>
        <field>Contact_Address_Copied__c</field>
        <literalValue>1</literalValue>
        <name>Update Contact address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Country</fullName>
        <field>Contact_Country__c</field>
        <formula>TEXT(Country__c)</formula>
        <name>Update Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_for_Web2Lead</fullName>
        <field>LeadSource</field>
        <literalValue>Company Website</literalValue>
        <name>Update Lead Source for Web2Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Text_Id</fullName>
        <description>Commercial Unit Offer Process 5633</description>
        <field>Lead_Text_Id__c</field>
        <formula>Id</formula>
        <name>Update Lead Text Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Venue_Booking_REcord_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Venue_Hire</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Venue Booking REcord type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Assign Record Type - Commercial office space</fullName>
        <actions>
            <name>Long_term_Office_Space</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Leasing Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Leasing_Period__c</field>
            <operation>equals</operation>
            <value>Long-term Office Space (Commercial office space)</value>
        </criteriaItems>
        <description>Long-term Office Space Commercial office space</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type - Events</fullName>
        <actions>
            <name>Assign_Record_Type_Events</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>contains</operation>
            <value>Hire a venue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
            <value>DIFC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sub_Source__c</field>
            <operation>contains</operation>
            <value>Events Venues</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and enquiry type entered by the client is Hire event space, then assign it to the Event Record Type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type - Financial</fullName>
        <actions>
            <name>Assign_Record_Type_Financial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 1) OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Company_Type__c</field>
            <operation>equals</operation>
            <value>Financial - related</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Business__c</field>
            <operation>contains</operation>
            <value>Banking,Insurance,Wealth Management,Capital Markets</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
            <value>DIFC</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and company type entered by the client is Financial, then assign it to the Financial Record Type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type - Non Financial</fullName>
        <actions>
            <name>Assign_Record_Type_Non_Financial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 1) OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Company_Type__c</field>
            <operation>equals</operation>
            <value>Non - financial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Business__c</field>
            <operation>contains</operation>
            <value>Professional Services,Management Offices,Global Corporates,Other,Management Office</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
            <value>DIFC</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and company type entered by the client is Non Financial, then assign it to the Non Financial Record Type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type - Retail</fullName>
        <actions>
            <name>Assign_Record_Type_Retail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Source_for_Web2Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(((1 AND 4) OR (2 AND 3) OR 5) AND 6) OR (7 AND 8)</booleanFilter>
        <criteriaItems>
            <field>Lead.Company_Type__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Business__c</field>
            <operation>contains</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
            <value>DIFC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Type_of_Property__c</field>
            <operation>notEqual</operation>
            <value>Lease a Kiosk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sub_Source__c</field>
            <operation>equals</operation>
            <value>Gate Avenue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Leasing_Period__c</field>
            <operation>contains</operation>
            <value>Retail Space</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>contains</operation>
            <value>Leasing Enquiry</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and company type entered by the client is Retail, then assign it to the Retail Record Type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type - Venue Booking</fullName>
        <actions>
            <name>Venue_Booking_REcord_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR  3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>contains</operation>
            <value>Hire a venue,Leasing Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Leasing_Period__c</field>
            <operation>contains</operation>
            <value>Co-working Space</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sub_Source__c</field>
            <operation>contains</operation>
            <value>Academy Venues</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and enquiry type entered by the client is Hire event space, then assign it to the Venue Booking</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type -long term office space</fullName>
        <actions>
            <name>Leasing_OfferProcess_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>contains</operation>
            <value>Leasing Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Leasing_Period__c</field>
            <operation>contains</operation>
            <value>Long-term Office Space (Commercial office space)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Company Website</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and enquiry type entered by the client is Leasing Enquiry , then assign it to the Leasing OfferProcess</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Record Type -short term office space</fullName>
        <actions>
            <name>Business_Centre_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>contains</operation>
            <value>Leasing Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Leasing_Period__c</field>
            <operation>contains</operation>
            <value>short term office space (serviced office)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Company Website</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and enquiry type entered by the client is Leasing Enquiry , then assign it to the Business Centre</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change_Lead_Status_to_Not_Applicable</fullName>
        <actions>
            <name>Change_Status_to_Not_Applicable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2  OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Site__c</field>
            <operation>equals</operation>
            <value>DIFC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Company registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Property registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Media enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>General enquiry</value>
        </criteriaItems>
        <description>Changes any leads coming from web-to-lead form to &apos;Not Applicable&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Dublin Leads</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Dublin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Gate Avenue Web Leads</fullName>
        <actions>
            <name>Lead_Creation_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Company Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sub_Source__c</field>
            <operation>equals</operation>
            <value>Gate Avenue</value>
        </criteriaItems>
        <description>Warning : Send email to customer when customer submit request online</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IF campaign</fullName>
        <actions>
            <name>IF_camp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>facebook,linkedin</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Initial Email Notification</fullName>
        <actions>
            <name>Lead_Creation_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Record_Type__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Send_Email__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Customer : Send email to customer if the &apos;Send email&apos; value -Yes</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Follow up Reminder</fullName>
        <actions>
            <name>Kindly_follow_up_with_the_lead</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Follow_up_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Notification for High Status</fullName>
        <active>false</active>
        <description>disabled : #5550</description>
        <formula>IsConverted = false &amp;&amp;  ISPICKVAL( Status , &apos;High&apos;) &amp;&amp;  NOT(ISBLANK(Date_of_notification__c )) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>35</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>42</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates_2nd_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Notification for Low Status</fullName>
        <active>false</active>
        <description>disabled : #5550</description>
        <formula>IsConverted = false &amp;&amp;  ISPICKVAL( Status , &apos;Low&apos;) &amp;&amp;  NOT(ISBLANK(Date_of_notification__c )) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>89</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>96</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates_2nd_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>82</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>75</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Notification for Medium Status</fullName>
        <active>false</active>
        <description>disabled : #5550</description>
        <formula>IsConverted = false &amp;&amp;  ISPICKVAL( Status , &apos;Medium&apos;) &amp;&amp;  NOT(ISBLANK(Date_of_notification__c )) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates_2nd_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>52</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>66</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>59</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Notification for No Interest Status</fullName>
        <active>false</active>
        <description>disabled for ticket #5010</description>
        <formula>IsConverted = false &amp;&amp;  ISPICKVAL( Status , &apos;No Interest&apos;) &amp;&amp;  NOT(ISBLANK(Date_of_notification__c )) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates_2nd_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Notification for No Response Status</fullName>
        <active>false</active>
        <description>disabled for ticket #5010</description>
        <formula>IsConverted = false &amp;&amp;  ISPICKVAL( Status , &apos;No Response&apos;) &amp;&amp;  NOT(ISBLANK(Date_of_notification__c )) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates_2nd_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>67</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Notification for Open Status</fullName>
        <active>false</active>
        <formula>IsConverted = false &amp;&amp;  ISPICKVAL( Status , &apos;Open&apos;) &amp;&amp;  NOT(ISBLANK(Date_of_notification__c )) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>104</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_user_for_lead_updates_2nd_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>97</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Date_of_notification__c</offsetFromField>
            <timeLength>111</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead status change time</fullName>
        <actions>
            <name>Set_Lead_Status_Changed_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Lead datetime field when Lead status changed from open</description>
        <formula>AND( ISCHANGED(Status) , text(PRIORVALUE(Status))=&apos;Open&apos;, $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Sales Support Website Lead</fullName>
        <active>true</active>
        <formula>TEXT(LeadSource) = &apos;Company Website&apos; &amp;&amp; TEXT(Status) = &apos;Open&apos; &amp;&amp;   OR(Owner:Queue.DeveloperName = &apos;BD_Sales_Support_Team&apos;) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Team_on_N_nbr_of_Days_after_lead_creation</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Team_on_N_nbr_of_Days_after_lead_creation</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Team_on_N_nbr_of_Days_after_lead_creation</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify Sector Lead on Open Website Lead</fullName>
        <active>true</active>
        <formula>TEXT(LeadSource) = &apos;Company Website&apos; &amp;&amp; TEXT(Status) = &apos;Open&apos; &amp;&amp;   OR( Owner:User.Email = &apos;vikrant.bhansali@difc.ae&apos;, Owner:User.Email = &apos;chris.divito@difc.ae&apos;, Owner:User.Email = &apos;khadija.ali@difc.ae&apos;, Owner:User.Email = &apos;gracita.aoadegracia@difc.ae&apos; ) &amp;&amp; $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Team_on_N_nbr_of_Days_after_lead_creation</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Team_on_N_nbr_of_Days_after_lead_creation</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OB Assign Record Type - Speciality Leasing</fullName>
        <actions>
            <name>Assign_Record_Type_Speciality_Leasing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND( 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.Company_Type__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Type_of_Property__c</field>
            <operation>equals</operation>
            <value>Lease a Kiosk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Company Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>If the lead is created through WebtoLead and entity type entered by the client is Retail and Type of Property is Lease a Kiosk then assign it to the Speciality Leasing Record Type</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Lead Creation - Website %26 Call Center</fullName>
        <actions>
            <name>Lead_Creation_Website_and_Call_Center</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Telephone Inquiries</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sub_Source__c</field>
            <operation>notEqual</operation>
            <value>Self Registration</value>
        </criteriaItems>
        <description>Customer : This workflow rule will be used when a lead is created either from the DIFC Website or Call Center to sent out to the customer.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Lead Creation - Website Self Registration Financial Funds</fullName>
        <actions>
            <name>Lead_Creation_Website_Self_Registration_Financial_Funds</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>OB_Lead_Creation_Website_Self_Registration_Financial_Funds</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Company Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sub_Source__c</field>
            <operation>equals</operation>
            <value>Self Registration</value>
        </criteriaItems>
        <description>This email template will be used when a lead is created from the DIFC Website Self Registration for Financial and Investment Funds.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reset Notification Date when Status Changed</fullName>
        <actions>
            <name>Reset_Notification_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reset Notification Date when status changed in to (High, Low, Medium, No Interest, No Response)</description>
        <formula>AND(  ISCHANGED(Status), OR( ISPICKVAL(Status, &apos;Open&apos;),   ISPICKVAL(Status, &apos;Low&apos;),   ISPICKVAL(Status, &apos;Medium&apos;),   ISPICKVAL(Status, &apos;High&apos;),   ISPICKVAL(Status, &apos;No Response&apos;),  ISPICKVAL(Status, &apos;No Interest&apos;)), $User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Lead Record Type</fullName>
        <actions>
            <name>Set_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type_for_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISNEW() || ISCHANGED(RecordTypeId) || ISCHANGED( Area_of_Business__c )) &amp;&amp; NOT( ISNULL(RecordTypeId )) &amp;&amp;   Record_Type__c != &quot;Retail&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Time line Changed</fullName>
        <actions>
            <name>Time_line_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>BD would like to track when Time line was Changed Gracita (BD) knows the requests</description>
        <formula>ISCHANGED(Time_line__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Contact Address Copied</fullName>
        <actions>
            <name>Update_Contact_address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Country for Contact</fullName>
        <actions>
            <name>Update_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$User.Username &lt;&gt; $Label.User_WF_Trigger_Disabled</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Text Id</fullName>
        <actions>
            <name>Update_Lead_Text_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Commercial Unit Offer Process 5633</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Kindly_follow_up_with_the_lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.Follow_up_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Kindly follow up with the lead</subject>
    </tasks>
</Workflow>