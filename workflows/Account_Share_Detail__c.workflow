<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Share_Capital_Update</fullName>
        <field>Share_Capital_Updated__c</field>
        <formula>NOW()</formula>
        <name>Account Share Capital Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Issued_Capital_Update</fullName>
        <field>Issued_Capital__c</field>
        <formula>Total_Issued__c * Nominal_Value__c</formula>
        <name>Issued Capital Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Change in Share Detail</fullName>
        <actions>
            <name>Account_Share_Capital_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Issued_Capital_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( No_of_shares_per_class__c ) || ISCHANGED( Nominal_Value__c ) || For_Updates__c=true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>