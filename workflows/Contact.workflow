<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Liquidators</fullName>
        <description>Email to Liquidators</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/AccountContactRelationLiquidators</template>
    </alerts>
    <alerts>
        <fullName>GS_record_Type_Update</fullName>
        <description>GS record Type Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>c-veeranarayana.p@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chandrakumar.k@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GS_record_Type_Update</template>
    </alerts>
    <alerts>
        <fullName>OB_new_community_user</fullName>
        <description>OB new community user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_New_community_user</template>
    </alerts>
    <alerts>
        <fullName>Reminder_Violator_Email</fullName>
        <description>Reminder Violator Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Principle_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Violator_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Market_Email_Alert_To_Gs_Contacts</fullName>
        <description>Send Market Email Alert To Gs Contacts</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GS_System_Emails/Update_Contact_Details</template>
    </alerts>
    <alerts>
        <fullName>Violator_Email</fullName>
        <description>Violator Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Principle_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Violator_Email</template>
    </alerts>
    <alerts>
        <fullName>Visa_Expired_Notification</fullName>
        <description>Visa Expired Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Portal_User_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Visa_Expired_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visa_Expired_Reminder</fullName>
        <description>Visa Expired Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Principle_User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Visa_Expired_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assigned_to_Salmaa</fullName>
        <field>OwnerId</field>
        <lookupValue>salmaan.jaffery@difc.ae</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assigned to Salmaa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Update_Record_Type</fullName>
        <description>Update Record Type to Portal User</description>
        <field>RecordTypeId</field>
        <lookupValue>Portal_User</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OB Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Passport_Updated_On</fullName>
        <field>Passport_Updated_On__c</field>
        <formula>NOW()</formula>
        <name>Passport Updated On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_BP_Creation_Time</fullName>
        <field>BP_Created_Time__c</field>
        <formula>NOW()</formula>
        <name>Populate BP Creation Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Block_Service_Date</fullName>
        <field>Block_Services_Date__c</field>
        <formula>TODAY()+10</formula>
        <name>Populate Block Service Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Update_to_True</fullName>
        <field>SAP_Update__c</field>
        <literalValue>1</literalValue>
        <name>SAP Update to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Update_to_false</fullName>
        <field>SAP_Update__c</field>
        <literalValue>0</literalValue>
        <name>SAP Update to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Building_Name</fullName>
        <field>ADDRESS_LINE1__c</field>
        <formula>Account.Building_Name__c</formula>
        <name>Update Building Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_City</fullName>
        <field>CITY1__c</field>
        <formula>Account.City__c</formula>
        <name>Update City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_County</fullName>
        <description>Copies the county value from the account address to the related contacts.</description>
        <field>US_County__c</field>
        <formula>Account.US_County__c</formula>
        <name>Update County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Office_Number</fullName>
        <field>Office_Unit_Number__c</field>
        <formula>Account.Office__c</formula>
        <name>Update Office Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_P_O_Box</fullName>
        <field>PO_Box_HC__c</field>
        <formula>Account.PO_Box__c</formula>
        <name>Update P.O Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Postal_Code</fullName>
        <field>Postal_Code__c</field>
        <formula>Account.Postal_Code__c</formula>
        <name>Update Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Principle_User_Email</fullName>
        <field>Account_Principle_User_Email__c</field>
        <formula>Account.Principal_User__r.Email</formula>
        <name>Update Principle User Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_GS_Contact</fullName>
        <field>RecordTypeId</field>
        <lookupValue>GS_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to GS Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Street</fullName>
        <field>ADDRESS_LINE2__c</field>
        <formula>Account.Street__c</formula>
        <name>Update Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sys_Send_Email_to_False</fullName>
        <field>Sys_Send_Email__c</field>
        <literalValue>0</literalValue>
        <name>Update Sys Send Email to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Contact Not Updated by SAP</fullName>
        <actions>
            <name>SAP_Update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISCHANGED( Updated_from_SAP__c )) &amp;&amp; SAP_Update__c=TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Updated by SAP</fullName>
        <actions>
            <name>SAP_Update_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Updated_from_SAP__c ) &amp;&amp; SAP_Update__c=FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Account Address</fullName>
        <actions>
            <name>Update_Building_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_County</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Office_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_P_O_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Copy_Contact_Address__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email to Liquidators</fullName>
        <actions>
            <name>Email_to_Liquidators</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Liquidator_Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OB Update Record Type to Portal User</fullName>
        <actions>
            <name>OB_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Send_Portal_Login_Link__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Digital Onboarding : Update Record Type to Portal User if OB_Send_Portal_Link=Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Passport No changed</fullName>
        <actions>
            <name>Passport_Updated_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISNEW() &amp;&amp; NOT(ISBLANK(Passport_No__c))) || ISCHANGED( Passport_No__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate BP Creation Time</fullName>
        <actions>
            <name>Populate_BP_Creation_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow is use to populate creation datetime for GSO Authorized Signatory. The populated field is use in Informatica</description>
        <formula>BP_No__c &lt;&gt; &quot;&quot; &amp;&amp;  Use_ZGS1_Auth_Group__c = true &amp;&amp; RecordType.DeveloperName = &quot;GS_Contact&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Salmaan Contacts</fullName>
        <actions>
            <name>Assigned_to_Salmaa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.OwnerId</field>
            <operation>equals</operation>
            <value>Laila Shukralla</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email for Violator</fullName>
        <actions>
            <name>Violator_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Populate_Block_Service_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Principle_User_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Is_Violator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Violator_Email</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update the Individual record Type to GS Contact</fullName>
        <actions>
            <name>GS_record_Type_Update</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Record_Type_to_GS_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE( RecordTypeId )==&apos;0122000000034qG&apos; &amp;&amp; RecordTypeId ==&apos;0122000000033K4&apos; &amp;&amp; ISCHANGED(RecordTypeId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Visa Expired Notification</fullName>
        <actions>
            <name>Visa_Expired_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Visa_Expiry_Date__c</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Visa Expired Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Visa_Expired_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Visa Expired Notification New</fullName>
        <actions>
            <name>Visa_Expired_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Sys_Send_Email_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Sys_Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Visa Expired Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>