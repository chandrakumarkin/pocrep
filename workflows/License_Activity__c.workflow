<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_business_Activity</fullName>
        <description>New business Activity</description>
        <protected>false</protected>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ROC_Email_System_Emails/New_business_Activity</template>
    </alerts>
    <rules>
        <fullName>Activity Added Notification</fullName>
        <actions>
            <name>New_business_Activity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Registration_License_No__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send a notification to Nadine and Alya when new Activity added</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Auto Populate License Activity Name</fullName>
        <active>false</active>
        <criteriaItems>
            <field>License_Activity__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>