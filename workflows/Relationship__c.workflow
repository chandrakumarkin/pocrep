<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>End_Date</fullName>
        <field>End_Date__c</field>
        <formula>TODAY()</formula>
        <name>End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Relationship_Unique_No</fullName>
        <field>Relationship_Unqiue_No__c</field>
        <formula>SUBSTITUTE( Relationship_Unqiue_No__c , LEFT(RIGHT(Relationship_Unqiue_No__c,19),10) , IF(MONTH(End_Date__c)&lt;10,&apos;0&apos;&amp; TEXT(MONTH(End_Date__c)), TEXT(MONTH(End_Date__c)))&amp;&apos;/&apos; &amp; IF(DAY(End_Date__c)&lt;10,&apos;0&apos;&amp; TEXT(DAY(End_Date__c)), TEXT(DAY(End_Date__c)))&amp;&apos;/&apos; &amp; TEXT(YEAR(End_Date__c)) )</formula>
        <name>Relationship Unique No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Relationship_group</fullName>
        <field>Relationship_Group__c</field>
        <formula>CASE(Relationship_Type__c , 
&apos;Has Legal Adviser&apos;,&apos;ROC&apos;, 
&apos;Has Manager&apos;,&apos;ROC&apos;, 
&apos;Has Director&apos;,&apos;ROC&apos;, 
&apos;Has Company Secretary&apos;,&apos;ROC&apos;, 
&apos;Has Partner&apos;,&apos;ROC&apos;, 
&apos;Has General Partner-NA&apos;,&apos;ROC&apos;, 
&apos;Has Auditor&apos;,&apos;ROC&apos;, 
&apos;Has Designated Member&apos;,&apos;ROC&apos;, 
&apos;Has Member&apos;,&apos;ROC&apos;, 
&apos;Has RRC Member&apos;,&apos;ROC&apos;, 
&apos;Client Relations Officer (CRO)&apos;,&apos;ROC&apos;, 
&apos;Has LLP Member&apos;,&apos;ROC&apos;, 
&apos;Has LLC Member&apos;,&apos;ROC&apos;, 
&apos;Has Limited Partner - NA&apos;,&apos;ROC&apos;, 
&apos;Is Partner-Shareholder&apos;,&apos;ROC&apos;, 
&apos;Has Senior Executive Officer (SEO)&apos;,&apos;ROC&apos;, 
&apos;Has Chief Financial Officer (CFO)&apos;,&apos;ROC&apos;, 
&apos;Has Compliance Officer (CO)&apos;,&apos;ROC&apos;, 
&apos;Has Money Laundering Reporting Officer (MLRO)&apos;,&apos;ROC&apos;, 
&apos;Is Real Estate Main Contact&apos;,&apos;ROC&apos;, 
&apos;Was Contact Person&apos;,&apos;ROC&apos;, 
&apos;Has Contact Person&apos;,&apos;BD&apos;, 
&apos;Has Personal Assistant&apos;,&apos;ROC&apos;, 
&apos;Has CEO/Regional Head/Chairman&apos;,&apos;ROC&apos;, 
&apos;Has Office Manager&apos;,&apos;ROC&apos;, 
&apos;Has IT contact&apos;,&apos;ROC&apos;, 
&apos;Has IT Data Center contact&apos;,&apos;ROC&apos;, 
&apos;Has Affiliate&apos;,&apos;BD&apos;, 
&apos;Has Holding Company&apos;,&apos;ROC&apos;, 
&apos;Has Protected Cell Company (PCC)&apos;,&apos;ROC&apos;, 
&apos;Has Special Purpose Company (SPC)&apos;,&apos;ROC&apos;, 
&apos;Has Fund / Fund Vehicle&apos;,&apos;BD&apos;, 
&apos;Is Authorized Signatory for&apos;,&apos;ROC&apos;, 
&apos;Has Principal User&apos;,&apos;ROC&apos;, 
&apos;Has Corporate Communication&apos;,&apos;ROC&apos;, 
&apos;Has Affiliates with Same Management&apos;,&apos;BD&apos;, 
&apos;Is Developer NOL for&apos;,&apos;RORP&apos;, 
&apos;Has Founding Member&apos;,&apos;ROC&apos;, 
&apos;Has Authorized Representative&apos;,&apos;ROC&apos;, 
&apos;Has General Partner&apos;,&apos;ROC&apos;, 
&apos;Has Limited Partner&apos;,&apos;ROC&apos;, 
&apos;Has Service Document Responsible for&apos;,&apos;ROC&apos;, 
&apos;Is Member LLC Of&apos;,&apos;ROC&apos;, 
&apos;Beneficiary Owner&apos;,&apos;ROC&apos;, 
&apos;Has DIFC Sponsored Employee&apos;,&apos;GS&apos;, 
&apos;Has DIFC Local /GCC Employees&apos;,&apos;GS&apos;, 
&apos;Has DIFC Seconded&apos;,&apos;GS&apos;, 
&apos;Family Has the Member&apos;,&apos;GS&apos;, 
&apos;Is Domestic Helper Of&apos;,&apos;GS&apos;, 
&apos;Is Personal Visitor for&apos;,&apos;GS&apos;, 
&apos;Has Temporary Employee&apos;,&apos;GS&apos;, 
&apos;Is Business Visitor for&apos;,&apos;GS&apos;, 
&apos;Is Point of contact for&apos;,&apos;RORP&apos;, 
&apos;Is Public Relationship officer for&apos;,&apos;RORP&apos;, 
&apos;Has DIFC GCC Employee&apos;,&apos;GS&apos;, 
&apos;Has DIFC Local Employee&apos;,&apos;GS&apos;, 
&apos;GS Portal Contact&apos;,&apos;GS&apos;,
&apos;Is Shareholder Of&apos;,&apos;ROC&apos;,
&apos;Is a Shareholder of&apos;,&apos;RORP&apos;,
&apos;DIFC Authorized Signatory&apos;,&apos;RORP&apos;,
&apos;Is Fit Out Point of Contact&apos;,&apos;FitOut&apos;,
&apos;Is Fit Out Contractor&apos;,&apos;FitOut&apos;,
&apos;Has Exempt Fund&apos;,&apos;BD&apos;,
&apos;Has the Employee Responsible&apos;,&apos;BD&apos;,
&apos;ROC&apos;)</formula>
        <name>Relationship group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Update_to_True</fullName>
        <field>SAP_Update__c</field>
        <literalValue>1</literalValue>
        <name>SAP Update to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Update_to_false</fullName>
        <field>SAP_Update__c</field>
        <literalValue>0</literalValue>
        <name>SAP Update to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate End Date on Inactive Relationship</fullName>
        <actions>
            <name>End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Relationship__c.Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Relationship Not Updated by SAP</fullName>
        <actions>
            <name>SAP_Update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISCHANGED( Updated_from_SAP__c )) &amp;&amp; SAP_Update__c=TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Relationship Updated by SAP</fullName>
        <actions>
            <name>SAP_Update_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Updated_from_SAP__c ) &amp;&amp; SAP_Update__c=FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Relationship Group</fullName>
        <actions>
            <name>Relationship_group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Relationship__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Relationship__c.Relationship_Group__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Relationship Unique No</fullName>
        <actions>
            <name>Relationship_Unique_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Relationship__c.Relationship_Unqiue_No__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Relationship__c.Relationship_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>