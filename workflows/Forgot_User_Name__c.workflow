<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Forget_User_Name_Email</fullName>
        <description>Forget User Name Email</description>
        <protected>false</protected>
        <recipients>
            <field>User_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Forget_User_Name</template>
    </alerts>
    <rules>
        <fullName>User Forget User Name</fullName>
        <actions>
            <name>Forget_User_Name_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Forgot_User_Name__c.User_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to send email  to customer for forget user name</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>