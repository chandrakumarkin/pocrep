<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Portal_Name</fullName>
        <field>Portal_Service_Request_Name__c</field>
        <formula>Menutext__c</formula>
        <name>Update Portal Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Portal Sevice Request Name Autofill</fullName>
        <actions>
            <name>Update_Portal_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF( Name != Null,TRUE,FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>