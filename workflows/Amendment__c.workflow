<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_be_send_out_on_contractor_insurance_expiry</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Notification to be send out on contractor insurance expiry</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Address_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>saleh.abdulhalim@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thusith.dhananjaya@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Contractor_Insurance_expiry_Notification_for_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_be_send_out_on_contractor_insurance_expiry1</fullName>
        <description>Notification to be send out on contractor insurance expiry1</description>
        <protected>false</protected>
        <recipients>
            <recipient>DIFC_IT_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Contractor_Insurance_expiry_Notification_for_fit_out</template>
    </alerts>
    <fieldUpdates>
        <fullName>Amendment_SR_Document</fullName>
        <description>#6127</description>
        <field>Marital_Status__c</field>
        <literalValue>Unspecific</literalValue>
        <name>Amendment SR Document</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Name_DDP_update</fullName>
        <field>Name_DDP__c</field>
        <formula>Name__c</formula>
        <name>Name DDP update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_of_Desks</fullName>
        <field>No_of_Desks__c</field>
        <formula>Lease_Unit__r.SAP_No_of_Desks__c</formula>
        <name>No. of Desks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ownership_Type</fullName>
        <field>Ownership_Type__c</field>
        <formula>TEXT(Ownership_Type__c)</formula>
        <name>Ownership Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_BC_Unit_on_SR</fullName>
        <field>Summary__c</field>
        <formula>IF(ServiceRequest__r.Summary__c &lt;&gt; &quot;&quot; , ServiceRequest__r.Summary__c + &quot; , &quot; + Lease_Unit__r.Name  , Building__c + &quot; - &quot; + Lease_Unit__r.Name)</formula>
        <name>Populate BC Unit on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Entity_Name_on_SR_for_Lease_Reg</fullName>
        <field>Entity_Name_checkbox__c</field>
        <literalValue>1</literalValue>
        <name>Populate Entity Name on SR for Lease Reg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SR_Finalize_Flag_update</fullName>
        <description>#6127</description>
        <field>finalizeAmendmentFlg__c</field>
        <literalValue>1</literalValue>
        <name>SR Finalize Flag update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unit_Rent</fullName>
        <field>Unit_Rent__c</field>
        <formula>(Unit_Area__c * Rent_AED_per_sq_ft__c)/12</formula>
        <name>Unit Rent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unit_Rent_on_SR</fullName>
        <field>Sys_No_of_Shares__c</field>
        <formula>Rent_AED_per_sq_ft__c</formula>
        <name>Unit Rent on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Bene_Utlimate_Owner_field</fullName>
        <description>Update beneficial ultimate owner field when record type is UBO</description>
        <field>Name_of_Body_Corporate_Shareholder__c</field>
        <formula>IF(ISPICKVAL( Amendment_Type__c , &apos;Body Corporate&apos;), Company_Name__c , Given_Name__c +&apos; &apos;+ Family_Name__c )</formula>
        <name>Update Bene/Utlimate Owner field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Error_Message_field_on_SR</fullName>
        <field>Error_Message__c</field>
        <formula>Capacity__c</formula>
        <name>Update Error Message field on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Error_from_SAP_field_TRUE_SR</fullName>
        <description>#6881</description>
        <field>Is_Error_From_SAP__c</field>
        <literalValue>0</literalValue>
        <name>Update Is Error from SAP field TRUE SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Error_from_SAP_field_on_SR</fullName>
        <field>Is_Error_From_SAP__c</field>
        <literalValue>1</literalValue>
        <name>Update Is Error from SAP field on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Required_Service_field_on_SR</fullName>
        <field>Required_Service__c</field>
        <literalValue>Successfully Processed in SAP</literalValue>
        <name>Update Required Service field on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SAP_Process_Flag_field_on_SR</fullName>
        <field>SAP_Process_Flag__c</field>
        <formula>Description__c</formula>
        <name>Update SAP Process Flag field on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SAP_Transaction_No_field_on_SR</fullName>
        <field>SAP_OBELNR__c</field>
        <formula>Address2__c</formula>
        <name>Update SAP Transaction No. field on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Updated_from_SAP_field_on_SR</fullName>
        <field>Updated_from_SAP__c</field>
        <formula>Updated_from_SAP__c</formula>
        <name>Update Updated from SAP field on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updating_email_address1</fullName>
        <field>Email_Address_2__c</field>
        <formula>ServiceRequest__r.Email_Address__c</formula>
        <name>Updating email address1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updating_email_address_of_contractor</fullName>
        <field>Email_Address__c</field>
        <formula>ServiceRequest__r.Email__c</formula>
        <name>Updating email address of contractor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Usage_Type_on_SR</fullName>
        <field>Usage_Type__c</field>
        <formula>TEXT(Lease_Unit__r.Unit_Usage_Type__c)</formula>
        <name>Usage Type on SR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ServiceRequest__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Create SR document for Company Buyer</fullName>
        <actions>
            <name>Amendment_SR_Document</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SR_Finalize_Flag_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Request__c.Client_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Freehold_Transfer_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.Tenant_Type_RORP__c</field>
            <operation>equals</operation>
            <value>Company,Individual</value>
        </criteriaItems>
        <criteriaItems>
            <field>Amendment__c.Amendment_Type__c</field>
            <operation>equals</operation>
            <value>Buyer,Seller</value>
        </criteriaItems>
        <description>#6127</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Make Lease Certificate Visible for Reg DIFC Tenant and Other</fullName>
        <actions>
            <name>Populate_Entity_Name_on_SR_for_Lease_Reg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will update the field on SR - Entity_Name_checkbox__c	only if tenant type is other or DIFC registered. In case of DIFC registered company it will only update once company become active</description>
        <formula>SR_Record_Type__c = &apos;Lease_Registration&apos; &amp;&amp;  TEXT(Amendment_Type__c) = &apos;Tenant&apos; &amp;&amp;  OR(Record_Type_Name__c = &apos;Individual_Tenant&apos;,  Record_Type_Name__c = &apos;Body_Corporate_Tenant&apos; &amp;&amp;  TEXT(IssuingAuthority__c) &lt;&gt; &apos;DIFC - Under Formation&apos;) &amp;&amp; ServiceRequest__r.Entity_Name_checkbox__c = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification for contractor insurance</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Amendment__c.Service_Type__c</field>
            <operation>equals</operation>
            <value>Fit - Out Service Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Amendment__c.Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>Contractor_Insurance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.Change_Address__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Request__c.External_Status_Name__c</field>
            <operation>notEqual</operation>
            <value>Project Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Updating_email_address1</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Updating_email_address_of_contractor</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Amendment__c.End_Date__c</offsetFromField>
            <timeLength>-11</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Amendment__c.End_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_to_be_send_out_on_contractor_insurance_expiry</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Notification_to_be_send_out_on_contractor_insurance_expiry1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Amendment__c.End_Date__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate BC Unit on SR</fullName>
        <actions>
            <name>Populate_BC_Unit_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>this workflow will populate BC unit in existing &quot;Summary&quot; field</description>
        <formula>SR_Record_Type__c = &quot;Lease_Application_Request&quot;  &amp;&amp;  Lease_Unit__c &lt;&gt; NULL &amp;&amp;  SR_Status__c = &quot;Draft&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lease Registration Error on SR</fullName>
        <actions>
            <name>Update_Error_Message_field_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Is_Error_from_SAP_field_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SAP_Process_Flag_field_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( SR_Record_Type__c = &apos;Lease_Registration&apos; &amp;&amp; Record_Type_Name__c = &apos;Lease_Unit&apos; ,  OR(SR_Record_Type__c = &apos;Mortgage_Registration&apos;,SR_Record_Type__c = &apos;Mortgage_Variation&apos;,SR_Record_Type__c = &apos;Discharge_Mortgage&apos;) &amp;&amp; Record_Type_Name__c = &apos;Mortgage_Unit&apos;  ) &amp;&amp; Chilled_Water__c = TRUE &amp;&amp; Description__c = &apos;V&apos; &amp;&amp;  ServiceRequest__r.Is_Error_From_SAP__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lease Registration Sucess on SR</fullName>
        <actions>
            <name>Update_Is_Error_from_SAP_field_TRUE_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Required_Service_field_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_SAP_Transaction_No_field_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Updated_from_SAP_field_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&amp;&amp; ServiceRequest__r.Is_Error_From_SAP__c = FALSE,Description__c = &apos;&apos; &amp;&amp;</description>
        <formula>OR( SR_Record_Type__c = &apos;Lease_Registration&apos; &amp;&amp; Record_Type_Name__c = &apos;Lease_Unit&apos; ,  OR(SR_Record_Type__c = &apos;Mortgage_Registration&apos;,SR_Record_Type__c = &apos;Mortgage_Variation&apos;,SR_Record_Type__c = &apos;Discharge_Mortgage&apos;) &amp;&amp; Record_Type_Name__c = &apos;Mortgage_Unit&apos;  ) &amp;&amp; Address2__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Name of Beneficiary Owner FOR UBO</fullName>
        <actions>
            <name>Update_Bene_Utlimate_Owner_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update name of beneficiary owner for Individual UBO when UBO is added  from back end</description>
        <formula>AND(RecordType.DeveloperName == &apos;UBO&apos;,  ISBLANK($User.ContactId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Ownership Type on SR</fullName>
        <actions>
            <name>Ownership_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Amendment_Type__c,&apos;Buyer&apos;) &amp;&amp;  (ISCHANGED( Ownership_Type__c) || ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unit Rent and Usage Type</fullName>
        <actions>
            <name>No_of_Desks</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unit_Rent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unit_Rent_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Usage_Type_on_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name= &apos;Business Centre Office&apos; &amp;&amp; NOT(ISBLANK(Lease_Unit__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update name value for DDP</fullName>
        <actions>
            <name>Name_DDP_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED(Company_Name__c)||ISCHANGED(Given_Name__c) || ISCHANGED(Family_Name__c)|| ISCHANGED(Developer__c)||ISNEW()) &amp;&amp;  SR_Group__c &lt;&gt; &apos;Fit-Out &amp; Events&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>