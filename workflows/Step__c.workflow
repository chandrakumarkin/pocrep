<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AML_Email_Approval_Closed</fullName>
        <description>AML Email Approval _ Closed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>ROC_Officer</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/AML_Email_Approval_Closed</template>
    </alerts>
    <alerts>
        <fullName>AML_Step_Creation</fullName>
        <description>AML Step Creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lori.baker@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sadiqa.moosawi@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>t-iantha.antao@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wessam.iskandarani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Amount_deducted_for_the_Hoarding</fullName>
        <description>Amount deducted for the Hoarding</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sys_Additional_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All_with_Extra_info/Amount_deducted_for_the_Hoarding</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification</fullName>
        <description>Application Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_BC</fullName>
        <ccEmails>Businesscentre@difc.ae</ccEmails>
        <description>Application Approval Notification - BC</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_IT_Form</fullName>
        <ccEmails>IT.Infrastructure@difc.ae</ccEmails>
        <description>Application Approval Notification - IT Form</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_RORP</fullName>
        <ccEmails>RoRP.SRs@difc.ae</ccEmails>
        <description>Application Approval Notification RORP</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification_RORP</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_RORP_Freehold</fullName>
        <ccEmails>RoRP.SRs@difc.ae</ccEmails>
        <description>Application Approval Notification RORP Freehold</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification_RORP_Freehold</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_Tenant_Relation</fullName>
        <ccEmails>RoRP.SRs@difc.ae</ccEmails>
        <description>Application Approval Notification - Tenant Relation</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification_Tenant_Relation</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_for_RORP</fullName>
        <ccEmails>RoRP.SRs@difc.ae</ccEmails>
        <description>Application Approval Notification for RORP</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_Approval_Notification_for_Signage</fullName>
        <ccEmails>c-Danish.farooq@difc.ae</ccEmails>
        <ccEmails>difcsupervisor@enova-me.com</ccEmails>
        <ccEmails>Hasan.AlKhanjari@difc.ae</ccEmails>
        <description>Application Approval Notification for Signage</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Enova_User</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tashneem.tashrifwala@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification_Signage</template>
    </alerts>
    <alerts>
        <fullName>Application_In_Process_Notification_Tenant_Relation</fullName>
        <description>Application In Process Notification - Tenant Relation</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_In_Process_Notification_Tenant_Relation</template>
    </alerts>
    <alerts>
        <fullName>Application_Pending_Re_upload</fullName>
        <ccEmails>roc.helpdesk@difc.ae</ccEmails>
        <description>Application Pending Re-upload</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Pending_Re_upload</template>
    </alerts>
    <alerts>
        <fullName>Application_Rejection_Notification</fullName>
        <description>Application Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_Rejection_Notification_Car_Parking</fullName>
        <description>Application Rejection Notification Car Parking</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>difc@zoneparking.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Application_Rejection_Notification_Tenant_Relation</fullName>
        <description>Application Rejection Notification - Tenant Relation</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification_Tenant_Relation</template>
    </alerts>
    <alerts>
        <fullName>Application_Submission_AOR_NotifyFunds_Linda</fullName>
        <ccEmails>dfsafunds@dfsa.ae, linda.brooker@difc.ae</ccEmails>
        <description>Application Submission-AOR-NotifyFunds&amp;Linda</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Awaiting_Original_Documents</fullName>
        <description>Awaiting Original Documents</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Awaiting_Original_Documents</template>
    </alerts>
    <alerts>
        <fullName>Awaiting_Original_Documents_Reminder</fullName>
        <description>Awaiting Original Documents Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Original_Documents_Required_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Biometrics_Required</fullName>
        <description>Biometrics Required</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Biometrics_Required1</template>
    </alerts>
    <alerts>
        <fullName>Biometrics_required_for_Dependents</fullName>
        <description>Biometrics required for Dependents</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Biometrics_Required</template>
    </alerts>
    <alerts>
        <fullName>Cancellation_confirmation_Document</fullName>
        <description>Cancellation confirmation Document</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Cancel_Confirmation_Document</template>
    </alerts>
    <alerts>
        <fullName>Cancellation_of_Fitout_Milestone_Penalty</fullName>
        <ccEmails>mudasir.saleem@gmail.com</ccEmails>
        <description>Cancellation of Fitout Milestone Penalty</description>
        <protected>false</protected>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Cancellation_of_Fitout_Milestone_Penalty</template>
    </alerts>
    <alerts>
        <fullName>Car_Parking_Collection_Email_Alert</fullName>
        <description>Car Parking Collection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>difc@zoneparking.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Car_Parking_Collection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Car_Parking_Courier_Step_Email_Alert</fullName>
        <description>Car Parking Courier Step Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>difc@zoneparking.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Car_Parking_Courier_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Client_Notification_for_Fitout_Milestone_Penalty</fullName>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Client Notification for Fitout Milestone Penalty</description>
        <protected>false</protected>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Client_Notification_for_Fitout_Milestone_Penalty</template>
    </alerts>
    <alerts>
        <fullName>Completion_of_minor_snags_for_fit_out</fullName>
        <description>Completion of minor snags for fit out</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Snag_rectify_assigned_to_contractor</template>
    </alerts>
    <alerts>
        <fullName>Courier_Closed_Awaiting_Review</fullName>
        <description>Courier Closed Awaiting Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>GS_Front_Office_Team</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/GS_Notification_for_Courier_Closed</template>
    </alerts>
    <alerts>
        <fullName>Courier_Delivered</fullName>
        <description>Courier - Delivered</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Courier_Delivered</template>
    </alerts>
    <alerts>
        <fullName>Courier_Pending_Closed</fullName>
        <ccEmails>GS.BOPendings@difc.ae,shabbir.ahmed@difc.ae</ccEmails>
        <description>Courier Pending Closed</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/GS_Notification_for_Courier_Closed</template>
    </alerts>
    <alerts>
        <fullName>DFSA_funds_Application_Approval_Notification</fullName>
        <ccEmails>arun.singh@difc.ae</ccEmails>
        <ccEmails>DFSAFunds@dfsa.ae</ccEmails>
        <description>DFSA funds Application Approval Notification</description>
        <protected>false</protected>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/DFSA_Investment_Fund_Notification_Security</template>
    </alerts>
    <alerts>
        <fullName>Deduction_of_Hoarding_Cost_Notification</fullName>
        <description>Deduction of Hoarding Cost Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sys_Additional_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All_with_Extra_info/Deduction_of_Hoarding_Cost_Notification</template>
    </alerts>
    <alerts>
        <fullName>Deleted_Hoarding_installation_date_Notification</fullName>
        <description>Deleted Hoarding installation date Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sys_Additional_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All_with_Extra_info/Hoarding_installation_date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Doc_upload_neither_ROC_nor_RORP</fullName>
        <description>Doc upload - neither ROC nor RORP</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Roc_Officer_Notification</template>
    </alerts>
    <alerts>
        <fullName>Document_Ready_Escalation</fullName>
        <description>Document Ready Escalation</description>
        <protected>false</protected>
        <recipients>
            <recipient>DIFC_IT_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Document_Ready_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Document_Upload_Pending_DP</fullName>
        <description>Document Upload Pending DP</description>
        <protected>false</protected>
        <recipients>
            <recipient>Fine_Process_CoDP</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Document_Ready_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Document_Upload_Pending_ROC</fullName>
        <description>Document Upload Pending ROC</description>
        <protected>false</protected>
        <recipients>
            <recipient>Fine_Process_Registrar</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Document_Ready_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Customers_for_5_months_old_SRs</fullName>
        <description>Email Alert to Customers for 5 months old SRs</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Reminder_to_5_Months_Old_Pending_SRs</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_BO_for_notifying_medical_appointment_changes</fullName>
        <description>Email alert to BO for notifying medical appointment changes</description>
        <protected>false</protected>
        <recipients>
            <recipient>Medical_appointment_changes</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notify_BO_for_medical_step_changes</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_with_induction_timingss</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Email alert with induction timings</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>saleh.abdulhalim@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thusith.dhananjaya@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Induction_Scheduled_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_on_DIFC_FO_rejection_of_wallet_refund</fullName>
        <ccEmails>Vineeth.Dominic@idama.ae</ccEmails>
        <description>Email on DIFC FO rejection of wallet refund</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Email_on_DIFC_SVP_rejection_of_wallet_refund</fullName>
        <ccEmails>Vineeth.Dominic@enova-me.com</ccEmails>
        <ccEmails>Ahmed.Amin@enova-me.com</ccEmails>
        <ccEmails>Saleh.AbdulHalim@difc.ae</ccEmails>
        <ccEmails>Omar.Shafaamri@difc.ae</ccEmails>
        <ccEmails>Sherrylle.Arzaga@difc.ae</ccEmails>
        <description>Email on DIFC SVP rejection of wallet refund</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Email_on_FOSP_rejection_of_wallet_refund</fullName>
        <ccEmails>Saleh.AbdulHalim@difc.ae</ccEmails>
        <ccEmails>Omar.Shafaamri@difc.ae</ccEmails>
        <ccEmails>Sherrylle.Arzaga@difc.ae</ccEmails>
        <description>Email on FOSP rejection of wallet refund</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Email_on_rejection_for_client</fullName>
        <description>Email on rejection for client</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification_for_fit_out_client</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Applicant_Email_Voluntary_Strike_off</fullName>
        <description>Email to Applicant Email- Voluntary Strike off</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Request_for_voluntary_strike_off_is_received</template>
    </alerts>
    <alerts>
        <fullName>Entry_Permit_Upload_Notification</fullName>
        <description>Entry Permit Upload- Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Entry_Permit_Uploded_Notification</template>
    </alerts>
    <alerts>
        <fullName>Entry_Permit_is_issued_Notification</fullName>
        <description>Entry Permit is issued - Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Entry_Permit_is_issued_Notification</template>
    </alerts>
    <alerts>
        <fullName>Establishment_Card_form_is_typed_Notification_for_Violators</fullName>
        <description>Establishment Card form is typed Notification for Violators</description>
        <protected>false</protected>
        <recipients>
            <recipient>binita.balagopalan@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jyoti.dhanak@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>moosa.chola@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shema.saeedjamal@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Establishment_Card_form_is_typed_Notification_for_Violators</template>
    </alerts>
    <alerts>
        <fullName>Event_Inspection_Notification</fullName>
        <description>Event Inspection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>raneem.baassiri@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_Notification_for_events_for_organizer</template>
    </alerts>
    <alerts>
        <fullName>Event_Inspection_Notification2</fullName>
        <description>Event Inspection Notification2</description>
        <protected>false</protected>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_Notification_for_events_for_organizer</template>
    </alerts>
    <alerts>
        <fullName>Event_Inspection_Notification_for_events_raised_by_event_contractor</fullName>
        <description>Event Inspection Notification for events raised by event contractor1</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>raneem.baassiri@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_notification_for_events_raised_by_event_contractor</template>
    </alerts>
    <alerts>
        <fullName>Event_Inspection_Notification_for_events_raised_by_event_contractor2</fullName>
        <description>Event Inspection Notification for events raised by event contractor2</description>
        <protected>false</protected>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_Notification_for_events_for_organizer</template>
    </alerts>
    <alerts>
        <fullName>Exit_Process_Notification_to_Customer_when_first_step_is_verifed</fullName>
        <ccEmails>dissolution@difc.ae</ccEmails>
        <description>Exit Process : Notification to Customer when first step is verifed</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Liquidator_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Winding_Up/Exit_Process_Notification_to_Customer_when_first_step_is_verifed</template>
    </alerts>
    <alerts>
        <fullName>FD_is_Verified_but_not_posted</fullName>
        <description>FD is Verified but not posted</description>
        <protected>false</protected>
        <recipients>
            <recipient>c-shaikh.zoebakhtar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-veeranarayana.p@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chandrakumar.k@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GS_System_Emails/Front_Desk_Verified_Notification</template>
    </alerts>
    <alerts>
        <fullName>FOSP_Milestone_Penalty_Step</fullName>
        <description>FOSP Milestone Penalty Step</description>
        <protected>false</protected>
        <recipients>
            <recipient>FOSP</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/FOSP_Milestone_Penalty_Step</template>
    </alerts>
    <alerts>
        <fullName>FitOut_Watch_for_the_Contractor_steps</fullName>
        <description>FitOut Watch for the Contractor steps</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-mudasir.saleem@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Fit_Out_Contractor_Step_Watch</template>
    </alerts>
    <alerts>
        <fullName>Fit_Out_Inspection_notification</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fit Out Inspection notification1</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>omar.shafaamri@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>saleh.abdulhalim@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_Notification_for_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Fit_Out_Inspection_notification3</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <ccEmails>Sherrylle.Arzaga@difc.ae</ccEmails>
        <ccEmails>t-Shaheer.Sooppy@difc.ae</ccEmails>
        <description>Fit Out Inspection notification retail</description>
        <protected>false</protected>
        <recipients>
            <recipient>dalal.naqib@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.khoory@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olesya.buravskaya@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_Notification_for_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Fit_Out_Inspection_notification4</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>ala.jayyusi@difc.ae</ccEmails>
        <ccEmails>Sherrylle.Arzaga@difc.ae</ccEmails>
        <description>Fit Out Inspection notification office</description>
        <protected>false</protected>
        <recipients>
            <recipient>jayashree.narmada@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mukti.gurung@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Inspection_Notification_for_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Client_Feedback_Notification</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Client Feedback Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Email_Notification_for_Client_Feedback_Step</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Client_Feedback_Notification_Reminder_1</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Client Feedback Notification - Reminder 1</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Notification_for_Client_Feedback_Step_Reminder_1</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Client_Feedback_Notification_Reminder_2</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Client Feedback Notification - Reminder 2</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Notification_for_Client_Feedback_Step_Reminder_2</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Client_Feedback_Notification_Reminder_3</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Client Feedback Notification - Reminder 3</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Notification_for_Client_Feedback_Step_Reminder_3</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Contractor_Feedback_Notification</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Contractor Feedback Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Contractor_email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Email_Notification_for_Contractor_Feedback_Step</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Contractor_Feedback_Notification_Reminder_1</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Contractor Feedback Notification - Reminder 1</description>
        <protected>false</protected>
        <recipients>
            <field>Contractor_email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Notification_for_Contractor_Feedback_Step_Reminder_1</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Contractor_Feedback_Notification_Reminder_2</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Contractor Feedback Notification - Reminder 2</description>
        <protected>false</protected>
        <recipients>
            <field>Contractor_email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Notification_for_Contractor_Feedback_Step_Reminder_2</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Contractor_Feedback_Notification_Reminder_3</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Fitout Contractor Feedback Notification - Reminder 3</description>
        <protected>false</protected>
        <recipients>
            <field>Contractor_email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_Notification_for_Contractor_Feedback_Step_Reminder_3</template>
    </alerts>
    <alerts>
        <fullName>Fitout_Milestone_Penalty_on_Approval</fullName>
        <description>Fitout Milestone Penalty on Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Deduction_of_Fitout_Milestone_Penalty_Cost</template>
    </alerts>
    <alerts>
        <fullName>Head_of_Operations_Visa_exception_Notification</fullName>
        <description>Head of Operations Visa exception Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Head_Of_Operations_Visa_Exception</template>
    </alerts>
    <alerts>
        <fullName>Hoarding_Application_Approval_Notification</fullName>
        <description>Hoarding Application Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Hoarding_Application_Rejection_Notification</fullName>
        <description>Hoarding Application Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Hoarding_Cost_Step_Notification_to_FOSP_and_FOT</fullName>
        <description>Hoarding Cost Step Notification to FOSP and FOT</description>
        <protected>false</protected>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All_with_Extra_info/Hoarding_Cost_Step_Notification_to_FOSP_and_FOT</template>
    </alerts>
    <alerts>
        <fullName>Hoarding_Step_for_FOSP</fullName>
        <description>Hoarding Step for FOSP</description>
        <protected>false</protected>
        <recipients>
            <recipient>FOSP</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Hoarding_Verification_By_FOSP_Notification</fullName>
        <description>Hoarding Verification By FOSP Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_with_Extra_info/Hoarding_Step_assigned_to_FOSP</template>
    </alerts>
    <alerts>
        <fullName>Induction_Approval_SMS_for_event</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>Induction Approval SMS for event</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Induction_Approval_SMS_for_event</template>
    </alerts>
    <alerts>
        <fullName>Induction_Approval_SMS_for_fit_out</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>Induction Approval SMS for fit out</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Induction_Approval_SMS_for_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Induction_Scheduled_for_events</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Induction Scheduled for events</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>chantelle.fernandes@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>raneem.baassiri@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Induction_Scheduled_Notification_for_events_raised_by_contractor</template>
    </alerts>
    <alerts>
        <fullName>Induction_Scheduled_for_events_raised_by_client</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Induction Scheduled for events raised by client</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>chantelle.fernandes@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>raneem.baassiri@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Induction_Scheduled_Notification_for_event</template>
    </alerts>
    <alerts>
        <fullName>Initiate_refund_request_with_finance</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Initiate refund request with finance</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Initiate_refund_request_with_finance</template>
    </alerts>
    <alerts>
        <fullName>Internal_Clearance_to_DIFCA</fullName>
        <ccEmails>epn@difc.ae</ccEmails>
        <ccEmails>difc@zoneparking.ae</ccEmails>
        <ccEmails>roc.inspection@difc.ae</ccEmails>
        <ccEmails>Ayman.Mahmood@difc.ae</ccEmails>
        <ccEmails>Muzna.Ahmed@difc.ae</ccEmails>
        <description>Internal Clearance to DIFCA</description>
        <protected>false</protected>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.dejesus@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>muthahara.niamathullah@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nada.rustom@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nasser.belhabala@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pratiksha.gurung@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>raquel.batiancila@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sharon.plong@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sophia.janahi@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zainab.hassan@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Internal_Clearance_to_DIFCA_LTD_SPC_VFP</template>
    </alerts>
    <alerts>
        <fullName>LM_HOD_Rejected_PSA_Termination_Notification_to_FD</fullName>
        <description>LM/HOD Rejected PSA Termination Notification to FD</description>
        <protected>false</protected>
        <recipients>
            <recipient>GS_Front_Office_Team</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>c-veeranarayana.p@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GS_System_Emails/LM_HOD_Rejected_PSA_Termination_Notification_to_FD</template>
    </alerts>
    <alerts>
        <fullName>Listing_Pending_on_Customer_Notifications</fullName>
        <description>Listing : Pending on Customer Notifications</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Listing_Step_Pending_on_Customer_Notifications</template>
    </alerts>
    <alerts>
        <fullName>Listing_SR_Submission_Notification</fullName>
        <description>Listing : SR Submission Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>aristotle.eugenio@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hesham.mohamed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mukti.gurung@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Listing_Step_Pending_Review_Notifications</template>
    </alerts>
    <alerts>
        <fullName>Listing_Step_Pending_Review_Notifications</fullName>
        <description>Listing : Step Pending Review Notifications</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Listing_Step_Pending_Review_Notifications</template>
    </alerts>
    <alerts>
        <fullName>Medical_Certificate_Notification</fullName>
        <description>Medical Certificate Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Medical_Certificate_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_docs_generation_notification</fullName>
        <description>New docs generation notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Letter_Ready_for_Collection</template>
    </alerts>
    <alerts>
        <fullName>Notification_Closure_Step_is_closed_by_client_or_liquidator</fullName>
        <description>Notification :Closure Step is closed by client or liquidator</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notification_Closure_Step_is_closed_by_client_or_liquidator</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Step_owner_when_assigned_HOD_and_Line_Manager</fullName>
        <description>Notification to Step owner when assigned HOD and Line Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Step_assigned_to_DIFC_team_V1</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_close_Induction_Approval_Step</fullName>
        <description>Notification to close Induction Approval Step</description>
        <protected>false</protected>
        <recipients>
            <recipient>saleh.abdulhalim@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thusith.dhananjaya@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notify_to_close_Induction_Approval_Step</template>
    </alerts>
    <alerts>
        <fullName>Notify_Back_Office_Team_Customer_Pending</fullName>
        <ccEmails>gs.bopendings@difc.ae</ccEmails>
        <description>Notify Back Office Team Customer Pending</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notify_Front_Office_Team_Customer_Pending</template>
    </alerts>
    <alerts>
        <fullName>Notify_Front_Office_Team_Customer_Pending</fullName>
        <ccEmails>gs.bopendings@difc.ae</ccEmails>
        <description>Notify Front Office Team Customer Pending</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notify_Front_Office_Team_Customer_Pending</template>
    </alerts>
    <alerts>
        <fullName>Notify_GS_Team_after_Document_Uploaded</fullName>
        <description>Notify GS Team after Document Uploaded</description>
        <protected>false</protected>
        <recipients>
            <recipient>GS_Team</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Notify_GS_Team_after_pending_document_Uploaded</template>
    </alerts>
    <alerts>
        <fullName>Objection_Approval_Notification</fullName>
        <description>Objection Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fine_Process/Objection_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Objection_Approval_Notification_DP</fullName>
        <description>Objection Approval Notification DP</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fine_Process/Objection_Approval_Notification_DP</template>
    </alerts>
    <alerts>
        <fullName>Objection_Rejection_Notification</fullName>
        <description>Objection Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fine_Process/Objection_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Objection_Rejection_Notification_DP</fullName>
        <description>Objection Rejection Notification - DP</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fine_Process/Objection_Rejection_Notification_DP</template>
    </alerts>
    <alerts>
        <fullName>Original_Documents_Required</fullName>
        <description>Original Documents Required</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Original_Documents_Required</template>
    </alerts>
    <alerts>
        <fullName>Original_Documents_Required_SMS</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>Original Documents Required SMS</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/Original_Documents_Required_SMS</template>
    </alerts>
    <alerts>
        <fullName>PSA_Email_to_CC_LM_and_cashier_when_HOD_step_is_approved</fullName>
        <description>PSA Email to CC LM and cashier when HOD step is approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>Cashier</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>GS_Customer_Care_Team</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>GS_Line_Manager_Team</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GS_System_Emails/PSA_Email_to_CC_LM_and_cashier_when_HOD_step_is_approved</template>
    </alerts>
    <alerts>
        <fullName>Pending_Voluntary_Strike_off_Notification</fullName>
        <description>Pending Voluntary Strike off Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Pending_Voluntary_Strike_off</template>
    </alerts>
    <alerts>
        <fullName>Po_Box_Approval_Notification</fullName>
        <description>Po Box Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/PO_Box_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>ROC_App_Awaiting_Original_Notification_to_Client</fullName>
        <description>ROC - App Awaiting Original Notification to Client</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/ROC_App_Awaiting_Original_Notification_to_Client</template>
    </alerts>
    <alerts>
        <fullName>ROC_Step_SLA_Breached</fullName>
        <ccEmails>arun.singh@difc.ae</ccEmails>
        <description>ROC Step SLA Breached</description>
        <protected>false</protected>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>asma.almadani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hanaa.almunaifi@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khalid.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_SLA_breaches_Notification</template>
    </alerts>
    <alerts>
        <fullName>RORP_Document_uploaded_Req_info_Uploaded</fullName>
        <ccEmails>RoRP.SRs@difc.ae</ccEmails>
        <ccEmails>Mariam.Mussa@difc.ae</ccEmails>
        <description>RORP Document uploaded/ Req info Uploaded</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Roc_Officer_Notification</template>
    </alerts>
    <alerts>
        <fullName>RORP_Notification_to_FO_Team_Success_from_SAP</fullName>
        <description>RORP - Notification to FO Team Success from SAP</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/RORP_Success_from_SAP</template>
    </alerts>
    <alerts>
        <fullName>Re_Upload_Documents_Notification_Client</fullName>
        <description>Re_Upload_Documents_Notification_Client</description>
        <protected>false</protected>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Re_Upload_Documents_Notification_Client</template>
    </alerts>
    <alerts>
        <fullName>Re_upload_Document</fullName>
        <description>Re-upload Document</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/ReUpload_Documents_Notification</template>
    </alerts>
    <alerts>
        <fullName>Re_upload_Document_Car_Parking</fullName>
        <description>Re-upload Document Car Parking</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>difc@zoneparking.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/ReUpload_Documents_Notification</template>
    </alerts>
    <alerts>
        <fullName>Re_upload_Document_Reminder</fullName>
        <description>Re-upload Document - Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Re_Upload_Documents_Notification_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Re_upload_for_fit_out</fullName>
        <description>Re upload for fit out</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/ReUpload_Documents_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Re_upload_for_look_feel</fullName>
        <description>Re upload for look &amp; feel</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/ReUpload_Documents_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Rectify_Sangs_in_Project_Cancelled</fullName>
        <description>Rectify Sangs in Project Cancelled</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_contractor</template>
    </alerts>
    <alerts>
        <fullName>Rectify_snags_for_events</fullName>
        <description>Rectify snags for events</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>raneem.baassiri@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Snag_rectify_in_events</template>
    </alerts>
    <alerts>
        <fullName>Refund_ROC_Line_Manager_Notification</fullName>
        <ccEmails>roc.helpdesk@difc.ae</ccEmails>
        <description>Refund ROC Line Manager Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Refund_RORP_Line_Manager_Notification</fullName>
        <ccEmails>rorp@difc.ae</ccEmails>
        <description>Refund RORP Line Manager Notification</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Registrar_Approval_Notifications_Mortgage</fullName>
        <description>Registrar Approval Notifications - Mortgage</description>
        <protected>false</protected>
        <recipients>
            <recipient>RORP_Team_Notification</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Mortgage_Email_Templates/Registrar_Approval_Notifications_Mortg</template>
    </alerts>
    <alerts>
        <fullName>Registrar_Approval_on_Exit_Process</fullName>
        <ccEmails>dissolution@difc.ae</ccEmails>
        <description>Registrar Approval on Exit Process</description>
        <protected>false</protected>
        <recipients>
            <recipient>RoC_Officer</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Winding_Up/Registrar_Approval_on_Exit_Process</template>
    </alerts>
    <alerts>
        <fullName>Reject_SR_SMS</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>Reject SR SMS</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SR_Rejection_SMS</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Deduction_of_Hoarding_Cost_request</fullName>
        <description>Rejected Deduction of Hoarding Cost request</description>
        <protected>false</protected>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>All_with_Extra_info/Rejected_Deduction_of_Hoarding_Cost_request</template>
    </alerts>
    <alerts>
        <fullName>Rejected_SR_SMS_fit_outs</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>Rejected SR SMS fit out</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SR_Rejection_SMS_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Renewed_Establishment_Card_Notification</fullName>
        <description>Renewed Establishment Card Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Renewal_Establishment_Card_Template</template>
    </alerts>
    <alerts>
        <fullName>Require_More_Information_from_Customer_Notification</fullName>
        <ccEmails>roc.helpdesk@difc.ae</ccEmails>
        <description>Require More Information from Customer Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Pending_returned_for_more_information</template>
    </alerts>
    <alerts>
        <fullName>Require_more_information_for</fullName>
        <description>Require more information for fit out</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Return_for_More_Info_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Retunf_for_more_info_in_look_feel</fullName>
        <description>Retunf for more info in look &amp; feel</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Return_for_More_Info_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Return_for_More_Info_Notification</fullName>
        <description>Return for More Info. Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Return_for_More_Info_Notification</template>
    </alerts>
    <alerts>
        <fullName>Return_for_More_Info_Notification_Car_Parking</fullName>
        <description>Return for More Info. Notification Car Parking</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>difc@zoneparking.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khushboo.tahilramani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Return_for_More_Info_Notification</template>
    </alerts>
    <alerts>
        <fullName>Return_for_More_Info_Notification_Reminder</fullName>
        <description>Return for More Info. Notification - Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Return_for_More_Info_Notification_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Return_for_More_Info_Notification_Tenant_Relation</fullName>
        <description>Return for More Info. Notification - Tenant Relation</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Return_for_More_Info_Notification_Tenant_Relation</template>
    </alerts>
    <alerts>
        <fullName>Roc_Document_uploaded_Req_info_Uploaded</fullName>
        <ccEmails>RoC.SRs@difc.ae</ccEmails>
        <description>Roc Document uploaded/ Req info Uploaded</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Roc_Officer_Notification</template>
    </alerts>
    <alerts>
        <fullName>Roc_Officer_Notification</fullName>
        <description>Roc Officer Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Roc_Officer_Notification</template>
    </alerts>
    <alerts>
        <fullName>SLA_crossed_notification_for_fit_out</fullName>
        <ccEmails>Omar.Shafaamri@difc.ae</ccEmails>
        <ccEmails>Saleh.AbdulHalim@difc.ae</ccEmails>
        <ccEmails>Sherrylle.Arzaga@difc.ae</ccEmails>
        <description>SLA crossed notification for fit out</description>
        <protected>false</protected>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/SLA_Crossed_Notification_for_fit_ot</template>
    </alerts>
    <alerts>
        <fullName>SMS_for_fit_out_status_update</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SMS for fit out status update</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SMS_for_fit_out_status_update_step</template>
    </alerts>
    <alerts>
        <fullName>SR_Approval_SMS_RORP</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SR Approval SMS RORP</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SR_Approval_SMS_RORP</template>
    </alerts>
    <alerts>
        <fullName>SR_Approval_SMS_RORP_Freehold</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SR Approval SMS RORP Freehold</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SR_Approval_SMS_RORP_Freehold</template>
    </alerts>
    <alerts>
        <fullName>SR_Reject_SMS_for_fit_out_client</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SR Reject SMS for fit out client</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SR_Rejection_SMS_for_project_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>SR_approval_SMS</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SR approval SMS</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_SMS_Templates/SR_Approval_SMS</template>
    </alerts>
    <alerts>
        <fullName>SR_approval_SMS_for_Signage</fullName>
        <ccEmails>sms_service@27l44bv4w0yo0k7kwo6hkryoyxv113nwttbo7s9fh608ddvzzo.2-nkz5eai.eu0.apex.salesforce.com</ccEmails>
        <description>SR approval SMS for Signage</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Approval_Notification_Signage</template>
    </alerts>
    <alerts>
        <fullName>Security_Email_Approval_Closed</fullName>
        <description>Security Email Approval _ Closed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>ROC_Officer</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Security_Email_Approval_Closed</template>
    </alerts>
    <alerts>
        <fullName>Security_Email_Approval_On_Hold</fullName>
        <description>Security Email Approval -On Hold</description>
        <protected>false</protected>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>ROC_Officer</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Security_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>Security_ROC_Step_SLA_Breached</fullName>
        <ccEmails>arun.singh@difc.ae</ccEmails>
        <description>Security ROC Step SLA Breached</description>
        <protected>false</protected>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>asma.almadani@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hanaa.almunaifi@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nada.rustom@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_SLA_breaches_Notification_Security</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_Finance</fullName>
        <ccEmails>c-shoaib.tariq@difc.ae</ccEmails>
        <description>Send Email To Finance</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Finance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Digital_Onboarding_Project/Step_assigned_to_Finance</template>
    </alerts>
    <alerts>
        <fullName>Service_Request_Cancelled</fullName>
        <description>Service Request Cancelled</description>
        <protected>false</protected>
        <recipients>
            <recipient>moosa.chola@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Service_Request_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>Service_Request_Rejection_for_Fit_Out</fullName>
        <description>Service Request Rejection for Fit - Out</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Application_Rejection_Notification_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Approval_of_Delete_Encashment_Step_by_DIFC_FOT</fullName>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <ccEmails>vineeth.dominic@idama.ae</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <description>Site Visit Report Approval of Delete Encashment Step by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Approval_of_Delete_Encashment_Step</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Approval_of_Waiver_Request_for_Blackpoints_by_DIFC_FOT</fullName>
        <ccEmails>vineeth.dominic@idama.ae</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Site Visit Report Approval of Waiver Request for Blackpoints by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Approval_of_Waiver_Request_for_Blackpoints</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Approval_of_Waiver_Request_for_Fine_and_Blackpoints_by_DIFC_FO</fullName>
        <ccEmails>vineeth.dominic@idama.ae</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Site Visit Report Approval of Waiver Request for Fine and Blackpoints by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Approval_of_Waiver_Request_for_Fine_and_Blackpoints</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Approval_of_Waiver_Request_for_Fine_by_DIFC_FOT</fullName>
        <ccEmails>vineeth.dominic@idama.ae</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Site Visit Report Approval of Waiver Request for Fine by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Approval_of_Waiver_Request_for_Fine</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Decline_of_Delete_Encashment_Step_by_DIFC_FOT</fullName>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>sixtus.ugochukwu@idama.ae.invalid</ccEmails>
        <ccEmails>sherrylle.arzaga@difc.ae.invalid</ccEmails>
        <ccEmails>omar.shafaamri@difc.ae.invalid</ccEmails>
        <description>Site Visit Report Decline of Delete Encashment Step by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Decline_of_Delete_Encashment_Step</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Decline_of_Waiver_Request_for_Blackpoints_by_DIFC_FOT</fullName>
        <ccEmails>vineeth.dominic@idama.ae</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>sixtus.ugochukwu@idama.ae.invalid</ccEmails>
        <ccEmails>sherrylle.arzaga@difc.ae.invalid</ccEmails>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Site Visit Report Decline of Waiver Request for Blackpoints by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Decline_of_Waiver_Request_for_Blackpoints</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Decline_of_Waiver_Request_for_Fine_and_Blackpoints_by_DIFC_FOT</fullName>
        <ccEmails>vineeth.dominic@idama.ae</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>sixtus.ugochukwu@idama.ae.invalid</ccEmails>
        <ccEmails>sherrylle.arzaga@difc.ae.invalid</ccEmails>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Site Visit Report Decline of Waiver Request for Fine and Blackpoints by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Decline_of_Waiver_Request_for_Fine_and_Blackpoints</template>
    </alerts>
    <alerts>
        <fullName>Site_Visit_Report_Decline_of_Waiver_Request_for_Fine_by_DIFC_FOT</fullName>
        <ccEmails>DIFC.FITOUT@ejadah.ae</ccEmails>
        <ccEmails>sixtus.ugochukwu@idama.ae</ccEmails>
        <ccEmails>sherrylle.arzaga@difc.ae.invalid</ccEmails>
        <ccEmails>j.vinod96@gmail.com</ccEmails>
        <description>Site Visit Report Decline of Waiver Request for Fine by DIFC FOT</description>
        <protected>false</protected>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Site_Visit_Report_Decline_of_Waiver_Request_for_Fine</template>
    </alerts>
    <alerts>
        <fullName>Step_Approval_Email_Alert</fullName>
        <description>Step Approval - Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>RoC_Officer</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pratiksha.gurung@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_verified_Email</template>
    </alerts>
    <alerts>
        <fullName>Step_Assignment_Changed_Notification_GS</fullName>
        <description>Step Assignment Changed Notification GS</description>
        <protected>false</protected>
        <recipients>
            <recipient>saeed.shams@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Step_Assignment_for_Look_and_Feel</fullName>
        <description>Step Assignment for Look and Feel</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assignment_Look_and_Feel</template>
    </alerts>
    <alerts>
        <fullName>Step_Assignment_for_Look_and_Feel_Retail</fullName>
        <description>Step Assignment for Look and Feel Retail</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>samina.sheikh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assignment_Look_and_Feel</template>
    </alerts>
    <alerts>
        <fullName>Step_Closure_Pending</fullName>
        <description>Step Closure Pending</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Closure_Pending</template>
    </alerts>
    <alerts>
        <fullName>Step_Pending_Notification_after_3_months</fullName>
        <ccEmails>roc.helpdesk@difc.ae</ccEmails>
        <description>Application Step Pending Notification after 3 months</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Pending_Notification_after_3_months</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_CEO_in_fit_out</fullName>
        <ccEmails>Saleh.AlAkrabi@difc.ae</ccEmails>
        <ccEmails>Annie.Gueizelar@difc.ae</ccEmails>
        <description>Step assigned to DIFC CEO in fit out</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_SVP_in_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_Cashier_in_fit_out</fullName>
        <description>Step assigned to DIFC Cashier in fit out</description>
        <protected>false</protected>
        <recipients>
            <recipient>raquel.batiancila@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_Commercial_Leasing_in_fit_out</fullName>
        <ccEmails>leasing@difc.ae</ccEmails>
        <description>Step assigned to DIFC Commercial Leasing in fit out</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_Fit_out</fullName>
        <ccEmails>Sherrylle.Arzaga@difc.ae</ccEmails>
        <ccEmails>c-vinod.jetti@difc.ae</ccEmails>
        <description>Step assigned to DIFC Fit out</description>
        <protected>false</protected>
        <recipients>
            <recipient>omar.shafaamri@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>saleh.abdulhalim@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_Marketing_in_fit_out</fullName>
        <ccEmails>Armi.Enriquez@difc.ae</ccEmails>
        <description>Step assigned to DIFC Marketing in fit out</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_Retail_in_fit_out</fullName>
        <ccEmails>retail@difc.ae</ccEmails>
        <description>Step assigned to DIFC Retail in fit out</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_DIFC_SVP_in_fit_out</fullName>
        <ccEmails>Mohammad.AlNajjar@difc.ae</ccEmails>
        <description>Step assigned to DIFC SVP in fit out</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_SVP_in_fit_out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_Fitout_Team_for_Milestone_Penalty</fullName>
        <ccEmails>mudasir.saleem@gmail.com</ccEmails>
        <description>Step assigned to Fitout Team for Milestone Penalty</description>
        <protected>false</protected>
        <recipients>
            <recipient>sherrylle.arzaga@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vineeth.dominic@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Fitout_Team_Milestone_Penalty_Step</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_IDAMA_HSE</fullName>
        <description>Step assigned to IDAMA HSE</description>
        <protected>false</protected>
        <recipients>
            <recipient>sixtus.ugochukwu@idama.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_DIFC_Fit_Out</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_client</fullName>
        <description>Step assigned to client</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contractor_email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_client</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_contractor</fullName>
        <ccEmails>vinod.jetti@outlook.com</ccEmails>
        <description>Step assigned to contractor</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_contractor</template>
    </alerts>
    <alerts>
        <fullName>Step_assigned_to_contractor_site_visit</fullName>
        <ccEmails>vineeth.dominic@idama.ae.invalid</ccEmails>
        <ccEmails>DIFC.FITOUT@ejadah.ae.invalid</ccEmails>
        <description>Step assigned to contractor site visit</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Client_Email_for_fit_out__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_assigned_to_contractor</template>
    </alerts>
    <alerts>
        <fullName>Step_creation_notification_DP</fullName>
        <description>Step creation notification - DP</description>
        <protected>false</protected>
        <recipients>
            <recipient>lori.baker@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Permit_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Step_creation_notification_Line_Manager</fullName>
        <description>Step creation notification-Line Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>hanaa.almunaifi@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pratiksha.gurung@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Step_creation_notification_RORP_Registrar</fullName>
        <description>Step creation notification-RORP Registrar</description>
        <protected>false</protected>
        <recipients>
            <recipient>jaber.alsuwaidi@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Step_creation_notification_Registrar</fullName>
        <description>Step creation notification-Registrar</description>
        <protected>false</protected>
        <recipients>
            <recipient>alya.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khalid.alzarouni@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Step_creation_notification_RoC</fullName>
        <description>Step creation notification-RoC</description>
        <protected>false</protected>
        <recipients>
            <recipient>RoC_Team_Notification</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>ROC_Line_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>ROC_Officer</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Step_Creation_Notification_Roc</template>
    </alerts>
    <alerts>
        <fullName>Venky_Step_creation_notification_Registrar_ROC</fullName>
        <description>ROC Registrar Step Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Registrar</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification_V1</template>
    </alerts>
    <alerts>
        <fullName>Venky_Step_creation_notification_Registrar_RORP</fullName>
        <description>(Venky) Step creation notification-Registrar(RORP)</description>
        <protected>false</protected>
        <recipients>
            <recipient>RORP_notification_receivers_Registrar</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visa_Exception_HOD_Notification</fullName>
        <description>Visa Exception HOD Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/HOD_Visa_Exception_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visa_Exception_RM_Notification</fullName>
        <description>Visa Exception RM Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/RM_Visa_Exception_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visit_Visa_Upload_Notification</fullName>
        <description>Visit Visa Upload Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Visit_Visa_Template</template>
    </alerts>
    <alerts>
        <fullName>Winding_Up_Re_upload_Documents_Notification</fullName>
        <ccEmails>dissolution@difc.ae</ccEmails>
        <description>Winding Up Re-upload Documents Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Liquidator_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Winding_Up/Winding_Up_Re_Upload_Documents_Notification</template>
    </alerts>
    <alerts>
        <fullName>Winding_Up_Return_for_more_Info_Notification</fullName>
        <ccEmails>dissolution@difc.ae</ccEmails>
        <description>Winding Up Return for more Info Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Liquidator_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Winding_Up/Winding_Up_Return_For_More_Info_Notification</template>
    </alerts>
    <alerts>
        <fullName>notification_to_Step_owner_when_assigned</fullName>
        <description>notification to Step owner when assigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>c-vinod.jetti@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Step_assigned_to_DIFC_team</template>
    </alerts>
    <fieldUpdates>
        <fullName>Additional_Email_for_fitout</fullName>
        <field>Client_Email_for_fit_out__c</field>
        <formula>SR__r.Additional_Email__c</formula>
        <name>Additional Email for fitout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Applicant_Email_ID</fullName>
        <field>Applicant_Email__c</field>
        <formula>IF(SR_Type_Step__c=&apos;Site Visit Report&apos; || SR_Type_Step__c=&apos;Meeting with Contractor&apos; || SR_Type_Step__c=&apos;Look &amp; Feel Approval Request&apos;,  SR__r.Linked_SR__r.Email__c,IF(SR_Step__r.Assigned_to_Client__c,SR__r.Email_Address__c,SR__r.Email__c))</formula>
        <name>Applicant Email ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Applicant_Mobile</fullName>
        <field>Applicant_Mobile__c</field>
        <formula>SR__r.Send_SMS_To_Mobile__c</formula>
        <name>Applicant Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contractor_email_fit_out</fullName>
        <field>Contractor_email_for_fit_out__c</field>
        <formula>SR__r.Email__c</formula>
        <name>Contractor email fit out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Domestic_Helper_On_Typist_Step</fullName>
        <field>Domestic_Helper_in_Sharing_Rule__c</field>
        <literalValue>1</literalValue>
        <name>Domestic Helper On Typist Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GS_Appointment_Creation</fullName>
        <description>7365</description>
        <field>AppointmentCreation__c</field>
        <literalValue>Create</literalValue>
        <name>GS Appointment Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GS_Send_reminder_for_document_collection</fullName>
        <description>Created for 7191</description>
        <field>Document_Collection_Reminder__c</field>
        <literalValue>1</literalValue>
        <name>GS Send reminder for document collection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Originals_Requested_Date</fullName>
        <field>Originals_Awaited_Date__c</field>
        <formula>NOW()</formula>
        <name>Originals Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Status_Change_to_Verified_Date</fullName>
        <field>Status_Change_to_Verified_Date__c</field>
        <formula>Now()</formula>
        <name>Populate Status Change to Verified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Verified_by_Field</fullName>
        <description>Populate Verified by Field for comfort letters other wise posting is failing</description>
        <field>Verified_By__c</field>
        <lookupValue>integration@difc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Populate Verified by Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_evaluate_From_Date_c</fullName>
        <description>This field update is used to check whether email alert should be sent still or not</description>
        <field>From_Date__c</field>
        <formula>From_Date__c</formula>
        <name>Re-evaluate From_Date__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Step_Closed_Date</fullName>
        <field>Closed_Date__c</field>
        <formula>Today()</formula>
        <name>Step Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Step_Closed_Date_Time</fullName>
        <field>Closed_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Step Closed Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Liquidator_Email</fullName>
        <description>This workflow is used to update Liquidator email when step is created</description>
        <field>Liquidator_Email__c</field>
        <formula>SR__r.Additional_Email__c</formula>
        <name>Update Liquidator Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Snags_fix_not_completed</fullName>
        <description>This field update to trigger the code to update status field on step to not completed.</description>
        <field>Exceptional_Remarks__c</field>
        <formula>&quot;Snag Fix Pending&quot;</formula>
        <name>Update Snag fix not completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Step_to_Not_Fixed</fullName>
        <field>Exceptional_Remarks__c</field>
        <formula>&quot;Not Fixed&quot;</formula>
        <name>Update Step to Not Fixed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Total_Payable_amount_for_Dissolut</fullName>
        <description>Update Total Payable amount for Dissolution</description>
        <field>Total_Payable_Amount__c</field>
        <formula>SR__r.Annual_Amount_AED__c</formula>
        <name>Update Total Payable amount for Dissolut</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_date_for_3_Days</fullName>
        <field>From_Date__c</field>
        <formula>created_date__c</formula>
        <name>Update date for 3 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updating_client_email_for_fit_out</fullName>
        <field>Client_Email_for_fit_out__c</field>
        <formula>SR__r.Email_Address__c</formula>
        <name>Updating client email for fit out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>add_3_Days</fullName>
        <field>From_Date__c</field>
        <formula>From_Date__c  + 3</formula>
        <name>add 3 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AML Email Approval _ Closed</fullName>
        <actions>
            <name>AML_Email_Approval_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>AML Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>notEqual</operation>
            <value>Application_of_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email Alert to ROC team when AML step closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AML Email Approval _ Created</fullName>
        <actions>
            <name>AML_Step_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>AML Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>notEqual</operation>
            <value>Application_of_Registration</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification</fullName>
        <actions>
            <name>Application_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Except RORP,GS , BC and IT SR Groups and Signage SR</description>
        <formula>SR_Status__c == &quot;Approved&quot;  &amp;&amp; if(AND(SR_Record_Type__c =&apos;Change_of_Address_Details&apos;,SR__r.Address_Changed__c =false), true,NOT(SR__r.SR_Template__r.Do_not_send_SR_Approval_Notification__c)) &amp;&amp; SR__r.SR_Template__r.Is_Letter__c == false   &amp;&amp; SR_Group__c != &quot;RORP&quot; &amp;&amp; SR_Group__c &lt;&gt; &quot;BC&quot; &amp;&amp; SR_Group__c &lt;&gt; &quot;IT&quot; &amp;&amp; SR__r.Record_Type_Name__c != &quot;Signage&quot; &amp;&amp; SR__r.Record_Type_Name__c != &quot;Objection_SR&quot; &amp;&amp; SR_Group__c &lt;&gt; &quot;Marketing&quot; &amp;&amp; SR_Group__c  &lt;&gt; &quot;Fit-Out &amp; Events&quot; &amp;&amp; SR__r.Record_Type_Name__c != &quot;PO_BOX&quot; &amp;&amp; SR__r.Record_Type_Name__c != &quot;Add_a_New_Service_on_the_Index_Card&quot;&amp;&amp;  SR__r.Record_Type_Name__c != &quot;GS_Comfort_Letters&quot; &amp;&amp;  SR__r.Record_Type_Name__c != &quot;Car_Parking&quot; &amp;&amp;     SR__r.Record_Type_Name__c != &quot;Account_Statement_Report&quot; &amp;&amp;  SR__r.Record_Type_Name__c != &quot;Company_Conversion_Certificates&quot; &amp;&amp;    SR__r.Record_Type_Name__c != &quot;NOC_to_be_a_shareholder_outside_the_DIFC&quot; &amp;&amp;    SR__r.Record_Type_Name__c != &quot;Property_Listing_User_Access_Form&quot; &amp;&amp;  NOT( ISPICKVAL(SR__r.Name_of_the_Authority__c, &quot;Individual User&quot;) )   &amp;&amp; Step_Name__c &lt;&gt; &apos;Upload Signed Certificate of Incorporation&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification - BC</fullName>
        <actions>
            <name>Application_Approval_Notification_BC</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify customer and BC team on approval of notification</description>
        <formula>IF(AND( OR(Step_Status__c = &apos;Approved&apos; , Step_Status__c = &apos;Document Ready&apos; &amp;&amp;  SR_Status__c = &apos;Approved&apos;)  , SR_Group__c  = &apos;BC&apos;),true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification - IT Form</fullName>
        <actions>
            <name>Application_Approval_Notification_IT_Form</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify IT team on approval of notification</description>
        <formula>IF(AND( OR(Step_Status__c = &apos;Approved&apos; , Step_Status__c = &apos;Document Ready&apos; &amp;&amp;  SR_Status__c = &apos;Approved&apos;)  , SR_Group__c  = &apos;IT&apos;),true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification - Tenant Relation</fullName>
        <actions>
            <name>Application_Approval_Notification_Tenant_Relation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify customer once tenant relation request is completed RORP</description>
        <formula>IF(AND(Step_Status__c = &apos;Approved&apos;,SR_Status__c = &apos;Approved&apos;   ,  SR_Record_Type__c = &apos;Tenant_Relation&apos;),true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification - USER Acc Form</fullName>
        <actions>
            <name>Application_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify User upon update/Removal request&apos;s Approval</description>
        <formula>AND(Step_Status__c = &apos;Approved&apos;,SR_Status__c = &apos;Approved&apos;,SR_Group__c  = &apos;ROC&apos;, SR__r.RecordType.DeveloperName =&apos;User_Access_Form&apos;, TEXT(SR__r.User_Form_Action__c) !=&apos;Create New User&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification RORP</fullName>
        <actions>
            <name>Application_Approval_Notification_RORP</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_Approval_SMS_RORP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>RORP Approval Email Lease Registration</description>
        <formula>SR_Status__c == &quot;Approved&quot; &amp;&amp; SR__r.SR_Template__r.Is_Letter__c == false &amp;&amp; SR__r.Record_Type_Name__c =&quot;Lease_Registration&quot; &amp;&amp; SR_Group__c = &quot;RORP&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification RORP For Others</fullName>
        <actions>
            <name>Application_Approval_Notification_for_RORP</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Not Including Lease_Registration, Freehold_Transfer_Registration, RORP_User_Access_Form, Tenant_Relation</description>
        <formula>SR_Group__c = &quot;RORP&quot; &amp;&amp;  SR_Status__c == &quot;Approved&quot; &amp;&amp;  SR__r.SR_Template__r.Is_Letter__c == false &amp;&amp;  SR__r.Record_Type_Name__c != &quot;Lease_Registration&quot; &amp;&amp;  SR__r.Record_Type_Name__c != &quot;Freehold_Transfer_Registration&quot; &amp;&amp;  SR__r.Record_Type_Name__c != &quot;RORP_User_Access_Form&quot; &amp;&amp;  SR__r.Record_Type_Name__c != &quot;Tenant_Relation&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Approval Notification RORP Freehold</fullName>
        <actions>
            <name>Application_Approval_Notification_RORP_Freehold</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_Approval_SMS_RORP_Freehold</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>RORP Approval Email Freehold Registration</description>
        <formula>SR_Status__c == &quot;Approved&quot; &amp;&amp; SR__r.SR_Template__r.Is_Letter__c == false &amp;&amp; SR__r.Record_Type_Name__c =&quot;Freehold_Transfer_Registration&quot; &amp;&amp; SR_Group__c = &quot;RORP&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application In Process Notification - Tenant Relation</fullName>
        <actions>
            <name>Application_In_Process_Notification_Tenant_Relation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow will use when tenant relation team the step status to In Process</description>
        <formula>SR__r.Record_Type_Name__c = &quot;Tenant_Relation&quot; &amp;&amp; ISCHANGED(Step_Status__c) &amp;&amp;  PRIORVALUE(Step_Status__c) &lt;&gt; &apos;In Process&apos; &amp;&amp; Step_Status__c = &apos;In Process&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Application Rejection Notification</fullName>
        <actions>
            <name>Application_Rejection_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reject_SR_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 10 ) OR ( 1 AND 8 AND 9 )</booleanFilter>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>REJECTED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>notEqual</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>notEqual</operation>
            <value>Tenant_Relation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>notEqual</operation>
            <value>Fit-Out &amp; Events</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>notEqual</operation>
            <value>Objection_SR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>notEqual</operation>
            <value>GS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>notEqual</operation>
            <value>Car_Parking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Refund_Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Front Desk Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>notEqual</operation>
            <value>Other</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Rejection Notification - Tenant Relation</fullName>
        <actions>
            <name>Application_Rejection_Notification_Tenant_Relation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reject_SR_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>REJECTED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>equals</operation>
            <value>Tenant_Relation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Rejection Notification Car Parking</fullName>
        <actions>
            <name>Application_Rejection_Notification_Car_Parking</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reject_SR_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>REJECTED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Car_Parking</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Submission notification Investment Fund</fullName>
        <actions>
            <name>DFSA_funds_Application_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>T#4507 Send email to DFSA funds if AOR activity is Investment Fund</description>
        <formula>SR_Status__c == &quot;Approved&quot;  &amp;&amp; SR__r.Record_Type_Name__c == &quot;Application_of_Registration&quot;  &amp;&amp; Sector_Classification__c==&apos;Investment Fund&apos; &amp;&amp; CONTAINS(SR__r.Customer__r.License_Activity__c,&apos;042&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Submission-AOR-NotifyFunds%26Linda</fullName>
        <actions>
            <name>Application_Submission_AOR_NotifyFunds_Linda</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This email goes to the DFSA funds department and linda brooker on the submission of AOR only.</description>
        <formula>AND(  NOT(SR__r.Pre_GoLive__c) , SR__r.Internal_SR_Status__r.Name =&apos;Submitted&apos;, SR__r.Record_Type_Name__c =&apos;Application_of_Registration&apos;,  OR(  ISPICKVAL(SR__r.Legal_Structures__c, &apos;LTD IC&apos;),  CONTAINS(SR__r.Customer__r.License_Activity__c,&apos;042&apos;)  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Biometrics Required</fullName>
        <actions>
            <name>Biometrics_Required</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Biometrics Required</description>
        <formula>SR_Group__c = &apos;GS&apos;  &amp;&amp;  Step_Name__c = &apos;Emirates ID Registration is Completed&apos; &amp;&amp;  Is_Closed__c = true &amp;&amp;  PRIORVALUE(Is_Closed__c ) = false &amp;&amp; (SR_Record_Type__c = &apos;DIFC_Sponsorship_Visa_New&apos; || SR_Record_Type__c  = &apos;Employment_Visa_from_DIFC_to_DIFC&apos; || SR_Record_Type__c = &apos;Employment_Visa_Govt_to_DIFC&apos; || SR_Record_Type__c = &apos;Non_DIFC_Sponsorship_Visa_New&apos; || SR_Record_Type__c = &apos;Visa_from_Individual_sponsor_to_DIFC&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Biometrics Required Dependent visa</fullName>
        <actions>
            <name>Biometrics_required_for_Dependents</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>SR_Group__c = &apos;GS&apos;  &amp;&amp;  Step_Name__c = &apos;Emirates ID Registration is Completed&apos; &amp;&amp;  Is_Closed__c = true &amp;&amp;  PRIORVALUE(Is_Closed__c ) = false &amp;&amp;  Text(Biometrics_Required__c) = &apos;Yes&apos; &amp;&amp; (SR_Record_Type__c &lt;&gt; &apos;DIFC_Sponsorship_Visa_New&apos; &amp;&amp; SR_Record_Type__c  &lt;&gt; &apos;Employment_Visa_from_DIFC_to_DIFC&apos; &amp;&amp; SR_Record_Type__c &lt;&gt; &apos;Employment_Visa_Govt_to_DIFC&apos; &amp;&amp; SR_Record_Type__c &lt;&gt; &apos;Non_DIFC_Sponsorship_Visa_New&apos; &amp;&amp; SR_Record_Type__c &lt;&gt; &apos;Visa_from_Individual_sponsor_to_DIFC&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Car Parking Application Approval Notification</fullName>
        <actions>
            <name>Application_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Only for car parking</description>
        <formula>SR_Status__c == &apos;Approved&apos; &amp;&amp; SR__r.Record_Type_Name__c == &apos;Car_Parking&apos; &amp;&amp;  Step_Code__c == &apos;VERIFICATION_OF_APPLICATION&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Car Parking Collected Step</fullName>
        <actions>
            <name>Car_Parking_Collection_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Car_Parking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>READY_FOR_COLLECTION</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>notEqual</operation>
            <value>Pending</value>
        </criteriaItems>
        <description>Only for car parking</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Car Parking Courier Step</fullName>
        <actions>
            <name>Car_Parking_Courier_Step_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is only for car Parking</description>
        <formula>SR__r.Service_Type__c == &apos;Car Parking Process&apos; &amp;&amp; Step_Template__r.Code__c == &apos;COURIER_DELIVERY&apos; &amp;&amp; Status__r.Code__c == &apos;PENDING_DELIVERY&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Car Parking Re-Upload Documents Notification</fullName>
        <actions>
            <name>Re_upload_Document_Car_Parking</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.SR_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Re-upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Car_Parking</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Pending Site Visit Steps</fullName>
        <active>true</active>
        <formula>( SR_Record_Type__c == &apos;Site_Visit_Report&apos; ||    SR_Record_Type__c  == &apos;Site_Visit_for_Events&apos;) &amp;&amp; NOT(ISBLANK( Status_Change_to_Verified_Date__c )) &amp;&amp; Step_Status__c != &apos;Completed&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Step_to_Not_Fixed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Step__c.Status_Change_to_Verified_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Courier - Delivered</fullName>
        <actions>
            <name>Courier_Delivered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Step_Status__c =&quot;Delivered&quot;, SR_Group__c =&quot;ROC&quot;, ISPICKVAL( SR__r.Avail_Courier_Services__c , &apos;Yes&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Courier Closed Awaiting Review</fullName>
        <actions>
            <name>Courier_Closed_Awaiting_Review</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>In case of Awaiting Originals &amp; Courier Pending, send notification to Front Office when the Courier Step is Closed.</description>
        <formula>SR_Group__c = &apos;GS&apos; &amp;&amp; Parent_Step__c &lt;&gt; NULL &amp;&amp; OR(Step_Status__c = &apos;Delivered&apos; , Step_Status__c = &apos;Collected&apos;) &amp;&amp;  SR_Status__c = &apos;Awaiting Originals&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Courier Pending Closed</fullName>
        <actions>
            <name>Courier_Pending_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>In case of Awaiting Originals &amp; Courier Pending, send notification to Front Office when the Courier Step is Closed.</description>
        <formula>SR_Group__c = &apos;GS&apos; &amp;&amp; Parent_Step__c &lt;&gt; NULL &amp;&amp; OR(Step_Status__c = &apos;Delivered&apos; , Step_Status__c = &apos;Collected&apos;) &amp;&amp;  Pending__c &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DP Notification to DP team</fullName>
        <actions>
            <name>Step_creation_notification_DP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Step_Template__r.Code__c=&apos;DATA_PROTECTION_COMMISSIONER_APPROVAL&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Document Upload Pending DP</fullName>
        <active>true</active>
        <description>This workflow rule sends an email to ROC when documents are generated and not verified in one working day</description>
        <formula>Step_Status__c =&apos;Document Generated&apos;  &amp;&amp;  SR__r.Record_Type_Name__c  = &apos;Issue_Fine&apos; &amp;&amp;  TEXT(SR__r.Type_of_Request__c) = &apos;Notification Renewal&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Document_Upload_Pending_DP</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Document Upload Pending ROC</fullName>
        <active>true</active>
        <description>This workflow rule sends an email to ROC when documents are generated and not verified in one working day</description>
        <formula>Step_Status__c =&apos;Document Generated&apos;  &amp;&amp;  SR__r.Record_Type_Name__c  = &apos;Issue_Fine&apos; &amp;&amp;  TEXT(SR__r.Type_of_Request__c) != &apos;Notification Renewal&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Document_Upload_Pending_ROC</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Domestic Helper On Typist Step</fullName>
        <actions>
            <name>Domestic_Helper_On_Typist_Step</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.SAP_ACRES__c</field>
            <operation>equals</operation>
            <value>TY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Domestic_Helper__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>notEqual</operation>
            <value>Emirates ID Registration is Completed,Under process</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Entry Permit is issued - Notification</fullName>
        <actions>
            <name>Entry_Permit_is_issued_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(Step_Template__r.Name =&apos;Entry Permit is Issued&apos;, OR(CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-00005&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-00006&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-00008&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-00009&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-VIP05&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-VIP06&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-VIP08&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-VIP09&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-00221&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-00222&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-VIP221&apos;),CONTAINS( SR__r.SAP_MATNR__c,&apos;GO-VIP222&apos;)), Status__r.Name == &apos;Closed&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Exit Process %3A Notification to Customer when first step is verifed</fullName>
        <actions>
            <name>Exit_Process_Notification_to_Customer_when_first_step_is_verifed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When the RoC officer verifies the first step for resolution and declaration, the system should send an email informing clients that their status has changed and they need to complete the next step and submit originals.</description>
        <formula>AND(OR( SR__r.Record_Type_Name__c =&apos;Dissolution&apos;,SR__r.Record_Type_Name__c =&apos;De_Registration&apos;, SR__r.Record_Type_Name__c =&apos;Transfer_from_DIFC&apos;), Step_Name__c = &apos;Verification of Application&apos;, Step_Status__c = &apos;Verified&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FD Verified But not posted</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Front Desk Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Status__c</field>
            <operation>notEqual</operation>
            <value>Approved,Process Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Is_Letter__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FD_is_Verified_but_not_posted</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.X15_Mins_After_Closed_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GS Appointment Creation</fullName>
        <actions>
            <name>GS_Appointment_Creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Medical Fitness Test Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <description>7365, disabling this workflow will impact on GS appointment</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GS Establishment Card form is typed</fullName>
        <actions>
            <name>Establishment_Card_form_is_typed_Notification_for_Violators</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Establishment Card form is typed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Violators__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>CLOSED</value>
        </criteriaItems>
        <description>Establishment Card form is typed	and Violators is yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GS Reminder to Applicant for document collection</fullName>
        <active>true</active>
        <description>Created as a part of ticket number 7191</description>
        <formula>AND(IF(TODAY() - DATEVALUE(CreatedDate)&gt;2, true,false),  ISNULL( Closed_Date__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GS_Send_reminder_for_document_collection</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Step__c.GS_Time_Trigger__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Head Of Operations Visa Exception Notification</fullName>
        <actions>
            <name>Head_of_Operations_Visa_exception_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Head Of Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Visa_Exception</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Head of Operations Notification</fullName>
        <actions>
            <name>notification_to_Step_owner_when_assigned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Head of Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>notEqual</operation>
            <value>Visa_Exception</value>
        </criteriaItems>
        <description>Head of Operations</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Internal Clearance to DIFCA</fullName>
        <actions>
            <name>Internal_Clearance_to_DIFCA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>equals</operation>
            <value>Request_for_Voluntary_Strike_off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Verification of Application</value>
        </criteriaItems>
        <description>Changed for : 8921</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LM%2FHOD Rejected PSA Termination Notification to FD</fullName>
        <actions>
            <name>LM_HOD_Rejected_PSA_Termination_Notification_to_FD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Step_Name__c =&apos;Review and Approve&apos;&amp;&amp;
(SR_Record_Type_Text__c =&apos;PSA_Termination&apos;&amp;&amp;
SR__r.is_RM_Rejected__c = true ) ||
SR_Record_Type_Text__c =&apos;PSA_Deduction&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lease Registration Step assig</fullName>
        <actions>
            <name>notification_to_Step_owner_when_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lease Registration</description>
        <formula>AND(OR(SR__r.Sponsor_Mother_Full_Name__c=&apos;Yes&apos;,SR__r.Sponsor_Mother_Full_Name__c=&apos;previously paid (renewal)&apos;),SR__r.Usage_Type__c!=&apos;Commercial&apos; ,SR_Record_Type_Text__c =&apos;Lease_Registration&apos; , OR( Step_Code__c=&apos;NEW_DOCUMENT_GENERATION_&amp;_REGISTRAR_SIGNATURE&apos; ,Step_Code__c=&apos;DIFC_Finance_Payment_Confirmation&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Letter Ready to Collect- Not verified</fullName>
        <active>false</active>
        <description>This workflow rule sends an email to ROC when documents are generated and not verified in one working day</description>
        <formula>Step_Status__c =&apos;Document Generated&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Document_Ready_Escalation</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Line Manager Approval Step Notification</fullName>
        <actions>
            <name>Step_creation_notification_Line_Manager</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>Step_Template__r.Code__c=&apos;LINE_MANAGER_APPROVAL&apos; &amp;&amp;  SR_Template__c &lt;&gt; &apos;Refund_Request&apos; &amp;&amp;  SR_Group__c==&apos;ROC&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Listing %3A SR Submission Notification</fullName>
        <actions>
            <name>Listing_SR_Submission_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4) OR (3 AND 4 AND 5 AND 6)</booleanFilter>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Verification of Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Pending Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.OwnerId</field>
            <operation>equals</operation>
            <value>DIFC Leasing Reviewer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>Listing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Review and Approve</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Approval</value>
        </criteriaItems>
        <description>Email goes to the leasing team on the submission of the SR for listings , deactivated by arun to active PSA workflows</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Listing %3A Step Pending Review Notifications</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Verification of Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Pending Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.OwnerId</field>
            <operation>equals</operation>
            <value>DIFC Leasing Reviewer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>Listing</value>
        </criteriaItems>
        <description>Listing : Step Pending Review Notifications after 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Listing_Step_Pending_Review_Notifications</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Listing %3A Step Pending on Customer Notifications</fullName>
        <active>false</active>
        <booleanFilter>((1 AND 2) OR (4 AND 5)) AND 3 AND 6</booleanFilter>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Require More Information from Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Pending Customer Inputs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.OwnerId</field>
            <operation>equals</operation>
            <value>Client Entry User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Re-upload Document</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Re-upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>Listing</value>
        </criteriaItems>
        <description>Listing : Step Pending on Customer Notifications for 2 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Listing_Pending_on_Customer_Notifications</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New docs generated notification</fullName>
        <actions>
            <name>New_docs_generation_notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>5353:replaced SR_Status__c =Status__r.Name for Zero_Tax_Certificate
Part of E-Certificate changed condition to remove AoR and change of Entity Name, backup</description>
        <formula>OR(AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Change_of_Address_Details&apos;, SR__r.Address_Changed__c =True), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Change_Add_or_Remove_Authorized_Signatory&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;NOC_to_open_a_Branch_Subsidiary_of_DIFC_Entity_outside_DIFC_Jurisdiction&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Change_of_Business_Activity&apos;), AND( SR_Status__c =&apos;Approved&apos;,SR_Template__c =&apos;License_Renewal&apos;,Step_Name__c &lt;&gt; &apos;Upload Signed Commercial License&apos;,Step_Name__c &lt;&gt; &apos;Upload Signed Commercial license and Signed Certificate of Incorporation&apos;), AND(Status__r.Name =&apos;Approved&apos;, SR_Template__c =&apos;Zero_Tax_Certificate&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Letter_of_Good_Standing&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Certificate_of_Incumbency&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Certificate_of_Status_Under_formation&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Providing_an_Extract_of_Information_from_the_Public_Register&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Replacement_of_Commercial_License&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;NOC_to_be_a_shareholder_outside_the_DIFC&apos;), AND( SR_Status__c =&apos;Approved&apos;, SR_Template__c =&apos;Providing_an_Extract_of_any_Document&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification %3AClosure Step is closed by client or liquidator</fullName>
        <actions>
            <name>Notification_Closure_Step_is_closed_by_client_or_liquidator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>email should go out when the closure step is closed by client or liquidator #1219</description>
        <formula>AND( Step_Name__c = &apos;Company Closure Information&apos; ,  Step_Status__c = &apos;Required Information Updated&apos;, OR( SR__r.Record_Type_Name__c = &apos;Transfer_from_DIFC&apos;,SR__r.Record_Type_Name__c = &apos;De_Registration&apos;,SR__r.Record_Type_Name__c = &apos;Dissolution&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification - Reupload document and fine payment</fullName>
        <actions>
            <name>Notify_GS_Team_after_Document_Uploaded</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This rule triggers and email alert to the GS Team if the portal user uploaded document which is pending TKT#1359</description>
        <formula>And(SR__r.SR_Group__c =&apos;GS&apos;,Step_Name__c =&apos;Front Desk Review&apos;, SR__r.Record_Type_Name__c = &apos;Over_Stay_Fine&apos;,SR__r.External_Status_Name__c=&apos;Submitted&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification when a step is assigned to DIFC Fit out</fullName>
        <actions>
            <name>Step_assigned_to_DIFC_Fit_out</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>SR_Group__c = &apos;Fit-Out &amp; Events&apos; &amp;&amp;  Owner:Queue.QueueName = &apos;DIFC Fit-Out Reviewer&apos;  &amp;&amp;  (SR_Menu_Text__c  &lt;&gt; &apos;Request for Technical Study/Inquiry&apos; || SR_Menu_Text__c  &lt;&gt; &apos;Request for Technical Clarification Form&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification when a step is assigned to IDAMA HSE</fullName>
        <actions>
            <name>Step_assigned_to_IDAMA_HSE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>SR_Group__c = &apos;Fit-Out &amp; Events&apos; &amp;&amp;  (Owner:Queue.QueueName = &apos;FOSP HSE&apos; || Step_Name__c = &apos;Review by HSE &amp; Others&apos; || Step_Name__c = &apos;Detailed Design Review by HSE &amp; Others&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Back Office Team Customer Pending</fullName>
        <actions>
            <name>Notify_Back_Office_Team_Customer_Pending</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Back Office Team Customer Pending</description>
        <formula>AND( NOT(ISBLANK( Pending__c ) ), Step_Status__c = &apos;Required Information Updated&apos;,  CreatedBy.Alias = &apos;integ&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Back office for Medical Step changes</fullName>
        <actions>
            <name>Email_alert_to_BO_for_notifying_medical_appointment_changes</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  SR__r.SR_Group__c = &apos;GS&apos;,  SR_Status__c = &apos;Medical fitness test is scheduled&apos;,  OR(    Step_Status__c = &apos;Re-Scheduled&apos;,    Step_Status__c = &apos;Scheduled&apos;  ),  Medical_Process_Completed__c = TRUE,  NOT(ISNULL(Application_Reference__c)),  NOT(ISNULL(Medical_Completed_On__c)),  OR(  ISCHANGED( Second_Medical_Completed_On__c ),  ISCHANGED(  Third_Medical_Completed_On__c  ),  ISCHANGED(  Marked_Unfit_On__c )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify DIFC FoT for not closing Induction Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Verify &amp; Approve by FOSP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>equals</operation>
            <value>Fit_Out_Induction_Request</value>
        </criteriaItems>
        <description>#3963: Email notification to be sent to DIFC FOT -if the step is not closed (Induction Approval by FOSP Manager step) an email notification should be sent to DIFC FOT.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_to_close_Induction_Approval_Step</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Closed_Date_Time__c</offsetFromField>
            <timeLength>16</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify Front Office Team Customer Pending</fullName>
        <actions>
            <name>Notify_Front_Office_Team_Customer_Pending</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Front Office Team Customer Pending</description>
        <formula>AND( NOT(ISBLANK( Pending__c ) ), Step_Status__c = &apos;Required Information Updated&apos;,  CreatedBy.Alias != &apos;integ&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Objection Approval Notification</fullName>
        <actions>
            <name>Objection_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Status_Code__c = &apos;APPROVED&apos;,  SR_Group__c =&apos;ROC&apos;,  SR_Record_Type__c =&apos;Objection_SR&apos;, Type_of_Request__c  &lt;&gt; &apos;Notification Renewal&apos;,  SR__r.Fee_Exemption__c = false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Objection Approval Notification DP</fullName>
        <actions>
            <name>Objection_Approval_Notification_DP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Status_Code__c = &apos;APPROVED&apos;,  SR_Group__c =&apos;ROC&apos;,  SR_Record_Type__c =&apos;Objection_SR&apos;, Type_of_Request__c  = &apos;Notification Renewal&apos;,  SR__r.Fee_Exemption__c = false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Objection Rejection Notification</fullName>
        <actions>
            <name>Objection_Rejection_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Status_Code__c = &apos;REJECTED&apos;,  SR_Group__c =&apos;ROC&apos;,  SR_Record_Type__c =&apos;Objection_SR&apos;, Type_of_Request__c  &lt;&gt; &apos;Notification Renewal&apos;,  SR__r.Fee_Exemption__c = false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Objection Rejection Notification DP</fullName>
        <actions>
            <name>Objection_Rejection_Notification_DP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Status_Code__c = &apos;REJECTED&apos;,  SR_Group__c =&apos;ROC&apos;,  SR_Record_Type__c =&apos;Objection_SR&apos;, Type_of_Request__c  = &apos;Notification Renewal&apos;,  SR__r.Fee_Exemption__c = false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Original Documents Required</fullName>
        <actions>
            <name>Awaiting_Original_Documents</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Original_Documents_Required_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Step__c.Pre_Verification_Done__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Originals</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>GS</value>
        </criteriaItems>
        <description>This rule gets triggered when the front desk tick the checkbox of pre-verification done and is waiting for the client to come at the front desk with original documents. This rule triggers an email for the client.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Originals Requested Date Population</fullName>
        <actions>
            <name>Originals_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(Status_Type__c=&apos;Intermediate&apos; &amp;&amp; SR_Status__c = &apos;Awaiting Originals&apos;,true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PSA Email to CC LM and cashier when HOD step is approved</fullName>
        <actions>
            <name>PSA_Email_to_CC_LM_and_cashier_when_HOD_step_is_approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>Receipt_Issued</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>PSA_Deduction</value>
        </criteriaItems>
        <description>#9703 Email to CC LM and cashier when HOD step is approved</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PSA Objection Closed after 30 Days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>PSA_Termination_Objection</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>PSA_Termination</value>
        </criteriaItems>
        <description>#9703</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Closed_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Step_Closed_Date_Time</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Re-upload Document Documents</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>RE_UPLOAD_DOCUMENT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Application_of_Registration</value>
        </criteriaItems>
        <description>Warning :Reminder must go out to entities portal user cc RoC team, RM after one week of changing the status of inc/reg application to “return for more inf / re-upload”. The same should be repeated on weekly basis.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Application_Pending_Re_upload</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Application_Pending_Re_upload</name>
                <type>Alert</type>
            </actions>
            <timeLength>70</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Application_Pending_Re_upload</name>
                <type>Alert</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Application_Pending_Re_upload</name>
                <type>Alert</type>
            </actions>
            <timeLength>42</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Pending_Notification_after_3_months</name>
                <type>Alert</type>
            </actions>
            <timeLength>144</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Application_Pending_Re_upload</name>
                <type>Alert</type>
            </actions>
            <timeLength>84</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Pending_Notification_after_3_months</name>
                <type>Alert</type>
            </actions>
            <timeLength>114</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Application_Pending_Re_upload</name>
                <type>Alert</type>
            </actions>
            <timeLength>56</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Pending_Notification_after_3_months</name>
                <type>Alert</type>
            </actions>
            <timeLength>174</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Voluntary Strike off</fullName>
        <actions>
            <name>Pending_Voluntary_Strike_off_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>ISSUE_WEBSITE_NOTICE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Request_for_Voluntary_Strike_off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <description>Warning : Sent when Registrar review step is verified #8921</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Status Change to Verified Datetime</fullName>
        <actions>
            <name>Populate_Status_Change_to_Verified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>A workflow will populate the current datetime as soon as status change to verify for SR Template letters</description>
        <formula>Step_Status__c == &quot;Verified&quot; &amp;&amp;  SR__r.SR_Template__r.Is_Letter__c == true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ROC - Application Awaiting Original Notification to Client</fullName>
        <actions>
            <name>ROC_App_Awaiting_Original_Notification_to_Client</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>SR_Group__c = &apos;ROC&apos; &amp;&amp;  OR(SR_Record_Type__c = &apos;Notice_of_Amemdment_of_AOA&apos;,SR_Record_Type__c = &apos;Notice_of_Amendment_of_Partnership_Agreement&apos;,SR_Record_Type__c = &apos;Notice_of_Amemdment_of_Standard_Charter&apos;) &amp;&amp;   Status__r.Code__c = &apos;AWAITING_ORIGINALS&apos; &amp;&amp; NOT(ISNULL(Originals_Awaited_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RORP - Notification to FO Team Success from SAP</fullName>
        <actions>
            <name>RORP_Notification_to_FO_Team_Success_from_SAP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>System will send a notification to Step Owner, when the Service is approved in SAP</description>
        <formula>AND( SR_Group__c=&apos;RORP&apos;,  OR(Status_Code__c = &apos;DOCUMENT_READY&apos;,Status_Code__c = &apos;CLOSED&apos;), OR(Step_Name__c=&apos;New Document Generation &amp; Registrar Signature&apos;,Step_Name__c=&apos;Registrar Approval&apos;), OR(SR__r.Record_Type_Name__c=&apos;Lease_Registration&apos;,SR__r.Record_Type_Name__c=&apos;Surrender_Termination_of_Lease_and_Sublease&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RORP Registrar Approval Step  Notification to Registrar</fullName>
        <actions>
            <name>Step_creation_notification_RORP_Registrar</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(IF(SR_Step__r.Registrar_Step__c==TRUE,TRUE,FALSE),  SR__r.SR_Group__c =&apos;RORP&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Re-Upload Documents Notification</fullName>
        <actions>
            <name>Re_upload_Document</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(SR_Status__c = &apos;Awaiting Re-upload&apos;, SR_Record_Type_Text__c != &apos;Car_Parking&apos;, Owner:Queue.QueueName!=&apos;Security&apos;, Owner:Queue.QueueName!=&apos;Data Protection Commissioner&apos;,Owner:Queue.QueueName!=&apos;Line Manager&apos;, Owner:Queue.QueueName!=&apos;Registrar&apos; ,SR_Group__c &lt;&gt; &apos;Fit-Out &amp; Events&apos;,SR_Group__c &lt;&gt;&apos;GS&apos;,SR_Group__c &lt;&gt; &apos;Marketing&apos;,SR_Record_Type_Text__c &lt;&gt; &apos;Dissolution&apos;,SR_Record_Type_Text__c &lt;&gt; &apos;De_Registration&apos;,SR_Record_Type_Text__c &lt;&gt; &apos;Transfer_from_DIFC&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Refund - Line Manager Approval Step Notification</fullName>
        <actions>
            <name>Refund_ROC_Line_Manager_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Step_Template__r.Code__c=&apos;LINE_MANAGER_APPROVAL&apos; &amp;&amp;  SR_Template__c = &apos;Refund_Request&apos; &amp;&amp; SR__r.SR_Group__c!=&apos;RORP&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Refund - Line Manager Approval Step Notification RORP</fullName>
        <actions>
            <name>Refund_RORP_Line_Manager_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Step_Template__r.Code__c=&apos;LINE_MANAGER_APPROVAL&apos; &amp;&amp;  SR_Template__c = &apos;Refund_Request&apos; &amp;&amp; SR__r.SR_Group__c=&apos;RORP&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Registrar Approval Notification - Mortgage</fullName>
        <actions>
            <name>Registrar_Approval_Notifications_Mortgage</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule would trigger an email to the internal team upon approval of the Registrar Approval step</description>
        <formula>if(AND(OR(Step_Template__r.Code__c=&apos;Upload_SIGNED_GENERATED_DOCUMENTS&apos;,Step_Template__r.Code__c=&apos;Upload_SIGNED_TITLE_DEED&apos;), SR__r.SR_Group__c=&apos;RORP&apos;,  SR__r.Record_Type_Name__c =&apos;Mortgage_Registration&apos;),true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Registrar Approval Step  Notification to Registrar</fullName>
        <actions>
            <name>Step_creation_notification_Registrar</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>IF(SR_Step__r.Registrar_Step__c==TRUE,TRUE,FALSE)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Registrar Approval on Exit Process</fullName>
        <actions>
            <name>Registrar_Approval_on_Exit_Process</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(Step_Code__c = &apos;REGISTRAR_APPROVAL&apos;, Step_Status__c &lt;&gt; &apos;Awaiting Approval&apos;,OR( SR_Menu_Text__c = &apos;De-Registration&apos;,SR_Menu_Text__c =&apos;Application for Winding up&apos;,SR_Menu_Text__c = &apos;Transfer from DIFC&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RejectionOfReturnedApplications</fullName>
        <active>true</active>
        <formula>AND(SR__r.SR_Group__c = &quot;GS&quot;, SR__r.SAP_PR_No__c = &quot;&quot;, SR__r.SAP_SO_No__c = &quot;&quot;, SR__r.SAP_Unique_No__c = &quot;&quot;, OR(AND(Step_Name__c = &quot;Require More Information from Customer&quot;, Status__r.Code__c = &quot;PENDING_CUSTOMER_INPUTS&quot;), AND(Step_Name__c = &quot;Re-upload Document&quot;, Status__r.Code__c = &quot;AWAITING_RE_UPLOAD&quot;)), DATEVALUE(CreatedDate) = TODAY(), SR__r.External_SR_Status__c &lt;&gt;&apos;Cancelled&apos;,SR__r.External_SR_Status__c &lt;&gt;&apos;Refund Processed&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_to_Customers_for_5_months_old_SRs</name>
                <type>Alert</type>
            </actions>
            <timeLength>150</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Relationship Manager Step Creation</fullName>
        <actions>
            <name>Visa_Exception_RM_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Relationship Manager Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Visa_Exception</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Request for Voluntary Strike off  Objection  Close after  90 Days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>PSA_Termination_Objection</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Request_for_Voluntary_Strike_off</value>
        </criteriaItems>
        <description>Request for Voluntary Strike off  Objection  Close after  90 Days</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Closed_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Step_Closed_Date_Time</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Require More Information from Customer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>REQUIRE_MORE_INFORMATION_FROM_CUSTOMER</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Application_of_Registration</value>
        </criteriaItems>
        <description>Warning :Reminder must go out to entities portal user cc RoC team, RM after one week of changing the status of inc/reg application to “return for more inf / re-upload”. The same should be repeated on weekly basis.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Pending_Notification_after_3_months</name>
                <type>Alert</type>
            </actions>
            <timeLength>114</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Require_More_Information_from_Customer_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>42</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Require_More_Information_from_Customer_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Require_More_Information_from_Customer_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Require_More_Information_from_Customer_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>70</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Require_More_Information_from_Customer_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>56</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Require_More_Information_from_Customer_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>84</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Pending_Notification_after_3_months</name>
                <type>Alert</type>
            </actions>
            <timeLength>144</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Pending_Notification_after_3_months</name>
                <type>Alert</type>
            </actions>
            <timeLength>174</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Return for More Info%2E Notification</fullName>
        <actions>
            <name>Return_for_More_Info_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( SR_Status__c = &apos;Return for More Information&apos;, Owner:Queue.QueueName!=&apos;Security&apos;,Owner:Queue.QueueName!=&apos;Data Protection Commissioner&apos;,Owner:Queue.QueueName!=&apos;Line Manager&apos;, Owner:Queue.QueueName!=&apos;Registrar&apos;, SR_Group__c &lt;&gt; &apos;Marketing&apos; , SR_Group__c &lt;&gt; &apos;Fit-Out &amp; Events&apos;,SR_Group__c &lt;&gt; &apos;GS&apos; , SR_Record_Type__c &lt;&gt; &apos;Tenant_Relation&apos;,SR_Record_Type__c &lt;&gt; &apos;Car_Parking&apos;,SR_Record_Type__c &lt;&gt; &apos;Dissolution&apos;,SR_Record_Type__c &lt;&gt; &apos;De_Registration&apos;,SR_Record_Type__c &lt;&gt; &apos;Transfer_from_DIFC&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Return for More Info%2E Notification - Tenant Relation</fullName>
        <actions>
            <name>Return_for_More_Info_Notification_Tenant_Relation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( SR_Status__c = &apos;Return for More Information&apos;, Owner:Queue.QueueName!=&apos;Security&apos;,Owner:Queue.QueueName!=&apos;Data Protection Commissioner&apos;,Owner:Queue.QueueName!=&apos;Line Manager&apos;, Owner:Queue.QueueName!=&apos;Registrar&apos; ,   SR_Record_Type__c = &apos;Tenant_Relation&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Return for More Info%2E Notification Car Parking</fullName>
        <actions>
            <name>Return_for_More_Info_Notification_Car_Parking</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( SR_Status__c = &apos;Return for More Information&apos;,  SR_Record_Type__c = &apos;Car_Parking&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RoC officer generate doc step Notification to RoC Officer</fullName>
        <actions>
            <name>Step_creation_notification_RoC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Step_Template__r.Code__c=&apos;NEW_DOCUMENT_GENERATION_&amp;_REGISTRAR_SIGNATURE&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Roc Officer Notification</fullName>
        <actions>
            <name>Roc_Officer_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 3) OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Required Information Updated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Status__c</field>
            <operation>equals</operation>
            <value>Document Re-uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>notEqual</operation>
            <value>Fit-Out &amp; Events</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SLA Crossed Notification for fit ot</fullName>
        <active>true</active>
        <formula>SR_Group__c = &quot;Fit-Out &amp; Events&quot;  &amp;&amp;  NOT(ISNULL(Due_Date__c))  &amp;&amp;  Is_Closed__c = False  &amp;&amp; SR_Menu_Text__c  &lt;&gt; &quot;Request for Technical Study/Inquiry&quot;  &amp;&amp; SR_Menu_Text__c  &lt;&gt; &quot;Request for Technical Clarification Form&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SLA_crossed_notification_for_fit_out</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SLA Security step is not verified in 5 working days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>SECURITY_EMAIL_APPROVAL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Application_of_Registration</value>
        </criteriaItems>
        <description>Ticket #4773 Portal notification when the Security step is not verified in 5 working days</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Security_ROC_Step_SLA_Breached</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Security Email Approval _ Closed</fullName>
        <actions>
            <name>Security_Email_Approval_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Security Email Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email Alert when Security team closed Security step</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Security Email Approval _ Put on Hold</fullName>
        <actions>
            <name>Security_Email_Approval_On_Hold</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Security Email Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Department__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Put_on_Hold__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Email Alert when Security team put step on hold</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email One Hour After Step Status Change to Verified</fullName>
        <active>true</active>
        <description>Custom time based workflow which will only fire for normal service requests where the request is letter type. The email will be sent 4 hours after the approval of request</description>
        <formula>Step_Status__c == &quot;Posted&quot; &amp;&amp;  SR__r.Internal_Status_Name__c == &quot;Approved&quot;  &amp;&amp; SR__r.SR_Template__r.Is_Letter__c == true &amp;&amp;  SR__r.Express_Service__c == false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_docs_generation_notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SR_approval_SMS</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Closed_Date_Time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Email When Step Status Change to Verified for Express Service</fullName>
        <actions>
            <name>New_docs_generation_notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SR_approval_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Custom time based workflow which will only fire for normal service requests where the request is letter type and is express service</description>
        <formula>Step_Status__c == &quot;Posted&quot; &amp;&amp;  SR_Status__c == &quot;Approved&quot; &amp;&amp; SR__r.SR_Template__r.Is_Letter__c == true &amp;&amp;  SR__r.Express_Service__c == true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Snag Fix not Completed for Fit Out</fullName>
        <active>true</active>
        <description>This rule will run when snag fix would not be completed within time.</description>
        <formula>(Step_Template__r.Code__c == &apos;COMPLETION_OF_MINOR_SNAGS&apos; ||  Step_Template__r.Code__c == &apos;RECTIFY_SNAGS&apos;)  &amp;&amp;  Status__c == &apos;Pending&apos; &amp;&amp;  (SR_Template__c == &apos;Fit_Out_Service_Request&apos; ||  SR_Template__c == &apos;Event_Service_Request&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Snags_fix_not_completed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Step__c.Notification_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Snag Fix not Completed for events</fullName>
        <active>false</active>
        <description>This rule will run when snag fix would not be completed within time.</description>
        <formula>Step_Template__r.Code__c == &apos;COMPLETION_OF_MINOR_SNAGS&apos; &amp;&amp;  Status__c == &apos;Pending&apos; &amp;&amp;  SR_Template__c == &apos;Event_Service_Request&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Step__c.Notification_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Step Assignment Changed Notification to GS Line Manager</fullName>
        <actions>
            <name>Step_Assignment_Changed_Notification_GS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IF(PRIORVALUE(OwnerId)!= OwnerId &amp;&amp; SR_Group__c=&apos;GS&apos; &amp;&amp;  LastModifiedBy.Alias!=&apos;ssham&apos; &amp;&amp;  NOT(CONTAINS(PRIORVALUE(OwnerId),&apos;00G&apos;)) &amp;&amp; $Profile.Id=&apos;00e200000012sq9&apos; &amp;&amp; PRIORVALUE(OwnerId)!=&apos;005200000048RKm&apos;,true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Step Breached SLA</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 5 OR 6 OR (9 AND 10) ) AND 2 AND 3 AND 4 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Application_of_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Assigned_to_client__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Change_of_Entity_Name</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Change_of_Business_Activity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>notEqual</operation>
            <value>CLOSED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Code__c</field>
            <operation>equals</operation>
            <value>REGISTRAR_APPROVAL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Sale_or_Transfer_of_Shares_or_Membership_Interest</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Legal_Structures__c</field>
            <operation>equals</operation>
            <value>LTD SPC</value>
        </criteriaItems>
        <description>When step time between created date and closed date exceeds the service turnaround time, send a system notification to the ROC team cc Alya, Hanaa and Nadine to close the step.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ROC_Step_SLA_Breached</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Step__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Step Closure Pending</fullName>
        <actions>
            <name>Update_date_for_3_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email to portal users to collect document from GS within 3 days</description>
        <formula>AND(SR_Group__c = &apos;GS&apos;, OR(SR__r.Record_Type_Name__c = &apos;DIFC_Sponsorship_Visa_New&apos;, SR__r.Record_Type_Name__c = &apos;Non_DIFC_Sponsorship_Visa_New&apos;,  						   SR__r.Record_Type_Name__c = &apos;DIFC_Sponsorship_Visa_Renewal&apos;, SR__r.Record_Type_Name__c = &apos;Non_DIFC_Sponsorship_Visa_Renewal&apos;,  						   SR__r.Record_Type_Name__c = &apos;Employment_Visa_Govt_to_DIFC&apos;, SR__r.Record_Type_Name__c = &apos;Visa_from_Individual_sponsor_to_DIFC&apos;,  						   SR__r.Record_Type_Name__c = &apos;DIFC_Sponsorship_Visa_Amendment&apos;, SR__r.Record_Type_Name__c = &apos;Non_DIFC_Sponsorship_Visa_Amendment&apos;), 						OR(Step_Name__c == &apos;Completed&apos;, Step_Name__c = &apos;Passport is Ready for Collection&apos;, Step_Name__c = &apos;Entry Permit is Issued&apos;),Is_Closed__c = False, TEXT(SR__r.Avail_Courier_Services__c)=&apos;No&apos;, NOT(SR__r.IsCancelled__c),NOT( Is_Rejected_Status__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Step_Closure_Pending</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>add_3_Days</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Step__c.From_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Step assigned Notification</fullName>
        <actions>
            <name>notification_to_Step_owner_when_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR ( 2 AND 4)) AND 3) OR  (5 AND 6) OR (7 AND 8 AND 11 AND (9 OR 10))</booleanFilter>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Line Manager Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>HOD Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>notEqual</operation>
            <value>GS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Department__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Head Of Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>notEqual</operation>
            <value>Visa_Exception</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Verification of Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Sys_SR_Group__c</field>
            <operation>equals</operation>
            <value>RORP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>equals</operation>
            <value>Lease_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Record_Type__c</field>
            <operation>equals</operation>
            <value>Surrender_Termination_of_Lease_and_Sublease</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Status__c</field>
            <operation>notEqual</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>Created for Line manger and HOD , Can be extend for others... in case &apos;Head Of Operations&apos; assigned  not visa sent notiftaion</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Step assigned to Finance</fullName>
        <actions>
            <name>Send_Email_To_Finance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Finance Refund Verification</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>PENDING_REVIEW</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Customer details on step</fullName>
        <actions>
            <name>Applicant_Email_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Applicant_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Step Closed Date</fullName>
        <actions>
            <name>Step_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Step_Closed_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Step__c.Is_Closed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Status_Type__c</field>
            <operation>equals</operation>
            <value>End</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Total Payable amount for Dissolution</fullName>
        <actions>
            <name>Update_Total_Payable_amount_for_Dissolut</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Total Payable amount for Dissolution</description>
        <formula>AND( NOT( ISBLANK( SR__r.Annual_Amount_AED__c ) ),  SR__r.Record_Type_Name__c = &apos;Dissolution&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update contractor and client details on step -Fit Out</fullName>
        <actions>
            <name>Contractor_email_fit_out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updating_client_email_for_fit_out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SR_Group__c =&apos;Fit-Out &amp; Events&apos; &amp;&amp; Step_Name__c != &apos;Client Feedback&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update contractor and client details on step -Fit Out for Client Feedback</fullName>
        <actions>
            <name>Additional_Email_for_fitout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contractor_email_fit_out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SR_Group__c =&apos;Fit-Out &amp; Events&apos; &amp;&amp; Step_Name__c = &apos;Client Feedback&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Upload Stakeholder Notification Upload</fullName>
        <actions>
            <name>Email_to_Applicant_Email_Voluntary_Strike_off</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Status_Code__c</field>
            <operation>equals</operation>
            <value>UPLOAD_STAKEHOLDER_NOTIFICATION</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Request_for_Voluntary_Strike_off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Group__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <description>Warning : Sent when Registrar review step is verified #8921</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Verified By Field update</fullName>
        <actions>
            <name>Populate_Verified_by_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>Front Desk Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Menu_Text__c</field>
            <operation>equals</operation>
            <value>Employment Confirmation Letter – GCC National Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Menu_Text__c</field>
            <operation>equals</operation>
            <value>Employment Confirmation Letter on Pre-Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Menu_Text__c</field>
            <operation>equals</operation>
            <value>Salary Certificates and NOCs – DIFC Sponsored Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Menu_Text__c</field>
            <operation>equals</operation>
            <value>Supporting letter to travel for Business/Personal – DIFC Sponsored Employee</value>
        </criteriaItems>
        <description>Populate Verified by Field for comfort letters other wise posting is failing</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Visa Exception HOD Review Approval</fullName>
        <actions>
            <name>Visa_Exception_HOD_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.Step_Name__c</field>
            <operation>equals</operation>
            <value>HOD Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.SR_Template__c</field>
            <operation>equals</operation>
            <value>Visa_Exception</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Winding Up Re-Upload Documents Notification</fullName>
        <actions>
            <name>Winding_Up_Re_upload_Documents_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification is used when &apos;Reupload Documents&apos; step is created for winding up SR</description>
        <formula>AND(SR_Status__c = &apos;Awaiting Re-upload&apos;, CONTAINS(Owner:Queue.QueueName, &apos;Client&apos;),OR(SR_Record_Type_Text__c = &apos;Dissolution&apos;,SR_Record_Type_Text__c = &apos;De_Registration&apos;,SR_Record_Type_Text__c = &apos;Transfer_from_DIFC&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Winding Up Return for More Information</fullName>
        <actions>
            <name>Winding_Up_Return_for_more_Info_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This notification is used when &apos;Return for More information&apos; step is created for winding up SR</description>
        <formula>AND(SR_Status__c = &apos;Return for More Information&apos;, SR_Group__c = &apos;ROC&apos; , OR(SR_Record_Type__c = &apos;Dissolution&apos;,SR_Record_Type__c = &apos;Transfer_from_DIFC&apos;,SR_Record_Type__c = &apos;De_Registration&apos;), CONTAINS(Owner:Queue.QueueName,&apos;Client&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Winding Up UpdateLiquidatorEmail</fullName>
        <actions>
            <name>Update_Liquidator_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Step__c.SR_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Dissolution</value>
        </criteriaItems>
        <criteriaItems>
            <field>Step__c.Sys_SR_Group__c</field>
            <operation>equals</operation>
            <value>ROC</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>