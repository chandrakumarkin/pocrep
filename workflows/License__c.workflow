<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_SF_Admin</fullName>
        <description>Email to SF Admin</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shabbir.ahmed@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/SF_admin_Notification</template>
    </alerts>
    <alerts>
        <fullName>FinTech_Company_renewed_Notification</fullName>
        <ccEmails>kiran.cornelio@difc.ae</ccEmails>
        <ccEmails>Sharon.PLong@difc.ae</ccEmails>
        <ccEmails>Nasser.Belhabala@difc.ae</ccEmails>
        <description>FinTech Company renewed Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fintech/Fintech_License_renewed</template>
    </alerts>
    <alerts>
        <fullName>Fintech_Email_Notification_to_Fintech_Team</fullName>
        <ccEmails>kiran.cornelio@difc.ae</ccEmails>
        <ccEmails>Sharon.PLong@difc.ae</ccEmails>
        <ccEmails>Nasser.Belhabala@difc.ae</ccEmails>
        <description>Fintech Email Notification to Fintech Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>arun.singh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fintech/Fintech_license_expiring</template>
    </alerts>
    <fieldUpdates>
        <fullName>ROC_Status_to_Not_Renewed_License</fullName>
        <field>ROC_Status__c</field>
        <literalValue>Not Renewed</literalValue>
        <name>ROC Status to Not Renewed License</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>FinTech Company Expiry Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>License__c.ROC_Status_List__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FinTech_Classification__c</field>
            <operation>equals</operation>
            <value>FinTech</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Fintech_Email_Notification_to_Fintech_Team</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>License__c.License_Expiry_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FinTech Company renewed  Notification</fullName>
        <actions>
            <name>FinTech_Company_renewed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send notification to Fintech team when Company renew their license</description>
        <formula>AND(Not(ISNEW()), ISCHANGED(Last_Renewal_Date__c) , ISPICKVAL(ROC_Status_List__c, &apos;Active&apos;),ISPICKVAL(Account__r.FinTech_Classification__c, &apos;FinTech&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ROC Status to Not Renewed License</fullName>
        <active>true</active>
        <criteriaItems>
            <field>License__c.License_Expiry_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>License__c.ROC_Status_List__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>License__c.Not_Renewed_Applicable__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Change Account ROC status to Not Renewed after 30 days form Next renewal date ticket #5812</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_SF_Admin</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>ROC_Status_to_Not_Renewed_License</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>License__c.License_Expiry_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>