<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HexaBPM__Update_Command_Button_Section_Record_Typ</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Command_Button_Section</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Command Button Section Record Typ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HexaBPM__Update_Page_Block_Section_Record_Typ</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Page_Block_Section</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Page Block Section Record Typ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HexaBPM__Update_Side_Bar_Section_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HexaBPM__Side_Bar_Section</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Side Bar Section Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HexaBPM__Update Record Type Command Button Section</fullName>
        <actions>
            <name>HexaBPM__Update_Command_Button_Section_Record_Typ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section__c.HexaBPM__Section_Type__c</field>
            <operation>equals</operation>
            <value>CommandButtonSection</value>
        </criteriaItems>
        <description>Update to Command Button Section Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HexaBPM__Update Record Type Page Block Section</fullName>
        <actions>
            <name>HexaBPM__Update_Page_Block_Section_Record_Typ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section__c.HexaBPM__Section_Type__c</field>
            <operation>equals</operation>
            <value>PageBlockSection</value>
        </criteriaItems>
        <description>Update to Page Block Section Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HexaBPM__Update Record Type Side Bar Section Section</fullName>
        <actions>
            <name>HexaBPM__Update_Side_Bar_Section_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Section__c.HexaBPM__Section_Type__c</field>
            <operation>equals</operation>
            <value>SideBarSection</value>
        </criteriaItems>
        <description>Update to Side Bar Section Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>