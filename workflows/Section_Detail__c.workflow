<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Object_Name_Field_update</fullName>
        <field>Object_Name__c</field>
        <formula>Section__r.Page__r.Page_Flow__r.Master_Object__c</formula>
        <name>Object Name Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Object Name Defualt value Rule</fullName>
        <actions>
            <name>Object_Name_Field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Section_Detail__c.Component_Type__c</field>
            <operation>equals</operation>
            <value>Input Field,Output Field</value>
        </criteriaItems>
        <criteriaItems>
            <field>Section_Detail__c.Object_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>