<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_End_Date</fullName>
        <field>Date_Time_To__c</field>
        <formula>ActivityDateTime + DurationInMinutes/1440</formula>
        <name>Copy End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Start_Date</fullName>
        <field>Date_Time_From__c</field>
        <formula>ActivityDateTime</formula>
        <name>Copy Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Copy Date fields</fullName>
        <actions>
            <name>Copy_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.StartDateTime</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Event.EndDateTime</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>