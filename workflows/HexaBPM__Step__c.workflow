<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Bank_Application_Update</fullName>
        <description>Bank Application Update</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Thankyou_Email_to_Mashreq_Customers</template>
    </alerts>
    <alerts>
        <fullName>Dubai_Police_Security_Notification</fullName>
        <ccEmails>C-saikalyan.s@difc.ae</ccEmails>
        <description>Dubai Police- Security Notification</description>
        <protected>false</protected>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WM_BD_Templates/Dubai_Police_Security_Notification</template>
    </alerts>
    <alerts>
        <fullName>Internal_clearance_Finance_License_Suspension</fullName>
        <description>Internal clearance Finance - License Suspension</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Internal_clearance_Finance_License_Suspension</template>
    </alerts>
    <alerts>
        <fullName>OB_Email_to_RRC_on_Return_Rejection</fullName>
        <description>OB Email to RRC on Return / Rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>OB_Committee</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Email_to_RRC_on_Return_Rejection</template>
    </alerts>
    <alerts>
        <fullName>OB_Notification_to_RS_Team</fullName>
        <description>OB Notification to RS Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Step_Creation_Notification_RS_Officer</template>
    </alerts>
    <alerts>
        <fullName>OB_Notify_BD_Self_Registration</fullName>
        <description>OB Notify BD Self Registration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Notify_BD_Self_Registration</template>
    </alerts>
    <alerts>
        <fullName>OB_Registrar_Step_Creation_Notification_Commercial_Permission</fullName>
        <description>OB Registrar Step Creation Notification - Commercial Permission</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Step_Creation_Commercial_Permission_Registrar</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_Edit</fullName>
        <description>OB Return for Edit</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_Edit</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_Edit_Commercial_Permission</fullName>
        <description>OB Return for Edit - Commercial Permission</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_Edit_Commercial_Permission</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_Edit_Commercial_Permission_Renewal</fullName>
        <description>OB Return for Edit Commercial Permission Renewal</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_Edit_Commercial_Permission_Renewal</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_Edit_Non_In_Principle</fullName>
        <description>OB Return for Edit Non In Principle</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_Edit_Non_In_Principle</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_More_Information</fullName>
        <description>OB Return for More Information</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_More_Information</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_More_Information_Non_In_Principle</fullName>
        <description>OB Return for More Information Non In Principle</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_More_Information_Non_In_Principle</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_Reupload</fullName>
        <description>OB Return for Reupload</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_Reupload</template>
    </alerts>
    <alerts>
        <fullName>OB_Return_for_Reupload_Non_In_Principle</fullName>
        <description>OB Return for Reupload Non In Principle</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Return_for_Reupload_Non_In_Principle</template>
    </alerts>
    <alerts>
        <fullName>OB_Step_Creation_CLO_CFO_Registrar</fullName>
        <description>OB Step Creation CLO/ CFO/ Registrar</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Step_Creation_CLO_CFO_Registrar</template>
    </alerts>
    <alerts>
        <fullName>OB_Step_Creation_Notification</fullName>
        <description>OB Step Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Step_Creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>OB_Step_Creation_Notification_for_application_withdrawal</fullName>
        <description>OB Step Creation Notification for application withdrawal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Step_Creation_Notification_for_Application_withdrawal</template>
    </alerts>
    <alerts>
        <fullName>OB_Step_Security_Re_trigger_Notification</fullName>
        <description>OB Step Security Re-trigger Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Security_Re_trigger_Notification</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>nada.rustom@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Digital_Onboarding_Project/OB_Step_Security_Re_trigger_Notification</template>
    </alerts>
    <alerts>
        <fullName>OB_notification_when_application_withdrawal_request_is_approved</fullName>
        <description>OB notification when application withdrawal request is approved</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Application_Withdrawal_Approval</template>
    </alerts>
    <alerts>
        <fullName>OB_notification_when_application_withdrawal_request_is_rejected</fullName>
        <description>OB notification when application withdrawal request is rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Application_Withdrawal_Rejection</template>
    </alerts>
    <alerts>
        <fullName>OB_notification_when_application_withdrawal_request_is_submitted</fullName>
        <description>OB notification when application withdrawal request is submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Application_Withdrawal</template>
    </alerts>
    <rules>
        <fullName>Internal clearance Finance - License Suspension</fullName>
        <actions>
            <name>Internal_clearance_Finance_License_Suspension</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Suspension_Of_License</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Returned__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Application withdrawal</fullName>
        <actions>
            <name>OB_notification_when_application_withdrawal_request_is_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>BD_ENTITY_WITHDRAWAL_REVIEW</value>
        </criteriaItems>
        <description>notification to customer when application withdrawal request is submitted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Application withdrawal approval</fullName>
        <actions>
            <name>OB_notification_when_application_withdrawal_request_is_approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>BD_ENTITY_WITHDRAWAL_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>notification to customer when application withdrawal request is approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Application withdrawal rejection</fullName>
        <actions>
            <name>OB_notification_when_application_withdrawal_request_is_rejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>BD_ENTITY_WITHDRAWAL_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>notification to customer when application withdrawal request is rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Email to RRC on Return %2F Rejection</fullName>
        <actions>
            <name>OB_Email_to_RRC_on_Return_Rejection</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND (4 OR 5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Return to BD</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Return to Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>CFO_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>REGISTRAR_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>CLO_REVIEW</value>
        </criteriaItems>
        <description>Email template created for the RRC team when the application is returned to BD / Compliance or Rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Notify BD Self Registration</fullName>
        <actions>
            <name>OB_Notify_BD_Self_Registration</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>RM_Assignment</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Confirmation</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>New_User_Registration</value>
        </criteriaItems>
        <description>This email template is created for the BD team when a Self Registration Sr is created / RM assignment step is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for Edit</fullName>
        <actions>
            <name>OB_Return_for_Edit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>AWAITING_CHANGES</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for edit, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for Edit - Commercial Permission</fullName>
        <actions>
            <name>OB_Return_for_Edit_Commercial_Permission</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>AWAITING_CHANGES</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>workflow created for the customer when the commercial permission application is returned for edit, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for Edit Commercial Permission Renewal</fullName>
        <actions>
            <name>OB_Return_for_Edit_Commercial_Permission_Renewal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>AWAITING_CHANGES</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission_Renewal</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for edit, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for Edit Non In Principle</fullName>
        <actions>
            <name>OB_Return_for_Edit_Non_In_Principle</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>AWAITING_CHANGES</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>Commercial_Permission_Renewal</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for edit, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for More Information</fullName>
        <actions>
            <name>OB_Return_for_More_Information</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (1 AND 2 AND 4)</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Additional Information</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>AWAITING_INFORMATION_FROM_ENTITY</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for more information, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for More Information Non In Principle</fullName>
        <actions>
            <name>OB_Return_for_More_Information_Non_In_Principle</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Additional Information</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>AWAITING_INFORMATION_FROM_ENTITY</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for more information, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for Reupload</fullName>
        <actions>
            <name>OB_Return_for_Reupload</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (1 AND 2 AND 4)</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Re-upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>RE_UPLOAD_DOCUMENT</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for more documents, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Return for Reupload Non In Principle</fullName>
        <actions>
            <name>OB_Return_for_Reupload_Non_In_Principle</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Re-upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>RE_UPLOAD_DOCUMENT</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>In_Principle</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <description>workflow created for the customer when the application is returned for more documents, an email will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Step Creation CLO%2F CFO%2F Registrar</fullName>
        <actions>
            <name>OB_Step_Creation_CLO_CFO_Registrar</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 4) OR (2 AND 4) OR (3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>CFO_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>CLO_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>REGISTRAR_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>Suspension_Of_License</value>
        </criteriaItems>
        <description>This workflow will send an email to the action item owner when the CFO, CLO, or Registrar action item is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Step Creation Notification</fullName>
        <actions>
            <name>OB_Step_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>notEqual</operation>
            <value>SECURITY_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>notEqual</operation>
            <value>CFO_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>notEqual</operation>
            <value>CLO_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>Client Entry User</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>notEqual</operation>
            <value>REGISTRAR_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>New_User_Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>notEqual</operation>
            <value>Name_Check</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>notEqual</operation>
            <value>BD_ENTITY_WITHDRAWAL_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>notContain</operation>
            <value>RS_OFFICER_QUICK_REVIEW,RS_OFFICER_REVIEW,ENTITY_SIGNING,RS_MANAGER_REVIEW,ENTITY_NAME_REVIEW</value>
        </criteriaItems>
        <description>This workflow will send an email to the action item owner when the action item is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Step Creation Notification RS Team</fullName>
        <actions>
            <name>OB_Notification_to_RS_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>contains</operation>
            <value>RS_OFFICER_QUICK_REVIEW,RS_OFFICER_REVIEW,ENTITY_SIGNING,RS_MANAGER_REVIEW,ENTITY_NAME_REVIEW</value>
        </criteriaItems>
        <description>This workflow will send an email to the action item owner when the action item is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Step Creation Notification for application withdrawal</fullName>
        <actions>
            <name>OB_Step_Creation_Notification_for_application_withdrawal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>BD_ENTITY_WITHDRAWAL_REVIEW</value>
        </criteriaItems>
        <description>This workflow will send an email to the action item owner when an application withdrawal action item is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Step Creation Registrar - Commercial Permission</fullName>
        <actions>
            <name>OB_Registrar_Step_Creation_Notification_Commercial_Permission</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Commercial_Permission</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.Step_Template_Code__c</field>
            <operation>equals</operation>
            <value>REGISTRAR_REVIEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.SR_Recordtype_Name__c</field>
            <operation>equals</operation>
            <value>Suspension_Of_License</value>
        </criteriaItems>
        <description>This workflow will send an email to the action item owner when the Registrar action item is created for commercial permission applications</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OB Step Security Re-trigger Notification</fullName>
        <actions>
            <name>OB_Step_Security_Re_trigger_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Summary__c</field>
            <operation>equals</operation>
            <value>Re Trigger Security Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Security Email status update notification</fullName>
        <actions>
            <name>Dubai_Police_Security_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.DIFC_Security_Email_Response__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Thankyou Email to Mashreq Customers</fullName>
        <actions>
            <name>Bank_Application_Update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Summary__c</field>
            <operation>equals</operation>
            <value>Create a Bank Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM__Step__c.HexaBPM__Step_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>