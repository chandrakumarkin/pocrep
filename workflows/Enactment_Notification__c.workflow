<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Aligning_Business_Activities_Email</fullName>
        <description>Aligning Business Activities Email</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>roc@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Aligning_Business_Activities</template>
    </alerts>
    <alerts>
        <fullName>Contact_Info_Notification</fullName>
        <description>Contact Info Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_5__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/Client_contact_form_reminders</template>
    </alerts>
    <alerts>
        <fullName>FRC_Enactment_Notification</fullName>
        <description>FRC  Enactment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_5__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/All_other_legal_structures</template>
    </alerts>
    <alerts>
        <fullName>LTD_Enactment_Notification</fullName>
        <description>LTD Enactment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_5__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>System_Templates/LTDs_converted_to_Private_Company</template>
    </alerts>
    <alerts>
        <fullName>Liquidator_Pending_Dissolution</fullName>
        <description>Liquidator Pending Dissolution</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Liquidator_Pending_Dissolution</template>
    </alerts>
    <alerts>
        <fullName>Liquidator_Pending_Dissolution_One_Week_Remainder</fullName>
        <description>Liquidator Pending Dissolution One Week Remainder</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Liquidator_Pending_Dissolution_One_Week_Remainder</template>
    </alerts>
    <alerts>
        <fullName>Liquidator_Pending_Dissolution_Three_Week_Remainder</fullName>
        <description>Liquidator Pending Dissolution Three Week Remainder</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Liquidator_Pending_Dissolution_Three_Week_Remainder</template>
    </alerts>
    <alerts>
        <fullName>Liquidator_Pending_Dissolution_Two_Week_Remainder</fullName>
        <description>Liquidator Pending Dissolution Two Week Remainder</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Liquidator_Pending_Dissolution_Two_Week_Remainder</template>
    </alerts>
    <alerts>
        <fullName>Prescribed_Notification</fullName>
        <ccEmails>portal@difc.ae</ccEmails>
        <description>Prescribed Notification</description>
        <protected>false</protected>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>User_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ROC_Email_System_Emails/Prescribed_Company_Notification</template>
    </alerts>
    <alerts>
        <fullName>Reminder_to_update_missing_or_outdated_details</fullName>
        <description>Reminder to update missing or outdated details</description>
        <protected>false</protected>
        <recipients>
            <field>Portal_User_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Principal_User_5__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>User_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Reminder_to_update_missing_or_outdated_details</template>
    </alerts>
    <rules>
        <fullName>Contact Details Notification</fullName>
        <actions>
            <name>Contact_Info_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Type__c</field>
            <operation>equals</operation>
            <value>Contact Info</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Contact_Details_Provided__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.ROC_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contact_Info_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Enactment_Notification__c.CreatedDate</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Contact_Info_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Enactment_Notification__c.CreatedDate</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Contact_Info_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Enactment_Notification__c.CreatedDate</offsetFromField>
            <timeLength>270</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Enactment Notification - Account Business Activity</fullName>
        <actions>
            <name>Aligning_Business_Activities_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Type__c</field>
            <operation>equals</operation>
            <value>Account Business Activity</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enactment Notification FRC</fullName>
        <actions>
            <name>FRC_Enactment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>GP</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enactment Notification LTD</fullName>
        <actions>
            <name>LTD_Enactment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Legal_Type_of_Entity__c</field>
            <operation>equals</operation>
            <value>LTD SPC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enactment Notification Notification Prescribed</fullName>
        <actions>
            <name>Prescribed_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Type__c</field>
            <operation>equals</operation>
            <value>Prescribed Company</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Liquidator Pending Dissolution Customer Remainders</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Type__c</field>
            <operation>equals</operation>
            <value>Liquidator Pending Dissolution Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Liquidator_Pending_Dissolution_Two_Week_Remainder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Liquidator_Pending_Dissolution_14th_Day_Remainder</name>
                <type>Task</type>
            </actions>
            <timeLength>13</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Liquidator_Pending_Dissolution_Three_Week_Remainder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Liquidator_Pending_Dissolution_21st_Day_Remainder</name>
                <type>Task</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Liquidator_Pending_Dissolution_One_Week_Remainder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Liquidator_Pending_Dissolution_7_Day_Remainder</name>
                <type>Task</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Liquidator Pending Dissolution Initial Remainder</fullName>
        <actions>
            <name>Liquidator_Pending_Dissolution</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent_Liq</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Type__c</field>
            <operation>equals</operation>
            <value>Liquidator Pending Dissolution</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Account_Business_Activity_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reminder to update missing or outdated details</fullName>
        <actions>
            <name>Reminder_to_update_missing_or_outdated_details</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Enactment_Notification__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Enactment_Notification__c.Type__c</field>
            <operation>equals</operation>
            <value>Relationship Missing Details</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_to_update_missing_or_outdated_details</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Sent</name>
                <type>Task</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_to_update_missing_or_outdated_details</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Sent</name>
                <type>Task</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_to_update_missing_or_outdated_details</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Sent</name>
                <type>Task</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Email_Sent</fullName>
        <assignedTo>arun.singh@difc.ae</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Account Business Activity Email Sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Email_Sent_Liq</fullName>
        <assignedTo>integration@difc.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Liquidator Pending Dissolution Remainder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email Sent Liquidator Pending Dissolution</subject>
    </tasks>
    <tasks>
        <fullName>Liquidator_Pending_Dissolution_14th_Day_Remainder</fullName>
        <assignedTo>integration@difc.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Liquidator Pending Dissolution  - 14th Day Remainder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Liquidator Pending Dissolution  - 14th Day Remainder</subject>
    </tasks>
    <tasks>
        <fullName>Liquidator_Pending_Dissolution_21st_Day_Remainder</fullName>
        <assignedTo>integration@difc.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Liquidator Pending Dissolution  - 21st Day Remainder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Liquidator Pending Dissolution  - 21st Day Remainder</subject>
    </tasks>
    <tasks>
        <fullName>Liquidator_Pending_Dissolution_7_Day_Remainder</fullName>
        <assignedTo>integration@difc.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Liquidator Pending Dissolution  - 7th Day Remainder</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Liquidator Pending Dissolution  - 7 Day Remainder</subject>
    </tasks>
</Workflow>