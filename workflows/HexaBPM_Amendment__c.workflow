<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OB_Sending_Super_User_Notification_to_Authorized_Signatory</fullName>
        <description>OB Sending Super User Notification to Authorized Signatory</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Super_User_Access_Notification_to_Authorized_Signatory</template>
    </alerts>
    <alerts>
        <fullName>OB_Sending_Super_User_Notification_to_Director</fullName>
        <description>Sending Super User Notification to Director</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Digital_Onboarding_Project/OB_Super_User_Access_Notification_to_Director</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Country_of_Registration</fullName>
        <field>Country_of_Registration__c</field>
        <literalValue>United Arab Emirates</literalValue>
        <name>Update Country of Registration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OB Notification of Super User to Authorized Signatory</fullName>
        <actions>
            <name>OB_Sending_Super_User_Notification_to_Authorized_Signatory</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification of Creation of Super User to the Authorized Signatory</description>
        <formula>ISCHANGED(Is_Authorized_Signatory_Flag__c) &amp;&amp; Is_Authorized_Signatory_Flag__c = true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OB Notification of Super User to Director</fullName>
        <actions>
            <name>OB_Sending_Super_User_Notification_to_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification of Creation of Super User to the Directors</description>
        <formula>ISCHANGED( Is_Director_Flag__c) &amp;&amp; Is_Director_Flag__c = true &amp;&amp; Is_Authorized_Signatory_Flag__c = false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Country of Registration</fullName>
        <actions>
            <name>Update_Country_of_Registration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HexaBPM_Amendment__c.Is_this_Entity_registered_with_DIFC__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM_Amendment__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Body Corporate</value>
        </criteriaItems>
        <criteriaItems>
            <field>HexaBPM_Amendment__c.Country_of_Registration__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>