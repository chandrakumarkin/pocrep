<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OB_Update_Letter_Template_Id</fullName>
        <field>HexaBPM__LetterTemplate__c</field>
        <formula>DDP_Letter_Template__c</formula>
        <name>OB Update Letter Template Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Letter_Template_Id</fullName>
        <field>HexaBPM__LetterTemplate__c</field>
        <formula>DDP_Letter_Template__c</formula>
        <name>Update Letter Template Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>OB Update Letter Template Id on Document Master</fullName>
        <actions>
            <name>OB_Update_Letter_Template_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(DDP_Letter_Template__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Letter Template Id on Document Master</fullName>
        <actions>
            <name>Update_Letter_Template_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>