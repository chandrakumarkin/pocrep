<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Listing_Notification_to_Owner_for_New_Intrest</fullName>
        <description>Listing : Notification to Owner for New Intrest</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Listing_Notification_to_Owner_for_Interest</template>
    </alerts>
    <alerts>
        <fullName>Listing_Notification_to_User_for_Listing</fullName>
        <description>Listing : Notification to User for Listing</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Listing_Notification_to_User_for_Listing</template>
    </alerts>
    <fieldUpdates>
        <fullName>Listing_Update_Sys_Send_Email_to_False</fullName>
        <description>Listing : Update Sys Send Email to False</description>
        <field>Sys_Send_Email__c</field>
        <literalValue>0</literalValue>
        <name>Listing : Update Sys Send Email to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Listing %3A Notification to Owner for New Intrest</fullName>
        <actions>
            <name>Listing_Notification_to_Owner_for_New_Intrest</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inquiry__c.Sender_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Inquiry__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Interest</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inquiry__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Listing : Notification to Owner for New Intrest</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Listing %3A Notification to User for New Listing</fullName>
        <actions>
            <name>Listing_Notification_to_User_for_Listing</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Listing_Update_Sys_Send_Email_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inquiry__c.Sys_Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inquiry__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Inquiry__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <description>Listing : Notification to User for New Listing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>