<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Share_Update</fullName>
        <field>For_Updates__c</field>
        <literalValue>1</literalValue>
        <name>Account Share Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Account_Share__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Nominal_Value</fullName>
        <field>Total_Nominal_Value__c</field>
        <formula>Nominal_Value__c</formula>
        <name>Total Nominal Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Share_Capital</fullName>
        <field>Share_Capital_Updated__c</field>
        <formula>NOW()</formula>
        <name>Update Account Share Capital</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Change in Shareholder Detail</fullName>
        <actions>
            <name>Account_Share_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Share_Capital</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( No_of_Shares__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populates Total Nominal Value</fullName>
        <actions>
            <name>Account_Share_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Total_Nominal_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Shareholder_Detail__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>