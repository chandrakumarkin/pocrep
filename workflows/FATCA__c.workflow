<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_FATCA_Key</fullName>
        <field>Key__c</field>
        <formula>GIIN__c &amp;&apos;--&apos;&amp; US_TIN__c &amp;&apos;--&apos;&amp; TEXT(Account_Holder_Type__c) &amp;&apos;--&apos;&amp; TEXT(Payment_Type__c) &amp;&apos;--&apos;&amp; TEXT(Account_Type__c) &amp;&apos;--&apos;&amp; Account_Number__c</formula>
        <name>Update FATCA Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate FATCA Key</fullName>
        <actions>
            <name>Update_FATCA_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>