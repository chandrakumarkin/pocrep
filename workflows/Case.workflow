<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Escalate_Case</fullName>
        <description>Escalate Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>khadija.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Case_Escalation_Expired</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Business_Centre</fullName>
        <description>Case Closure-Business Centre</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Business_Centre</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Business_Development</fullName>
        <description>Case Closure-Business Development</description>
        <protected>false</protected>
        <recipients>
            <recipient>Business_Development_Case_Closure</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Community_Management</fullName>
        <description>Case Closure-Community Management</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Community_Management</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_DIFC_Academy</fullName>
        <description>Case Closure-DIFC Academy</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_DIFC_Academy</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Data_Centre</fullName>
        <description>Case Closure-Data Centre</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Data_Centre</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Data_Protection</fullName>
        <description>Case Closure-Data Protection</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_ROS_Data_Protection</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_FinTech_Hive</fullName>
        <description>Case Closure-FinTech Hive</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_FinTech_Hive</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Finance</fullName>
        <description>Case Closure-Finance</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Finance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Email_templates/Case_Forward_to_Email</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Gate_District</fullName>
        <description>Case Closure-Gate District</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Gate_District</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Email_templates/Case_Forward_to_Email</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Gate_Village</fullName>
        <description>Case Closure-Gate Village</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Gate_Village</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Email_templates/Case_Forward_to_Email</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Mall_Management</fullName>
        <description>Case Closure-Mall Management</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Mall_Management</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Marketing</fullName>
        <description>Case Closure-Marketing</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Marketing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Office_Leasing</fullName>
        <description>Case Closure-Office Leasing</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Office_Leasing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Property_Development</fullName>
        <description>Case Closure-Property Development</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Property_Development</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Property_Management</fullName>
        <description>Case Closure-Property Management</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Property_Management</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Registrar_of_Companies</fullName>
        <description>Case Closure-Registrar of Companies</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Registrar_of_Companies</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Registry_Services_Finance</fullName>
        <description>Case Closure-Registry Services (Finance)</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Registry_Services_Finance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Retail</fullName>
        <description>Case Closure-Retail</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Retail</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Security</fullName>
        <description>Case Closure-Security</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Security</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Specialty_Leasing</fullName>
        <description>Case Closure-Specialty Leasing</description>
        <protected>false</protected>
        <recipients>
            <recipient>Speciality_Leasing</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Case_Closure_Truck_Tunnel</fullName>
        <description>Case Closure-Truck Tunnel</description>
        <protected>false</protected>
        <recipients>
            <recipient>GN_Truck_Tunnel</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Queue_Members_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Acknowledgment_Complaint</fullName>
        <description>Email Acknowledgment Complaint</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Acknowledgment_Complaint</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Acknowledgment_Compliment</fullName>
        <description>Email Acknowledgment Compliment</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Acknowledgment_Compliment</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Acknowledgment_Query</fullName>
        <description>Email Acknowledgment Query</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Acknowledgment_Query</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Acknowledgment_Suggestion</fullName>
        <description>Email Acknowledgment Suggestion</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Acknowledgment_Suggestion</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Case_SLA_Expiration</fullName>
        <description>Email Case SLA Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Case_SLA_Expiration</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Case_SLA_Expiration_BD</fullName>
        <description>Email Case SLA Expiration (BD)</description>
        <protected>false</protected>
        <recipients>
            <recipient>khadija.ali@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Case_SLA_Expiration</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Client_Awaiting_Response</fullName>
        <description>GN - Email Client Awaiting Response</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Client_for_Awaiting_Response</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_Client_for_Case_Closure</fullName>
        <description>Email Client for Case Closure</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_Email_Client_for_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>GN_Email_New_Case_Assignment</fullName>
        <description>Email New Case Assignment</description>
        <protected>false</protected>
        <recipients>
            <recipient>leo.cruz@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mohammed.odeh@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply.portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GN_Case_Automated_Email_Templates/GN_New_Case_Assignment</template>
    </alerts>
    <alerts>
        <fullName>GN_Notify_case_owner_when_the_case_is_reopened</fullName>
        <description>Notify case owner when the case is reopened</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DIFC_Email_Templates/GN_Email_Case_Owner_for_Case_Reopened</template>
    </alerts>
    <alerts>
        <fullName>On_Case_creation_send_notification_to_GS</fullName>
        <ccEmails>gs.helpdesk@difc.ae</ccEmails>
        <description>On Case creation send notification to GS</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Case_Creation_notification_to_RoC</template>
    </alerts>
    <alerts>
        <fullName>On_Case_creation_send_notification_to_IT</fullName>
        <ccEmails>IT.Infrastructure@difc.ae</ccEmails>
        <description>On Case creation send notification to IT</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Case_Creation_notification_to_RoC</template>
    </alerts>
    <alerts>
        <fullName>On_Case_creation_send_notification_to_RoC</fullName>
        <description>On Case creation send notification to RoC</description>
        <protected>false</protected>
        <recipients>
            <recipient>nadine.chaar@difc.ae</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Case_Creation_notification_to_RoC</template>
    </alerts>
    <alerts>
        <fullName>On_Case_creation_send_notification_to_RoRP</fullName>
        <ccEmails>rorp@difc.ae</ccEmails>
        <description>On Case creation send notification to RoRP</description>
        <protected>false</protected>
        <senderAddress>portal@difc.ae</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DIFC_Email_Templates/Case_Creation_notification_to_RoC</template>
    </alerts>
    <fieldUpdates>
        <fullName>GN_Populate_FullName_from_Chat</fullName>
        <field>Full_Name__c</field>
        <formula>GN_First_Name__c + &quot; &quot; + GN_Last_Name__c</formula>
        <name>GN - Populate FullName from Chat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Auto_Close_Case_to_Blank</fullName>
        <field>GN_Auto_Close_Case_Date__c</field>
        <name>GN - Update Auto Close Case to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Awaiting_Response_Client_SD</fullName>
        <field>GN_Awaiting_Response_Client_Start_Date__c</field>
        <formula>NOW()</formula>
        <name>GN - Update Awaiting Response Client SD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Case_Age_SLA</fullName>
        <field>GN_Case_Age_SLA__c</field>
        <name>GN - Update Case Age SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Case_Origin_from_Chat</fullName>
        <field>Origin</field>
        <literalValue>Chat</literalValue>
        <name>GN - Update Case Origin from Chat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Case_Origin_to_Portal</fullName>
        <description>Update Case Origin to Portal</description>
        <field>Origin</field>
        <literalValue>Portal</literalValue>
        <name>Update Case Origin to Portal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Case_Reopened_Count</fullName>
        <field>GN_Case_Reopened_Count__c</field>
        <formula>GN_Case_Reopened_Count__c +1</formula>
        <name>GN - Update Case Reopened Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Client_Type_to_Non_DIFC</fullName>
        <description>GN - Update Client Type to Non-DIFC</description>
        <field>GN_Client_Type__c</field>
        <literalValue>Non-DIFC</literalValue>
        <name>Update Client Type to Non-DIFC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Custom_Email_to_Web_Email</fullName>
        <description>Update Custom Email to Web Email</description>
        <field>Email__c</field>
        <formula>SuppliedEmail</formula>
        <name>Update Custom Email to Web Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Custom_Field_Escalated</fullName>
        <description>Update Custom Field Escalated</description>
        <field>GN_Escalated__c</field>
        <literalValue>1</literalValue>
        <name>Update Custom Field Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_End_Date_to_Blank</fullName>
        <field>GN_Awaiting_Response_Client_End_Date__c</field>
        <name>GN - Update End Date to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Escalated_Flag_to_False</fullName>
        <field>GN_Escalated__c</field>
        <literalValue>0</literalValue>
        <name>GN - Update Escalated Flag to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Full_Name_to_Web_Name</fullName>
        <description>Update Full Name to Web Name</description>
        <field>Full_Name__c</field>
        <formula>SuppliedName</formula>
        <name>Update Full Name to Web Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Full_Name_with_Contact_Name</fullName>
        <field>Full_Name__c</field>
        <formula>Contact.FirstName+&quot; &quot;+Contact.LastName</formula>
        <name>Update Full Name with Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Line_Manager_Email</fullName>
        <field>Line_Manager_Email__c</field>
        <formula>Owner:User.GN_Line_Manager_Email__c</formula>
        <name>GN - Update Line Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Line_Manager_Email_External</fullName>
        <field>GN_Line_Manager_Email_External__c</field>
        <formula>Owner:User.GN_Line_Manager_Email_External__c</formula>
        <name>GN - Update Line Manager Email External</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Record_Type_to_Default</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Default</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>GN - Update Record Type to Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Record_Type_to_Default_False</fullName>
        <field>GN_Update_Record_Type_to_Default__c</field>
        <literalValue>0</literalValue>
        <name>GN - Update Record Type to Default False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Start_Date_to_Blank</fullName>
        <field>GN_Awaiting_Response_Client_Start_Date__c</field>
        <name>GN - Update Start Date to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed - Resolved</literalValue>
        <name>GN - Update Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Status_to_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>GN - Update Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Status_to_Invalid</fullName>
        <field>Status</field>
        <literalValue>Invalid</literalValue>
        <name>Update Status to Invalid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Subject_from_Web</fullName>
        <description>GN - Update Subject from Web</description>
        <field>Subject</field>
        <formula>IF(LEN(Description) &lt; 255,
   Description,
   &apos;&apos;)</formula>
        <name>Update Subject from Web</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_Type_to_Invalid</fullName>
        <field>GN_Type_of_Feedback__c</field>
        <literalValue>Invalid</literalValue>
        <name>Update Type to Invalid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_to_Close_Case_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>GN_Closed_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Close Case Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GN_Update_to_Mystery_Shopping_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>GN_Mystery_Shopping</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Mystery Shopping RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Type_to_DIFC</fullName>
        <description>Update Client Type to DIFC</description>
        <field>GN_Client_Type__c</field>
        <literalValue>DIFC</literalValue>
        <name>Update Client Type to DIFC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_with_Contact_Email</fullName>
        <field>Email__c</field>
        <formula>Contact.Email</formula>
        <name>Update Email with Contact Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Priority_Field_from_Web</fullName>
        <description>Update Priority Field from Web-to-Case</description>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Update Priority Field from Web</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Closed_Compliment</fullName>
        <description>Update Status to Closed - Compliment</description>
        <field>Status</field>
        <literalValue>Closed - Compliment</literalValue>
        <name>Update Status to Closed - Compliment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_New</fullName>
        <description>Update Status to New</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Case Creation notification to GS</fullName>
        <actions>
            <name>On_Case_creation_send_notification_to_GS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Type,&apos;Employee Services&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Creation notification to IT</fullName>
        <actions>
            <name>On_Case_creation_send_notification_to_IT</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Type,&apos;IT Services&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Creation notification to RORP</fullName>
        <actions>
            <name>On_Case_creation_send_notification_to_RoRP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Type,&apos;Property Services&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Creation notification to RoC</fullName>
        <actions>
            <name>On_Case_creation_send_notification_to_RoC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISPICKVAL(Type,&apos;Employee Services&apos;)) &amp;&amp;  NOT(ISPICKVAL(Type,&apos;Property Services&apos;)) &amp;&amp;  NOT(ISPICKVAL(Type,&apos;IT Services&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GN - Auto Close Case</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Response - Client</value>
        </criteriaItems>
        <description>Auto close the case after 10 days of Awaiting Response from Client</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GN_Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.GN_Auto_Close_Case_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GN - Case Assignment to BD</fullName>
        <actions>
            <name>GN_Email_New_Case_Assignment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Case assignment to BD - Leo and Odeh</description>
        <formula>AND( Owner:Queue.QueueName = &apos;Business Development&apos;, ISCHANGED(  OwnerId  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Custom Email field with Web Email Value</fullName>
        <actions>
            <name>GN_Update_Custom_Email_to_Web_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Custom Email field with Web Email Value</description>
        <formula>AND(     OR(        TEXT(Origin)=&apos;Email&apos;,        TEXT(Origin)=&apos;Web&apos;),     /*ISBLANK(Full_Name__c),*/     ISBLANK(Email__c)    )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GN - Email Case SLA Expiration</fullName>
        <active>true</active>
        <description>Case SLA expiration in 2 days to the case owner</description>
        <formula>AND(     IsClosed != TRUE,     TEXT(GN_Department__c)  &lt;&gt; &apos;Government Services&apos;,     TEXT(GN_Department__c)  &lt;&gt; &apos;Contact Centre&apos;,     TEXT(GN_Department__c)  &lt;&gt; &apos;Registry Services (Properties)&apos;,     TEXT(GN_Department__c)  &lt;&gt; &apos;Registry Services (Companies)&apos;, TEXT(GN_Department__c) &lt;&gt; &apos;Call Centre&apos;, 	TEXT(GN_Department__c) &lt;&gt; &apos;Corporate Development&apos;,     GN_Owner_Name__c &lt;&gt; &apos;Invalid Cases&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GN_Email_Case_SLA_Expiration</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.GN_Due_Date_Time__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GN - Email Case SLA Expiration %28BD%29</fullName>
        <active>true</active>
        <description>Case SLA expiration in 2 days to the case owner (BD)</description>
        <formula>AND(     IsClosed != TRUE,     GN_Owner_Name__c &lt;&gt; &apos;Invalid Cases&apos;,     GN_Sys_Case_Queue__c = &apos;Business Development&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GN_Email_Case_SLA_Expiration_BD</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.GN_Due_Date_Time__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GN - Email Case SLA Expiration%288 hours%29</fullName>
        <active>false</active>
        <description>Case SLA expiration in 8 hours to the case owner</description>
        <formula>AND(     IsClosed != TRUE,     TEXT(GN_Department__c)  =  &apos;Call Centre&apos;,     TEXT(GN_Department__c)   =  &apos;Corporate Development&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Email Client Awaiting Response</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Response - Client</value>
        </criteriaItems>
        <description>Email Client that case is still Awaiting Response - Client for 5 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GN_Email_Client_Awaiting_Response</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.GN_Awaiting_Response_Client_End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GN - Email Client for Case Closure</fullName>
        <actions>
            <name>GN_Email_Client_for_Case_Closure</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email Client for Case Closure</description>
        <formula>AND(     TEXT(Status) = &apos;Closed - Resolved&apos;,     TEXT(GN_Type_of_Feedback__c) = &apos;Complaint&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Email owner when case is reopened</fullName>
        <actions>
            <name>GN_Notify_case_owner_when_the_case_is_reopened</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email notification to the case owner when the case is re-opened. Deactivating this rule since a standard email is already being sent to the owner.</description>
        <formula>AND( Text(GN_Department__c)&lt;&gt;&apos;Registry Services (Companies)&apos;, Text(GN_Department__c)&lt;&gt;&apos;Registry Services (Properties)&apos;, Text(GN_Department__c)&lt;&gt;&apos;Government Services&apos;, ISPICKVAL(PRIORVALUE(Status) , &apos;Closed - Resolved&apos;) , ISPICKVAL(Status, &apos;In Progress&apos;)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Escalate Case</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.GN_Type_of_Feedback__c</field>
            <operation>equals</operation>
            <value>Complaint,Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>,Closed - Resolved,Closed - Forwarded to Department,Duplicate,Invalid,Awaiting Response - Client,Awaiting Response - Internal Department,Awaiting Response - Authorities</value>
        </criteriaItems>
        <description>Escalate Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GN_Update_Custom_Field_Escalated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.GN_Due_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GN - Escalate Case %28BD%29</fullName>
        <actions>
            <name>Escalate_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.GN_Type_of_Feedback__c</field>
            <operation>equals</operation>
            <value>Complaint,Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed - Resolved,Closed - Forwarded to Department,Duplicate,Invalid,Awaiting Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GN_Sys_Case_Queue__c</field>
            <operation>equals</operation>
            <value>Business Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.GN_Escalated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Escalate Case (BD)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Populate FullName from Chat</fullName>
        <actions>
            <name>GN_Populate_FullName_from_Chat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GN_Update_Case_Origin_from_Chat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Live Chat</value>
        </criteriaItems>
        <description>#5598: auto populate case origin and full name</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GN - Uncheck Escalated Indicator</fullName>
        <actions>
            <name>GN_Update_Escalated_Flag_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Unchecks the flag to Escalated if the Duedate time of the case changes</description>
        <formula>AND(       NOT(ISNULL(PRIORVALUE(GN_Due_Date_Time__c))),       ISCHANGED(GN_Due_Date_Time__c)     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Awaiting Response - Client Dates to Blank</fullName>
        <actions>
            <name>GN_Update_Auto_Close_Case_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GN_Update_End_Date_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GN_Update_Start_Date_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Awaiting Response - Client</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Awaiting Response - Client SD</fullName>
        <actions>
            <name>GN_Update_Awaiting_Response_Client_SD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Response - Client</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Case Age SLA</fullName>
        <actions>
            <name>GN_Update_Case_Age_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Blanks the Case Age SLA field when case owner changes</description>
        <formula>ISCHANGED( GN_Sys_Case_Queue__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Case Origin to Portal</fullName>
        <actions>
            <name>GN_Update_Case_Origin_to_Portal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GN_Update_Full_Name_with_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Email_with_Contact_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Case Origin to Portal</description>
        <formula>CreatedBy.ContactId  &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Client Type to DIFC</fullName>
        <actions>
            <name>Update_Client_Type_to_DIFC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Client Type to DIFC if Account Name is populated</description>
        <formula>AccountId != &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Client Type to Non-DIFC</fullName>
        <actions>
            <name>GN_Update_Client_Type_to_Non_DIFC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>GN - Update Client Type to Non-DIFC if Account Name is blank</description>
        <formula>AccountId = &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Full Name with Web Name</fullName>
        <actions>
            <name>GN_Update_Full_Name_to_Web_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Full Name with Web Name</description>
        <formula>AND(     OR(        TEXT(Origin)=&apos;Email&apos;,        TEXT(Origin)=&apos;Web&apos;),     ISBLANK(Full_Name__c)     /*ISBLANK(Email__c),*/    )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Line Manager Email</fullName>
        <actions>
            <name>GN_Update_Line_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GN_Update_Line_Manager_Email_External</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED(OwnerId),  NOT(ISBLANK(CaseNumber)), LEFT(OwnerId,3)==&apos;005&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Record Type to Default</fullName>
        <actions>
            <name>GN_Update_Record_Type_to_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GN_Update_Status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.GN_Update_Record_Type_to_Default__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Record Type to Default False</fullName>
        <actions>
            <name>GN_Update_Record_Type_to_Default_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Default</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Status to Closed - Compliment</fullName>
        <actions>
            <name>Update_Status_to_Closed_Compliment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.GN_Type_of_Feedback__c</field>
            <operation>equals</operation>
            <value>Compliment</value>
        </criteriaItems>
        <description>Update Status to Closed - Compliment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Status to Invalid</fullName>
        <actions>
            <name>GN_Update_Type_to_Invalid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Invalid</value>
        </criteriaItems>
        <description>Update Status to Invalid</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Status to New</fullName>
        <actions>
            <name>Update_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Status to New if Department changes</description>
        <formula>AND(     ISCHANGED( GN_Department__c ),     OR(          TEXT(Status) = &apos;New&apos;,          TEXT(Status) = &apos;In Progress&apos;,          TEXT(Status) = &apos;Awaiting Response&apos;        ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update Subject if it blank</fullName>
        <actions>
            <name>GN_Update_Subject_from_Web</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Priority_Field_from_Web</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>GN - Update Subject if it blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update to Close Case Record Type</fullName>
        <actions>
            <name>GN_Update_to_Close_Case_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(     TEXT(Status) = &apos;Closed - Resolved&apos;,     TEXT(Status) = &apos;Closed - Compliment&apos;,     TEXT(Status) = &apos;Closed - Forwarded to Department&apos;,    TEXT(Status) = &apos;Invalid&apos;,     TEXT(Status) = &apos;Duplicate&apos;     ),  text(Origin)!=&apos;Chat&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GN - Update to Mystery Shopping Record Type</fullName>
        <actions>
            <name>GN_Update_to_Mystery_Shopping_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mystery Shopping</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>